package com.lansingareashoplocal.room

import androidx.room.*
import androidx.room.Dao


@Dao
interface Dao {





    @Insert()
    fun insertBecon(becon: Becons)

    @Query("SELECT * FROM becons WHERE major LIKE :major AND minor LIKE :minor")
    fun idBeconPresent(major: Int,minor:Int): Boolean

    @Query("SELECT * FROM becons WHERE major LIKE :major AND minor LIKE :minor")
    fun getBecon(major: Int,minor:Int): Becons

    @Query("DELETE  FROM becons WHERE major LIKE :major AND minor LIKE :minor")
    fun deleteNews(major: Int,minor:Int)

    @Update()
    fun updateBecon(becon: Becons)


    @Query("DELETE FROM becons")
    fun deleteAllBecons()





}