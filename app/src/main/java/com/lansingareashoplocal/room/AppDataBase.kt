package com.lansingareashoplocal.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [Becons::class],version = 2,exportSchema = false)
abstract class AppDataBase :RoomDatabase(){
    abstract fun Dao(): com.lansingareashoplocal.room.Dao


    companion object {
        var INSTANCE: AppDataBase? = null
        fun getAppDatabase(context: Context): AppDataBase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.applicationContext, AppDataBase::class.java, AppConstants.DB_NAME)
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build()
            }
            return INSTANCE as AppDataBase
        }
    }
}