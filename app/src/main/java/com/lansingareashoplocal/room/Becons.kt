package com.lansingareashoplocal.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity(tableName = "becons")
class Becons() {


    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    var id: Int = 0


    @SerializedName("major")
    @ColumnInfo(name = "major")
    var major:Int  = 0


    @SerializedName("minor")
    @ColumnInfo(name = "minor")
    var minor:Int  = 0


    @SerializedName("date")
    @ColumnInfo(name = "date")
    var date: String = ""




    @SerializedName("proximityUUID")
    @ColumnInfo(name = "proximityUUID")
    var proximityUUID: String = ""




}