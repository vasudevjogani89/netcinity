package com.lansingareashoplocal.api_call_with_ui

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.content.Context
import android.provider.Settings

class TrackPromotion(var businessPlaceId:String,var context: Context, var promotionId:String,var type:String ) {



    public fun callPromotionTrackApi(){
        val inputParams=HashMap<String,String>()
        inputParams.put(ApiConstants.BUSINESS_PLACE_ID,businessPlaceId)
        inputParams.put(ApiConstants.USER_ID, CommonMethods.getUserId(context)!!)
        inputParams.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputParams.put(ApiConstants.DEVICE_UDID, Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID))
        inputParams.put(ApiConstants.PROMOTION_ID,promotionId)
        inputParams.put(ApiConstants.TYPE,type)
        val call=ApiClient.getRetrofitInterface()?.promotionTracking(inputParams)
        WSClient<Response<Any>>().request(context,100,false,call,object :ISuccessHandler<Response<Any>>{
            override fun successResponse(requestCode: Int, mResponse: Response<Any>) {


            }

        },object :IFailureHandler{
            override fun failureResponse(requestCode: Int, message: String) {

            }

        })




    }
}