package com.lansingareashoplocal.presentation

/**
 * UI state the picker can get itself into.
 */
enum class UnsplashPickerState {
    IDLE, SEARCHING, PHOTO_SELECTED
}
