package com.lansingareashoplocal.presentation

import Response
import WSClient
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.widget.EditText
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.jakewharton.rxbinding2.widget.RxTextView
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent
import com.lansingareashoplocal.data.UnsplashPhoto
import com.lansingareashoplocal.domain.Repository
import com.lansingareashoplocal.photopicker.UnsplashPhotoPicker
import com.lansingareashoplocal.utility.helper.logs.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * View model for the picker screen.
 * This will use the repository to fetch the photos depending on the search criteria.
 * This is using rx binding.
 */
class UnsplashPickerViewModel constructor(private val repository: Repository) : BaseViewModel() {

    private val mPhotosLiveData = MutableLiveData<PagedList<UnsplashPhoto>>()
    val photosLiveData: LiveData<PagedList<UnsplashPhoto>> get() = mPhotosLiveData

    override fun getTag(): String {
        return UnsplashPickerViewModel::class.java.simpleName
    }

    /**
     * Binds the edit text using rx binding to listen to text change.
     *
     * @param editText the edit text to listen to
     */
    fun bindSearch(editText: EditText) {
        RxTextView.textChanges(editText)
            .debounce(500, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                mLoadingLiveData.postValue(true)
            }
            .observeOn(Schedulers.io())
            .switchMap { text ->
                if (TextUtils.isEmpty(text)) {
                    repository.loadPhotos(UnsplashPhotoPicker.getPageSize())
                } else {
                    repository.searchPhotos(text.toString(), UnsplashPhotoPicker.getPageSize())
                }

            }
            .subscribe(object : BaseObserver<PagedList<UnsplashPhoto>>() {
                override fun onSuccess(data: PagedList<UnsplashPhoto>?) {
                    mPhotosLiveData.postValue(data)
                }
            })


        /*RxTextView.textChangeEvents(editText)
           // Better store the value in a constant like Constant.DEBOUNCE_SEARCH_REQUEST_TIMEOUT
            .debounce(500, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                mLoadingLiveData.postValue(true)
            }
            .observeOn(Schedulers.io())
            .map {




            }
            .flatMap {
                if (TextUtils.isEmpty(editText.text.toString())) {
                    repository.loadPhotos(UnsplashPhotoPicker.getPageSize())
                } else {
                    repository.searchPhotos(editText.text.toString(), UnsplashPhotoPicker.getPageSize())
                }
            } // Or .flatMap(this::getSearch)
            .subscribe(object : BaseObserver<PagedList<UnsplashPhoto>>() {
                override fun onSuccess(data: PagedList<UnsplashPhoto>?) {
                    mPhotosLiveData.postValue(data)
                }
            })*/


    }

    /**
     * To abide by the API guidelines,
     * you need to trigger a GET request to this endpoint every time your application performs a download of a photo
     *
     * @param photos the list of selected photos
     */
    fun trackDownloads(photos: ArrayList<UnsplashPhoto>) {
        for (photo in photos) {
            repository.trackDownload(photo.links.download_location)
        }
    }




}
