package com.lansingareashoplocal.presentation

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.lansingareashoplocal.R
import com.lansingareashoplocal.data.UnsplashPhoto
import com.lansingareashoplocal.photopicker.Injector
import com.lansingareashoplocal.ui.interfaces.EditTextChangeCallBack
import com.lansingareashoplocal.utility.helper.logs.Log
import com.lansingareashoplocal.utility.helper.others.CustomTextWatcher
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_picker.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.*
import java.io.*
import java.net.MalformedURLException
import java.net.URL
import java.net.URLConnection

/**
 * Main screen for the picker.
 * This will show a list a photos and a search component.
 * The list is has an infinite scroll.
 */
class UnsplashPickerActivity : AppCompatActivity(), OnPhotoSelectedListener{

    private lateinit var mLayoutManager: StaggeredGridLayoutManager

    private lateinit var mAdapter: UnsplashPhotoAdapter

    private lateinit var mViewModel: UnsplashPickerViewModel

    private var mIsMultipleSelection = false

    private var mCurrentState = UnsplashPickerState.IDLE

    private var mPreviousState = UnsplashPickerState.IDLE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)
       // unsplash_picker_edit_text.addTextChangedListener(CustomTextWatcher(unsplash_picker_edit_text, this))
       // mIsMultipleSelection = intent.getBooleanExtra(EXTRA_IS_MULTIPLE, false)
        // recycler view layout manager
        mLayoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        // recycler view adapter
        mAdapter = UnsplashPhotoAdapter(this, mIsMultipleSelection)
        mAdapter.setOnImageSelectedListener(this)
        // recycler view configuration
        unsplash_picker_recycler_view.setHasFixedSize(true)
        unsplash_picker_recycler_view.itemAnimator = null
        unsplash_picker_recycler_view.layoutManager = mLayoutManager
        unsplash_picker_recycler_view.adapter = mAdapter
        // click listeners
        tvLeft.setOnClickListener {
            finish()
        }
        tvTitle.text = getString(R.string.un_splash_screen_title)

        unsplash_picker_clear_image_view.setOnClickListener { onBackPressed() }

        // get the view model and bind search edit text
        mViewModel =
                ViewModelProviders.of(this, Injector.createPickerViewModelFactory())
                    .get(UnsplashPickerViewModel::class.java)
        observeViewModel()
        mViewModel.bindSearch(unsplash_picker_edit_text)
    }

    /**
     * Observes the live data in the view model.
     */
    private fun observeViewModel() {
        mViewModel.errorLiveData.observe(this, Observer {
            Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
        })
        mViewModel.messageLiveData.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })
        mViewModel.loadingLiveData.observe(this, Observer {
            if(unsplash_picker_edit_text.text?.isNotEmpty()!!){
                unsplash_picker_clear_image_view.visibility=View.VISIBLE
                mCurrentState = UnsplashPickerState.SEARCHING
                updateUiFromState()
            }
            else{
                unsplash_picker_clear_image_view.visibility=View.GONE
            }
            unsplash_picker_progress_bar_layout.visibility = if (it != null && it) View.VISIBLE else View.GONE
        })
        mViewModel.photosLiveData.observe(this, Observer {
            unsplash_picker_no_result_text_view.visibility =
                    if (it == null || it.isEmpty()) View.VISIBLE
                    else View.GONE
            mAdapter.submitList(it)
        })
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        // we want the recycler view to have 3 columns when in landscape and 2 in portrait
        mLayoutManager.spanCount =
                if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) 3
                else 2
        mAdapter.notifyDataSetChanged()
    }

    override fun onPhotoSelected(nbOfSelectedPhotos: Int) {
        // if multiple selection
        if (mIsMultipleSelection) {

        }
        // if single selection send selected photo as a result
        else if (nbOfSelectedPhotos > 0) {
            val photos: ArrayList<UnsplashPhoto> = mAdapter.getImages()
            if(photos.isNotEmpty() && photos[0].urls.regular!=null){
                val folder = File(Environment.getExternalStorageDirectory().getAbsolutePath(), "unsplash")
                val outputFile = File(folder, "${CommonMethods.getFileNameFromURl(photos[0].urls.regular!!)}.jpg")
                val path: String = outputFile.toString()
                val filePath = File(path)
                if (filePath.exists()) {
                    try {
                        CropImage.activity(Uri.fromFile(filePath.absoluteFile))
                            .start(this@UnsplashPickerActivity)
                    } catch (e: Exception) {
                    }
                } else {
                    DownloadTask().execute(photos[0].urls.regular)
                }
            }
            else if (photos.isNotEmpty() && photos[0].urls.small.isNotEmpty()){
                val folder = File(Environment.getExternalStorageDirectory().getAbsolutePath(), "unsplash")
                val outputFile = File(folder, "${CommonMethods.getFileNameFromURl(photos[0].urls.small)}.jpg")
                val path: String = outputFile.toString()
                val filePath = File(path)
                if (filePath.exists()) {
                    try {
                        CropImage.activity(Uri.fromFile(filePath.absoluteFile))
                            .start(this@UnsplashPickerActivity)
                    } catch (e: Exception) {
                    }
                } else {
                    DownloadTask().execute(photos[0].urls.small)
                }
            }

            //sendPhotosAsResult()
        }
    }

    /**
     * Sends images in the result intent as a result for the calling activity.
     */
    private fun sendPhotosAsResult() {
        // get the selected photos
        val photos: ArrayList<UnsplashPhoto> = mAdapter.getImages()
        // track the downloads
        mViewModel.trackDownloads(photos)
        // send them back to the calling activity
        val data = Intent()
        data.putExtra(EXTRA_PHOTOS, photos)
        setResult(Activity.RESULT_OK, data)
        finish()
    }

    override fun onPhotoLongPress(imageView: ImageView, url: String) {
        startActivity(PhotoShowActivity.getStartingIntent(this, url))
    }

    override fun onBackPressed() {
        when (mCurrentState) {
            UnsplashPickerState.IDLE -> {
                super.onBackPressed()
            }
            UnsplashPickerState.SEARCHING -> {
                // updating states
                mCurrentState = UnsplashPickerState.IDLE
                mPreviousState = UnsplashPickerState.SEARCHING
                // updating ui
                updateUiFromState()
            }
            UnsplashPickerState.PHOTO_SELECTED -> {
                // updating states
                mCurrentState = if (mPreviousState == UnsplashPickerState.SEARCHING) {
                    UnsplashPickerState.SEARCHING
                } else {
                    UnsplashPickerState.IDLE
                }
                mPreviousState = UnsplashPickerState.PHOTO_SELECTED
                // updating ui
                updateUiFromState()
            }
        }
    }

    /*
    STATES
     */

    private fun updateUiFromState() {
        when (mCurrentState) {
            UnsplashPickerState.IDLE -> {
                // back and search buttons visible

                // edit text cleared and gone
                if (!TextUtils.isEmpty(unsplash_picker_edit_text.text)) {
                    unsplash_picker_edit_text.setText("")
                }

                // keyboard down
                unsplash_picker_edit_text.closeKeyboard(this)
                // action bar with unsplash

                // clear list selection
                mAdapter.clearSelection()
                mAdapter.notifyDataSetChanged()
            }
            UnsplashPickerState.SEARCHING -> {
                // back, cancel, done or search buttons gone

                // edit text visible and focused

                // keyboard up
                unsplash_picker_edit_text.requestFocus()
                unsplash_picker_edit_text.openKeyboard(this)
                // clear list selection
                mAdapter.clearSelection()
                mAdapter.notifyDataSetChanged()
            }
            UnsplashPickerState.PHOTO_SELECTED -> {
                // back and search buttons gone

                // edit text gone

                // keyboard down
                unsplash_picker_edit_text.closeKeyboard(this)
            }
        }
    }

    companion object {
        const val EXTRA_PHOTOS = "EXTRA_PHOTOS"
        private const val EXTRA_IS_MULTIPLE = "EXTRA_IS_MULTIPLE"

        /**
         * @param callingContext the calling context
         * @param isMultipleSelection true if multiple selection, false otherwise
         *
         * @return the intent needed to come to this activity
         */
        fun getStartingIntent(callingContext: Context, isMultipleSelection: Boolean): Intent {
            val intent = Intent(callingContext, UnsplashPickerActivity::class.java)
            intent.putExtra(EXTRA_IS_MULTIPLE, isMultipleSelection)
            return intent
        }
    }
    inner class DownloadTask : AsyncTask<String?, Int, String?>() {
        var progressDialog: ProgressDialog? = null

        /**
         * Set up a ProgressDialog
         */
        protected override fun onPreExecute() {
            /* progressDialog = ProgressDialog(this@UnSplashActivity)
             progressDialog?.setTitle("Download in progress...")
             progressDialog?.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
             progressDialog?.setMax(100)
             progressDialog?.setProgress(0)
             progressDialog?.show()*/
            CommonMethods.showProgress(this@UnsplashPickerActivity)
        }

        /**
         * Background task
         */
        protected override fun doInBackground(vararg params: String?): String? {
            val path = params[0]
            val fileLength: Int
            try {
                val url = URL(path)
                val urlConnection: URLConnection = url.openConnection()
                urlConnection.connect()
                fileLength = urlConnection.getContentLength()
                /**
                 * Create a folder
                 */
                val newFolder = File(Environment.getExternalStorageDirectory(), getString(R.string.app_name))
                if (!newFolder.exists()) {
                    if (newFolder.mkdir()) {
                        Log.i("Info", "Folder succesfully created")
                    } else {
                        Log.i("Info", "Failed to create folder")
                    }
                } else {
                    Log.i("Info", "Folder already exists")
                }
                /**
                 * Create an output file to store the image for download
                 */
                val output_file = File(newFolder, "profile.jpg")
                val outputStream: OutputStream = FileOutputStream(output_file)
                val inputStream: InputStream = BufferedInputStream(url.openStream(), 8192)
                val data = ByteArray(1024)
                var total = 0
                var count: Int
                while (inputStream.read(data).also { count = it } != -1) {
                    total += count
                    outputStream.write(data, 0, count)
                    val progress = 100 * total / fileLength
                    publishProgress(progress)
                    Log.i("Info", "Progress: " + Integer.toString(progress))
                }
                inputStream.close()
                outputStream.close()
                Log.i("Info", "file_length: " + Integer.toString(fileLength))
            } catch (e: MalformedURLException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return path
        }

        protected override fun onProgressUpdate(vararg values: Int?) {
            /* values[0]?.let { progressDialog?.setProgress(it) }*/
        }

        protected override fun onPostExecute(result: String?) {
            CommonMethods.hideProgress()

            val folder =
                File(Environment.getExternalStorageDirectory().getAbsolutePath(), getString(R.string.app_name))
            val outputFile = File(folder, "profile.jpg")
            if(outputFile!=null && outputFile.exists()){
                val path: String = outputFile.toString()
                val filePath = File(path)
                try {
                    CropImage.activity(Uri.fromFile(filePath.absoluteFile))
                        .start(this@UnsplashPickerActivity);
                } catch (e: Exception) {

                }


                Log.i("Info", "Path: $path")
            }

        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK && result != null) {
                val intent = Intent()
                intent.putExtra(AppConstants.RESULT_URL, result.uri.path)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
    }


}
