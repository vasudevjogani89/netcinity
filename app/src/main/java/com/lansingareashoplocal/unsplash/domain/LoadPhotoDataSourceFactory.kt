package com.lansingareashoplocal.domain

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.lansingareashoplocal.data.NetworkEndpoints
import com.lansingareashoplocal.data.UnsplashPhoto

/**
 * Android paging library data source factory.
 * This will create the load photo data source.
 */
class LoadPhotoDataSourceFactory constructor(private val networkEndpoints: NetworkEndpoints) :
    DataSource.Factory<Int, UnsplashPhoto>() {

    val sourceLiveData = MutableLiveData<LoadPhotoDataSource>()

    override fun create(): DataSource<Int, UnsplashPhoto> {
        val source = LoadPhotoDataSource(networkEndpoints)
        sourceLiveData.postValue(source)
        return source
    }
}
