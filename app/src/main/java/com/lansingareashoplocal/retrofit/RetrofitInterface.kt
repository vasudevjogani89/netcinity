import androidx.paging.PagedList
import com.lansingareashoplocal.data.UnsplashPhoto
import com.lansingareashoplocal.ui.model.*
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.Observer
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*


interface RetrofitInterface {


    /* @GET("WS/login?")
     fun login(@QueryMap inputParam: Map<String, String>): Call<Response<List<UserDetails>>>


     @GET("WS/forgot_password?")
     fun forgotPassword(@QueryMap inputParam: Map<String, String>): Call<Response<List<ForgotPasswordResponse>>>



     @Multipart
     @POST("WS/registration")
     fun signup(@Part mobileNo: MultipartBody.Part, @Part userName: MultipartBody.Part, @Part userMail: MultipartBody.Part, @Part password: MultipartBody.Part, @Part deviceToken: MultipartBody.Part, @Part deviceType: MultipartBody.Part, @Part deviceOs: MultipartBody.Part, @Part deviceName: MultipartBody.Part, @Part appVersion: MultipartBody.Part?): Call<Response<List<UserDetails>>>
 */



    @GET("https://api.openweathermap.org/data/2.5/onecall")

    fun getWetherReport(@QueryMap inputParam: Map<String, String>): Call<Temperature>

    @GET("WS/static_pages")
    fun staticPagesApi(@Query("page_code") page_code: String?): Call<Response<List<StaticPageModel>>>




    @Multipart
    @POST("WS/signup?")
    fun signup(@Part firstName: MultipartBody.Part, @Part lastName: MultipartBody.Part, @Part userMail: MultipartBody.Part, @Part password: MultipartBody.Part, @Part dob: MultipartBody.Part, @Part gender: MultipartBody.Part, @Part deviceToken: MultipartBody.Part, @Part deviceOS: MultipartBody.Part, @Part deviceName: MultipartBody.Part, @Part deviceType: MultipartBody.Part?
    ,@Part vesionName: MultipartBody.Part,@Part cummnityID: MultipartBody.Part,@Part deviceUDID: MultipartBody.Part,@Part imageFIle: MultipartBody.Part): Call<Response<List<UserDetails>>>

    @Multipart
    @POST("WS/signup?")
    fun signup(@Part firstName: MultipartBody.Part, @Part lastName: MultipartBody.Part, @Part userMail: MultipartBody.Part, @Part password: MultipartBody.Part, @Part dob: MultipartBody.Part, @Part gender: MultipartBody.Part, @Part deviceToken: MultipartBody.Part, @Part deviceOS: MultipartBody.Part, @Part deviceName: MultipartBody.Part, @Part deviceType: MultipartBody.Part?
               ,@Part vesionName: MultipartBody.Part,@Part cummnityID: MultipartBody.Part,@Part deviceUDID: MultipartBody.Part): Call<Response<List<UserDetails>>>
    @GET("WS/login?")
    fun logIn(@QueryMap inputParam: Map<String, String>): Call<Response<List<UserDetails>>>


    @GET("WS/forgot_password?")
    fun forgotPassword(@Query("email") email: String?): Call<Response<List<Any>>>

    @GET("WS/password_otp_verification")
    fun verifyOtp(@QueryMap inputParam: Map<String, String>): Call<Response<List<OtpModel>>>

    @GET("WS/reset_password")
    fun resetPassword(@QueryMap inputParam: Map<String, String>): Call<Response<List<Any>>>

    @GET("WS/social_login")
    fun socialSignUp(@QueryMap inputParam: Map<String, String>): Call<Response<List<UserDetails>>>

    @GET("WS/change_password")
    fun changePassword(@QueryMap inputParam: Map<String, String>): Call<Response<List<Any>>>
    @Multipart
    @POST("WS/edit_profile")
    fun updateProfile(@Part firstName: MultipartBody.Part, @Part lastName: MultipartBody.Part, @Part userMail: MultipartBody.Part, @Part gender: MultipartBody.Part, @Part dob: MultipartBody.Part,@Part userID: MultipartBody.Part,@Part profileImage: MultipartBody.Part): Call<Response<List<UserDetails>>>


    @Multipart
    @POST("WS/edit_profile")
    fun updateProfile(@Part firstName: MultipartBody.Part, @Part lastName: MultipartBody.Part, @Part userMail: MultipartBody.Part, @Part gender: MultipartBody.Part, @Part dob: MultipartBody.Part,@Part userID: MultipartBody.Part): Call<Response<List<UserDetails>>>
    @GET("WS/category_list")
    fun getCategory(@Query("community_id") community_id: String = ""): Call<Response<List<Category>>>

    @GET("WS/events_list")
    fun getCommunityEvents(@QueryMap inputParam: Map<String, String>): Call<Response<List<CommunityEvents>>>


    @GET("WS/news_list")

    fun getNewsAndAlert(@Query("community_id") community_id: String = "", @Query("page_index") page_index: String = "1"): Call<Response<List<News>>>


    @GET("WS/resend_email_verification?")
    fun resendEmailVerification(@Query("user_id") user_id: String?): Call<Response<List<Any>>>

    @GET("WS/featured_business_list?")
    fun getFeaturedBusiness(@QueryMap inputParam: Map<String, String> = HashMap<String, String>()): Call<Response<List<FeaturedBusiness>>>

    @GET("WS/non_featured_business_list?")
    fun getNonFeaturedBusiness(@QueryMap inputParam: Map<String, String> = HashMap<String, String>()): Call<Response<List<FeaturedBusiness>>>


    @GET("WS/make_favourite?")
    fun makeFavourite(@QueryMap inputParam: Map<String, String>): Call<Response<List<Any>>>


    @GET("WS/remove_favourite?")
    fun removeFavourite(@QueryMap inputParam: Map<String, String>): Call<Response<List<Any>>>


    @GET("WS/business_detail?")
    fun getBusinessDetails(@QueryMap inputParam: Map<String, String>): Call<Response<List<BusinessDetails>>>


    @GET("WS/favourite_business_list?")
    fun getFavouriteList(@QueryMap inputParam: Map<String, String>): Call<Response<List<FeaturedBusiness>>>

    @GET("WS/notifications?")
    fun getNotificationList(@QueryMap inputParam: Map<String, String>): Call<Response<List<FeaturedBusiness>>>

    @GET("WS/sync_device_token")
    fun syncDeviceToken(@QueryMap inputParam: Map<String, String>): Call<Response<List<Any>>>


    @GET("WS/events_detail")
    fun eventsDetail(@QueryMap inputParam: Map<String, String>): Call<Response<List<CommunityEvents>>>


    @GET("WS/news_detail")
    fun newDetails(@QueryMap inputParam: Map<String, String>): Call<Response<List<News>>>


    @GET("WS/beacon_hit")
    fun bitBecon(@QueryMap inputParam: Map<String, String>): Call<Response<List<BusinessDetails>>>


    @GET("WS/get_user_notifications")
    fun getUserNotification(@QueryMap inputParam: Map<String, String>): Call<Response<List<UserNotification>>>


    @GET("WS/global_search")
    fun getGlobalSearchResult(@QueryMap inputParam: Map<String, String>): Call<Response<List<GlobalSearch>>>


    @GET("WS/event_category")
    fun getEventCategory(): Call<Response<List<EventCategory>>>



    @GET("WS/review_listing")
    fun getReviewsList(@QueryMap inputParam: Map<String, String>):Call<Response<ReviewList>>

    @GET("WS/my_reviews")
    fun getMyReview(@QueryMap inputParam: Map<String, String>):Call<Response<List<MyReviews>>>

    @GET("WS/add_review")
    fun addReview(@QueryMap inputParam: Map<String, String>):Call<Response<List<Any>>>

    @GET("WS/edit_review")
    fun editReview(@QueryMap inputParam: Map<String, String>):Call<Response<List<Any>>>


    @GET("WS/inaccuracy_report_type_list")
    fun getInaccuracyReportTypeList():Call<Response<List<Report>>>

    @GET("WS/report_business_inaccuracy")
    fun reportBusinessInaccuracy(@QueryMap inputParam: Map<String, String>):Call<Response<List<Any>>>

    @GET("WS/featured_business_of_the_week")
    fun getFeatureBusinessOFTheWeek(@QueryMap inputParam: Map<String, String>):Call<Response<List<FeaturedBusinessWeek>>>
    @GET("WS/featured_business_of_week_list")
    fun getFeatureBusinessOfTheWeekList(@QueryMap inputParam: Map<String, String>):Call<Response<List<FeaturedBusiness>>>
    @GET("WS/featured_business_places_map_view")
    fun getFeatureBusinessPlacesMapView(@QueryMap inputParam: Map<String, String>):Call<Response<List<FeaturedBusiness>>>

    @GET("WS/faq_list")
    fun getfaqList(@QueryMap inputParam: Map<String, String>):Call<Response<List<FAQ>>>

    @GET("https://api.unsplash.com/search/photos")
    fun getSearchPhotos(@Query("query") query: String?, @Query("page") page: Int?, @Query("per_page") perPage: Int?, @Query("orientation") orientation: String=""): Call<SearchResults>

    @GET("WS/reason_list")
    fun getReasonList():Call<Response<List<Reason>>>

    @GET("WS/send_contact_us_request")
    fun sendContactUsRequest(@QueryMap inputParam: Map<String, String>):Call<Response<Any>>

    @GET("WS/sub_category_list")
    fun getSubCategory(@QueryMap inputParam: Map<String, String>):Call<Response<List<SubCategories>>>
    @GET("WS/events_list_calendar_view")
    fun getCommunityEventsListCaender(@Query("from_date") from_date: String ="", @Query("to_date") to_date: String ="", @Query("sort_by") sort_by: String ="distance_asc", @Query("lat") lat: String ="", @Query("lon") lon: String ="", @Query("community_id") community_id: String =""): Call<Response<List<CommunityEvents>>>

    @GET("WS/promotion_track")
    fun promotionTracking(@QueryMap inputParam: Map<String, String>):Call<Response<Any>>


    @GET("WS/profanity_check")
    fun profanityCheckApi(@Query("query") query: String):Call<Response<Any>>

    @GET("WS/track_app_download")
    fun trackAppDownload(@QueryMap inputParam: Map<String, String>) :Call<Response<Any>>










}




