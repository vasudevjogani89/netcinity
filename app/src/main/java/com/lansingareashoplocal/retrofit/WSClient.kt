
import android.content.Context
import android.net.ConnectivityManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class WSClient<T> {
    // public ProgressDialog progressDialog;

    init {
        if (requestQue == null) {
            requestQue = HashMap<String,String>()
        }
    }

    fun request(mContext: Context,
                requestId: Int,
                showProgress: Boolean =false,
                call: Call<T>?,
                mSuccessHandler: ISuccessHandler<T>?,
                mFailureHandler: IFailureHandler?) {


        if (showProgress) {

            CommonMethods.showProgress(mContext)

        }

        if (!CommonMethods.checkInternetConnection(mContext)) {
            if (showProgress) CommonMethods.hideProgress()

            mFailureHandler?.failureResponse(requestId, "No Internet Connection. Make sure your device is connected to the internet")

        } else {
            call?.enqueue(object : Callback<T> {
                override fun onResponse(call: Call<T>, response: Response<T>) {
                    //app is getting crash some things
                    if (showProgress) CommonMethods.hideProgress()

                    if(response.body()!=null){
                        response.body()?.let {
                            mSuccessHandler?.successResponse(requestId, it) }
                    }
                    else{
                        mFailureHandler?.failureResponse(requestId, "Server not responding.Please try later.")
                        call.cancel()
                    }


                }

                override fun onFailure(call: Call<T>, t: Throwable) {
                    if (showProgress) CommonMethods.hideProgress()
                    mFailureHandler?.failureResponse(requestId, "Server not responding.Please try later.")
                    call.cancel()
                }
            })
        }
        fun checkInternetConnection(mActivity: Context): Boolean {
            // <uses-permission android:firstName="android.permission.ACCESS_NETWORK_STATE"></uses-permission>

            val manager = mActivity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val mNetworkInfo = manager.activeNetworkInfo
            return mNetworkInfo?.isConnectedOrConnecting ?: false

        }
    }


    companion object {
        var requestQue: HashMap<*, *>? = null
    }

}