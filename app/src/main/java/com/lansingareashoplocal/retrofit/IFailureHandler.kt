
interface IFailureHandler {

    fun failureResponse(requestCode: Int, message: String)

}
