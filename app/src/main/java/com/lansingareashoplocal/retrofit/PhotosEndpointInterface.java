package com.lansingareashoplocal.retrofit;

import com.lansingareashoplocal.ui.model.SearchResults;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PhotosEndpointInterface {



    @GET("search/photos")
    Call<SearchResults> searchPhotos(@Query("query") String query,
                                     @Query("page") Integer page,
                                     @Query("per_page") Integer perPage,
                                     @Query("orientation") String orientation);
}