import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.utility.helper.HeaderInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {
    private var mRetrofitInterface: RetrofitInterface? = null
    public val BASE_URL = BuildConfig.BASE_URL

    fun getRetrofitInterface(): RetrofitInterface? {
        if (BuildConfig.DEBUG) {
            if (mRetrofitInterface == null) {
                val logging = HttpLoggingInterceptor()
                logging.level = (HttpLoggingInterceptor.Level.BODY)
                val httpClient = OkHttpClient.Builder()
                httpClient.addInterceptor(logging)

                // httpClient.addInterceptor(LoggerInterceptor())
                httpClient.callTimeout(5, TimeUnit.MINUTES)
                httpClient.readTimeout(5, TimeUnit.MINUTES)
                httpClient.writeTimeout(5, TimeUnit.MINUTES)
                val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build()
                mRetrofitInterface = retrofit.create(RetrofitInterface::class.java)
            }

            return mRetrofitInterface
        } else {
            if (mRetrofitInterface == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                mRetrofitInterface = retrofit.create(RetrofitInterface::class.java)
            }
            return mRetrofitInterface
        }


    }

}
