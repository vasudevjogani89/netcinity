
interface ISuccessHandler<T> {

    fun successResponse(requestCode: Int, mResponse: T)

}
