package com.lansingareashoplocal.service

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import androidx.core.app.NotificationCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition

import com.kontakt.sdk.android.ble.configuration.ScanMode
import com.kontakt.sdk.android.ble.configuration.ScanPeriod
import com.kontakt.sdk.android.ble.device.BeaconRegion
import com.kontakt.sdk.android.ble.exception.ScanError
import com.kontakt.sdk.android.ble.manager.ProximityManager
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory
import com.kontakt.sdk.android.ble.manager.listeners.EddystoneListener
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener
import com.kontakt.sdk.android.ble.manager.listeners.ScanStatusListener
import com.kontakt.sdk.android.ble.manager.listeners.SpaceListener
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleEddystoneListener
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleIBeaconListener
import com.kontakt.sdk.android.common.profile.IBeaconDevice
import com.kontakt.sdk.android.common.profile.IBeaconRegion
import com.kontakt.sdk.android.common.profile.IEddystoneDevice
import com.kontakt.sdk.android.common.profile.IEddystoneNamespace
import com.kontakt.sdk.android.common.profile.RemoteBluetoothDevice
import com.lansingareashoplocal.R
import com.lansingareashoplocal.room.AppDataBase
import com.lansingareashoplocal.room.Becons
import com.lansingareashoplocal.ui.intro.SplashActivity
import com.lansingareashoplocal.ui.model.BusinessDetails
import org.jetbrains.anko.runOnUiThread
import java.util.*

import java.util.concurrent.TimeUnit

class BackgroundScanService : Service() {
    private val handler = Handler()
    private var proximityManager: ProximityManager? = null
    private var isRunning: Boolean = false // Flag indicating if service is already running.
    private var devicesCount: Int = 0 // Total discovered devices count
    private var notificationManager: NotificationManager? = null

    override fun onCreate() {
        super.onCreate()
        setupProximityManager()
        isRunning = false
    }

    private fun setupProximityManager() {
        // logger.dumpCustomEvent("BeaconSdk:Initialize", "")

        val iBeaconRegion = BeaconRegion.Builder()
            .identifier("My Region")
            .proximity(UUID.fromString(CommonMethods.proximate_uuid))
            .build()
        //Create proximity manager instance
        proximityManager = ProximityManagerFactory.create(this)
        //Configure proximity manager basic options
        proximityManager!!.configuration()
            //Using ranging for continuous scanning or MONITORING for scanning with intervals
            .scanPeriod(ScanPeriod.RANGING)
            //Using BALANCED for best performance/battery ratio
            .scanMode(ScanMode.BALANCED)
            .deviceUpdateCallbackInterval(TimeUnit.SECONDS.toMillis(5))
        //Setting up iBeacon and Eddystone listeners
        proximityManager!!.setIBeaconListener(createIBeaconListener())
        proximityManager!!.setEddystoneListener(createEddystoneListener())
        proximityManager!!.setScanStatusListener(object : ScanStatusListener {
            override fun onMonitoringCycleStop() {
                Log.d("onMonitoringCycleStop", "onMonitoringCycleStop")
            }

            override fun onScanStop() {
                // logger.dumpCustomEvent("BeaconSdk:onScanStop", "")
                Log.d("onScanStop", "onScanStop")
            }

            override fun onMonitoringCycleStart() {
                Log.d("onMonitoringCycleStart", "onMonitoringCycleStart")
            }

            override fun onScanStart() {
                // logger.dumpCustomEvent("BeaconSdk:onScanStart", "")
            }

            override fun onScanError(scanError: ScanError?) {
                // logger.dumpCustomEvent("BeaconSdk:onScanError", "")

            }

        })
        proximityManager!!.setSpaceListener(createSpaceListener())
        proximityManager!!.spaces().iBeaconRegion(iBeaconRegion)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        //Check if service is already active
        //logger.dumpCustomEvent("onStartCommand", "")
        if (isRunning) {
            return START_STICKY
        }
        startScanning()
        isRunning = true
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun startScanning() {
        proximityManager!!.connect {
            proximityManager!!.startScanning()
            // logger.dumpCustomEvent("BeaconSdk:onServiceReady", "")
            devicesCount = 0
        }
        //  stopAfterDelay()
    }

    private fun stopAfterDelay() {
        handler.postDelayed({
            proximityManager!!.disconnect()
            stopSelf()
        }, TIMEOUT)
    }

    private fun createIBeaconListener(): IBeaconListener {
        return object : SimpleIBeaconListener() {
            override fun onIBeaconDiscovered(ibeacon: IBeaconDevice?, region: IBeaconRegion?) {
                //  logger.dumpCustomEvent( "BeaconSdk:onIBeaconDiscovered","major:${ibeacon?.major}  minor :${ibeacon?.minor}  proximityUUID:${ibeacon?.proximityUUID}")
                if (ibeacon != null && ibeacon?.proximityUUID?.toString() != null) {
                    if (CommonMethods.proximate_uuid.equals(
                            ibeacon?.proximityUUID?.toString(),
                            true
                        )
                    ) {
                        checkIsrequireTocallBeconHitApi(ibeacon)
                    }
                    Log.i(TAG, "onIBeaconDiscovered: " + ibeacon!!.toString())
                } else {
                    //   logger.debugEvent("proximityUUID: null", "")
                }

            }
        }
    }


    private fun createSpaceListener(): SpaceListener {
        return object : SpaceListener {
            override fun onRegionEntered(region: IBeaconRegion) {
                // logger.dumpCustomEvent( "BeaconSdk:onRegionEntered", "major:${region?.major}  minor :${region?.minor}  proximityUUID:${region?.proximity}")


                /* if(AppConstants.PROXIMATE_ID.equals(region?.proximity.toString(),true)){
                     checkIsrequireTocallBeconHitApi(region)
                 }*/
                Log.i(TAG, "New Region entered: " + region.identifier)
            }

            override fun onRegionAbandoned(region: IBeaconRegion) {
                // logger.dumpCustomEvent("BeaconSdk:onRegionAbandoned", "major:${region?.major}  minor :${region?.minor}  proximityUUID:${region?.proximity}")
                Log.e(TAG, "Region abandoned " + region.identifier)
            }

            override fun onNamespaceEntered(namespace: IEddystoneNamespace) {


                Log.i(TAG, "New Namespace entered: " + namespace.identifier)
            }

            override fun onNamespaceAbandoned(namespace: IEddystoneNamespace) {

                Log.i(TAG, "Namespace abandoned: " + namespace.identifier)
            }
        }
    }

    private fun createEddystoneListener(): EddystoneListener {
        return object : SimpleEddystoneListener() {
            override fun onEddystoneDiscovered(
                eddystone: IEddystoneDevice?,
                namespace: IEddystoneNamespace?
            ) {

                Log.i(TAG, "onEddystoneDiscovered: " + eddystone!!.toString())
            }
        }
    }


    override fun onDestroy() {
        //logger.dumpCustomEvent("BackgroundScanService", " onDestroy")
        handler.removeCallbacksAndMessages(null)
        if (proximityManager != null) {
            proximityManager!!.disconnect()
            proximityManager = null
        }

        super.onDestroy()
    }

    companion object {

        val TAG = "BackgroundScanService"
        val ACTION_DEVICE_DISCOVERED = "DeviceDiscoveredAction"
        val EXTRA_DEVICE = "DeviceExtra"
        val EXTRA_DEVICES_COUNT = "DevicesCountExtra"

        private val TIMEOUT = TimeUnit.SECONDS.toMillis(30)
    }


    fun checkIsrequireTocallBeconHitApi(ibeacon: IBeaconDevice) {

        // logger = Logger(this)
        if (!AppDataBase.getAppDatabase(this).Dao().idBeconPresent(
                ibeacon.major,
                ibeacon.minor
            )
        ) {
            callBeaconHitApi(AppConstants.NEW, ibeacon)
        } else {
            val becons =
                AppDataBase.getAppDatabase(this).Dao().getBecon(ibeacon.major, ibeacon.minor)
            if (CommonMethods.isDateCross24Hours(
                    CommonMethods.getTimeInDateFormate(becons.date),
                    CommonMethods.getCurrentTimeInDateFormate()
                )
            ) {
                Log.d("BeaconHit:", "Api started")
                callBeaconHitApi(AppConstants.UPDATE, ibeacon)
            } else {
                //     logger.debugEvent("BeaconHit", "Already hit before 24 hours")
            }


        }
    }

    private fun callBeaconHitApi(
        type: String,
        ibeacon: IBeaconDevice
    ) {
        // logger.dumpCustomEvent("BeaconHitApi:started", "MAJOR: ${ibeacon.major} MINOR: ${ibeacon.minor} PROXIMATE_ID:${AppConstants.PROXIMATE_ID} ")
        val inputParam = HashMap<String, String>()
        inputParam.put(
            ApiConstants.DEVICE_UDID,
            Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
        )
        inputParam.put(ApiConstants.USER_ID, CommonMethods.getUserId(this)!!)
        inputParam.put(ApiConstants.MAJOR, ibeacon.major.toString())
        inputParam.put(ApiConstants.MINOR, ibeacon.minor.toString())
        inputParam.put(ApiConstants.PROXIMITY, CommonMethods.proximate_uuid)


        val call = ApiClient.getRetrofitInterface()?.bitBecon(inputParam)

        WSClient<Response<List<BusinessDetails>>>().request(
            this,
            100,
            false,
            call,
            object : ISuccessHandler<Response<List<BusinessDetails>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<BusinessDetails>>
                ) {
                    if (mResponse.settings?.success.equals("1")) {
                        // logger.dumpCustomEvent("SucessMessage:${mResponse.settings?.message}", mResponse.data.toString())
                        if (mResponse.data != null && mResponse.data!!.isNotEmpty()) {
                            if (type.equals(AppConstants.UPDATE)) {
                                val becon =
                                    AppDataBase.getAppDatabase(this@BackgroundScanService).Dao()
                                        .getBecon(ibeacon.major, ibeacon.minor)
                                becon.date = CommonMethods.getCurrentTimeInString()
                                AppDataBase.getAppDatabase(this@BackgroundScanService).Dao()
                                    .updateBecon(becon)
                            } else {
                                val becon = Becons()
                                becon.major = ibeacon.major
                                becon.minor = ibeacon.minor
                                becon.date = CommonMethods.getCurrentTimeInString()
                                AppDataBase.getAppDatabase(this@BackgroundScanService).Dao()
                                    .insertBecon(becon)

                            }
                            showNotification(mResponse.data!!.get(0))
                        } else {
                            // logger.dumpCustomEvent("Response:", "empty or null")
                        }

                    } else {
                        // logger.dumpCustomEvent("FailMessage: ${mResponse.settings?.message}", mResponse.data.toString())
                    }


                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    // logger.dumpCustomEvent("FailMessage:", message)
                }

            })


    }


    fun showNotification(businessDetails: BusinessDetails) {
        // logger.dumpCustomEvent("LocalNotification:starting", "")
        var title = ""
        var subtitle = ""

        if (!businessDetails.getPromotionList.isEmpty()) {
            title = businessDetails.getPromotionList[0].title
            subtitle = "${businessDetails.placeName} - ${businessDetails.getPromotionList[0].subTitle}"

        } else {
            title = businessDetails.placeName
        }
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels()
        }

        val notificationId = 1
        //        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        val intent = Intent(this, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra(AppConstants.BUSINESS_ID, businessDetails.businessPlaceId)
        intent.putExtra(AppConstants.TYPE, "local")
        val contentIntent =
            PendingIntent.getActivity(
                this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_ONE_SHOT
            )

        val notificationBuilder = NotificationCompat.Builder(
            this,
            "200"
        )
            .setSmallIcon(notificationIcon)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setContentTitle(title)
            .setContentText(subtitle)
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/raw/push_notification_sound"))
        //               .setSound(defaultSoundUri);


        if (businessDetails.getPromotionList.isNotEmpty() && businessDetails.getPromotionList[0].getPromotionImages.isNotEmpty() && businessDetails.getPromotionList[0].getPromotionImages[0].image.isNotEmpty()) {
            runOnUiThread {
                Glide.with(this).asBitmap()
                    .load(businessDetails.getPromotionList.get(0).getPromotionImages.get(0).image)
                    .into(object : SimpleTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {

                            notificationBuilder.setStyle(
                                NotificationCompat.BigPictureStyle().bigPicture(resource)
                            )
                            notificationBuilder.setContentIntent(contentIntent)
                            notificationManager =
                                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                            notificationManager!!.notify(
                                notificationId,
                                notificationBuilder.build()
                            )
                        }

                    })
            }


        } else {
            notificationBuilder.setContentIntent(contentIntent)
            notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager!!.notify(notificationId, notificationBuilder.build())
        }


        //logger.dumpCustomEvent("LocalNotification","completed")

    }

    val notificationIcon: Int
        get() {
            val useWhiteIcon =
                android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP
            return if (useWhiteIcon) {
                R.drawable.ic_stat_notiicon // we need to add transparent imag
            } else {
                R.mipmap.ic_launcher
            }
        }

    private fun setupChannels() {
        val channelName = getString(R.string.local_notifications_admin_channel_name)
        val channelDescription = getString(R.string.local_notifications_admin_channel_description)
        var adminChannel: NotificationChannel? = null
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            adminChannel = NotificationChannel(
                "200",
                channelName,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()

            adminChannel.description = channelDescription
            adminChannel.enableLights(true)
            adminChannel.setSound(
                Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/raw/push_notification_sound"),
                attributes
            )
            adminChannel.lightColor = Color.RED
            adminChannel.enableVibration(true)
            if (notificationManager != null) {
                notificationManager!!.createNotificationChannel(adminChannel)
            }
        }


    }
}
