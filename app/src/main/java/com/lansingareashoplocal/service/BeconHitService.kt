package com.lansingareashoplocal.service

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.app.*
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import androidx.core.app.NotificationCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.lansingareashoplocal.R
import com.lansingareashoplocal.ui.intro.SplashActivity
import com.lansingareashoplocal.ui.model.BusinessDetails
import com.lansingareashoplocal.room.AppDataBase
import com.lansingareashoplocal.room.Becons
import org.jetbrains.anko.runOnUiThread
import kotlin.collections.HashMap

class BeconHitService : IntentService("becon") {
    // lateinit var logger: Logger
    private var notificationManager: NotificationManager? = null
    var major = 0
    var minor = 0

    override fun onHandleIntent(intent: Intent?) {


        major =
            CommonMethods.getIntDataFromSharePreference(this@BeconHitService, AppConstants.MAJOR)
        minor =
            CommonMethods.getIntDataFromSharePreference(this@BeconHitService, AppConstants.MINOR)
        //    logger = Logger(this)
        if (!AppDataBase.getAppDatabase(this@BeconHitService).Dao().idBeconPresent(
                major,
                minor
            )
        ) {
            callBeaconHitApi(AppConstants.NEW)
        } else {
            val becons =
                AppDataBase.getAppDatabase(this@BeconHitService).Dao().getBecon(major, minor)
            if (CommonMethods.isDateCross24Hours(
                    CommonMethods.getTimeInDateFormate(becons.date),
                    CommonMethods.getCurrentTimeInDateFormate()
                )
            ) {

                Log.d("BeaconHit:", "Api started")

                callBeaconHitApi(AppConstants.UPDATE)

            }


        }
    }


    override fun onBind(intent: Intent?): IBinder? {


        return null
    }


    private fun callBeaconHitApi(type: String) {

        // logger.dumpCustomEvent("BeaconHitApi:started", "MAJOR: ${major} MINOR: ${minor} PROXIMATE_ID:${AppConstants.PROXIMATE_ID} ")
        val inputParam = HashMap<String, String>()
        inputParam.put(
            ApiConstants.DEVICE_UDID,
            Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
        )
        inputParam.put(ApiConstants.USER_ID, CommonMethods.getUserId(this@BeconHitService)!!)
        inputParam.put(ApiConstants.MAJOR, major.toString())
        inputParam.put(ApiConstants.MINOR, minor.toString())
        inputParam.put(ApiConstants.PROXIMITY, CommonMethods.proximate_uuid)


        val call = ApiClient.getRetrofitInterface()?.bitBecon(inputParam)


        WSClient<Response<List<BusinessDetails>>>().request(
            this@BeconHitService,
            100,
            false,
            call,
            object : ISuccessHandler<Response<List<BusinessDetails>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<BusinessDetails>>
                ) {


                    if (mResponse.settings?.success.equals("1")) {
                        //logger.dumpCustomEvent("SucessMessage:${mResponse.settings?.message}", mResponse.data.toString())
                        if (type.equals(AppConstants.NEW)) {
                            val becon = Becons()
                            becon.major = major
                            becon.minor = minor
                            becon.date = CommonMethods.getCurrentTimeInString()
                            AppDataBase.getAppDatabase(this@BeconHitService).Dao()
                                .insertBecon(becon)
                            if (mResponse.data != null && mResponse.data!!.isNotEmpty())
                                showNotification(mResponse.data!!.get(0))

                        } else if (type.equals(AppConstants.UPDATE)) {
                            val becon =
                                AppDataBase.getAppDatabase(this@BeconHitService).Dao()
                                    .getBecon(major, minor)
                            becon.date = CommonMethods.getCurrentTimeInString()
                            AppDataBase.getAppDatabase(this@BeconHitService).Dao()
                                .updateBecon(becon)
                        }

                    } else {
                        //  logger.dumpCustomEvent("FailMessage: ${mResponse.settings?.message}", mResponse.data.toString())
                    }


                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {


                }

            })


    }


    fun showNotification(businessDetails: BusinessDetails) {
        //logger.dumpCustomEvent("LocalNotification:showing", "")
        var title = ""
        var subtitle = ""

        if (!businessDetails.getPromotionList.isEmpty()) {
            title = businessDetails.getPromotionList.get(0).title
            title = "$title ${businessDetails.getPromotionList.get(0).priceDiscount}"
            subtitle = businessDetails.getPromotionList.get(0).subTitle

        } else {
            title = businessDetails.placeName
        }
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels()
        }


        val notificationId = 1

        //        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        val intent = Intent(this, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra(AppConstants.BUSINESS_ID, businessDetails.businessPlaceId)
        intent.putExtra(AppConstants.TYPE, "local")
        val contentIntent =
            PendingIntent.getActivity(
                this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_ONE_SHOT
            )

        val notificationBuilder = NotificationCompat.Builder(
            this,
            "200"
        )
            .setSmallIcon(notificationIcon)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setContentTitle(title)
            .setContentText(subtitle)
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/raw/push_notification_sound"))

        if (businessDetails.getPromotionList.isNotEmpty() && businessDetails.getPromotionList.get(0).getPromotionImages.isNotEmpty()) {
            runOnUiThread {
                Glide.with(this).asBitmap()
                    .load(businessDetails.getPromotionList.get(0).getPromotionImages.get(0).image_org)
                    .into(object : SimpleTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            notificationBuilder.setStyle(
                                NotificationCompat.BigPictureStyle().bigPicture(resource)
                            )
                            notificationBuilder.setContentIntent(contentIntent)
                            notificationManager =
                                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                            notificationManager!!.notify(
                                notificationId,
                                notificationBuilder.build()
                            )
                        }

                    })
            }
        } else {
            notificationBuilder.setContentIntent(contentIntent)
            notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager!!.notify(notificationId, notificationBuilder.build())
        }
        //               .setSound(defaultSoundUri);

        /* else if (code?.equals(AppConstants.ALERT)!!) {
             val message = remoteMessage.data["message"]
             val news = remoteMessage.data["others"]

             if (message != null) {

                 runOnUiThread(Runnable {
                     alert(message) {
                         okButton {
                             if (news != null) {
                                 val map = Gson().fromJson<HashMap<String, String>>(
                                     news,
                                     object : TypeToken<HashMap<String, String>>() {
                                     }.type
                                 )
                                 if (map != null) {
                                     if (news.contains(AppConstants.COMMUNITY_ID)&&  news.contains(AppConstants.NOTIFICATION_ID)) {
                                         val community_id = map[AppConstants.COMMUNITY_ID]
                                         val notification_id = map[AppConstants.NOTIFICATION_ID]
                                         if (community_id != null && notification_id != null) {
                                             val dialogNewAlertDetails = DialogNewAlertDetails(this@BaseActivity,null,community_id,notification_id)
                                             dialogNewAlertDetails.showSortAlert()

                                         }

                                     }
                                 }
                             }
                         }
                     }.show()
                 })

             }
         }



         else if (code?.equals(AppConstants.DIRECT)!!) {




             val message = remoteMessage.data["message"]
             if (message != null) {
                 runOnUiThread(Runnable {
                     alert(message) {
                         okButton {

                         }
                     }.show()
                 })

             }

         }*/


    }

    val notificationIcon: Int
        get() {
            val useWhiteIcon =
                android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP
            return if (useWhiteIcon) {
                R.drawable.ic_stat_notiicon // we need to add transparent imag
            } else {
                R.mipmap.ic_launcher
            }
        }

    private fun setupChannels() {
        val channelName = getString(R.string.local_notifications_admin_channel_name)
        val channelDescription = getString(R.string.local_notifications_admin_channel_description)
        var adminChannel: NotificationChannel? = null
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            adminChannel = NotificationChannel(
                "200",
                channelName,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()

            adminChannel.description = channelDescription
            adminChannel.enableLights(true)
            adminChannel.setSound(
                Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/raw/push_notification_sound"),
                attributes
            )
            adminChannel.lightColor = Color.RED
            adminChannel.enableVibration(true)
            if (notificationManager != null) {
                notificationManager!!.createNotificationChannel(adminChannel)
            }
        }


    }

}