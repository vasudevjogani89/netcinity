package com.lansingareashoplocal.service

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.RemoteViews

import androidx.core.app.NotificationCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.core.LifecycleHandler
import com.lansingareashoplocal.ui.intro.SplashActivity
import com.lansingareashoplocal.utility.helper.TimeUtils
import org.jetbrains.anko.runOnUiThread

import java.util.HashMap

class NotificationsMessagingService : FirebaseMessagingService() {
    private var notificationManager: NotificationManager? = null


    val notificationIcon: Int
        get() {
            val useWhiteIcon =
                android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP
            return if (useWhiteIcon) {
                R.drawable.ic_stat_notiicon // we need to add transparent imag
            } else {
                R.mipmap.ic_launcher
            }
        }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        if (token != null) {
            storeTokenInPref(token)
            if (CommonMethods.getUserId(this)!!.isEmpty()) {
                callSyncDeviceId()
            }

        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        if (remoteMessage != null) {
            Log.d("notification", remoteMessage.toString())
            if (LifecycleHandler.isApplicationInForeground && LifecycleHandler.currentActivity!=null && !LifecycleHandler.currentActivity?.isFinishing!!) {
                (LifecycleHandler.currentActivity as BaseActivity).showPushAlert(remoteMessage)
            } else {
                val hMap = HashMap<String, String>()
                hMap.putAll(remoteMessage.data)
                showNotification(remoteMessage, hMap)
            }
        }


    }

    private fun sendRegistrationToServer(token: String) {

    }

    private fun storeTokenInPref(token: String) {

        CommonMethods.setDataIntoSharePreference(
            applicationContext,
            AppConstants.DEVICE_TOKEN,
            token
        )
    }

    fun showNotification(
        remoteMessage: RemoteMessage,
        notificationObject: HashMap<String, String>
    ) {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels()
        }


        val notificationId = 1

        //        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        Log.d("inBackground", "inBackground")
        val intent = Intent(this, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)


        intent.putExtra("BundleData", notificationObject)

        intent.putExtra("type", "live")
        val contentIntent =
            PendingIntent.getActivity(
                this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_ONE_SHOT
            )

        val notificationBuilder = NotificationCompat.Builder(
            this,
            ADMIN_CHANNEL_ID
        )
            .setSmallIcon(notificationIcon)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setContentTitle(remoteMessage.data["title"])
            .setContentText(remoteMessage.data["message"])
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/raw/push_notification_sound"))

        if(remoteMessage.data["image"]!=null && remoteMessage.data["image"]!!.isNotEmpty()){
            runOnUiThread {
                Glide.with(this).asBitmap().load(remoteMessage.data["image"])
                    .into(object : SimpleTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {

                            notificationBuilder.setStyle(NotificationCompat.BigPictureStyle().bigPicture(resource))
                            notificationBuilder.setContentIntent(contentIntent)
                            notificationManager =
                                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                            notificationManager!!.notify(notificationId, notificationBuilder.build())
                        }

                    })
            }


        }
        else{
            notificationBuilder.setContentIntent(contentIntent)
            notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager!!.notify(notificationId, notificationBuilder.build())
        }

    }

    /**
     * creates a new channels for android O and above
     */
    private fun setupChannels() {
        val channelName = getString(R.string.notifications_admin_channel_name)
        val channelDescription = getString(R.string.notifications_admin_channel_description)
        var adminChannel: NotificationChannel? = null
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            adminChannel = NotificationChannel(
                ADMIN_CHANNEL_ID,
                channelName,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()

            adminChannel.description = channelDescription
            adminChannel.enableLights(true)
            adminChannel.setSound(
                Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/raw/push_notification_sound"),
                attributes
            )
            adminChannel.lightColor = Color.RED
            adminChannel.enableVibration(true)
            if (notificationManager != null) {
                notificationManager!!.createNotificationChannel(adminChannel)
            }
        }


    }

    companion object {
         val ADMIN_CHANNEL_ID = "100"
    }


    fun callSyncDeviceId() {


        val inputParam = HashMap<String, String>()
        inputParam.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputParam.put(
            ApiConstants.DEVICE_UDID,
            Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
        )
        inputParam.put(
            ApiConstants.DEVICE_TOKEN,
            CommonMethods.getDataFromSharePreference(this, AppConstants.DEVICE_TOKEN)
        )
        inputParam.put(ApiConstants.DEVICE_TYPE, getString(R.string.android))


        var call = ApiClient.getRetrofitInterface()?.syncDeviceToken(inputParam)



        WSClient<Response<List<Any>>>().request(
            this,
            100,
            false,
            call,
            object : ISuccessHandler<Response<List<Any>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Any>>) {

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                }
            })

    }


}