package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName


data class FeaturedBusinessWeek(
    @SerializedName("address")
    var address: String = "",
    @SerializedName("banner_image")
    var bannerImage: String = "",
    @SerializedName("business_place_id")
    var businessPlaceId: String = "",
    @SerializedName("business_place_of_week_id")
    var businessPlaceOfWeekId: String = "",
    @SerializedName("community_id")
    var communityId: String = "",
    @SerializedName("email")
    var email: String = "",
    @SerializedName("end_date")
    var endDate: String = "",
    @SerializedName("favourite_count")
    var favouriteCount: String = "",
    @SerializedName("phone")
    var phone: String = "",
    @SerializedName("place_name")
    var placeName: String = "",
    @SerializedName("start_date")
    var startDate: String = "",
    @SerializedName("icon")
    var icon: String = ""

)