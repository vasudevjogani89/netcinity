package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName


data class OtpModel(
    @SerializedName("user_id")
    var userId: String = ""
)