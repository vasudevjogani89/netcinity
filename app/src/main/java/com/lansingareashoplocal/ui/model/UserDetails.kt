package com.lansingareashoplocal.ui.model

import com.google.gson.annotations.SerializedName


data class UserDetails(
    @SerializedName("app_version")
    var appVersion: String = "",
    @SerializedName("avatar")
    var avatar: String = "",
    @SerializedName("device_name")
    var deviceName: String = "",
    @SerializedName("device_os")
    var deviceOs: String = "",
    @SerializedName("device_type")
    var deviceType: String = "",
    @SerializedName("dob")
    var dob: String = "",
    @SerializedName("email")
    var email: String = "",
    @SerializedName("first_name")
    var firstName: String = "",
    @SerializedName("gender")
    var gender: String = "",
    @SerializedName("last_login")
    var lastLogin: String = "",
    @SerializedName("last_name")
    var lastName: String = "",
    @SerializedName("notification_pref")
    var notificationPref: String = "",
    @SerializedName("sound_pref")
    var soundPref: String = "",
    @SerializedName("status")
    var status: String = "",
    @SerializedName("user_id")
    var userId: String = "",

    @SerializedName("facebook_id")
    var facebook_id: String = "",


    @SerializedName("social_type")
    var social_type: String = "",
    @SerializedName("apple_id")
    var apple_id: String = "",
    @SerializedName("google_id")
    var google_id: String = ""





) {

}