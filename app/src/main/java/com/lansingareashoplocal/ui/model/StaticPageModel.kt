package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName


data class StaticPageModel(
    @SerializedName("page_code")
    var page_code: String = "",
    @SerializedName("page_title")
    var pageTitle: String = "",
    @SerializedName("meta_title")
    var meta_title: String = "",
    @SerializedName("page_url")
    var page_url: String = ""
)