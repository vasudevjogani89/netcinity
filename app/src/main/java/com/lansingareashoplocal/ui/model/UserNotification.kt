package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName


data class UserNotification(
    @SerializedName("business_place_id")
    var businessPlaceId: String = "",
    @SerializedName("event_id")
    var eventId: String = "",
    @SerializedName("message")
    var message: String = "",
    @SerializedName("noti_date")
    var notiDate: String = "",
    @SerializedName("notification_id")
    var notificationId: String = "",
    @SerializedName("type")
    var type: String = "",
    @SerializedName("user_notification_id")
    var userNotificationId: String = "",
        @SerializedName("image")
    var image: String = ""
)