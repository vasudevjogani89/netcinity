package com.lansingareashoplocal.ui.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
class ImageDetails(

    var imageUrl: String = "",
    var type: String = "",
    var videoType: String = "",
    var videoUrl: String = "",
    var thumnailImage:String="",
    var isSelected: Boolean = false

) : Parcelable