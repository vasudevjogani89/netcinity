package com.lansingareashoplocal.ui.model

import java.io.Serializable

open class Promotions : Serializable {

    var name: String? = null
    var image: String? = null
    var address: String? = null
    var header: String? = null
    var type: Int? = null
    var position:Int?=null
    var isLast: Boolean = false

    var isFeature=true
}