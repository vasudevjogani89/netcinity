package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName


data class Reason(
    @SerializedName("reason")
    var reason: String = "",
    @SerializedName("reason_id")
    var reasonId: String = ""
)