package com.lansingareashoplocal.ui.model
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Review(
    @SerializedName("business_place_id")
    var businessPlaceId: String = "",
    @SerializedName("avatar")
    var profileImage: String = "",
    @SerializedName("rating")
    var rating: String = "",
    @SerializedName("review")
    var review: String = "",
    @SerializedName("review_date")
    var reviewDate: String = "",
    @SerializedName("review_id")
    var reviewId: String = "",
    @SerializedName("user_id")
    var userId: String = "",
    @SerializedName("user_name")
    var userName: String = "",
    @SerializedName("publish_status")
    var publish_status: String = ""


): Parcelable



