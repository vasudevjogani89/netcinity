package com.lansingareashoplocal.ui.model

import android.graphics.Bitmap

class CategoryBitMap {
    var bitMap: Bitmap? = null
    var category: String = ""
    var categoryId: String = ""
}