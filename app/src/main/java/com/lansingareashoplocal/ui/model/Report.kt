package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName


data class Report(
    @SerializedName("report_type")
    var reportType: String = "",
    @SerializedName("report_type_id")
    var reportTypeId: String = "",
    var isSelected: Boolean =false
)