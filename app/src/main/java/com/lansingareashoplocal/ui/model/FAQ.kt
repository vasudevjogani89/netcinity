package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName


data class FAQ(
    @SerializedName("answer")
    var answer: String = "",
    @SerializedName("faq_master_id")
    var faqMasterId: String = "",
    @SerializedName("question")
    var question: String = "",
    @SerializedName("sequence")
    var sequence: String = ""
)