package com.lansingareashoplocal.ui.model

import com.google.gson.annotations.SerializedName


data class Temperature(
    @SerializedName("current")
    var current: Current,




    @SerializedName("daily")
    var daily: List<Daily> = listOf(),
    @SerializedName("hourly")
    var hourly: List<Hourly> = listOf(),
    @SerializedName("lat")
    var lat: Double = 0.0,
    @SerializedName("lon")
    var lon: Double = 0.0,
    @SerializedName("timezone")
    var timezone: String = ""
) {
    inner class Current(
        @SerializedName("clouds")
        var clouds: Int = 0,
        @SerializedName("dew_point")
        var dewPoint: Double = 0.0,
        @SerializedName("dt")
        var dt: Long = 0,
        @SerializedName("feels_like")
        var feelsLike: String = "",
        @SerializedName("humidity")
        var humidity: String = "",
        @SerializedName("pressure")
        var pressure:Float = 0.0F,
        @SerializedName("sunrise")
        var sunrise: Long = 0,
        @SerializedName("sunset")
        var sunset: Int = 0,
        @SerializedName("temp")
        var temp: Float = 0.0F,
        @SerializedName("uvi")
        var uvi: Double = 0.0,
        @SerializedName("visibility")
        var visibility: Double = 0.0,
        @SerializedName("weather")
        var weather: List<Weather> = listOf(),
        @SerializedName("wind_deg")
        var windDeg: Int = 0,
        @SerializedName("wind_speed")
        var windSpeed: Double = 0.0
    )

    inner class Weather(
        @SerializedName("description")
        var description: String = "",
        @SerializedName("icon")
        var icon: String = "",
        @SerializedName("id")
        var id: Int = 0,
        @SerializedName("main")
        var main: String = ""
    )

    inner class Daily(
        @SerializedName("clouds")
        var clouds: Int = 0,
        @SerializedName("dew_point")
        var dewPoint: Double = 0.0,
        @SerializedName("dt")
        var dt: Long = 0,
        @SerializedName("feels_like")
        var feelsLike: FeelsLike = FeelsLike(),
        @SerializedName("humidity")
        var humidity: Int = 0,
        @SerializedName("pressure")
        var pressure: Int = 0,
        @SerializedName("rain")
        var rain: Double = 0.0,
        @SerializedName("sunrise")
        var sunrise: Int = 0,
        @SerializedName("sunset")
        var sunset: Int = 0,
        @SerializedName("temp")
        var temp: Temp = Temp(),
        @SerializedName("uvi")
        var uvi: Double = 0.0,
        @SerializedName("weather")
        var weather: List<WeatherX> = listOf(),
        @SerializedName("wind_deg")
        var windDeg: Int = 0,
        @SerializedName("wind_speed")
        var windSpeed: Double = 0.0
    )

    inner class FeelsLike(
        @SerializedName("day")
        var day: Double = 0.0,
        @SerializedName("eve")
        var eve: Double = 0.0,
        @SerializedName("morn")
        var morn: Double = 0.0,
        @SerializedName("night")
        var night: Double = 0.0
    )

    inner class Temp(
        @SerializedName("day")
        var day: Double = 0.0,
        @SerializedName("eve")
        var eve: Double = 0.0,
        @SerializedName("max")
        var max: Double = 0.0,
        @SerializedName("min")
        var min: Double = 0.0,
        @SerializedName("morn")
        var morn: Double = 0.0,
        @SerializedName("night")
        var night: Double = 0.0
    )

    inner class WeatherX(
        @SerializedName("description")
        var description: String = "",
        @SerializedName("icon")
        var icon: String = "",
        @SerializedName("id")
        var id: Int = 0,
        @SerializedName("main")
        var main: String = ""
    )

    inner class Hourly(
        @SerializedName("clouds")
        var clouds: Int = 0,
        @SerializedName("dew_point")
        var dewPoint: Double = 0.0,
        @SerializedName("dt")
        var dt: Int = 0,
        @SerializedName("feels_like")
        var feelsLike: Double = 0.0,
        @SerializedName("humidity")
        var humidity: Int = 0,
        @SerializedName("pressure")
        var pressure: Int = 0,
        @SerializedName("temp")
        var temp: Double = 0.0,
        @SerializedName("weather")
        var weather: List<WeatherXX> = listOf(),
        @SerializedName("wind_deg")
        var windDeg: Int = 0,
        @SerializedName("wind_speed")
        var windSpeed: Double = 0.0
    )

    inner class WeatherXX(
        @SerializedName("description")
        var description: String = "",
        @SerializedName("icon")
        var icon: String = "",
        @SerializedName("id")
        var id: Int = 0,
        @SerializedName("main")
        var main: String = ""
    )
}

