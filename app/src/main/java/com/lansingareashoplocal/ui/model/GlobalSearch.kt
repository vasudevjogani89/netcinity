package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName


data class GlobalSearch(
    @SerializedName("business_place_id")
    var businessPlaceId: String = "",
    @SerializedName("client_id")
    var clientId: String = "",
    @SerializedName("notification_id")
    var communitiyNotificationsId: String = "",
    @SerializedName("entity_type")
    var entityType: String = "",
    @SerializedName("event_id")
    var eventId: String = "",
    @SerializedName("search_log_id")
    var searchLogId: String = "",
    @SerializedName("title")
    var title: String = "",
    @SerializedName("icon")
    var icon: String = ""
)