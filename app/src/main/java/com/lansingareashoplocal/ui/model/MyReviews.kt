package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName


data class MyReviews(
    @SerializedName("business_place_id")
    var businessPlaceId: String = "",
    @SerializedName("flag_approval_status")
    var flagApprovalStatus: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("profile_image")
    var profileImage: String = "",
    @SerializedName("publish_status")
    var publishStatus: String = "",
    @SerializedName("rating")
    var rating: String = "",
    @SerializedName("review")
    var review: String = "",
    @SerializedName("review_date")
    var reviewDate: String = "",
    @SerializedName("review_flag")
    var reviewFlag: String = "",
    @SerializedName("review_id")
    var reviewId: String = "",
    @SerializedName("user_id")
    var userId: String = "",
    @SerializedName("icon")
    var icon: String = "",
    @SerializedName("place_name")
    var place_name: String = ""




)