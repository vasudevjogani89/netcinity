package com.lansingareashoplocal.ui.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SubCategories {
    @SerializedName("sub_category_id")
    var sub_category_id: String = ""
    @SerializedName("sub_category")
    var sub_category: String = ""
    @SerializedName("tag_count")
    var tag_count: String = ""
    var isSelected: Boolean = false
}