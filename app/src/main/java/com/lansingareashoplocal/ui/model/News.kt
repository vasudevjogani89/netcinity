package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class News(
    @SerializedName("added_date")
    var addedDate: String = "",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("image")
    var image: String = "",
    @SerializedName("notification_id")
    var notificationId: String = "",
    @SerializedName("status")
    var status: String = "",
    @SerializedName("title")
    var title: String = ""
): Serializable

