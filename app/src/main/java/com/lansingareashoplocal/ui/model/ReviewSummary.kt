package com.lansingareashoplocal.ui.model
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReviewSummary(
    @SerializedName("avg_rating")
    var avgRatingCount: String = "",
    @SerializedName("tot_1_star")
    var starRatingCountOne: String = "",
    @SerializedName("tot_2_star")
    var starRatingCountTwo: String = "",
    @SerializedName("tot_3_star")
    var starRatingCountThree: String = "",
    @SerializedName("tot_4_star")
    var starRatingCountFour: String = "",
    @SerializedName("tot_5_star")
    var starRatingCountFive: String = "",
    @SerializedName("tot_rating")
    var totalReviewCount: String = "",
    @SerializedName("is_review_added")
    var is_review_added: String = ""


): Parcelable

