package com.lansingareashoplocal.ui.model

import android.location.Location
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*
@Parcelize
class Photo :Parcelable {
    @SerializedName("id")
    @Expose
     val id: String? = null

    @SerializedName("created_at")
    @Expose
     val createdAt: String? = null

    @SerializedName("updated_at")
    @Expose
     val updatedAt: String? = null

    @SerializedName("width")
    @Expose
     val width: Int? = null

    @SerializedName("height")
    @Expose
     val height: Int? = null

    @SerializedName("color")
    @Expose
     val color: String? = null

    @SerializedName("downloads")
    @Expose
     val downloads: Int? = null

    @SerializedName("likes")
    @Expose
     val likes: Int? = null

    @SerializedName("liked_by_user")
    @Expose
     val likedByUser: Boolean? = null

    @SerializedName("location")
    @Expose
     val location: Location? = null

    @SerializedName("current_user_collections")
    @Expose
     val currentUserCollections: List<Collection<*>> =
        ArrayList()

    @SerializedName("urls")
    @Expose
     val urls: Urls = Urls()

    @SerializedName("categories")
    @Expose
     val categories: List<Category> =
        ArrayList()
}