package com.lansingareashoplocal.ui.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class SearchResults :Parcelable {
    @SerializedName("total")
    @Expose
     val total: Int = 0

    @SerializedName("total_pages")
    @Expose
     val totalPages: Int = 0

    @SerializedName("results")
    @Expose
     val photoList: List<Photo> = listOf()
}