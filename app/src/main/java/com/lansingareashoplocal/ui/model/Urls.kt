package com.lansingareashoplocal.ui.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Urls : Parcelable{
    @SerializedName("raw")
    @Expose
     val raw: String? = null

    @SerializedName("full")
    @Expose
     val full: String? = null

    @SerializedName("regular")
    @Expose
     val regular: String? = null

    @SerializedName("small")
    @Expose
     val small: String? = null

    @SerializedName("thumb")
    @Expose
     val thumb: String? = null
}