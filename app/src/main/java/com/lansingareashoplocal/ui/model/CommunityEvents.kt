package com.lansingareashoplocal.ui.model

import com.google.gson.annotations.SerializedName
import org.joda.time.LocalDate
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

public
data class CommunityEvents(
    @SerializedName("category")
    var category: String = "",
    @SerializedName("event_date_time")
    var eventDateTime: String = "",
    @SerializedName("event_description")
    var eventDescription: String = "",
    @SerializedName("event_id")
    var eventId: String = "",
    @SerializedName("event_image")
    var eventImage: String = "",
    @SerializedName("event_latitude")
    var eventLatitude: String = "",
    @SerializedName("event_location")
    var eventLocation: String = "",
    @SerializedName("event_longitude")
    var eventLongitude: String = "",
    @SerializedName("event_sponser")
    var eventSponser: String = "",
    @SerializedName("event_title")
    var eventTitle: String = "",
    @SerializedName("event_start_date")
    var event_start_date: String = "",
    @SerializedName("event_end_date")
    var event_end_date: String = ""



): Serializable




