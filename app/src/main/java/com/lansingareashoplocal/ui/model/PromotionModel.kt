package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName


data class PromotionModel(
    @SerializedName("data")
    var data: MutableList<Data> = arrayListOf(),
    @SerializedName("header")
    var header: String = ""
)

data class Data(
    @SerializedName("address")
    var address: String = "",
    @SerializedName("image")
    var image: String = "",
    @SerializedName("name")
    var name: String = ""



)