package com.lansingareashoplocal.ui.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class FeaturedBusiness(
    @SerializedName("address")
    var address: String = "",
    @SerializedName("banner_image")
    var bannerImage: String = "",
    @SerializedName("business_place_id")
    var businessPlaceId: String = "",
    @SerializedName("get_promotion_list")
    var getPromotionList: List<GetPromotion> = listOf(),
    @SerializedName("phone")
    var phone: String = "",
    @SerializedName("place_name")
    var placeName: String = "",
    @SerializedName("owner_profile_image")
    var owner_profile_image: String = "",

    @SerializedName("distance")
    var distance: String = "",

    @SerializedName("bp_latitude")
    var latitude: String = "",
    @SerializedName("bp_longitude")
    var longitude: String = "",
    @SerializedName("category_icon")
    var category_icon: String = "",
    @SerializedName("category_id")
    var categoryId: String = "",
    @SerializedName("sub_category")
    var sub_category: String = "",
    @SerializedName("featured")
    var featured: String = "",





    var website: String = "",
    var header: String = "",
    var itemType: Int? = null,
    var position: Int = 0,
    var hasNext: Boolean = false,
    var emptyData: Boolean = false,

    var emptyMessage: String = "",

    var isFeatureBusiness: Boolean = true,

    var remainingCount: Int = 0






) : Serializable


data class GetPromotion(
    @SerializedName("peroid_end")
    var peroidEnd: String = "",
    @SerializedName("price_discount")
    var priceDiscount: String = "",
    @SerializedName("promotion_id")
    var promotionId: String = "",
    @SerializedName("set_expiration")
    var set_expiration: String = ""


) : Serializable




