package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


data class ReviewList(
    @SerializedName("review_listing")
    var reviewListing: List<Review> = listOf(),
    @SerializedName("review_summary")
    var reviewSummary: List<ReviewSummary> = listOf()
)

