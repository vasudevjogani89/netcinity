package com.lansingareashoplocal.ui.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BusinessDetails(

    @SerializedName("added_date")
    var addedDate: String = "",
    @SerializedName("address")
    var address: String = "",
    @SerializedName("address_detail")
    var addressDetail: String = "",
    @SerializedName("banner_image")
    var bannerImage: String = "",
    @SerializedName("business_place_id")
    var businessPlaceId: String = "",
    @SerializedName("category_icon")
    var categoryIcon: String = "",
    @SerializedName("category_id")
    var categoryId: String = "",
    @SerializedName("current_status")
    var currentStatus: String = "",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("email")
    var email: String = "",
    @SerializedName("get_business_hours")
    var getBusinessHours: List<GetBusinessHour> = listOf(),
    @SerializedName("get_other_link")
    var getOtherLink: List<GetOtherLink> = listOf(),
    @SerializedName("get_promotion_list")
    var getPromotionList: List<GetPromotionDetails> = listOf(),
    @SerializedName("get_social_link")
    var getSocialLink: List<GetSocialLink> = listOf(),
    @SerializedName("review_summary")
    var getReviewSummary: List<ReviewSummary> = listOf(),
    @SerializedName("review_listing")
    var getReviewListing: List<Review> = listOf(),


    @SerializedName("get_business_place_images")
    var getBusinessPlaceImages: List<BusinessPlaceImages> = listOf(),

    @SerializedName("designation")
    var designation: String = "",


    @SerializedName("icon")
    var icon: String = "",
    @SerializedName("is_favourite")
    var isFavourite: String = "",
    @SerializedName("latitude")
    var latitude: String = "",
    @SerializedName("longitude")
    var longitude: String = "",
    @SerializedName("phone")
    var phone: String = "",
    @SerializedName("place_name")
    var placeName: String = "",
    @SerializedName("status")
    var status: String = "",
    @SerializedName("tag_line")
    var tagLine: String = "",
    @SerializedName("type")
    var type: String = "",
    @SerializedName("video_url")
    var videoUrl: String = "",
    @SerializedName("website")
    var website: String = "",
    @SerializedName("owner_name")
    var owner_name: String = "",
    @SerializedName("owner_profile_image")
    var owner_profile_image: String = "",
    @SerializedName("owner_about_us")
    var owner_about_us: String = "",

    @SerializedName("is_featured")
    var is_featured: String = "",
    @SerializedName("favourite_count")
    var favourite_count: String = "",
    @SerializedName("sub_category")
    var sub_category: String = ""



) : Parcelable

@Parcelize
data class GetBusinessHour(
    @SerializedName("close_time")
    var closeTime: String = "",
    @SerializedName("day_display")
    var dayDisplay: String = "",
    @SerializedName("is_closed")
    var isClosed: String = "",
    @SerializedName("open24x7")
    var open24x7: String = "",
    @SerializedName("open_time")
    var openTime: String = ""
) : Parcelable

@Parcelize
data class GetOtherLink(
    @SerializedName("other_key")
    var otherKey: String = "",
    @SerializedName("other_value")
    var otherValue: String = ""
) : Parcelable

@Parcelize
data class GetPromotionDetails(
    @SerializedName("added_date")
    var addedDate: String = "",
    @SerializedName("get_promotion_images")
    var getPromotionImages: List<GetPromotionImage> = listOf(),
    @SerializedName("main_image")
    var mainImage: String = "",
    @SerializedName("period_start")
    var periodStart: String = "",
    @SerializedName("peroid_end")
    var peroidEnd: String = "",
    @SerializedName("price_discount")
    var priceDiscount: String = "",
    @SerializedName("promotion_id")
    var promotionId: String = "",
    @SerializedName("promotion_place_trans_id")
    var promotionPlaceTransId: String = "",
    @SerializedName("qr_code")
    var qrCode: String = "",
    @SerializedName("sub_title")
    var subTitle: String = "",
    @SerializedName("title")
    var title: String = "",
    @SerializedName("set_expiration")
    var set_expiration: String = ""


) : Parcelable

@Parcelize
data class GetPromotionImage(
    @SerializedName("image")
    var image: String = "",
    @SerializedName("promotion_image_id")
    var promotionImageId: String = "",
    @SerializedName("image_org")
    var image_org: String = "",
    @SerializedName("media_type")
    var media_type: String = "",
    @SerializedName("video_url")
    var video_url: String = ""


) : Parcelable

@Parcelize
data class GetSocialLink(
    @SerializedName("key")
    var key: String = "",
    @SerializedName("value")
    var value: String = ""
) : Parcelable

@Parcelize
data class BusinessPlaceImages(
    @SerializedName("business_place_image_id")
    var businessPlaceImageId: String = "",
    @SerializedName("image")
    var image: String = ""
) : Parcelable

