package com.lansingareashoplocal.ui.model

import android.graphics.Bitmap
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable


data class Category(
    @SerializedName("category")
    var category: String = "",
    @SerializedName("category_id")
    var categoryId: String = "",
    @SerializedName("icon")
    var icon: String = "",
    @SerializedName("type")
    var type: String = "",

    var isLocal: Boolean = false,


    var localImage: Int = 0


) : Serializable