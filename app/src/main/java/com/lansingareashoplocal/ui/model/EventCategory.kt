package com.lansingareashoplocal.ui.model
import com.google.gson.annotations.SerializedName


data class EventCategory(

    @SerializedName("category")
    var category: String = "",
    @SerializedName("category_id")
    var category_id: String = "",
    var isSelected: Boolean = false
)