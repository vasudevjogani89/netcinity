package com.lansingareashoplocal.ui.promotions

import AppConstants
import android.graphics.Bitmap
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.databinding.ItemThumnailGalleryBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.adapter.ViewPagerAdapter
import com.lansingareashoplocal.ui.interfaces.CommonCallBack
import com.lansingareashoplocal.ui.model.ImageDetails
import kotlinx.android.synthetic.main.activity_view_images_activity.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.*


class ViewImagesActivity : BaseActivity(), CommonCallBack {


    var thumnailAdapter: GenericAdapter<ImageDetails, ItemThumnailGalleryBinding>? = null


    override fun commonCallBack(type: String, data: Any, position: Int) {
        val isImageZoomed = data as Boolean
        if (isImageZoomed) {
            vwPager.setPagingEnabled(false)
            rvThumnail.visibility=View.GONE

        } else {
            vwPager.setPagingEnabled(true)
            rvThumnail.visibility=View.VISIBLE

        }
    }


    var isRequireToShowTooBar = true


    var currentImagePostion = 0
    var galleryImagesList: MutableList<ImageDetails>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_view_images_activity)
        getData()
        setData()
        if (isRequireToShowTooBar) {
            toolBarImageViewing.visibility = View.VISIBLE
            // tvTitle.text = galleryImagesList?.get(currentImagePostion)?.title
            ivClear.visibility = View.GONE


        } else {
            toolBarImageViewing.visibility = View.GONE
            ivClear.visibility = View.VISIBLE
        }
        ivClear.setOnClickListener {
            finish()
        }
        tvLeft.setOnClickListener {
            finish()
        }
        setThumnailImagesAdapter()
       // rlGallery.startBlur()
        //rlGallery.setBlurRadius(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1F, resources.displayMetrics))




    }

    private fun setData() {
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        galleryImagesList?.forEachIndexed { index, moduleDetails ->
            viewPagerAdapter.addFragment(
                ViewImagesFragmnet(moduleDetails.imageUrl,moduleDetails.type, moduleDetails.videoType, moduleDetails.videoUrl, this), "")
        }
        vwPager.adapter = viewPagerAdapter
        vwPager.setCurrentItem(currentImagePostion)
        if (galleryImagesList != null && galleryImagesList!!.isNotEmpty()) {
            vwPager.offscreenPageLimit = galleryImagesList?.size!! - 1
        }
        vwPager.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                makeThumnailAsSelected(position)
            }

        })

    }

    private fun setThumnailImagesAdapter() {
        val linearLayoutManager =
            LinearLayoutManager(this@ViewImagesActivity, RecyclerView.HORIZONTAL, false)
        rvThumnail.layoutManager = linearLayoutManager
        thumnailAdapter =
            object : GenericAdapter<ImageDetails, ItemThumnailGalleryBinding>(galleryImagesList) {
                override fun getLayoutId(): Int {
                    return R.layout.item_thumnail_gallery
                }

                override fun onBindData(
                    model: ImageDetails?,
                    position: Int,
                    dataBinding: ItemThumnailGalleryBinding?
                ) {
                   var imageUrl=model?.thumnailImage
                    if(imageUrl?.isEmpty()!!){
                        imageUrl=model?.imageUrl
                    }
                    Glide.with(this@ViewImagesActivity).asBitmap().load(imageUrl).apply(RequestOptions().override(500, 500)).into(object : SimpleTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            dataBinding?.progressThumnail?.visibility=View.GONE
                            dataBinding?.ivVideoThumnail?.setImageBitmap(resource)
                        }


                    })



                    dataBinding?.root?.setOnClickListener {
                        makeThumnailAsSelected(position)
                        vwPager.setCurrentItem(position)
                    }

                    if (model?.isSelected!!) {
                        dataBinding?.rlThumnail?.setBackgroundResource( R.drawable.border_selector)
                    } else {
                        dataBinding?.rlThumnail?.setBackgroundColor(ContextCompat.getColor(this@ViewImagesActivity, R.color.transparent))
                    }

                }

            }
        rvThumnail.adapter = thumnailAdapter
    }

    private fun getData() {
        currentImagePostion = intent.getIntExtra(AppConstants.POSITION, 0)
        galleryImagesList = intent.getParcelableArrayListExtra(AppConstants.SHARE_KEY)
        if(galleryImagesList!=null){
            galleryImagesList!![currentImagePostion].isSelected=true
        }
        isRequireToShowTooBar = intent.getBooleanExtra(AppConstants.TOOL_BAR_IS_REQUIRE, false)
    }

    private fun makeThumnailAsSelected(position:Int){
        galleryImagesList?.forEachIndexed { index, imageDetails ->
            galleryImagesList!![index].isSelected = false
        }
        galleryImagesList?.get(position)?.isSelected = true
       thumnailAdapter?.notifyDataSetChanged()
    }
}