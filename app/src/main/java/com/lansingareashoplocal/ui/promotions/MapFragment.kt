package com.lansingareashoplocal.ui.promotions

import AppConstants
import CommonMethods
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseFragment
import com.lansingareashoplocal.databinding.FragmentMapBinding
import com.lansingareashoplocal.ui.model.BusinessDetails
import kotlinx.android.synthetic.main.activity_promotion_details.*


class MapFragment : BaseFragment<FragmentMapBinding>(), OnMapReadyCallback {


    var businessDetails: BusinessDetails? = null
    var latLng: LatLng? = null
    var mMap: GoogleMap? = null
    override fun onBackPressed(): Boolean {
        return false
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_map
    }

    override fun init() {
        businessDetails = arguments?.getParcelable(AppConstants.BUSINESS_MODEL)
        if (businessDetails?.latitude?.isNotEmpty()!! && businessDetails?.longitude?.isNotEmpty()!!) {
            latLng = LatLng(
                businessDetails?.latitude?.toDouble()!!,
                businessDetails?.longitude?.toDouble()!!
            )
            showMap()
            binding.ivBusinessRecenterMap.setOnClickListener {
                mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14F))
            }
        }
        setmapHeight()



    }



    private fun setmapHeight() {
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.getDefaultDisplay().getMetrics(displayMetrics)
        val height: Int = displayMetrics.heightPixels
        val width: Int = displayMetrics.widthPixels
        binding.tvMap.viewTreeObserver.addOnGlobalLayoutListener(object :ViewTreeObserver.OnGlobalLayoutListener{
            override fun onGlobalLayout() {
                val tvMapheight: Int =  binding.tvMap.measuredHeight
                var businessDetailsActivity: PromotionDetailsActivity? = null
                if(activity!=null){
                    businessDetailsActivity = activity as PromotionDetailsActivity
                    if (businessDetailsActivity != null) {
                        if (businessDetailsActivity.viewPagerHeight != null && tvMapheight!=null) {
                            binding.cvMap.layoutParams.height = height- (businessDetailsActivity.viewPagerHeight!!+tvMapheight+20)

                            //  val layoutparams = cvMap.layoutParams as LinearLayout.LayoutParams
                            //layoutparams.width=width
                            //  layoutparams.height=height- businessDetailsActivity.viewPagerHeight!!
                            // cvMap.layoutParams=layoutparams
                        }
                    }
                }


            }

        })




    }

    private fun showMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


    }


    override fun onMapReady(p0: GoogleMap?) {
        if (p0 != null) {
            mMap = p0
            if (businessDetails?.latitude?.isNotEmpty()!! && businessDetails?.longitude?.isNotEmpty()!!) {
                try {

                    if (latLng != null) {
                        p0?.addMarker(MarkerOptions().position(latLng!!))
                        p0?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14F))
                        p0?.setMinZoomPreference(14F)
                    }
                    p0?.setOnMapClickListener {
                        var geoUri: String? = null
                        if (CommonMethods.getDataFromSharePreference(
                                activity!!,
                                AppConstants.LATITUDE
                            ).isNotEmpty() && CommonMethods.getDataFromSharePreference(
                                activity!!,
                                AppConstants.LONGITUDE
                            ).isNotEmpty()
                        ) {
                            geoUri =
                                "http://maps.google.com/maps?saddr=${CommonMethods.getDataFromSharePreference(
                                    activity!!,
                                    AppConstants.LATITUDE
                                )},${CommonMethods.getDataFromSharePreference(
                                    activity!!,
                                    AppConstants.LONGITUDE
                                )}&daddr=${businessDetails?.latitude},${businessDetails?.longitude}"

                        } else {
                            geoUri =
                                "http://maps.google.com/maps?q=loc:${businessDetails?.latitude},${businessDetails?.longitude} (${businessDetails?.placeName})"
                        }

                        if (businessDetails?.longitude?.isNotEmpty()!! && businessDetails?.latitude?.isNotEmpty()!!) {
                            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
                            try {
                                intent.setPackage("com.google.android.apps.maps");
                                startActivity(intent)
                            } catch (e: Exception) {
                                Toast.makeText(
                                    activity,
                                    "Invalid link",
                                    Toast.LENGTH_SHORT
                                ).show()
                                e.printStackTrace()
                            }
                        }
                    }

                } catch (e: Exception) {

                }

            }
        }


    }
}