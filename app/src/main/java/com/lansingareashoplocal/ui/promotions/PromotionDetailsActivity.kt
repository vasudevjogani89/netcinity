package com.lansingareashoplocal.ui.promotions

import ApiClient
import ApiConstants
import AppConstants
import CommonMethods
import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.ViewTreeObserver
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayout
import com.lansingareashoplocal.R
import com.lansingareashoplocal.api_call_with_ui.TrackPromotion
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.adapter.ViewPagerAdapter
import com.lansingareashoplocal.ui.home.HomeActivity
import com.lansingareashoplocal.ui.model.BusinessDetails
import com.lansingareashoplocal.ui.model.ImageDetails
import com.lansingareashoplocal.ui.review.AddReviewActivity
import com.lansingareashoplocal.ui.user.LoginActivity
import kotlinx.android.synthetic.main.activity_promotion_details.*
import kotlinx.android.synthetic.main.tool_bar_with_out_bg.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import retrofit2.Call


class PromotionDetailsActivity : BaseActivity() {


    var businessDetails : BusinessDetails?=null

    var businessId = ""
    var businessInfoFragment: BusinessInfoFragment? = null
    var reviewFragment: ReviewFragment? = null
    var meetTheOwnerFragment: MeetTheOwnerFragment? = null

    var mapFragment: MapFragment? = null
    var mResumeWindow: Int = 0
    var mResumePosition: Long = 0
    var dataSourceFactory: DataSource.Factory? = null
    val STATE_RESUME_WINDOW = "resumeWindow"
    val STATE_RESUME_POSITION = "resumePosition"
    val STATE_PLAYER_FULLSCREEN = "playerFullscreen"
    var mExoPlayerFullscreen = false
    var viewPagerHeight: Int? = null

    var selectedTab = 0


    /*  var isTabSettingManuvally = false*/

    companion object {
        var favourite_request_code = 500
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_promotion_details)
        dataSourceFactory =
            DefaultDataSourceFactory(this, Util.getUserAgent(this, getString(R.string.app_name)))
        if (savedInstanceState != null) {
            mResumeWindow = savedInstanceState.getInt(STATE_RESUME_WINDOW)
            mResumePosition = savedInstanceState.getLong(STATE_RESUME_POSITION)
            mExoPlayerFullscreen = savedInstanceState.getBoolean(STATE_PLAYER_FULLSCREEN)
        }
        businessId = intent?.getStringExtra(AppConstants.BUSINESS_ID)!!

        tvLeft.setOnClickListener {

            if (isTaskRoot) {
                startActivity(Intent(this@PromotionDetailsActivity, HomeActivity::class.java))
                finish()
            } else {
                finish()
            }
            finish()
        }
        val isFromSearch = intent.getBooleanExtra(AppConstants.IS_FROM_SEARCH, false)
        if (isFromSearch) {
            businessDetails = intent.getParcelableExtra(AppConstants.BUSINESS_DETAILS_MODEL)
            maincontent.visibility = View.VISIBLE
            setData()
        } else {
            callBusinessDetailsApi()

        }
        ibFavorite.setOnClickListener {
            if (CommonMethods.getUserId(this@PromotionDetailsActivity)!!.isNotEmpty()) {
                // ibFavorite.clearAnimation()

                if (businessDetails?.isFavourite.equals("1")) {
                    callRemoveFavourite()
                } else {
                    callMakeFavourite()
                }

            } else {

                CommonMethods.setDataIntoSharePreference(
                    this@PromotionDetailsActivity,
                    AppConstants.BUSINESS_ID,
                    businessDetails?.businessPlaceId
                )
                startActivityForResult(
                    Intent(
                        this@PromotionDetailsActivity,
                        LoginActivity::class.java
                    ), favourite_request_code
                )
            }
        }
        tabLayoutBusiness.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {


            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab != null && tab.position != 0) {
                    selectedTab = tab.position
                }

                if(tab?.position==3){
                    disableScroll()
                }
                else{
                   enableScroll()
                }


            }

        })
        ivGallery.setOnClickListener {
            if (businessDetails!=null && businessDetails?.getBusinessPlaceImages!=null && businessDetails?.getBusinessPlaceImages?.isNotEmpty()!!) {
                val intent = Intent(this@PromotionDetailsActivity, ViewImagesActivity::class.java)
                intent.putExtra(AppConstants.POSITION, 1)
                val imageDetailsList: MutableList<ImageDetails> = ArrayList()
                businessDetails?.getBusinessPlaceImages?.forEachIndexed { index, getBusinessPlaceImages ->
                    if (index == 0) {
                        val imageDetail = ImageDetails()
                        imageDetail.imageUrl = businessDetails?.bannerImage!!
                        imageDetail.type = AppConstants.IMAGE
                        imageDetailsList.add(imageDetail)
                    }
                    val imageDetail = ImageDetails()
                    imageDetail.imageUrl = getBusinessPlaceImages.image
                    imageDetail.type = AppConstants.IMAGE
                    imageDetailsList.add(imageDetail)
                }
                intent.putParcelableArrayListExtra(AppConstants.SHARE_KEY, imageDetailsList as java.util.ArrayList<out ImageDetails>)
                intent.putExtra(AppConstants.TOOL_BAR_IS_REQUIRE, false)
                startActivity(intent)
            }

        }
        getcollapseToolbarHeight()
        rlBusinessRating.setOnClickListener {
            tabLayoutBusiness.getTabAt(1)?.select()
        }



    }

    private fun getcollapseToolbarHeight() {
        val vto = tab_appbar.viewTreeObserver;

        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                tab_appbar.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val width: Int = tab_appbar.measuredWidth
                val height: Int = tab_appbar.measuredHeight
                if(viewPagerHeight!=null){
                    viewPagerHeight=viewPagerHeight!!+height
                }
                else{
                    viewPagerHeight=height
                }

            }

        })
        tabLayoutBusiness.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                tabLayoutBusiness.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val width: Int = tabLayoutBusiness.measuredWidth
                val height: Int = tabLayoutBusiness.measuredHeight
                if(viewPagerHeight!=null){
                    viewPagerHeight=viewPagerHeight!!+height
                }
                else{
                    viewPagerHeight=height
                }
            }

        })

    }


    private fun setUpViewPager() {
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        businessInfoFragment = BusinessInfoFragment()
        val bundle = Bundle()
        bundle.putParcelable(AppConstants.BUSINESS_MODEL, businessDetails)
        businessInfoFragment?.arguments = bundle
        viewPagerAdapter.addFragment(businessInfoFragment, getString(R.string.info))


        reviewFragment = ReviewFragment()
        reviewFragment?.arguments = bundle

        viewPagerAdapter.addFragment(reviewFragment, getString(R.string.reviews))


        meetTheOwnerFragment = MeetTheOwnerFragment()
        meetTheOwnerFragment?.arguments = bundle
        if(!meetTheOwnerFragment?.isAdded!!)
        viewPagerAdapter.addFragment(meetTheOwnerFragment, "${getString(R.string.meet_the)} ${businessDetails?.designation}")


        mapFragment = MapFragment()
        mapFragment?.arguments = bundle
        viewPagerAdapter.addFragment(mapFragment, getString(R.string.maps))
        vwPagerBusiness.adapter = viewPagerAdapter
        tabLayoutBusiness.setupWithViewPager(vwPagerBusiness)
        tabLayoutBusiness.getTabAt(selectedTab)?.select()
        vwPagerBusiness.offscreenPageLimit = 3

    }


    private fun callBusinessDetailsApi() {
        CommonMethods.showProgress(this@PromotionDetailsActivity)

        var inputparam = HashMap<String, String>()
        inputparam.put(
            ApiConstants.USER_ID,
            CommonMethods.getUserId(this@PromotionDetailsActivity)!!
        )
        inputparam.put(ApiConstants.BUSINESS_PLACE_ID, businessId)
        inputparam.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputparam.put(
            ApiConstants.DEVICE_UDID,
            Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
        )

        val call = ApiClient.getRetrofitInterface()?.getBusinessDetails(inputparam)

        WSClient<Response<List<BusinessDetails>>>().request(
            this@PromotionDetailsActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<BusinessDetails>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<BusinessDetails>>
                ) {
                    maincontent.visibility = View.VISIBLE
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null && mResponse.data?.isNotEmpty()!!) {
                        businessDetails = mResponse.data!!.get(0)
                        setData()
                        if(intent.getStringExtra(AppConstants.IS_FROM_BECON_NOTIFICATION)!=null){
                         val isFromBeconNotification=intent.getBooleanExtra(AppConstants.IS_FROM_BECON_NOTIFICATION,false)
                            if(isFromBeconNotification){
                                callPromotionTrackApi()
                            }
                        }


                    }
                }

            }, object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    CommonMethods.hideProgress()


                    alert(message) {

                        okButton { }

                    }.show()

                }

            })
    }


    private fun setData() {
        //  addAnimationToFavourite()
        Glide.with(this@PromotionDetailsActivity).load(businessDetails?.bannerImage)
            .into(ivBannerLogo)
        Glide.with(this@PromotionDetailsActivity).load(businessDetails?.icon).into(ivSmallLogo)

        tvFavourite.text = businessDetails?.favourite_count

        if (businessDetails!=null && businessDetails?.getPromotionList!=null && businessDetails?.getPromotionList?.isNotEmpty()!!) {
            tvDiscount.text = businessDetails?.getPromotionList?.get(0)?.priceDiscount
            rlDiscount.visibility = View.VISIBLE

        } else {
            rlDiscount.visibility = View.GONE
        }

        if (businessDetails?.isFavourite.equals("1")) {
            ibFavorite.setImageDrawable(
                ContextCompat.getDrawable(
                    this@PromotionDetailsActivity,
                    R.drawable.ic_heart
                )
            )
        } else {
            Glide.with(this@PromotionDetailsActivity).load(R.raw.heart).into(ibFavorite)
        }

        if (businessDetails!=null  && businessDetails?.getReviewListing!=null &&  businessDetails?.getReviewListing?.isNotEmpty()!!) {
            tvRatingAvg.text = businessDetails?.getReviewSummary?.get(0)?.avgRatingCount
            tvReviewCount.text = businessDetails?.getReviewSummary?.get(0)?.totalReviewCount
        } else {
            tvRatingAvg.text = ""
            tvReviewCount.text = getString(R.string.no_ratings)
            ivRating.setImageDrawable(
                ContextCompat.getDrawable(
                    this@PromotionDetailsActivity,
                    R.drawable.ic_un_seleted_start
                )
            )
        }

        if (businessDetails!=null  && businessDetails?.getBusinessPlaceImages!=null &&  businessDetails?.getBusinessPlaceImages?.isNotEmpty()!!) {
            rlBannerGallery.visibility = View.VISIBLE
            tvGallaryCount.text = "+${businessDetails?.getBusinessPlaceImages?.size}"
        } else {
            rlBannerGallery.visibility = View.GONE

        }
        setUpViewPager()
    }


    fun callMakeFavourite() {
        var inputparam = HashMap<String, String>()
        inputparam.put(
            ApiConstants.USER_ID,
            CommonMethods.getUserId(this@PromotionDetailsActivity)!!
        )

        inputparam.put(ApiConstants.BUSINESS_PLACE_ID, businessId)
        var call = ApiClient.getRetrofitInterface()?.makeFavourite(inputparam)

        WSClient<Response<List<Any>>>().request(
            this@PromotionDetailsActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<Any>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Any>>) {

                    if (mResponse.settings?.success.equals("1")) {
                        ibFavorite.setImageDrawable(
                            ContextCompat.getDrawable(
                                this@PromotionDetailsActivity,
                                R.drawable.ic_heart
                            )
                        )
                        businessDetails?.isFavourite = "1"

                        var count = businessDetails?.favourite_count?.toInt()
                        count = count?.plus(1)
                        businessDetails?.favourite_count = count.toString()
                        tvFavourite.text = count.toString()
                    } else {
                        alert(mResponse.settings?.message.toString()) {
                            okButton { }
                        }.show()
                    }
                }
            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                }
            })
    }

    fun callRemoveFavourite() {
        var inputparam = HashMap<String, String>()
        inputparam.put(
            ApiConstants.USER_ID,
            CommonMethods.getUserId(this@PromotionDetailsActivity)!!
        )
        inputparam.put(ApiConstants.BUSINESS_PLACE_ID, businessId)
        var call = ApiClient.getRetrofitInterface()?.removeFavourite(inputparam)

        WSClient<Response<List<Any>>>().request(
            this@PromotionDetailsActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<Any>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Any>>) {
                    Glide.with(this@PromotionDetailsActivity).load(R.raw.heart).into(ibFavorite)
                    businessDetails?.isFavourite = "0"
                    var count = businessDetails?.favourite_count?.toInt()
                    count = count?.minus(1)
                    businessDetails?.favourite_count = count.toString()
                    tvFavourite.text = count.toString()
                }
            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                }

            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == favourite_request_code) {
            CommonMethods.setDataIntoSharePreference(
                this@PromotionDetailsActivity,
                AppConstants.BUSINESS_ID,
                ""
            )
            callMakeFavourite()
        } else if (resultCode == Activity.RESULT_OK && requestCode.equals(AppConstants.LOG_IN_REQUEST_FOR_ADD_REVIEW)) {
            redirecttoAddReview()
        } else if (resultCode == Activity.RESULT_OK && requestCode == AppConstants.ADD_REVIEW_REQUEST) {
            callBusinessDetailsApi()
        } else if (resultCode == Activity.RESULT_OK && requestCode == AppConstants.UPDATE_REVIEW_REQUEST) {
            callBusinessDetailsApi()
        }

    }


    override fun onBackPressed() {
        if (businessInfoFragment != null) {
            if (businessInfoFragment?.isFullScreen!! && businessInfoFragment?.youTubePlayer != null) {
                businessInfoFragment?.youTubePlayer?.setFullscreen(false)
            } else {
                super.onBackPressed()
            }
        }
        super.onBackPressed()


    }

    fun redirecttoAddReview() {
        var intent = Intent(this@PromotionDetailsActivity, AddReviewActivity::class.java)
        intent.putExtra(AppConstants.BUSINESS_ID, businessDetails?.businessPlaceId)
        intent.putExtra(AppConstants.BUSINESS_LOGO, businessDetails?.icon)
        intent.putExtra(AppConstants.BUSINESS_NAME, businessDetails?.placeName)
        intent.putExtra(AppConstants.IS_EDITING, false)
        startActivityForResult(intent, AppConstants.ADD_REVIEW_REQUEST)
    }

    public override fun onSaveInstanceState(outState: Bundle) {

        outState.putInt(STATE_RESUME_WINDOW, mResumeWindow)
        outState.putLong(STATE_RESUME_POSITION, mResumePosition)
        outState.putBoolean(STATE_PLAYER_FULLSCREEN, mExoPlayerFullscreen)

        super.onSaveInstanceState(outState)
    }
    fun isCollapseToolBarRequire(flag:Boolean){
        if(tab_appbar!=null){
            if(flag){
                tab_appbar.setExpanded(false)
            }
        }
    }
    private fun enableScroll() {
        val params =
            tab_collapse_toolbar.getLayoutParams() as AppBarLayout.LayoutParams
        params.scrollFlags = (
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                        or AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED
                )
        tab_collapse_toolbar.setLayoutParams(params)
    }

    private fun disableScroll() {
        val params =
            tab_collapse_toolbar.getLayoutParams() as AppBarLayout.LayoutParams
        params.scrollFlags = 0
        tab_collapse_toolbar.setLayoutParams(params)
    }

    private fun callPromotionTrackApi(){
        if(businessDetails?.businessPlaceId!=null && businessDetails?.getPromotionList!=null && businessDetails?.getPromotionList!!.isNotEmpty()){
            val trackPromotion=TrackPromotion(businessDetails?.businessPlaceId!!,this@PromotionDetailsActivity,businessDetails?.getPromotionList!![0].promotionId,"View")
            trackPromotion.callPromotionTrackApi()
        }
    }
}

