package com.lansingareashoplocal.ui.promotions

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.TypedValue
import android.view.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseFragment
import com.lansingareashoplocal.databinding.FragmentViewImagesBinding
import com.lansingareashoplocal.ui.interfaces.CommonCallBack
import com.lansingareashoplocal.video_players.ExoPlayerActivity
import com.lansingareashoplocal.video_players.YoutubePlayerActivity
import kotlinx.android.synthetic.main.activity_update_profile.*
import java.io.File

class ViewImagesFragmnet(
    var url: String,
    var type: String,
    var videoType: String,
    var videoUrl: String,
    var commonCallBack: CommonCallBack
) :


    BaseFragment<FragmentViewImagesBinding>(), GestureDetector.OnDoubleTapListener {


    override fun onBackPressed(): Boolean {
        return false
    }

    override fun getLayoutId(): Int {

        return R.layout.fragment_view_images
    }

    override fun init() {
        initView()

        if (type.equals("Image")) {
            binding.ivPlayVideo.visibility = View.GONE
            binding.ivVideoThumnail.visibility = View.GONE
        } else {
            binding.ivGallery.visibility = View.GONE
            binding.ivPlayVideo.visibility = View.VISIBLE
        }
        binding.ivGallery.setOnTouchImageViewListener {
            if (binding.ivGallery.isZoomed) {
                commonCallBack.commonCallBack("", true, 0)
            } else {
                commonCallBack.commonCallBack("", false, 0)
            }

        }

        binding.ivPlayVideo.setOnClickListener {
            if (videoType.equals(AppConstants.YOUTUBE)) {
                var intent = Intent(activity!!, YoutubePlayerActivity::class.java)
                intent.putExtra(AppConstants.YOUTUBE_ID, videoUrl)
                startActivity(intent)

            } else if (videoType.equals(AppConstants.VIMEO)) {
                val intent = Intent(
                    activity!!,
                    com.lansingareashoplocal.video_players.VimeoPlayerActivity::class.java
                )
                intent.putExtra(AppConstants.VIMEO_ID, videoUrl)
                startActivity(intent)

            } else if (videoType.equals(AppConstants.CUSTOM_URL)) {
                var intent = Intent(activity!!, ExoPlayerActivity::class.java)
                intent.putExtra(AppConstants.CUSTOM_URL, videoUrl)
                startActivity(intent)

            }
        }
    }


    override fun onDoubleTap(e: MotionEvent?): Boolean {


        return true
    }

    override fun onDoubleTapEvent(e: MotionEvent?): Boolean {

        return true
    }

    override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {

        return true
    }


    private fun initView() {
        binding.ivGallery.setOnDoubleTapListener(this)
        if (type.equals("Image")) {
            binding.progressImageDetail.visibility = View.VISIBLE
            Glide.with(activity!!).asBitmap().load(url).apply(RequestOptions().override(1000, 500))
                .into(object : SimpleTarget<Bitmap>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap>?
                    ) {
                        binding.progressImageDetail.visibility = View.GONE
                        binding.ivGallery.setImageBitmap(resource)
                    }


                })


        } else {
            binding.progressImageDetail.visibility = View.VISIBLE
            Glide.with(activity!!).asBitmap().load(url).apply(RequestOptions().override(1000, 500))
                .into(object : SimpleTarget<Bitmap>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap>?
                    ) {
                        binding.ivVideoThumnail.setImageBitmap(resource)
                        binding.progressImageDetail.visibility = View.GONE
                    }


                })
        }


    }


}