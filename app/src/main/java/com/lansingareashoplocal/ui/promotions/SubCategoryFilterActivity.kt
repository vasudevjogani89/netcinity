package com.lansingareashoplocal.ui.promotions

import ApiClient
import ApiConstants
import CommonMethods
import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.databinding.ItemSelectedSubCategoriesBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.adapter.TagAdapter
import com.lansingareashoplocal.ui.interfaces.EditTextChangeCallBack
import com.lansingareashoplocal.ui.model.SubCategories
import com.lansingareashoplocal.utility.helper.others.CustomTextWatcher
import com.zhy.view.flowlayout.FlowLayout
import kotlinx.android.synthetic.main.activity_events_filter.*
import kotlinx.android.synthetic.main.activity_subcategory_filter.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import java.util.*
import kotlin.collections.HashMap

class SubCategoryFilterActivity : BaseActivity(), Filterable, EditTextChangeCallBack {

    var afterFilterSubCategoryList = mutableListOf<SubCategories>()
    var beforeFilterSubCategoryList = mutableListOf<SubCategories>()
    var selectedSubCategoryList = mutableListOf<SubCategories>()
    var tagAdapter: TagAdapter<SubCategories>? = null
    private var valueFilter: ValueFilter? = null
    var subCategoriesIdList: List<String>? = null
    var categoryID: String? = null
    var selectedSubCategoryAdapter: GenericAdapter<SubCategories, ItemSelectedSubCategoriesBinding>? =
        null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subcategory_filter)
        setSeletedSubCategoryAdapter()
        if (intent != null) {
            categoryID = intent.getStringExtra(ApiConstants.CATEGORY_ID)
            var activePromotionsOnly: String? = null
            activePromotionsOnly = intent.getStringExtra(AppConstants.ACTIVE_PROMOTIONS_ONLY)
            if (activePromotionsOnly != null && activePromotionsOnly.isNotEmpty()) {
                if (activePromotionsOnly.equals("1")) {
                    switchActiviePromotionOnly.isChecked = true
                }
            }
            var open_now: String? = null
            open_now = intent.getStringExtra(AppConstants.OPEN_NOW)
            if (open_now != null && open_now.isNotEmpty()) {
                if (open_now.equals("1")) {
                    switchOpenBusinessOnly.isChecked = true
                }
            }
            var subCategoryId:String?=null
            subCategoryId=intent.getStringExtra(AppConstants.SUB_CATEGORY_ID)
            if (subCategoryId!=null &&subCategoryId.isNotEmpty()) {
                val subCategoryIds = intent.getStringExtra(AppConstants.SUB_CATEGORY_ID)
                if(subCategoryIds!=null){
                    subCategoriesIdList = subCategoryIds.split(",").map { it.trim() }
                }



            }


        }
        callSubCategoryApi()
        setCategoryAdapter()
        clearText()
        searchSubCategory()
        etSubCategorySuggestion.addTextChangedListener(
            CustomTextWatcher(
                etSubCategorySuggestion,
                this
            )
        )

        switchSelectAll.setOnClickListener {

            if (switchSelectAll.isChecked) {
                afterFilterSubCategoryList.forEachIndexed { subCategoryPos, subCategories ->
                    var isSubCategorySelected = false
                    afterFilterSubCategoryList[subCategoryPos].isSelected = true
                    selectedSubCategoryList.forEachIndexed { seletedSubCategoryPos, selectedSubCategories ->
                        if (afterFilterSubCategoryList[subCategoryPos].sub_category_id.equals(
                                selectedSubCategories.sub_category_id
                            )
                        ) {
                            isSubCategorySelected = true
                        }
                    }

                    if (!isSubCategorySelected) {
                        selectedSubCategoryList.add(subCategories)
                    }

                }
                tagAdapter?.notifyDataChanged()
                selectedSubCategoryAdapter?.notifyDataSetChanged()
            } else {
                afterFilterSubCategoryList.forEachIndexed { subCategoryPos, subCategories ->
                    var isSubCategorySelected = false
                    var flag = -1
                    afterFilterSubCategoryList[subCategoryPos].isSelected = false
                    selectedSubCategoryList.forEachIndexed { seletedSubCategoryPos, selectedSubCategories ->
                        if (afterFilterSubCategoryList[subCategoryPos].sub_category_id.equals(
                                selectedSubCategories.sub_category_id
                            )
                        ) {
                            isSubCategorySelected = true
                            flag = seletedSubCategoryPos
                        }
                    }

                    if (isSubCategorySelected && flag != -1) {
                        selectedSubCategoryList.removeAt(flag)
                        checkSelecteSubCategoryIsEmpty()
                    }

                }
                tagAdapter?.notifyDataChanged()
                selectedSubCategoryAdapter?.notifyDataSetChanged()

            }


        }
        tvLeft.setOnClickListener {
            finish()
        }
        tvRight.setOnClickListener {
            switchActiviePromotionOnly.isChecked = false
            switchOpenBusinessOnly.isChecked = false
            selectedSubCategoryList.clear()
            afterFilterSubCategoryList.forEachIndexed { index, subCategories ->
                afterFilterSubCategoryList[index].isSelected = false
            }
            beforeFilterSubCategoryList.forEachIndexed { index, subCategories ->
                beforeFilterSubCategoryList[index].isSelected = false
            }
            tagAdapter?.notifyDataChanged()
            selectedSubCategoryAdapter?.notifyDataSetChanged()


        }
        btnSubCategoryApply.setOnClickListener {
            var selectCategories = ""
            val intent = Intent()
            if (switchActiviePromotionOnly.isChecked) {
                intent.putExtra(AppConstants.ACTIVE_PROMOTIONS_ONLY, "1")
            } else {
                intent.putExtra(AppConstants.ACTIVE_PROMOTIONS_ONLY, "0")
            }

            if (switchOpenBusinessOnly.isChecked) {
                intent.putExtra(AppConstants.OPEN_NOW, "1")
            } else {
                intent.putExtra(AppConstants.OPEN_NOW, "0")
            }


            beforeFilterSubCategoryList.forEachIndexed { index, subCategory ->
                if (subCategory.isSelected) {
                    selectCategories += ",${subCategory.sub_category_id}"
                }
            }
            if (!selectCategories.isEmpty()) {
                selectCategories = selectCategories.removePrefix(",")
                intent.putExtra(AppConstants.SUB_CATEGORY_ID, selectCategories)
            }
            setResult(Activity.RESULT_OK, intent)
            finish()


        }
        setToolBar()


    }


    private fun setCategoryAdapter() {
        tagAdapter = object : TagAdapter<SubCategories>(afterFilterSubCategoryList) {
            override fun setSelected(position: Int, eventCategory: SubCategories?): Boolean {
                return eventCategory?.isSelected!!
            }

            override fun getView(parent: FlowLayout?, position: Int, t: SubCategories?): View {
                val view = LayoutInflater.from(this@SubCategoryFilterActivity)
                    .inflate(R.layout.item_sub_category, tagViewSubCategory, false)
                val textview: AppCompatTextView = view!!.findViewById(R.id.tvseletedSubCategory)
                val rlsubCategorybg: RelativeLayout = view.findViewById(R.id.rlsubCategorybg)
                val ivAdd: ImageView = view.findViewById(R.id.ivAdd)
                textview.text = t?.sub_category
                if (t?.isSelected!!) {
                    rlsubCategorybg.setBackgroundResource(R.drawable.event_category_seleted_state)
                    textview.setTextColor(
                        ContextCompat.getColor(
                            this@SubCategoryFilterActivity,
                            R.color.colorWhite
                        )
                    )
                    ivAdd.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@SubCategoryFilterActivity,
                            R.drawable.ic_clear
                        )
                    )
                } else {
                    rlsubCategorybg.setBackgroundResource(R.drawable.corner_radius_white_bg)
                    textview.setTextColor(
                        ContextCompat.getColor(
                            this@SubCategoryFilterActivity,
                            R.color.colorBlack
                        )
                    )
                    ivAdd.setImageDrawable(
                        ContextCompat.getDrawable(
                            this@SubCategoryFilterActivity,
                            R.drawable.ic_add
                        )
                    )
                }

                return view
            }
        }
        tagViewSubCategory.adapter = this.tagAdapter
        tagViewSubCategory.setOnTagClickListener(com.lansingareashoplocal.ui.adapter.TagFlowLayout.OnTagClickListener { view, position, parent ->
            var isSubCategorySelected = false
            afterFilterSubCategoryList.get(position).isSelected =
                !afterFilterSubCategoryList.get(position).isSelected
            selectedSubCategoryList.forEachIndexed { index, subCategories ->
                if (afterFilterSubCategoryList[position].sub_category_id.equals(subCategories.sub_category_id)) {
                    isSubCategorySelected = true
                }
            }

            if (isSubCategorySelected) {
                var flag = -1
                selectedSubCategoryList.forEachIndexed { index, selectedSubCategories ->
                    if (afterFilterSubCategoryList[position].sub_category_id.equals(
                            selectedSubCategories.sub_category_id
                        )
                    ) {
                        flag = index
                    }

                }
                if (flag != -1) {
                    selectedSubCategoryList.removeAt(flag)
                    selectedSubCategoryAdapter?.notifyDataSetChanged()
                }

            } else {
                selectedSubCategoryList.add(afterFilterSubCategoryList.get(position))
                selectedSubCategoryAdapter?.setData(selectedSubCategoryList)
            }
            tagAdapter?.notifyDataChanged()
            checkSelecteSubCategoryIsEmpty()
            true
        })
    }


    fun callSubCategoryApi() {
        val inputparams = HashMap<String, String>()
        inputparams.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        if (categoryID != null) {
            inputparams.put(ApiConstants.CATEGORY_ID, categoryID!!)
        }
        val call = ApiClient.getRetrofitInterface()?.getSubCategory(inputparams)
        WSClient<Response<List<SubCategories>>>().request(
            this@SubCategoryFilterActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<SubCategories>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<SubCategories>>
                ) {
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        llSubcategoryDetails.visibility = View.VISIBLE
                        afterFilterSubCategoryList = mResponse.data as MutableList<SubCategories>
                        beforeFilterSubCategoryList = mResponse.data as MutableList<SubCategories>
                        afterFilterSubCategoryList.forEachIndexed { index, subCategories ->
                            subCategoriesIdList?.forEachIndexed { i, s ->
                                if (subCategories.sub_category_id.equals(s)) {
                                    afterFilterSubCategoryList[index].isSelected = true
                                    selectedSubCategoryList.add(subCategories)
                                }

                            }
                        }
                        selectedSubCategoryAdapter?.setData(selectedSubCategoryList)


                        tagAdapter?.setData(afterFilterSubCategoryList)
                    } else {
                        llSubcategoryDetails.visibility = View.GONE
                    }
                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert(message) { okButton { } }.show()
                }

            })

    }

    override fun getFilter(): Filter {
        if (valueFilter == null) {
            valueFilter = ValueFilter()
        }
        return valueFilter as ValueFilter
    }

    public inner class ValueFilter : Filter() {
        @RequiresApi(Build.VERSION_CODES.N)
        override fun performFiltering(constraint: CharSequence): FilterResults {
            val results = FilterResults()
            if (constraint != null && constraint.length > 0) {
                val filterList = mutableListOf<SubCategories>()
                beforeFilterSubCategoryList.forEachIndexed { index, subCategories ->
                    if (subCategories.sub_category.toUpperCase(Locale.ROOT)
                            .contains(constraint.toString().toUpperCase(Locale.ROOT))
                    ) {
                        filterList.add(subCategories)
                    }
                }
                results.count = filterList.size
                results.values = filterList
            } else {
                results.count = beforeFilterSubCategoryList.size
                results.values = beforeFilterSubCategoryList
            }
            return results
        }

        @RequiresApi(Build.VERSION_CODES.N)
        override fun publishResults(
            constraint: CharSequence,
            results: FilterResults
        ) {

            afterFilterSubCategoryList = results.values as MutableList<SubCategories>
            tagAdapter?.setData(afterFilterSubCategoryList)
            if (afterFilterSubCategoryList.isEmpty()) {

            } else {

            }
            tagAdapter?.notifyDataChanged()

        }
    }

    override fun afterTextChanged(editText: AppCompatEditText) {
        if (editText.text != null) {
            filter.filter(editText.text!!.trim())
            if (editText.text!!.isNotEmpty()) {
                filter.filter(editText.text!!.trim())
                Glide.with(this@SubCategoryFilterActivity).load(R.drawable.cancel)
                    .into(ivSubcategorySearch)
            } else {
                Glide.with(this@SubCategoryFilterActivity).load(R.drawable.ic_search)
                    .into(ivSubcategorySearch)
            }
        }
    }

    private fun searchSubCategory() {
        etSubCategorySuggestion.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // isSearchDone = true
                    if (v?.text != null) {
                        filter.filter(v.text)
                        CommonMethods.hideKeypad(this@SubCategoryFilterActivity,
                            etSubCategorySuggestion
                        )
                    }

                    return true
                }
                return false
            }
        })
    }

    private fun clearText() {
        ivSubcategorySearch.setOnClickListener {
            if (etSubCategorySuggestion.text!!.isNotEmpty()) {
                CommonMethods.hideKeypad(
                    this@SubCategoryFilterActivity,
                    etSubCategorySuggestion
                )
                etSubCategorySuggestion.setText("")
                filter.filter("")
                etSubCategorySuggestion.clearFocus()
            } else {

            }
            //tvNoFAQData.visibility = View.GONE
        }
    }

    fun setSeletedSubCategoryAdapter() {
        val linearLayoutManager =
            LinearLayoutManager(this@SubCategoryFilterActivity, RecyclerView.HORIZONTAL, false)
        rvsubCategories.layoutManager = linearLayoutManager
        selectedSubCategoryAdapter =
            object : GenericAdapter<SubCategories, ItemSelectedSubCategoriesBinding>(
                selectedSubCategoryList
            ) {
                override fun getLayoutId(): Int {
                    return R.layout.item_selected_sub_categories
                }

                override fun onBindData(
                    model: SubCategories?,
                    position: Int,
                    dataBinding: ItemSelectedSubCategoriesBinding?
                ) {
                    dataBinding?.tvseletedSubCategory?.text = model?.sub_category
                    checkSelecteSubCategoryIsEmpty()
                    dataBinding?.root?.setOnClickListener {
                        unselectTheSubCategory(selectedSubCategoryList[position].sub_category_id)
                        selectedSubCategoryList.removeAt(position)
                        checkSelecteSubCategoryIsEmpty()
                        notifyDataSetChanged()
                    }

                }

            }
        rvsubCategories.adapter = selectedSubCategoryAdapter
    }

    private fun unselectTheSubCategory(subCategoryId: String) {
        beforeFilterSubCategoryList.forEachIndexed { index, subCategories ->
            if (subCategoryId.equals(subCategories.sub_category_id)) {
                beforeFilterSubCategoryList[index].isSelected =
                    !beforeFilterSubCategoryList[index].isSelected
                tagAdapter?.notifyDataChanged()
            }
        }


    }

    fun checkSelecteSubCategoryIsEmpty() {
        if (selectedSubCategoryList.isEmpty()) {
            tvseletedSubCategoryNoData.visibility = View.VISIBLE
        } else {
            tvseletedSubCategoryNoData.visibility = View.GONE
        }
    }
    private fun setToolBar() {
        tvRight.text = getString(R.string.reset)
        tvRight.visibility=View.VISIBLE
        tvTitle.text = getString(R.string.filter)
        tvLeft.text=""
        tvLeft.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_close_popup,0,0,0)
    }


}