package com.lansingareashoplocal.ui.promotions

import android.os.Build
import android.text.Layout
import android.view.Gravity
import android.view.View
import androidx.annotation.RequiresApi
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseFragment
import com.lansingareashoplocal.databinding.FragmentMeetTheOwnerBinding
import com.lansingareashoplocal.ui.model.BusinessDetails
import kotlinx.android.synthetic.main.activity_promotion_details.*
import kotlinx.android.synthetic.main.fragment_meet_the_owner.*

class MeetTheOwnerFragment : BaseFragment<FragmentMeetTheOwnerBinding>() {

    var businessDetails: BusinessDetails? = null
    override fun onBackPressed(): Boolean {
        return false
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_meet_the_owner
    }

    override fun init() {
        businessDetails=arguments?.getParcelable(AppConstants.BUSINESS_MODEL)
        setAboutUs()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setAboutUs() {
        Glide.with(activity!!).load(businessDetails?.owner_profile_image)
            .into(binding.ivOwnerBusinessDetails)
        if (businessDetails?.owner_profile_image?.isEmpty()!!) {
            binding.cvOwner.visibility = View.GONE
            binding.tvOwner.gravity = Gravity.LEFT
        }
        binding.tvOwnerDetails.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
        binding.tvOwner.text = businessDetails?.owner_name
        binding.tvOwnerDetails.text = businessDetails?.owner_about_us
        binding.tvAboutUs.text="${getString(R.string.meet_the)} ${businessDetails?.designation}"


    }
}