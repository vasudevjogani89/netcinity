package com.lansingareashoplocal.ui.promotions

import AppConstants
import CommonMethods
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.ct7ct7ct7.androidvimeoplayer.model.PlayerState
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerActivity
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseFragment
import com.lansingareashoplocal.databinding.*
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.adapter.TagAdapter
import com.lansingareashoplocal.ui.model.*
import com.lansingareashoplocal.ui.report.AddReportActivity
import com.lansingareashoplocal.utility.helper.TimeUtils
import com.lansingareashoplocal.utility.helper.YouTubeHelper
import com.lansingareashoplocal.utility.helper.setDifferentColor
import com.lansingareashoplocal.video_players.ExoPlayerActivity
import com.zhy.view.flowlayout.FlowLayout
import kotlinx.android.synthetic.main.activity_subcategory_filter.*
import kotlinx.android.synthetic.main.exo_playback_control_view.view.*
import kotlinx.android.synthetic.main.exo_simple_player_view.view.*
import kotlinx.android.synthetic.main.fragment_business_info.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class BusinessInfoFragment : BaseFragment<FragmentBusinessInfoBinding>(),
    YouTubePlayer.OnInitializedListener {
    internal var exoFullscreen = false

    var youTubePlayer: YouTubePlayer? = null
    var isFullScreen = false
    var cummunityPhotoList: MutableList<GetPromotionImage> = ArrayList()
    var otherLinkList = mutableListOf<GetOtherLink>()
    var businessHoursList = mutableListOf<GetBusinessHour>()
    var businessSocialMediaList = mutableListOf<GetSocialLink>()
    var youtubeVw: YouTubePlayerSupportFragment? = null
    var cummunityPhotosAdapter: GenericAdapter<GetPromotionImage, ItemCommunityPhotosBinding>? =
        null
    var businessOtherLinksAdapter: GenericAdapter<GetOtherLink, ItemOtherLinksBinding>? = null

    var businessHoursAdapter: GenericAdapter<GetBusinessHour, ItemBusinessTimeBinding>? = null

    var businessSocialMediaAdapter: GenericAdapter<GetSocialLink, ItemSocialMediaBinding>? = null

    private var simpleExoPlayer: SimpleExoPlayer? = null
    private lateinit var mediaDataSourceFactory: DataSource.Factory
    lateinit var mFullScreenDialog: Dialog
    private var mExoPlayerFullscreen = false
    lateinit var fullscreenButton: ImageView
    var subCategotyAdapter: TagAdapter<String>? = null
    var subCategoryList: List<String>? = null


    //exo player


    var businessDetails: BusinessDetails? = null
    override fun onBackPressed(): Boolean {
        return false
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_business_info
    }

    override fun init() {
        val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        businessDetails = arguments?.getParcelable(AppConstants.BUSINESS_MODEL)
        youtubeVw =
            childFragmentManager.findFragmentById(R.id.youtubeVw) as YouTubePlayerSupportFragment?

        binding.tvporomotionMore.setOnClickListener {
            if (binding.tvporomotionMore.text.equals("More")) {
                binding.tvPromotionDetails.setMaxLines(Integer.MAX_VALUE)
                binding.tvPromotionDetails.setText(binding.tvPromotionDetails.mainText)
                binding.tvporomotionMore.text = "Less"

            } else {
                binding.tvPromotionDetails.setMaxLines(4)
                binding.tvporomotionMore.text = "More"
            }
        }
        binding.tvCommunityName.setOnClickListener {

        }
        binding.btnQrCode.setOnClickListener {

            if (businessDetails?.getPromotionList?.isNotEmpty()!!) {
                DialogQRCode(activity!!).showQrCode(businessDetails?.getPromotionList?.get(0)?.qrCode!!)
            }
        }
        setBusinessOpenStatus()
        setCommunityPhotos()
        setBusinessHours()
        setOtherLinks()
        setSocialMedia()
        binding.ivShare.setOnClickListener {
            /*val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, businessDetails?.placeName)
                type = "text/plain"
            }
            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)*/
            shareImageFromURI(businessDetails?.bannerImage, businessDetails?.placeName!!)


        }
        binding.llAddress.setOnClickListener {

            var geoUri: String? = null
            if (CommonMethods.getDataFromSharePreference(
                    activity!!,
                    AppConstants.LATITUDE
                ).isNotEmpty() && CommonMethods.getDataFromSharePreference(
                    activity!!,
                    AppConstants.LONGITUDE
                ).isNotEmpty()
            ) {
                geoUri =
                    "http://maps.google.com/maps?saddr=${CommonMethods.getDataFromSharePreference(
                        activity!!,
                        AppConstants.LATITUDE
                    )},${CommonMethods.getDataFromSharePreference(
                        activity!!,
                        AppConstants.LONGITUDE
                    )}&daddr=${businessDetails?.latitude},${businessDetails?.longitude}"

            } else {
                geoUri =
                    "http://maps.google.com/maps?q=loc:${businessDetails?.latitude},${businessDetails?.longitude} (${businessDetails?.placeName})"
            }

            if (businessDetails?.longitude?.isNotEmpty()!! && businessDetails?.latitude?.isNotEmpty()!!) {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
                try {
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent)
                } catch (e: Exception) {
                    Toast.makeText(
                        activity,
                        "Invalid link",
                        Toast.LENGTH_SHORT
                    ).show()
                    e.printStackTrace()
                }
            }

        }

        binding.llWebLink.setOnClickListener {
            CommonMethods.openCustomtab(activity!!, businessDetails?.website!!)
        }
        binding.llMail.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"))
            intent.putExtra(Intent.EXTRA_EMAIL, businessDetails?.email)
            intent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
            intent.putExtra(Intent.EXTRA_TEXT, "I'm email body.")
            try {
                startActivity(intent)
            } catch (e: Exception) {
                Toast.makeText(activity!!, "Invalid link", Toast.LENGTH_SHORT)
                    .show()
                e.printStackTrace()
            }


        }

        binding.llPhone.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)

            if (businessDetails?.phone?.isNotEmpty()!!) {
                intent.data = Uri.parse("tel:${businessDetails?.phone}")
                try {
                    startActivity(intent)
                } catch (e: Exception) {
                    Toast.makeText(
                        activity!!,
                        "Invalid link",
                        Toast.LENGTH_SHORT
                    ).show()
                    e.printStackTrace()
                }
            }

        }

        binding.tvReport.setOnClickListener {
            val intent = Intent(activity!!, AddReportActivity::class.java)
            intent.putExtra(AppConstants.BUSINESS_ID, businessDetails?.businessPlaceId)
            intent.putExtra(AppConstants.BUSINESS_LOGO, businessDetails?.icon)
            intent.putExtra(AppConstants.BUSINESS_NAME, businessDetails?.placeName)
            startActivity(intent)

        }
        setData()
        setReport()
        setSubCategoryAdapter()
    }

    private fun setSubCategory() {

    }

    private fun setBusinessOpenStatus() {
        binding.tvBusinessStatus.text = businessDetails?.currentStatus
        if (businessDetails?.currentStatus.equals("Closed")) {
            Glide.with(activity!!).load(R.drawable.gray_circle)
                .into(binding.ivBusinessStatus)
        } else {
            Glide.with(activity!!).load(R.drawable.green_circle)
                .into(binding.ivBusinessStatus)
        }
    }

    private fun setReport() {
        binding.tvReport.setDifferentColor(
            getString(R.string.see_something_wrong),
            " ${getString(R.string.report_an_error)}",
            activity!!,
            binding.tvReport,
            Typeface.createFromAsset(context!!.assets, getString(R.string.museosans_500)),
            if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) ContextCompat.getColor(requireActivity(), R.color.colorDarkBlue) else ContextCompat.getColor(requireActivity(), R.color.colorPrimary))
    }

    private fun setData() {
        if (businessDetails?.getPromotionList?.isNotEmpty()!!) {

            binding.btnTodayOffer.text = businessDetails?.getPromotionList?.get(0)?.title
            binding.tvTodayOfferSubTitle.text = businessDetails?.getPromotionList?.get(0)?.subTitle
            if (businessDetails?.getPromotionList?.get(0)?.set_expiration.equals("Yes")) {
                binding.tvEventDate.visibility = View.VISIBLE
                binding.tvEventDate.text =
                    "Promotion Expires - ${TimeUtils.convertDateWithTimeForShowingApp(
                        businessDetails?.getPromotionList?.get(0)?.peroidEnd
                    )}"
            }
            if (businessDetails?.getPromotionList?.get(0)?.qrCode?.isNotEmpty()!!) {
                binding.btnQrCode.visibility = View.VISIBLE
            } else {
                binding.btnQrCode.visibility = View.GONE
            }
            if (businessDetails?.getPromotionList?.get(0)?.getPromotionImages != null) {
                cummunityPhotoList =
                    businessDetails?.getPromotionList?.get(0)?.getPromotionImages as MutableList<GetPromotionImage>
                cummunityPhotosAdapter?.setData(cummunityPhotoList)
            }
            if (businessDetails?.getPromotionList?.get(0)?.title?.isEmpty()!!) {
                binding.llBusinessTitle.visibility = View.GONE
            } else {
                binding.llBusinessTitle.visibility = View.VISIBLE
                //  var pulse = AnimationUtils.loadAnimation(activity!!, R.anim.pluse);
                //  binding.llBusinessTitle.startAnimation(pulse);
            }


        } else {

            binding.btnTodayOffer.visibility = View.GONE
            binding.tvTodayOfferSubTitle.visibility = View.GONE
            binding.tvPhoto.visibility = View.GONE
            binding.btnQrCode.visibility = View.GONE
            binding.llBusinessTitle.visibility = View.GONE

        }



        if (businessDetails?.getOtherLink?.isNotEmpty()!!) {
            otherLinkList = businessDetails?.getOtherLink as MutableList<GetOtherLink>
            businessOtherLinksAdapter?.setData(otherLinkList)

        }
        if (businessDetails?.getSocialLink?.isNotEmpty()!!) {
            businessSocialMediaList =
                businessDetails?.getSocialLink as MutableList<GetSocialLink>
            businessSocialMediaAdapter?.setData(businessSocialMediaList)
        }

        if (businessDetails?.getBusinessHours?.isNotEmpty()!!) {
            binding.tvAvalibleTimings.visibility = View.VISIBLE
            businessHoursList = businessDetails?.getBusinessHours as MutableList<GetBusinessHour>
            businessHoursAdapter?.setData(businessHoursList)
        } else {
            binding.tvAvalibleTimings.visibility = View.GONE
        }

        binding.tvCommunityName.text = businessDetails?.placeName
        binding.tvCommunitySubTitle.text = businessDetails?.tagLine
        binding.tvAddress.text = businessDetails?.address
        binding.tvPhoneNumber.text = businessDetails?.phone
        binding.tvLink.text = businessDetails?.website
        binding.tvMail.text = businessDetails?.email


        Handler().apply {
            val runnable = object : Runnable {
                override fun run() {


                    if (binding.tvPromotionDetails.lineCount > 4) {
                        binding.tvPromotionDetails.setShowingLine(4)
                        binding.tvporomotionMore.visibility = View.VISIBLE
                    } else {
                        binding.tvporomotionMore.visibility = View.GONE
                    }
                }
            }
            postDelayed(runnable, 100)
        }
        binding.tvPromotionDetails.text = businessDetails?.description
        if (businessDetails?.tagLine?.isEmpty()!!) {
            binding.tvCommunitySubTitle.visibility = View.GONE
        }

        if (businessDetails?.videoUrl?.isNotEmpty()!!) {
            if (CommonMethods.isYoutubeUrl(businessDetails?.videoUrl!!)) {
                binding.mainMediaFrame.visibility = View.GONE
                binding.vimeoPlayer.visibility = View.GONE
                setupYoutube()

            } else {
                if (businessDetails?.videoUrl!!.contains("vimeo", true)) {
                    binding.mainMediaFrame.visibility = View.GONE
                    binding.vimeoPlayer.visibility = View.VISIBLE
                    val vimeoId = CommonMethods.getSearchkey(businessDetails?.videoUrl!!)
                    if (vimeoId != 0) {
                        binding.vimeoPlayer.visibility = View.VISIBLE
                        activity!!.lifecycle.addObserver(binding.vimeoPlayer)
                        binding.vimeoPlayer.initialize(
                            true,
                            CommonMethods.getSearchkey(businessDetails?.videoUrl!!)
                        )
                        binding.vimeoPlayer.setFullscreenVisibility(true)
                        binding.vimeoPlayer.setFullscreenClickListener {
                            var requestOrientation = VimeoPlayerActivity.REQUEST_ORIENTATION_AUTO
                            activity!!.startActivityForResult(
                                VimeoPlayerActivity.createIntent(
                                    activity!!,
                                    requestOrientation,
                                    vimeoPlayer
                                ),
                                100
                            )
                        }
                    } else {
                        binding.cvPlayer.visibility = View.GONE
                    }

                } else {
                    initFullscreenDialog()
                    binding.mainMediaFrame.visibility = View.VISIBLE
                    binding.vimeoPlayer.visibility = View.GONE
                    simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(activity!!)
                    mediaDataSourceFactory = DefaultDataSourceFactory(
                        activity!!,
                        Util.getUserAgent(activity!!, getString(R.string.app_name))
                    )
                    val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory)
                        .createMediaSource(Uri.parse(businessDetails?.videoUrl))
                    simpleExoPlayer?.prepare(mediaSource, false, false)
                    simpleExoPlayer?.playWhenReady = false
                    binding.exoplayer.setShutterBackgroundColor(Color.TRANSPARENT)
                    binding.exoplayer.player = simpleExoPlayer
                    binding.exoplayer.requestFocus()
                    simpleExoPlayer?.addListener(object : Player.EventListener {
                        override fun onTimelineChanged(
                            timeline: Timeline?,
                            manifest: Any?,
                            reason: Int
                        ) {
                            Log.e("Timeline", "")
                        }

                        override fun onTracksChanged(
                            trackGroups: TrackGroupArray?,
                            trackSelections: TrackSelectionArray?
                        ) {
                            Log.e("TrackGroupArray", "")
                        }

                        override fun onLoadingChanged(isLoading: Boolean) {
                            Log.e("isLoading", "")
                        }

                        override fun onPlayerStateChanged(
                            playWhenReady: Boolean,
                            playbackState: Int
                        ) {
                            when (playbackState) {
                                Player.STATE_ENDED -> {

                                }
                                Player.STATE_READY -> {
                                    binding.progressBarExoPlayer?.setVisibility(View.INVISIBLE)
                                }
                                Player.STATE_BUFFERING -> {
                                    binding.progressBarExoPlayer?.setVisibility(View.VISIBLE)
                                }
                                Player.STATE_IDLE -> {
                                    Log.d("STATE_IDLE", "STATE_IDLE")
                                }
                            }
                        }

                        override fun onRepeatModeChanged(repeatMode: Int) {
                            Log.e("", "")
                        }

                        override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {
                            Log.e("", "")
                        }

                        override fun onPlayerError(error: ExoPlaybackException?) {
                            Log.e("", "")
                        }

                        override fun onPositionDiscontinuity(reason: Int) {
                            Log.e("", "")
                        }

                        override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {
                            Log.e("", "")
                        }

                        override fun onSeekProcessed() {
                            Log.e("", "")
                        }
                    })

                    fullscreenButton = binding.exoplayer.exo_fullscreen_icon
                    fullscreenButton.setOnClickListener {
                        if (exoFullscreen) {
                            closeFullscreenDialog()
                            exoFullscreen = false
                        } else {
                            openFullscreenDialog()
                            exoFullscreen = true

                        }
                    }

                }


            }
        } else {
            binding.cvPlayer.visibility = View.GONE
        }
        if (businessDetails?.videoUrl?.isEmpty()!! && businessDetails?.getPromotionList?.isEmpty()!!) {
            binding.ivLine.visibility = View.GONE
        }
        if (businessDetails?.phone?.isEmpty()!!) {
            binding.llPhone.visibility = View.GONE
        }
        if (businessDetails?.address?.isEmpty()!!) {
            binding.llAddress.visibility = View.GONE
        }
        if (businessDetails?.website?.isEmpty()!!) {
            binding.llWebLink.visibility = View.GONE
        }
        if (businessDetails?.email?.isEmpty()!!) {
            binding.llMail.visibility = View.GONE
        }
    }


    private fun setSocialMedia() {
        val linearLayoutManager =
            LinearLayoutManager(activity!!, RecyclerView.HORIZONTAL, false)
        binding.rvSocialMedia.layoutManager = linearLayoutManager
        businessSocialMediaAdapter = object :
            GenericAdapter<GetSocialLink, ItemSocialMediaBinding>(businessSocialMediaList) {
            override fun getLayoutId(): Int {
                return R.layout.item_social_media
            }

            override fun onBindData(
                model: GetSocialLink?,
                position: Int,
                dataBinding: ItemSocialMediaBinding?
            ) {
                if (model?.key.equals(AppConstants.YOUTUBE)) {
                    Glide.with(activity!!).load(R.drawable.ic_youtube).into(
                        dataBinding?.ivSocial!!
                    )
                } else if (model?.key.equals(AppConstants.INSTAGRAM)) {
                    Glide.with(activity!!).load(R.drawable.ic_insta).into(
                        dataBinding?.ivSocial!!
                    )
                } else if (model?.key.equals(AppConstants.LINKEDIN)) {
                    Glide.with(activity!!).load(R.drawable.ic_in).into(
                        dataBinding?.ivSocial!!
                    )
                } else if (model?.key.equals(AppConstants.TWITTER)) {
                    Glide.with(activity!!).load(R.drawable.ic_twitter).into(
                        dataBinding?.ivSocial!!
                    )
                } else if (model?.key.equals(AppConstants.FACEBOOK)) {
                    Glide.with(activity!!).load(R.drawable.ic_fb).into(
                        dataBinding?.ivSocial!!
                    )
                } else if (model?.key.equals(AppConstants.YELP)) {
                    Glide.with(activity!!).load(R.drawable.ic_yelp).into(
                        dataBinding?.ivSocial!!
                    )

                } else if (model?.key.equals(AppConstants.PINTREST)) {
                    Glide.with(activity!!).load(R.drawable.ic_pinterest).into(
                        dataBinding?.ivSocial!!
                    )
                }

                dataBinding?.root?.setOnClickListener {


                    CommonMethods.openCustomtab(activity!!, model!!.value)


                }
            }

        }
        binding.rvSocialMedia.adapter = businessSocialMediaAdapter
    }

    private fun setBusinessHours() {
        businessHoursAdapter = object :
            GenericAdapter<GetBusinessHour, ItemBusinessTimeBinding>(businessHoursList) {
            override fun getLayoutId(): Int {
                return R.layout.item_business_time
            }

            override fun onBindData(
                model: GetBusinessHour?,
                position: Int,
                dataBinding: ItemBusinessTimeBinding?
            ) {

                dataBinding?.tvDay?.text = model?.dayDisplay
                if (model?.open24x7.equals("No")) {
                    if (model?.isClosed.equals("Yes")) {
                        dataBinding?.tvOPenTime?.text = getString(R.string.closed)
                    } else {
                        dataBinding?.tvOPenTime?.text = "${model?.openTime} to ${model?.closeTime}"
                    }
                } else {
                    dataBinding?.tvOPenTime?.text = getString(R.string.time_24_7)
                }
            }

        }
        binding.rvBusinessHours.adapter = businessHoursAdapter

    }

    private fun setOtherLinks() {
        businessOtherLinksAdapter =
            object : GenericAdapter<GetOtherLink, ItemOtherLinksBinding>(otherLinkList) {
                override fun getLayoutId(): Int {
                    return R.layout.item_other_links
                }

                override fun onBindData(
                    model: GetOtherLink?,
                    position: Int,
                    dataBinding: ItemOtherLinksBinding?
                ) {
                    dataBinding?.btnOtherLink?.text = model?.otherKey
                    dataBinding?.btnOtherLink?.setOnClickListener {
                        if (model?.otherValue?.isNotEmpty()!!) {
                            CommonMethods.openCustomtab(
                                activity!!,
                                model.otherValue
                            )
                        }
                    }
                }
            }

        binding.rvOtherLinks.adapter = businessOtherLinksAdapter


    }

    private fun setCommunityPhotos() {

        val linearLayoutManager =
            LinearLayoutManager(activity!!, RecyclerView.HORIZONTAL, false)
        binding.rvCommunityPhotos.layoutManager = linearLayoutManager
        cummunityPhotosAdapter = object :
            GenericAdapter<GetPromotionImage, ItemCommunityPhotosBinding>(cummunityPhotoList) {
            override fun getLayoutId(): Int {
                return R.layout.item_community_photos
            }

            override fun onBindData(
                model: GetPromotionImage?,
                position: Int,
                dataBinding: ItemCommunityPhotosBinding?
            ) {
                Glide.with(activity!!).asBitmap().load(model?.image)
                    .apply(RequestOptions().override(500, 500))
                    .into(object : SimpleTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            dataBinding?.progressPhotosThumnail?.visibility = View.GONE
                            dataBinding?.ivCummnuityPhoto?.setImageBitmap(resource)
                        }


                    })

                if (model?.media_type.equals("Image")) {
                    dataBinding?.ivPlay?.visibility = View.GONE
                } else {
                    dataBinding?.ivPlay?.visibility = View.VISIBLE
                }


                dataBinding?.ivCummnuityPhoto?.setOnClickListener {
                    if (cummunityPhotoList.isNotEmpty()) {
                        if (!model?.media_type.equals("Image")) {
                            var intent = Intent(activity!!, ExoPlayerActivity::class.java)
                            intent.putExtra(AppConstants.CUSTOM_URL, model?.video_url)
                            startActivity(intent)
                            /*if (CommonMethods.isYoutubeUrl(model?.video_url!!)) {
                                val intent = Intent(activity!!, YoutubePlayerActivity::class.java)
                                intent.putExtra(AppConstants.YOUTUBE_ID, model.video_url)
                                startActivity(intent)
                            } else if (model.video_url.contains("vimeo", true)) {
                                val intent = Intent(activity!!, com.lansingareashoplocal.video_players.VimeoPlayerActivity::class.java)
                                val vimeoId = CommonMethods.getSearchkey(model.video_url)
                                if(vimeoId!=0){
                                    intent.putExtra(AppConstants.VIMEO_ID, vimeoId)
                                    startActivity(intent)
                                }
                            } else  {
                                var intent =Intent(activity!!, MainActivity::class.java)
                                intent.putExtra(AppConstants.CUSTOM_URL, model.video_url)
                                startActivity(intent)


                            }*/
                        } else {
                            val intent =
                                Intent(activity!!, ViewImagesActivity::class.java)
                            intent.putExtra(AppConstants.POSITION, position)
                            val imageDetailsList: MutableList<ImageDetails> = ArrayList()
                            cummunityPhotoList.forEachIndexed { index, getPromotionImage ->
                                val imageDetail = ImageDetails()
                                imageDetail.imageUrl = getPromotionImage.image_org
                                imageDetail.type = getPromotionImage.media_type
                                if (!getPromotionImage?.media_type.equals("Image", true)) {
                                    /*if (CommonMethods.isYoutubeUrl(getPromotionImage?.video_url!!)) {
                                        imageDetail.videoType = AppConstants.YOUTUBE
                                        imageDetail.videoUrl=getPromotionImage.video_url
                                    } else if (getPromotionImage?.video_url?.contains("vimeo", true)!!) {
                                        imageDetail.videoType = AppConstants.VIMEO
                                        imageDetail.videoUrl=getPromotionImage.video_url
                                    } else {
                                        imageDetail.videoType = AppConstants.CUSTOM_URL
                                        imageDetail.videoUrl=getPromotionImage.video_url
                                    }*/
                                    imageDetail.videoType = AppConstants.CUSTOM_URL
                                    imageDetail.videoUrl = getPromotionImage.video_url
                                }
                                imageDetailsList.add(imageDetail)
                            }
                            intent.putParcelableArrayListExtra(
                                AppConstants.SHARE_KEY,
                                imageDetailsList as java.util.ArrayList<out ImageDetails>
                            )
                            intent.putExtra(AppConstants.TOOL_BAR_IS_REQUIRE, false)
                            startActivity(intent)
                        }

                    }

                }
            }

        }
        binding.rvCommunityPhotos.adapter = cummunityPhotosAdapter

    }

    fun setupYoutube() {
        youtubeVw?.initialize(AppConstants.YOUTUBE_KEY, this)
    }

    override fun onInitializationSuccess(
        p0: YouTubePlayer.Provider?,
        youTubePlayer: YouTubePlayer?,
        p2: Boolean
    ) {

        this.youTubePlayer = youTubePlayer
        val youtube_id = YouTubeHelper().getYoutubeId(businessDetails?.videoUrl)
        if (youtube_id.isNotEmpty()) {
            youTubePlayer?.cueVideo(youtube_id)
            this.youTubePlayer?.setOnFullscreenListener(YouTubePlayer.OnFullscreenListener { b ->
                isFullScreen = b
            })
        }
        youTubePlayer?.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT)


    }

    override fun onInitializationFailure(
        p0: YouTubePlayer.Provider?,
        p1: YouTubeInitializationResult?
    ) {
    }


    fun shareImageFromURI(url: String?, postText: String) {
        Glide.with(activity!!).asBitmap().load(businessDetails?.bannerImage)
            .into(object : SimpleTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    var totalPromotionText = ""
                    var totalBusinessDetails = ""
                    val intent = Intent(Intent.ACTION_SEND)
                    intent.type = "image/*"
                    intent.action = Intent.ACTION_SEND

                    if (businessDetails?.placeName?.isNotEmpty()!!) {
                        totalBusinessDetails = "${businessDetails?.placeName}\n"
                    }
                    if (businessDetails?.addressDetail?.isNotEmpty()!!) {
                        totalBusinessDetails =
                            "${totalBusinessDetails}${businessDetails?.address}\n"
                    }

                    if (businessDetails?.website?.isNotEmpty()!!) {
                        totalBusinessDetails =
                            "${totalBusinessDetails}${businessDetails?.website}\n"
                    }


                    if (businessDetails?.getPromotionList?.isNotEmpty()!!) {
                        if (businessDetails?.getPromotionList?.get(0)?.title?.isNotEmpty()!!) {
                            totalPromotionText = businessDetails?.getPromotionList?.get(0)?.title!!
                        }

                        if (businessDetails?.getPromotionList?.get(0)?.priceDiscount?.isNotEmpty()!!) {
                            totalPromotionText =
                                "${totalPromotionText} Use Code ${businessDetails?.getPromotionList?.get(
                                    0
                                )?.priceDiscount}\n\n"
                        } else {
                            totalPromotionText = "${totalPromotionText}\n\n"
                        }

                    }

                    intent.putExtra(
                        Intent.EXTRA_TEXT,
                        "Check out this listing I found on the ${getString(R.string.app_name_full_name)} community APP!! ${CommonMethods.playStoreLink}\n${totalPromotionText}${totalBusinessDetails}Download ${getString(
                            R.string.app_name_full_name
                        )} for all the details of this business!! ${CommonMethods.playStoreLink}"
                    )


                    intent.putExtra(Intent.EXTRA_STREAM, getBitmapFromView(resource))
                    startActivity(Intent.createChooser(intent, "Share Image"))
                }


            })

    }


    fun getBitmapFromView(bmp: Bitmap?): Uri? {
        var bmpUri: Uri? = null
        try {
            val file =
                File(activity!!.externalCacheDir, System.currentTimeMillis().toString() + ".jpg")

            val out = FileOutputStream(file)
            bmp?.compress(Bitmap.CompressFormat.JPEG, 90, out)
            out.close()
            bmpUri = Uri.fromFile(file)

        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 100) {
            var playAt = data!!.getFloatExtra(VimeoPlayerActivity.RESULT_STATE_VIDEO_PLAY_AT, 0f)
            vimeoPlayer.seekTo(playAt)
            var playerState =
                PlayerState.valueOf(data!!.getStringExtra(VimeoPlayerActivity.RESULT_STATE_PLAYER_STATE))
            when (playerState) {
                PlayerState.PLAYING -> vimeoPlayer.play()
                PlayerState.PAUSED -> vimeoPlayer.pause()
                PlayerState.UNKNOWN -> vimeoPlayer.pause()
            }
        }
    }

    private fun initFullscreenDialog() {

        mFullScreenDialog =
            object : Dialog(activity!!, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
                override fun onBackPressed() {
                    if (mExoPlayerFullscreen)
                        closeFullscreenDialog()

                }
            }
    }

    private fun openFullscreenDialog() {
        activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        (binding.exoplayer?.getParent() as ViewGroup).removeView(binding.exoplayer)
        mFullScreenDialog?.addContentView(
            binding.exoplayer!!,
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
        fullscreenButton?.setImageDrawable(
            ContextCompat.getDrawable(
                activity!!,
                R.drawable.ic_fullscreen_skrink
            )
        )
        mExoPlayerFullscreen = true
        mFullScreenDialog?.show()

    }

    fun closeFullscreenDialog() {
        activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        (binding.exoplayer!!?.getParent() as ViewGroup).removeView(binding.exoplayer!!)
        ((binding.mainMediaFrame) as FrameLayout).addView(binding.exoplayer)
        mExoPlayerFullscreen = false
        mFullScreenDialog?.dismiss()
        fullscreenButton?.setImageDrawable(
            ContextCompat.getDrawable(activity!!, R.drawable.ic_fullscreen_expand)
        )

    }


    private fun setSubCategoryAdapter() {
        if (businessDetails?.sub_category?.isNotEmpty()!!) {
            subCategoryList = businessDetails?.sub_category!!.split(",")
        }
        if (subCategoryList != null) {
            subCategotyAdapter = object : TagAdapter<String>(subCategoryList) {
                override fun setSelected(position: Int, subCategory: String?): Boolean {
                    return false
                }

                override fun getView(parent: FlowLayout?, position: Int, t: String?): View {
                    val view = LayoutInflater.from(activity)
                        .inflate(
                            R.layout.item_sub_category_business_details,
                            tagViewSubCategory,
                            false
                        )
                    val textview: AppCompatTextView =
                        view!!.findViewById(R.id.tvSubCategoryBusinessDetail)
                    textview.text = t?.trim()
                    return view
                }
            }

            binding.tagViewBusinessSubCategory.adapter = this.subCategotyAdapter
        }


    }

    override fun onPause() {
        super.onPause()
        if (simpleExoPlayer != null) {
            simpleExoPlayer?.playWhenReady = false
        }

    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (simpleExoPlayer != null && !isVisibleToUser) {
            simpleExoPlayer?.playWhenReady = false
        } else if (binding?.vimeoPlayer != null && binding?.vimeoPlayer!!.visibility == View.VISIBLE) {
            if (!isVisibleToUser) {
                binding.vimeoPlayer.pause()
            }

        } else if (youTubePlayer != null && !isVisibleToUser) {
            youTubePlayer?.pause()
        }

    }
}