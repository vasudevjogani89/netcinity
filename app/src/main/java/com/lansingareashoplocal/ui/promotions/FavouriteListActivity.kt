package com.lansingareashoplocal.ui.promotions

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.ItemPromotionsBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.interfaces.EditTextChangeCallBack
import com.lansingareashoplocal.ui.model.FeaturedBusiness
import com.lansingareashoplocal.ui.tempareture.TemperatureActivity
import com.lansingareashoplocal.utility.helper.TimeUtils
import com.lansingareashoplocal.utility.helper.others.CustomTextWatcher
import kotlinx.android.synthetic.main.activity_favourite_list.*
import kotlinx.android.synthetic.main.activity_favourite_list.tvNoData
import kotlinx.android.synthetic.main.item_promotions.view.*
import kotlinx.android.synthetic.main.toolbar_with_weather_details.*
import kotlinx.android.synthetic.main.toolbar_with_weather_details.view.*
import org.jetbrains.anko.alert
import kotlin.math.roundToInt

class FavouriteListActivity : BaseActivity(), EditTextChangeCallBack {


    private var hasNext = true
    private var pageCount = 20
    var currentPage = 1
    var remainingCount = 0
    var searchKey = ""
    var isSearchDone = false
    var lat = ""
    var log = ""
    var favouriteList = mutableListOf<FeaturedBusiness>()


    var favouriteAdapter: GenericAdapter<FeaturedBusiness, ItemPromotionsBinding>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite_list)
        updateLocation()
        searchKey = intent?.getStringExtra(AppConstants.SHARE_KEY)!!
      //  favouriteListToolbar.ivLeft.setImageDrawable(resources.getDrawable(R.drawable.back_icon))
        tvLeft.setOnClickListener {
            finish()
        }
        rlTemperature.setOnClickListener {
            startActivity(Intent(this@FavouriteListActivity, TemperatureActivity::class.java))
        }
        setAdapter()
        callFavouriteListApi(true)

        val layoutManager = LinearLayoutManager(this@FavouriteListActivity)
        rvFavourite.setLayoutManager(layoutManager)
        swipeRefreshFavourite.setOnRefreshListener {
            updateLocation()
            currentPage = 1
            hasNext = false
            callFavouriteListApi(false)
        }
        if (CommonMethods.getFloatValve(
                this@FavouriteListActivity,
                AppConstants.CURRENT_TEMPERATURE
            ) != 0.0F
        ) {
            favouriteListToolbar.tvTemparature.text ="${CommonMethods.getFloatValve(this@FavouriteListActivity, AppConstants.CURRENT_TEMPERATURE).roundToInt()}${getString(R.string.degree)}"


            Glide.with(this@FavouriteListActivity).load(
                "${AppConstants.WETHER_ICON_URL}${CommonMethods.getDataFromSharePreference(
                    this@FavouriteListActivity,
                    AppConstants.CURRENT_WEATHER_ICON_ID
                )}@2x.png"
            ).into(favouriteListToolbar.ivCloud)

        }

        preFillingSearchKey()
        searchBusiness()
        etFavListSearch.addTextChangedListener(CustomTextWatcher(etFavListSearch, this))
        clearText()

    }

    private fun setAdapter() {
        favouriteAdapter =
            object : GenericAdapter<FeaturedBusiness, ItemPromotionsBinding>(favouriteList) {
                override fun getLayoutId(): Int {
                    return R.layout.item_promotions
                }

                override fun onBindData(
                    model: FeaturedBusiness?,
                    position: Int,
                    dataBinding: ItemPromotionsBinding?
                ) {


                    if (model!!.address.isEmpty()) {
                        dataBinding!!.rlpromotionAddress.visibility = View.GONE
                    } else {
                        dataBinding!!.rlpromotionAddress.visibility = View.VISIBLE
                    }
                    if (model!!.phone.isEmpty()) {
                        dataBinding!!.rlPhone.visibility = View.GONE
                    } else {
                        dataBinding!!.rlPhone.visibility = View.VISIBLE
                    }
                    dataBinding?.tvCommunityName?.text = model?.placeName
                    dataBinding?.tvCommunityAddress?.text = model?.address

                    Glide.with(this@FavouriteListActivity).load(model.owner_profile_image)
                        .into(dataBinding?.ivOwner)

                    if (model.owner_profile_image.isEmpty()) {
                        dataBinding?.ivOwner.visibility = View.GONE
                    } else {
                        dataBinding?.ivOwner.visibility = View.VISIBLE
                    }


                    if (model?.getPromotionList?.isNotEmpty()!!) {
                        if (model?.getPromotionList?.get(0).set_expiration.equals("Yes")) {
                            dataBinding?.tvEventExpireDate?.text =
                                "Exp.-${TimeUtils.convertDateWithTimeForShowingApp(
                                    model.getPromotionList.get(
                                        0
                                    ).peroidEnd
                                )}"
                        }

                        dataBinding.tvEventName.text =
                            model?.getPromotionList?.get(0)?.priceDiscount
                        dataBinding?.rlEventName?.visibility = View.VISIBLE
                        dataBinding.vwDummySpace.visibility=View.VISIBLE
                        dataBinding.tvEventExpireDate?.visibility = View.VISIBLE
                    } else {
                        dataBinding.rlEventName?.visibility = View.GONE
                        dataBinding.vwDummySpace.visibility=View.GONE
                        dataBinding?.tvEventExpireDate?.visibility = View.INVISIBLE
                    }
                    dataBinding?.tvPhoneNumber?.text = model.phone


                    Glide.with(this@FavouriteListActivity).load(model.bannerImage)
                        .into(dataBinding?.ivCommunity!!)



                    if (position == favouriteList.size - 1 && hasNext) {
                        dataBinding.tvShowMore.visibility = View.VISIBLE
                        dataBinding.tvShowMore.text =
                            "Show me ${remainingCount} more Favourite Business"

                    } else {
                        dataBinding.tvShowMore.visibility = View.GONE
                    }
                    if(model.sub_category.isEmpty()){
                        dataBinding.tvSubCategories.visibility=View.GONE
                    }else{
                        dataBinding.tvSubCategories.visibility=View.VISIBLE
                        try {
                            dataBinding.tvSubCategories?.text =CommonMethods.getCapsSentences(model.sub_category)
                        }
                        catch (e :Exception){
                            dataBinding.tvSubCategories?.text=model.sub_category
                        }
                    }
                    if (model.featured.equals("Yes")) {
                        dataBinding.triangleViewFeature.visibility = View.VISIBLE
                    } else {
                        dataBinding.triangleViewFeature.visibility = View.GONE
                    }

                    /*if (position == favouriteList.size - 1) {
                        dataBinding.btnListYourBusiness.visibility = View.VISIBLE
                    } else {
                        dataBinding.btnListYourBusiness.visibility = View.GONE
                    }



                    dataBinding.btnListYourBusiness.setOnClickListener {

                        CommonMethods.openWebviewActivity(this@FavouriteListActivity,AppConstants.BUSINESS_LINK)

                    }*/


                    dataBinding.cvMain.setOnClickListener {
                        val intent =
                            Intent(this@FavouriteListActivity, PromotionDetailsActivity::class.java)
                        intent.putExtra(AppConstants.BUSINESS_ID, model.businessPlaceId)
                        startActivity(intent)
                    }


                    dataBinding.tvShowMore.setOnClickListener {
                        hasNext = false
                        currentPage += 1
                        callFavouriteListApi(true)
                    }
                }

            }
        rvFavourite.adapter = favouriteAdapter


    }

    private fun preFillingSearchKey() {
        if (searchKey.isNotEmpty()) {
            etFavListSearch.setText(searchKey)
        }
        if (searchKey.isNotEmpty()) {
            ivFavSearch.setImageDrawable(
                ContextCompat.getDrawable(
                    this@FavouriteListActivity,
                    R.drawable.cancel
                )
            )
        } else {
            ivFavSearch.setImageDrawable(
                ContextCompat.getDrawable(
                    this@FavouriteListActivity,
                    R.drawable.ic_search
                )
            )
        }
    }

    private fun clearText() {

        ivFavSearch.setOnClickListener {
            if (etFavListSearch.text!!.isNotEmpty()) {
                isSearchDone = true
                CommonMethods.hideKeypad(this@FavouriteListActivity, etFavListSearch)
                etFavListSearch.setText("")
                performSearch("")
                etFavListSearch.clearFocus()
            } else {

            }
        }
    }


    override fun afterTextChanged(editText: AppCompatEditText) {


        if (editText.text != null) {
            if (editText.text!!.isNotEmpty()) {
                Glide.with(this@FavouriteListActivity).load(R.drawable.cancel).into(ivFavSearch)
            } else {
                Glide.with(this@FavouriteListActivity).load(R.drawable.ic_search).into(ivFavSearch)
            }
        }
    }

    private fun searchBusiness() {
        etFavListSearch.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    isSearchDone = true
                    if (v?.text != null) {
                        performSearch(v.text)
                        CommonMethods.hideKeypad(this@FavouriteListActivity, etFavListSearch)

                    }

                    return true
                }
                return false
            }
        })
    }

    private fun performSearch(text: CharSequence) {
        currentPage = 1
        hasNext = false
        searchKey = text.toString()
        callFavouriteListApi(true)


    }


    private fun callFavouriteListApi(isRequireToShowLoader: Boolean) {
        val inputParam = HashMap<String, String>()
        inputParam.put(ApiConstants.PAGE_INDEX, "" + currentPage)
        inputParam.put(
            ApiConstants.USER_ID,
            CommonMethods.getUserId(this@FavouriteListActivity!!)!!
        )

        inputParam.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputParam.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputParam.put(ApiConstants.KEYWORD, searchKey)
        inputParam.put(
            ApiConstants.LAT,
            lat
        )
        inputParam.put(
            ApiConstants.LON,
           log
        )
        val call = ApiClient.getRetrofitInterface()?.getFavouriteList(inputParam)
        WSClient<Response<List<FeaturedBusiness>>>().request(
            this@FavouriteListActivity,
            100,
            isRequireToShowLoader,
            call,
            object : ISuccessHandler<Response<List<FeaturedBusiness>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<FeaturedBusiness>>
                ) {
                    if (mResponse.settings?.success.equals("1")) {
                        rlFavSearchBar.visibility = View.VISIBLE
                        remainingCount = mResponse.settings?.count?.minus(
                            (mResponse.settings?.perPage?.times(mResponse.settings?.currPage!!)!!)
                        )!!
                        if (remainingCount > mResponse.settings?.perPage!!) {
                            remainingCount = mResponse.settings?.perPage!!
                        } else {
                            remainingCount = remainingCount
                        }
                        hasNext = mResponse.settings?.nextPage == 1

                        if (currentPage == 1) {
                            favouriteList = mResponse.data as MutableList<FeaturedBusiness>
                            favouriteAdapter?.setData(favouriteList)
                        } else {
                            favouriteAdapter?.addItems(mResponse.data as MutableList<FeaturedBusiness>)

                        }

                    } else {
                        favouriteList.clear()
                        favouriteAdapter?.setData(favouriteList)
                        if (searchKey.isEmpty() && !isSearchDone) {
                            rlFavSearchBar.visibility = View.GONE
                        } else {
                            rlFavSearchBar.visibility = View.VISIBLE
                        }

                    }
                    if (swipeRefreshFavourite.isRefreshing) {
                        swipeRefreshFavourite.isRefreshing = false
                    }
                    if (favouriteList.isEmpty()) {
                        tvNoData.text = mResponse.settings?.message

                        tvNoData.visibility = View.VISIBLE
                    } else {

                        tvNoData.visibility = View.INVISIBLE
                    }

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    CommonMethods.hideProgress()
                    if (swipeRefreshFavourite.isRefreshing) {
                        swipeRefreshFavourite.isRefreshing = false
                    }
                    alert(message) {

                    }.show()
                }

            })


    }
    fun updateLocation(){
        lat = CommonMethods.getDataFromSharePreference(
            this@FavouriteListActivity,
            AppConstants.LATITUDE
        )
        log = CommonMethods.getDataFromSharePreference(
            this@FavouriteListActivity,
            AppConstants.LONGITUDE
        )
    }
}