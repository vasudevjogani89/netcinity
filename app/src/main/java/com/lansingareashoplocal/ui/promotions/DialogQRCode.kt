package com.lansingareashoplocal.ui.promotions

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.DialogQrCodeBinding
import kotlinx.android.synthetic.main.dialog_qr_code.*

class DialogQRCode(var context: Context) {


    lateinit var qrCodeDailog: Dialog
    lateinit var qrCodeBinding: DialogQrCodeBinding

    fun showQrCode(imageurl: String) {
        qrCodeDailog = Dialog(context, R.style.MyAlertDialogStyle)
        qrCodeBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_qr_code,
            null,
            false
        )
        qrCodeDailog.setContentView(qrCodeBinding.getRoot())

        Glide.with(context).load(imageurl).into(qrCodeDailog.ivQrcode)
        qrCodeDailog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        /*if (qrCodeDailog.window != null) {
            qrCodeDailog.window!!.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
            );
        }*/
        qrCodeDailog.show()
        qrCodeBinding.ivCancel.setOnClickListener {
            qrCodeDailog.dismiss()
        }


    }
}