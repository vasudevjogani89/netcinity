package com.lansingareashoplocal.ui.promotions

import android.content.Intent
import android.view.View
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseFragment
import com.lansingareashoplocal.databinding.FragmentReviewsBinding
import com.lansingareashoplocal.databinding.ItemReviewsListBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.model.BusinessDetails
import com.lansingareashoplocal.ui.model.Review
import com.lansingareashoplocal.ui.review.AddReviewActivity
import com.lansingareashoplocal.ui.user.LoginActivity
import com.lansingareashoplocal.ui.review.ReviewsListActivity
import com.lansingareashoplocal.utility.helper.TimeUtils

class ReviewFragment : BaseFragment<FragmentReviewsBinding>() {
    var reviewsAdapter: GenericAdapter<Review, ItemReviewsListBinding>? = null
    var reviewList = mutableListOf<Review>()
    var businessDetails: BusinessDetails? = null
    override fun onBackPressed(): Boolean {
        return false
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_reviews
    }

    override fun init() {
        businessDetails = arguments?.getParcelable(AppConstants.BUSINESS_MODEL)
        if (businessDetails != null && businessDetails?.getReviewListing?.isNotEmpty()!!) {
            reviewList = businessDetails?.getReviewListing as MutableList<Review>
        }

        setReviewAdapter()
        binding.tvViewMore.setOnClickListener {
            val intent = Intent(activity!!, ReviewsListActivity::class.java)
            intent.putExtra(AppConstants.BUSINESS_ID, businessDetails?.businessPlaceId)
            intent.putExtra(AppConstants.SHARE_KEY, businessDetails?.placeName)
            intent.putExtra(AppConstants.BUSINESS_LOGO, businessDetails?.icon)
            startActivity(intent)
        }
        binding.tvAddReview.setOnClickListener {
            if (CommonMethods.getUserId(activity!!)?.isEmpty()!!) {
                CommonMethods.isComeFromSplashScreen = false
                activity!!.startActivityForResult(Intent(activity!!, LoginActivity::class.java), AppConstants.LOG_IN_REQUEST_FOR_ADD_REVIEW)
            } else {
                redirecttoAddReview()
            }

        }
        setReviewData()
        isRequireToShowAddReview()
    }

    private fun setReviewAdapter() {
        reviewsAdapter = object : GenericAdapter<Review, ItemReviewsListBinding>(reviewList) {
            override fun getLayoutId(): Int {
                return R.layout.item_reviews_list
            }

            override fun onBindData(
                model: Review?,
                position: Int,
                dataBinding: ItemReviewsListBinding?
            ) {
                Glide.with(activity!!).load(model?.profileImage)
                    .into(dataBinding?.ivReviewer!!)
                dataBinding?.tvReviewerName.text = model?.userName
                dataBinding?.tvReview.text = model?.review
                dataBinding?.tvReviewDate.text =
                    TimeUtils.convertDateWithTimeForShowingApp(model?.reviewDate)
                if (model?.rating?.isNotEmpty()!!) {
                    dataBinding?.ratingBar.rating = model?.rating?.toFloat()
                }

                if (position == reviewList.size - 1) {
                    dataBinding?.viewReviewSeparator.visibility = View.GONE
                }

                if (model.publish_status.equals("Pending", true)) {
                    dataBinding?.ivEditReview.visibility = View.VISIBLE
                } else {
                    dataBinding?.ivEditReview.visibility = View.GONE
                }

                dataBinding?.ivEditReview.setOnClickListener {
                    val intent =
                        Intent(activity!!, AddReviewActivity::class.java)
                    intent.putExtra(AppConstants.BUSINESS_ID, model.businessPlaceId)
                    intent.putExtra(AppConstants.BUSINESS_LOGO, businessDetails?.icon)
                    intent.putExtra(AppConstants.BUSINESS_NAME, businessDetails?.placeName)
                    intent.putExtra(AppConstants.RATING_ID, model.reviewId)
                    intent.putExtra(AppConstants.REVIEW, model.review)
                    intent.putExtra(AppConstants.RATING, model.rating)
                    intent.putExtra(AppConstants.IS_EDITING, true)
                    activity!!.startActivityForResult(intent, AppConstants.UPDATE_REVIEW_REQUEST)
                }


            }
        }
        binding.rvReviews.adapter = reviewsAdapter

    }

    private fun isRequireToShowAddReview() {
        if (businessDetails?.getReviewSummary?.get(0)?.is_review_added.equals("0")) {
        } else {
            binding.tvAddReview.visibility = View.GONE
        }
    }

    private fun setReviewData() {
        if (businessDetails?.getReviewListing?.isNotEmpty()!!) {
            binding.tvNoReviewAtNow.visibility=View.GONE
            binding.llReviewData.visibility = View.VISIBLE
            if (businessDetails?.getReviewSummary?.isNotEmpty()!!) {
                if (businessDetails?.getReviewSummary?.get(0)?.avgRatingCount?.isNotEmpty()!!) {
                    binding.tvRatingAvg.text =
                        "(${businessDetails?.getReviewSummary?.get(0)?.avgRatingCount})"
                } else {
                    binding.tvRatingAvg.text = getString(R.string.no_ratings)
                }

                if (businessDetails?.getReviewSummary?.get(0)?.totalReviewCount?.isNotEmpty()!!) {
                    binding.tvReviewCount.text =
                        "(${businessDetails?.getReviewSummary?.get(0)?.totalReviewCount})"
                } else {
                    binding.tvReviewCount.text = "(0)"
                }
                try {
                    binding.ratingBarBusiness.rating =
                        businessDetails?.getReviewSummary?.get(0)?.avgRatingCount?.toFloat()!!
                } catch (e: Exception) {

                }
            }


        } else {
            binding.tvRatingAvg.text = getString(R.string.no_ratings)
           binding. llReviewData.visibility = View.GONE
            binding.tvNoReviewAtNow.visibility=View.VISIBLE
        }


    }

    fun redirecttoAddReview() {
        var intent = Intent(activity!!, AddReviewActivity::class.java)
        intent.putExtra(AppConstants.BUSINESS_ID, businessDetails?.businessPlaceId)
        intent.putExtra(AppConstants.BUSINESS_LOGO, businessDetails?.icon)
        intent.putExtra(AppConstants.BUSINESS_NAME, businessDetails?.placeName)
        intent.putExtra(AppConstants.IS_EDITING, false)
        activity!!.startActivityForResult(intent, AppConstants.ADD_REVIEW_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

}