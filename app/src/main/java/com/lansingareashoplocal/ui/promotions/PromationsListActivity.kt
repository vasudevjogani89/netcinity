package com.lansingareashoplocal.ui.promotions

import ApiClient
import ApiConstants
import AppConstants
import CommonMethods
import CommonMethods.isLocationAppPermissionAvalible
import CommonMethods.showGpsLocationRequireAlert
import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.DisplayMetrics
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.maps.android.SphericalUtil
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.google.maps.android.ui.IconGenerator
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.service.BackgroundScanService
import com.lansingareashoplocal.sticky_header.ItemType
import com.lansingareashoplocal.sticky_header.KmHeaderItemDecoration
import com.lansingareashoplocal.ui.adapter.PromotionAdapter
import com.lansingareashoplocal.ui.interfaces.CommonCallBack
import com.lansingareashoplocal.ui.interfaces.EditTextChangeCallBack
import com.lansingareashoplocal.ui.map.MultiDrawable
import com.lansingareashoplocal.ui.map.Person
import com.lansingareashoplocal.ui.model.Category
import com.lansingareashoplocal.ui.model.CategoryBitMap
import com.lansingareashoplocal.ui.model.FeaturedBusiness
import com.lansingareashoplocal.ui.tempareture.TemperatureActivity
import com.lansingareashoplocal.utility.helper.TimeUtils
import com.lansingareashoplocal.utility.helper.others.CustomTextWatcher
import kotlinx.android.synthetic.main.feature_business_bottom_sheet.*
import kotlinx.android.synthetic.main.feature_business_bottom_sheet.rlpromotionAddress
import kotlinx.android.synthetic.main.feature_business_bottom_sheet.tvEventExpireDate
import kotlinx.android.synthetic.main.fragment_promotion_list.*
import kotlinx.android.synthetic.main.item_promotions.*
import kotlinx.android.synthetic.main.toolbar_with_weather_details.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.okButton
import java.util.*
import kotlin.collections.HashMap
import kotlin.math.roundToInt


class PromationsListActivity : BaseActivity(), CommonCallBack, EditTextChangeCallBack,
    OnMapReadyCallback {
    var bitmap: Bitmap? = null
    private var mMap: GoogleMap? = null
    var mapFragment: SupportMapFragment? = null
    lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>
    private var mClusterManager: ClusterManager<Person>? = null
    private var categoryIconsBitmapList: MutableList<CategoryBitMap>? = null
    private var featureWeekBusinessList: MutableList<FeaturedBusiness>? = null
    private var distanceInMiles: Double? = null
    private var mapCenterPoint: LatLng? = null
    private var circle: Circle? = null
    private var activePromotionsOnly: String = ""
    private var openNow: String = ""
    private var subCategoryIds: String = ""
    private var mapActivePromotionsOnly: String = ""
    private var mapOpenNow: String = ""
    private var mapSubCategoryIds: String = ""


    override fun commonCallBack(type: String, data: Any, position: Int) {
        if (type.equals(AppConstants.FEATURED_BUSINESS)) {
            hasFeatureNext = true
            featurecurrentPage += 1
            callFeaturedBusinessApi(true, data as Int, isCallingFromWeekOFThisBusinessApi = false)
        } else if (type.equals(AppConstants.NON_FEATURED_BUSINESS)) {
            hasFeatureNext = false
            nonFeaturecurrentPage += 1
            callNonFeaturedBusinessApi(true, data as Int)

        } else if (type.equals(AppConstants.ITEM_CLICK)) {
            if (data != null) {
                val intent =
                    Intent(this@PromationsListActivity, PromotionDetailsActivity::class.java)
                intent.putExtra(
                    AppConstants.BUSINESS_ID,
                    (data as FeaturedBusiness).businessPlaceId
                )
                startActivity(intent)
            }
        }
    }


    var promotionAdapter: PromotionAdapter? = null
    var category = Category()
    var searchKey = ""
    private var hasFeatureNext = true
    private var hasNonFeatureNext = true
    private var pageCount = 20
    var featurecurrentPage = 1
    var nonFeaturecurrentPage = 1
    var featuredBusinessCount = 0
    var isSearchDone = false

    var featureRemainingCount = 0
    var nonfeatureRemainingCount = 0

    var lat = ""
    var log = ""
    var isFirstCome = true
    var promotionList = mutableListOf<FeaturedBusiness>()
    var modelList = mutableListOf<FeaturedBusiness>()
    var featuredBusinessMapViewList = mutableListOf<FeaturedBusiness>()


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_promotion_list)
        updateLocation()

        searchKey = intent?.getStringExtra(AppConstants.SHARE_KEY)!!
        category = intent?.getSerializableExtra("category") as Category
        //   promotionListToolbar.ivLeft.setImageDrawable(resources.getDrawable(R.drawable.back_icon))
        promotionListToolbar.tvLeft.setOnClickListener {
            pop()
        }
        setPromotionList()
        initAdapter()
        promotionListToolbar.rlTemperature.setOnClickListener {
            startActivity(Intent(this@PromationsListActivity, TemperatureActivity::class.java))
        }
        callGetFeatureBusinessOfTheWeekApi(true)
        swipeRefreshPromotion.setOnRefreshListener {
            updateLocation()
            featurecurrentPage = 1
            nonFeaturecurrentPage = 1
            hasFeatureNext = false
            hasNonFeatureNext = false
            callBusinessApi(false)
        }
        if (CommonMethods.getFloatValve(
                this@PromationsListActivity,
                AppConstants.CURRENT_TEMPERATURE
            ) != 0.0F
        ) {
            promotionListToolbar.tvTemparature.text = "${CommonMethods.getFloatValve(
                this@PromationsListActivity,
                AppConstants.CURRENT_TEMPERATURE
            ).roundToInt()}${getString(R.string.degree)}"

            Glide.with(this@PromationsListActivity).load(
                "${AppConstants.WETHER_ICON_URL}${CommonMethods.getDataFromSharePreference(
                    this@PromationsListActivity,
                    AppConstants.CURRENT_WEATHER_ICON_ID
                )}@2x.png"
            ).into(promotionListToolbar.ivCloud)

        }

        searchBusiness()
        etBusinessListSearch.addTextChangedListener(CustomTextWatcher(etBusinessListSearch, this))
        clearText()
        if (isAllPermissionAreAvalible() && !isServiceRunning()) {
            startBackgroundService()
        }
        ivMapOrList.setOnClickListener {
            if (rlBusinessList.isVisible) {
                rlBusinessList.visibility = View.GONE
                rlMap.visibility = View.VISIBLE
                ivRecenterMap.visibility = View.VISIBLE
                showMap()
                categoryIconsBitmapList =
                    CommonMethods.getCategoryIcons(this@PromationsListActivity)
                //  callFeatureBusinessMapPlaceView()
                ivMapOrList.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@PromationsListActivity,
                        R.drawable.listicon
                    )
                )
            } else {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                rlBusinessList.visibility = View.VISIBLE
                ivRecenterMap.visibility = View.GONE
                rlMap.visibility = View.GONE
                ivMapOrList.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@PromationsListActivity,
                        R.drawable.mapicon
                    )
                )
            }
        }
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        ivRecenterMap.setOnClickListener {
            if (mMap != null) {
                var lat = ""
                var log = ""
                if (isGpsAndLocationPermissionAreAvalible()) {
                    lat = CommonMethods.getDataFromSharePreference(
                        this@PromationsListActivity,
                        AppConstants.LATITUDE
                    )
                    log = CommonMethods.getDataFromSharePreference(
                        this@PromationsListActivity,
                        AppConstants.LONGITUDE
                    )
                    if (lat.isEmpty() || log.isEmpty()) {
                        getLastLocation()
                        lat = CommonMethods.lat
                        log = CommonMethods.lon
                    }
                } else {
                    lat = CommonMethods.lat
                    log = CommonMethods.lon
                }
                if (lat.isNotEmpty() && log.isNotEmpty()) {
                    val latLng = LatLng(lat.toDouble(), log.toDouble())
                    mMap!!.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            latLng,
                            getZoomForMetersWide(
                                mMap!!,
                                getDeviceWidth(),
                                mMap!!.cameraPosition.target,
                                30000
                            ).toFloat()
                        )
                    )
                    callBusinessMapViewPlacesViewApiIfEwquired()
                }
            }
        }
        llSUbcategoryFilter.setOnClickListener {
            val intent = Intent(this@PromationsListActivity, SubCategoryFilterActivity::class.java)
            if (rlBusinessList.isVisible) {
                intent.putExtra(AppConstants.ACTIVE_PROMOTIONS_ONLY, activePromotionsOnly)
                intent.putExtra(AppConstants.OPEN_NOW, openNow)
                intent.putExtra(AppConstants.SUB_CATEGORY_ID, subCategoryIds)
            } else {
                intent.putExtra(AppConstants.ACTIVE_PROMOTIONS_ONLY, mapActivePromotionsOnly)
                intent.putExtra(AppConstants.OPEN_NOW, mapOpenNow)
                intent.putExtra(AppConstants.SUB_CATEGORY_ID, mapSubCategoryIds)
            }



            intent.putExtra(ApiConstants.CATEGORY_ID, category.categoryId)

            if (rlBusinessList.isVisible) {
                startActivityForResult(
                    intent,
                    AppConstants.FEATURE_BUSINESS_SUB_CATEGORY_FILTER_REQUEST_CODE
                )
            } else {
                startActivityForResult(
                    intent,
                    AppConstants.MAP_VIEW_SUB_CATEGORY_FILTER_REQUEST_CODE
                )
            }

        }
    }



    private fun clearText() {

        ivPromotionSearch.setOnClickListener {
            if (etBusinessListSearch.text!!.isNotEmpty()) {
                isSearchDone = true
                CommonMethods.hideKeypad(this@PromationsListActivity, etBusinessListSearch)
                etBusinessListSearch.setText("")
                performSearch("")
                etBusinessListSearch.clearFocus()
            } else {

            }
        }
    }


    fun updateLocation() {
        lat = CommonMethods.getDataFromSharePreference(
            this@PromationsListActivity,
            AppConstants.LATITUDE
        )
        log = CommonMethods.getDataFromSharePreference(
            this@PromationsListActivity,
            AppConstants.LONGITUDE
        )
    }

    override fun afterTextChanged(editText: AppCompatEditText) {
        if (editText.text != null) {
            if (editText.text!!.isNotEmpty()) {
                Glide.with(this@PromationsListActivity).load(R.drawable.cancel)
                    .into(ivPromotionSearch)
            } else {
                Glide.with(this@PromationsListActivity).load(R.drawable.ic_search)
                    .into(ivPromotionSearch)
            }
        }


    }


    private fun searchBusiness() {
        etBusinessListSearch.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    isSearchDone = true
                    if (v?.text != null) {
                        performSearch(v.text)
                        CommonMethods.hideKeypad(this@PromationsListActivity, etBusinessListSearch)

                    }

                    return true
                }
                return false
            }
        })
    }

    private fun performSearch(text: CharSequence) {
        featurecurrentPage = 1
        nonFeaturecurrentPage = 1
        hasFeatureNext = false
        hasNonFeatureNext = false
        searchKey = text.toString()
        callBusinessApi(true)


    }


    // var promationsAdapter: GenericAdapter<Promotions, ItemPromotionsBinding>? = null


    private fun callFeaturedBusinessApi(
        isRequireToShowLoader: Boolean,
        lastFeatureBusinessObjectposition: Int = 0,
        isCallingFromWeekOFThisBusinessApi: Boolean
    ) {
        if (isRequireToShowLoader) {
            CommonMethods.showProgress(this@PromationsListActivity)
        }

        val inputParam = HashMap<String, String>()
        inputParam.put(ApiConstants.CATEGORY_ID, category.categoryId)
        inputParam.put(ApiConstants.PAGE_INDEX, "" + featurecurrentPage)
        inputParam.put(ApiConstants.KEYWORD, searchKey.trim())
        inputParam.put(
            ApiConstants.LAT, lat
        )
        inputParam.put(
            ApiConstants.LON, log
        )
        inputParam.put(ApiConstants.KEYWORD, searchKey)
        inputParam.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        if (activePromotionsOnly.equals("1")) {
            inputParam.put(ApiConstants.IS_PROMOTION, "Yes")
        }
        inputParam.put(ApiConstants.SUB_CATEGORY_ID, subCategoryIds)
        val call = ApiClient.getRetrofitInterface()?.getFeaturedBusiness(inputParam)
        WSClient<Response<List<FeaturedBusiness>>>().request(
            this@PromationsListActivity,
            100,
            false,
            call,
            object : ISuccessHandler<Response<List<FeaturedBusiness>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<FeaturedBusiness>>
                ) {
                    if (mResponse.settings?.success.equals("1")) {
                        rlPromotionSearchBar.visibility = View.VISIBLE
                        featureRemainingCount = mResponse.settings?.count?.minus(
                            (mResponse.settings?.perPage?.times(mResponse.settings?.currPage!!)!!)
                        )!!
                        if (featureRemainingCount > mResponse.settings?.perPage!!) {
                            featureRemainingCount = mResponse.settings?.perPage!!
                        } else {
                            featureRemainingCount = featureRemainingCount
                        }
                        hasFeatureNext = mResponse.settings?.nextPage == 1
                        promotionList = mResponse.data as MutableList<FeaturedBusiness>
                        if (!(activePromotionsOnly.equals("1") || subCategoryIds.isNotEmpty() || searchKey.isNotEmpty())) {
                            promotionList =
                                removeFeatureWeekBusinessFromFeatureBusiness(promotionList)
                        }
                        if (featurecurrentPage == 1) {
                            if (!isCallingFromWeekOFThisBusinessApi) {
                                modelList.clear()
                            }
                            val featuredBusiness = FeaturedBusiness()
                            featuredBusiness.header = AppConstants.FEATURED_BUSINESS
                            featuredBusiness.itemType = ItemType.Header
                            modelList.add(featuredBusiness)
                            promotionList.forEachIndexed { i, featuredBusinessModel ->
                                featuredBusinessModel.itemType = ItemType.Post
                                if (hasFeatureNext && i == promotionList.size - 1) {
                                    featuredBusinessModel.hasNext = true
                                    featuredBusinessModel.remainingCount = featureRemainingCount
                                }
                                modelList.add(featuredBusinessModel)
                            }
                            promotionAdapter?.submitList(modelList)
                            promotionAdapter?.notifyDataSetChanged()
                            callNonFeaturedBusinessApi(false)
                        } else {
                            //here we are adding  feature item based on pagination
                            if (mResponse.data != null) {
                                mapFragment = null
                            }
                            promotionList.forEachIndexed { i, featuredBusinessModel ->
                                featuredBusinessModel.itemType = ItemType.Post
                                modelList.add(
                                    lastFeatureBusinessObjectposition + i,
                                    featuredBusinessModel
                                )
                            } // here i am appending new feature business  items to list
                            modelList.forEachIndexed { index, featuredBusiness ->
                                if (featuredBusiness.isFeatureBusiness) {
                                    modelList.get(index).hasNext = false
                                    featuredBusinessCount++
                                }
                            }// here i am making  we have pagination
                            if (hasFeatureNext) {
                                val lastFeatureObjectPosition =
                                    lastFeatureBusinessObjectposition + promotionList.size - 1
                                modelList.get(lastFeatureObjectPosition).hasNext = true
                                modelList.get(lastFeatureObjectPosition).remainingCount =
                                    featureRemainingCount
                            } // if next is avalible i am make as pagination is avalible
                            featuredBusinessCount = 0
                            promotionAdapter?.submitList(modelList)
                            promotionAdapter?.notifyDataSetChanged()
                            CommonMethods.hideProgress()
                        }

                    } else {// here list is empty we adding empty message
                        if (!isSearchDone) {
                            rlPromotionSearchBar.visibility = View.GONE
                        } else {
                            rlPromotionSearchBar.visibility = View.VISIBLE
                        }
                        modelList.clear()
                        val featuredBusiness = FeaturedBusiness()
                        featuredBusiness.header = AppConstants.FEATURED_BUSINESS
                        featuredBusiness.itemType = ItemType.Header
                        modelList.add(featuredBusiness)


                        val featuredBusinessEmpty = FeaturedBusiness()
                        featuredBusinessEmpty.header = AppConstants.FEATURED_BUSINESS
                        featuredBusinessEmpty.itemType = ItemType.Post
                        featuredBusinessEmpty.emptyData = true
                        featuredBusinessEmpty.emptyMessage = mResponse.settings?.message.toString()
                        modelList.add(featuredBusinessEmpty)
                        promotionAdapter?.submitList(modelList)
                        promotionAdapter?.notifyDataSetChanged()
                        callNonFeaturedBusinessApi(false)

                    }


                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    CommonMethods.hideProgress()
                    if (swipeRefreshPromotion.isRefreshing) {
                        swipeRefreshPromotion.isRefreshing = false
                    }
                    alert(message) { okButton { } }.show()
                }

            })


    }


    private fun callNonFeaturedBusinessApi(isRequireLoader: Boolean, position: Int = 0) {

        if (isRequireLoader) {
            CommonMethods.showProgress(this@PromationsListActivity)
        }
        var inputParam = HashMap<String, String>()
        inputParam.put(ApiConstants.CATEGORY_ID, category.categoryId)
        inputParam.put(ApiConstants.PAGE_INDEX, "" + nonFeaturecurrentPage)
        inputParam.put(ApiConstants.KEYWORD, searchKey)
        if (category.category.equals(getString(R.string.my_fav))) {
            inputParam.put(
                ApiConstants.USER_ID,
                CommonMethods.getUserId(this@PromationsListActivity)!!
            )
        }
        inputParam.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputParam.put(ApiConstants.LAT, lat)
        inputParam.put(ApiConstants.LON, log)


        val call = ApiClient.getRetrofitInterface()?.getNonFeaturedBusiness(inputParam)
        WSClient<Response<List<FeaturedBusiness>>>().request(
            this@PromationsListActivity,
            100,
            false,
            call,
            object : ISuccessHandler<Response<List<FeaturedBusiness>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<FeaturedBusiness>>
                ) {

                    if (mResponse.settings?.success.equals("1")) {
                        rlPromotionSearchBar.visibility = View.VISIBLE
                        nonfeatureRemainingCount = mResponse.settings?.count?.minus(
                            (mResponse.settings?.perPage?.times(mResponse.settings?.currPage!!)!!)
                        )!!
                        if (nonfeatureRemainingCount > mResponse.settings?.perPage!!) {
                            nonfeatureRemainingCount = mResponse.settings?.perPage!!
                        } else {
                            nonfeatureRemainingCount = nonfeatureRemainingCount
                        }
                        CommonMethods.hideProgress()
                        promotionList = mResponse.data as MutableList<FeaturedBusiness>
                        hasNonFeatureNext = mResponse.settings?.nextPage == 1
                        if (nonFeaturecurrentPage == 1) {
                            val nonFeaturedBusiness = FeaturedBusiness()
                            nonFeaturedBusiness.header = "Non Featured Business"
                            nonFeaturedBusiness.itemType = ItemType.Header
                            nonFeaturedBusiness.isFeatureBusiness = false
                            modelList.add(nonFeaturedBusiness)
                            promotionList.forEachIndexed { i, nonFeaturedBusinessModel ->
                                nonFeaturedBusinessModel.itemType = ItemType.Post
                                nonFeaturedBusinessModel.isFeatureBusiness = false
                                if (hasNonFeatureNext && i == promotionList.size - 1) {
                                    nonFeaturedBusinessModel.hasNext = true
                                    nonFeaturedBusinessModel.remainingCount =
                                        nonfeatureRemainingCount
                                }
                                modelList.add(nonFeaturedBusinessModel)
                            }
                            promotionAdapter?.submitList(modelList)
                            promotionAdapter?.notifyDataSetChanged()
                        } else { //here we are adding non  feature item based on pagination
                            CommonMethods.hideProgress()
                            promotionList.forEachIndexed { i, nonFeaturedBusinessModel ->
                                nonFeaturedBusinessModel.itemType = ItemType.Post
                                nonFeaturedBusinessModel.isFeatureBusiness = false
                                modelList.add(position + i, nonFeaturedBusinessModel)

                            }// here i am appending new  non feature business  items to list
                            modelList.forEachIndexed { index, featuredBusiness ->
                                if (!featuredBusiness.isFeatureBusiness)
                                    modelList.get(index).hasNext = false
                            }   // here i am making  we have pagination
                            if (hasNonFeatureNext) {
                                modelList.get(modelList.size - 1).hasNext = true
                                modelList.get(modelList.size - 1).remainingCount =
                                    nonfeatureRemainingCount

                            }       // if next is avalible i am make as pagination is avalible
                            promotionAdapter?.submitList(modelList)
                            promotionAdapter?.notifyDataSetChanged()
                        }

                    } else {
                        if (!isSearchDone && promotionList.isEmpty()) {
                            rlPromotionSearchBar.visibility = View.GONE
                        } else {
                            rlPromotionSearchBar.visibility = View.VISIBLE
                        }
                        // here list is empty we adding empty message
                        val nonFeaturedBusiness = FeaturedBusiness()
                        nonFeaturedBusiness.header = getString(R.string.non_faeture_business)
                        nonFeaturedBusiness.itemType = ItemType.Header
                        modelList.add(nonFeaturedBusiness)

                        val nonFeaturedBusinessEmpty = FeaturedBusiness()
                        nonFeaturedBusinessEmpty.header = getString(R.string.non_faeture_business)
                        nonFeaturedBusinessEmpty.itemType = ItemType.Post
                        nonFeaturedBusinessEmpty.emptyData = true
                        nonFeaturedBusinessEmpty.emptyMessage =
                            mResponse.settings?.message.toString()
                        modelList.add(nonFeaturedBusinessEmpty)
                        promotionAdapter?.submitList(modelList)
                        promotionAdapter?.notifyDataSetChanged()
                        CommonMethods.hideProgress()

                    }
                    if (swipeRefreshPromotion.isRefreshing) {
                        swipeRefreshPromotion.isRefreshing = false
                    }
                    if (isFirstCome) {
                        isFirstCome = false
                        when {
                            !CommonMethods.isBluetoothEnabled() -> {
                                val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                                startActivityForResult(intent, 100)
                            }

                            !isLocationAppPermissionAvalible(this@PromationsListActivity) -> {
                                checkPermissionAndStart()
                            }

                            !CommonMethods.isLocationEnabled(this@PromationsListActivity) -> {
                                showGpsLocationRequireAlert(this@PromationsListActivity)

                            }


                        }
                    }


                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    CommonMethods.hideProgress()
                    if (swipeRefreshPromotion.isRefreshing) {
                        swipeRefreshPromotion.isRefreshing = false
                    }
                    alert(message) { okButton { } }.show()
                }

            })


    }

    private fun initAdapter() {
        promotionAdapter = PromotionAdapter(this@PromationsListActivity, this)
        val layoutManager = LinearLayoutManager(this@PromationsListActivity)
        rvPromotions.setLayoutManager(layoutManager)
        var kmHeaderItemDecoration = KmHeaderItemDecoration(promotionAdapter)
        rvPromotions.setAdapter(promotionAdapter)
    }


    private fun setPromotionList() {
        tvCommunityType.text = category.category
        tvCommunityTitle.text = "${category.category} Communities(${promotionList.size})"

        if (category.isLocal) {
            Glide.with(this@PromationsListActivity).load(category.localImage).into(ivCommunityIcon)
        } else {

            Glide.with(this@PromationsListActivity).load(category.icon).into(ivCommunityIcon)
        }


    }

    private fun showExplanationDialog() {
        alert(CommonMethods.getLocationPermissionAlert(this@PromationsListActivity)) {
            okButton {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                val uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, AppConstants.LOCATION_REQUEST);
            }
            cancelButton {


            }
        }.show()
    }


    private fun checkPermissionAndStart(): Boolean {
        val checkSelfPermissionResult =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        if (PackageManager.PERMISSION_GRANTED == checkSelfPermissionResult) {
            //already granted
            return true
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                showExplanationDialog()
                //we should show some explanation for user here
            } else {
                //request permission
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ),
                    AppConstants.LOCATION_REQUEST
                )
            }
        }
        return false
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            AppConstants.BLUETOOTH_REQUEST -> {
                if (!isLocationAppPermissionAvalible(this@PromationsListActivity)) {
                    checkPermissionAndStart()
                } else if (!CommonMethods.isLocationEnabled(this@PromationsListActivity)) {
                    showGpsLocationRequireAlert(this@PromationsListActivity)

                }

            }
            AppConstants.GPS_REQUEST -> {
                if (isGpsAndLocationPermissionAreAvalible()) {
                    getLastLocation()
                }

            }
            AppConstants.LOCATION_REQUEST -> {
                if (isGpsAndLocationPermissionAreAvalible()) {
                    getLastLocation()
                } else {
                    if (isLocationAppPermissionAvalible(this@PromationsListActivity)) {
                        if (!CommonMethods.isLocationEnabled(this@PromationsListActivity)) {
                            showGpsLocationRequireAlert(this@PromationsListActivity)
                        }
                    }

                }

            }
        }

        if (requestCode == AppConstants.FEATURE_BUSINESS_SUB_CATEGORY_FILTER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                if (data?.getStringExtra(AppConstants.ACTIVE_PROMOTIONS_ONLY) != null) {
                    activePromotionsOnly = data.getStringExtra(AppConstants.ACTIVE_PROMOTIONS_ONLY)
                } else {
                    activePromotionsOnly = ""
                }

                if (data?.getStringExtra(AppConstants.OPEN_NOW) != null) {
                    openNow = data.getStringExtra(AppConstants.OPEN_NOW)
                } else {
                    openNow = ""
                }

                if (data?.getStringExtra(AppConstants.SUB_CATEGORY_ID) != null) {
                    subCategoryIds = data.getStringExtra(AppConstants.SUB_CATEGORY_ID)
                } else {
                    subCategoryIds = ""
                }

                updateLocation()
                featurecurrentPage = 1
                nonFeaturecurrentPage = 1
                hasFeatureNext = false
                hasNonFeatureNext = false
                callBusinessApi(true)

            }


        } else if (requestCode == AppConstants.MAP_VIEW_SUB_CATEGORY_FILTER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data?.getStringExtra(AppConstants.ACTIVE_PROMOTIONS_ONLY) != null) {
                mapActivePromotionsOnly = data.getStringExtra(AppConstants.ACTIVE_PROMOTIONS_ONLY)
            } else {
                mapActivePromotionsOnly = ""
            }

            if (data?.getStringExtra(AppConstants.OPEN_NOW) != null) {
                mapOpenNow = data.getStringExtra(AppConstants.OPEN_NOW)
            } else {
                mapOpenNow = ""
            }

            if (data?.getStringExtra(AppConstants.SUB_CATEGORY_ID) != null) {
                mapSubCategoryIds = data.getStringExtra(AppConstants.SUB_CATEGORY_ID)
            } else {
                mapSubCategoryIds = ""
            }

            callFeatureBusinessMapPlaceView()

        }
    }


    fun isAllPermissionAreAvalible(): Boolean {
        return isLocationAppPermissionAvalible(this@PromationsListActivity) && CommonMethods.isLocationEnabled(
            this@PromationsListActivity
        ) && CommonMethods.isBluetoothEnabled()
    }

    private fun startBackgroundService() {
        startService(Intent(this@PromationsListActivity, BackgroundScanService::class.java))
    }

    private fun isServiceRunning(): Boolean {
        return CommonMethods.isMyServiceRunning(
            this@PromationsListActivity, BackgroundScanService::class.java
        )
    }

    private fun showMap() {
        if (mapFragment == null) {
            mapFragment =
                supportFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
            mapFragment?.getMapAsync(this)

        }


    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap != null) {
            this.mMap = googleMap
            var lat = CommonMethods.getDataFromSharePreference(
                this@PromationsListActivity,
                AppConstants.LATITUDE
            )
            var log = CommonMethods.getDataFromSharePreference(
                this@PromationsListActivity,
                AppConstants.LONGITUDE
            )
            if (lat.isEmpty() && log.isEmpty()) {
                lat = CommonMethods.lat
                log = CommonMethods.lon
            }
            if (lat.isNotEmpty() && log.isNotEmpty()) {
                val latLng = LatLng(lat.toDouble(), log.toDouble())
                mMap!!.setMinZoomPreference(
                    getZoomForMetersWide(
                        mMap!!,
                        getDeviceWidth(),
                        mMap!!.cameraPosition.target,
                        30000
                    ).toFloat()
                )
                mMap!!.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        latLng,
                        getZoomForMetersWide(
                            mMap!!,
                            getDeviceWidth(),
                            mMap!!.cameraPosition.target,
                            30000
                        ).toFloat()
                    )
                )


                val visibleRegion = mMap!!.projection.visibleRegion
                distanceInMiles = SphericalUtil.computeDistanceBetween(
                    visibleRegion.farLeft,
                    mMap!!.cameraPosition.target
                ) * 0.000621371
                mapCenterPoint = mMap?.cameraPosition?.target
                callFeatureBusinessMapPlaceView()
                googleMap.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
                    override fun onMarkerClick(marker: Marker?): Boolean {
                        loop@ featuredBusinessMapViewList.forEachIndexed { index, featuredBusiness ->
                            if (marker?.position?.latitude == featuredBusiness.latitude.toDouble() && marker.position.longitude == featuredBusiness.longitude.toDouble()) {
                                if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
                                    tvSubCategoriesBottomSheet?.setTextColor(ContextCompat.getColor(this@PromationsListActivity, R.color.colorDarkBlue))
                                }
                                Glide.with(this@PromationsListActivity)
                                    .load(featuredBusiness.bannerImage)
                                    .into(ivBusinessImg)
                                Glide.with(this@PromationsListActivity)
                                    .load(featuredBusiness.bannerImage)
                                    .into(ivBusinessImg)
                                tvBusinessName.text = featuredBusiness.placeName
                                tvBusinessAddress.text = featuredBusiness.address
                                Glide.with(this@PromationsListActivity)
                                    .load(featuredBusiness.owner_profile_image)
                                    .into(ivBusinessOwner)
                                if (featuredBusiness.owner_profile_image.isEmpty()) {
                                    ivBusinessOwner.visibility = View.GONE
                                } else {
                                    ivBusinessOwner.visibility = View.VISIBLE
                                }

                                if (featuredBusiness.getPromotionList.isNotEmpty()) {
                                    if (featuredBusiness.getPromotionList.get(0).set_expiration.equals(
                                            "Yes"
                                        )
                                    ) {
                                        tvEventExpireDate.text =
                                            "Exp.-${TimeUtils.convertDateWithTimeForShowingApp(
                                                featuredBusiness.getPromotionList.get(
                                                    0
                                                ).peroidEnd
                                            )}"
                                    }
                                    tvPromotionName.text =
                                        featuredBusiness.getPromotionList.get(0).priceDiscount
                                    rlPromotionDetails.visibility = View.VISIBLE
                                    tvEventExpireDate.visibility = View.VISIBLE
                                } else {
                                    rlPromotionDetails.visibility = View.INVISIBLE
                                    tvEventExpireDate.visibility = View.INVISIBLE
                                }
                                tvBusinessPhoneNumber.text = featuredBusiness.phone
                                if (featuredBusiness.phone.isEmpty()) {
                                    rlBusinessPhone.visibility = View.GONE
                                } else {
                                    rlBusinessPhone.visibility = View.VISIBLE
                                }
                                if (featuredBusiness.distance.isEmpty()) {

                                } else {
                                    tvBusinessDistance.text = "${featuredBusiness.distance} mi"
                                }
                                if (featuredBusiness.address.isEmpty()) {
                                    rlpromotionAddress.visibility = View.GONE
                                } else {
                                    rlpromotionAddress.visibility = View.VISIBLE
                                }
                                if (featuredBusiness.sub_category.isEmpty()) {
                                    tvSubCategoriesBottomSheet.visibility = View.GONE
                                }
                                else {
                                    tvSubCategoriesBottomSheet.visibility = View.VISIBLE
                                    try {
                                        tvSubCategoriesBottomSheet.text = CommonMethods.getCapsSentences(featuredBusiness.sub_category)
                                    } catch (e: Exception) {
                                        tvSubCategoriesBottomSheet?.text = featuredBusiness.sub_category
                                    }
                                }



                                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                                bottom_sheet.setOnClickListener {
                                    val intent = Intent(
                                        this@PromationsListActivity,
                                        PromotionDetailsActivity::class.java
                                    )
                                    intent.putExtra(
                                        AppConstants.BUSINESS_ID,
                                        featuredBusiness.businessPlaceId
                                    )
                                    startActivity(intent)
                                }

                                return@forEachIndexed
                            }
                        }
                        return true
                    }

                })
                googleMap.setOnCameraMoveListener {
                    callBusinessMapViewPlacesViewApiIfEwquired()
                }
                googleMap.setOnMapClickListener {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                }

            }

        }


    }


    fun callBusinessMapViewPlacesViewApiIfEwquired() {
        if (mMap != null && mapCenterPoint != null)
            if (distance(
                    mapCenterPoint?.latitude!!,
                    mapCenterPoint?.longitude!!,
                    mMap?.cameraPosition?.target?.latitude!!,
                    mMap?.cameraPosition?.target?.longitude!!
                ) > 15
            ) {
                mMap?.clear()
                mapCenterPoint = mMap?.cameraPosition?.target
                callFeatureBusinessMapPlaceView()
            }
    }


    fun addMarker(
        googleMap: GoogleMap?,
        featuredBusiness: FeaturedBusiness,
        isFirstBusiness: Boolean = false
    ) {
        var flag = false
        if (featuredBusiness.latitude.isNotEmpty() && featuredBusiness.longitude.isNotEmpty()) {

            try {
                val latLng = LatLng(
                    featuredBusiness.latitude.toDouble(),
                    featuredBusiness.longitude.toDouble()
                )
                // mClusterManager!!.addItem(Person(latLng, featuredBusiness.placeName, BitmapFactory.decodeResource(resources, R.drawable.ic_shop_local)))
                categoryIconsBitmapList?.forEachIndexed { index, categoryBitMap ->

                    if (categoryBitMap.categoryId.equals(featuredBusiness.categoryId) && categoryBitMap.bitMap != null) {
                        googleMap?.addMarker(
                            MarkerOptions().position(latLng).title(featuredBusiness.placeName)
                                .icon(BitmapDescriptorFactory.fromBitmap(categoryBitMap.bitMap))
                        )
                        flag = true
                    }
                }
                if (!flag) {
                    val icon = BitmapFactory.decodeResource(resources, R.drawable.ic_shop_local)
                    Bitmap.createScaledBitmap(icon, 80, 80, false)
                    googleMap?.addMarker(
                        MarkerOptions().position(latLng).title(featuredBusiness.placeName)
                            .icon(BitmapDescriptorFactory.fromBitmap(icon))
                    )
                    //  mClusterManager!!.addItem(Person(latLng, featuredBusiness.placeName, BitmapFactory.decodeResource(resources, R.drawable.ic_shop_local)))
                }


                //googleMap?.addMarker(MarkerOptions().position(latLng).title(featuredBusiness.placeName)/*.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)*/)


            } catch (e: Exception) {

            }

        }


    }


    inner class PersonRenderer :
        DefaultClusterRenderer<Person>(this@PromationsListActivity, mMap, mClusterManager) {
        private val mIconGenerator = IconGenerator(this@PromationsListActivity)
        private val mClusterIconGenerator = IconGenerator(this@PromationsListActivity)
        private val mImageView: ImageView
        private val mClusterImageView: ImageView
        private val mDimension: Int
        override fun onBeforeClusterItemRendered(
            person: Person,
            markerOptions: MarkerOptions
        ) {
            // Draw a single person - show their profile photo and set the info window to show their name
            markerOptions
                .icon(getItemIcon(person))
                .title(person.name)
        }

        protected fun onClusterItemUpdated(person: Person, marker: Marker) {
            // Same implementation as onBeforeClusterItemRendered() (to update cached markers)
            marker.setIcon(getItemIcon(person))
            marker.title = person.name
        }

        /**
         * Get a descriptor for a single person (i.e., a marker outside a cluster) from their
         * profile photo to be used for a marker icon
         *
         * @param person person to return an BitmapDescriptor for
         * @return the person's profile photo as a BitmapDescriptor
         */
        private fun getItemIcon(person: Person): BitmapDescriptor {
            mImageView.setImageBitmap(person.profilePhoto)
            val icon = mIconGenerator.makeIcon()
            return BitmapDescriptorFactory.fromBitmap(icon)
        }

        override fun onBeforeClusterRendered(
            cluster: Cluster<Person>,
            markerOptions: MarkerOptions
        ) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            markerOptions.icon(getClusterIcon(cluster))
        }

        protected fun onClusterUpdated(cluster: Cluster<Person>, marker: Marker) {
            // Same implementation as onBeforeClusterRendered() (to update cached markers)
            marker.setIcon(getClusterIcon(cluster))
        }

        /**
         * Get a descriptor for multiple people (a cluster) to be used for a marker icon. Note: this
         * method runs on the UI thread. Don't spend too much time in here (like in this example).
         *
         * @param cluster cluster to draw a BitmapDescriptor for
         * @return a BitmapDescriptor representing a cluster
         */
        private fun getClusterIcon(cluster: Cluster<Person>): BitmapDescriptor {
            val profilePhotos: MutableList<Drawable> =
                ArrayList(
                    Math.min(
                        4,
                        cluster.getSize()
                    )
                )
            val width = mDimension
            val height = mDimension
            for (p in cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size == 4) break
                val drawable: Drawable = BitmapDrawable(resources, p.profilePhoto)
                drawable.setBounds(0, 0, width, height)
                profilePhotos.add(drawable)
            }
            val multiDrawable = MultiDrawable(profilePhotos)
            multiDrawable.setBounds(0, 0, width, height)
            mClusterImageView.setImageDrawable(multiDrawable)
            val icon =
                mClusterIconGenerator.makeIcon(cluster.getSize().toString())
            return BitmapDescriptorFactory.fromBitmap(icon)
        }


        override fun onClusterItemRendered(clusterItem: Person?, marker: Marker?) {
            super.onClusterItemRendered(clusterItem, marker)
        }

        override fun onClusterRendered(cluster: Cluster<Person>?, marker: Marker?) {
            super.onClusterRendered(cluster, marker)
        }

        /*override fun shouldRenderAsCluster(cluster: Cluster<*>): Boolean {
            // Always render clusters.
            return cluster.size > 1
        }*/

        init {
            val multiProfile: View =
                getLayoutInflater().inflate(R.layout.multi_profile, null)
            mClusterIconGenerator.setContentView(multiProfile)
            mClusterImageView = multiProfile.findViewById(R.id.image)
            mImageView = ImageView(getApplicationContext())
            mDimension = getResources().getDimension(R.dimen.custom_profile_image).toInt()
            mImageView.layoutParams = ViewGroup.LayoutParams(mDimension, mDimension)
            val padding = getResources().getDimension(R.dimen.custom_profile_padding)
            mImageView.setPadding(
                padding.toInt(),
                padding.toInt(),
                padding.toInt(),
                padding.toInt()
            )
            mIconGenerator.setContentView(mImageView)
        }
    }

    fun callFeatureBusinessMapPlaceView() {
        val inputputParams = HashMap<String, String>()
        inputputParams.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        if (mapActivePromotionsOnly.equals("1")) {
            inputputParams.put(ApiConstants.IS_PROMOTION, "Yes")
        }
        inputputParams.put(ApiConstants.SUB_CATEGORY_ID, mapSubCategoryIds)
        if (mMap != null) {
            inputputParams.put(ApiConstants.LAT, mMap?.cameraPosition?.target?.latitude.toString())
            inputputParams.put(ApiConstants.LON, mMap?.cameraPosition?.target?.longitude.toString())
        }

        if (distanceInMiles != null) {
            inputputParams.put(ApiConstants.RADIUS, distanceInMiles.toString())
        }
        inputputParams.put(ApiConstants.CATEGORY_ID, category.categoryId)

        val call = ApiClient.getRetrofitInterface()?.getFeatureBusinessPlacesMapView(inputputParams)
        WSClient<Response<List<FeaturedBusiness>>>().request(
            this@PromationsListActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<FeaturedBusiness>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<FeaturedBusiness>>
                ) {
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        featuredBusinessMapViewList.clear()
                        featuredBusinessMapViewList.addAll(mResponse.data as MutableList<FeaturedBusiness>)
                        if (featuredBusinessMapViewList.isNotEmpty()) {
                            // mClusterManager = ClusterManager(this@PromationsListActivity, mMap)
                            // mClusterManager!!.setRenderer(PersonRenderer())
                            featuredBusinessMapViewList.forEachIndexed { index, featuredBusiness ->
                                addMarker(mMap, featuredBusiness)
                            }
                            //  mClusterManager!!.cluster()
                        }
                    }


                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                }

            })
    }

    private fun distance(
        lat1: Double,
        lng1: Double,
        lat2: Double,
        lng2: Double
    ): Double {
        val earthRadius = 3958.75 // in miles, change to 6371 for kilometer output
        val dLat = Math.toRadians(lat2 - lat1)
        val dLng = Math.toRadians(lng2 - lng1)
        val sindLat = Math.sin(dLat / 2)
        val sindLng = Math.sin(dLng / 2)
        val a = Math.pow(sindLat, 2.0) + (Math.pow(sindLng, 2.0)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(
            Math.toRadians(
                lat2
            )
        ))
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return earthRadius * c * 1.60934 // output distance, in MILES
    }

    fun getDeviceWidth(): Int {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.widthPixels
    }

    fun isGpsAndLocationPermissionAreAvalible(): Boolean {
        return isLocationAppPermissionAvalible(this@PromationsListActivity) && CommonMethods.isLocationEnabled(
            this@PromationsListActivity
        )
    }

    fun calcZoom(visible_distance: Int, img_width: Int): Double {
        // visible_distance -> in meters
        // img_width -> in pixels
        var visible_distance = visible_distance
        visible_distance = Math.abs(visible_distance)
        val equator_length = 40075016.0 // in meters

        // for an immage of 256 pixel pixel
        val zoom256 =
            Math.log(equator_length / visible_distance) / Math.log(2.0)

        // adapt the zoom to the image size
        val x =
            (Math.log(img_width / 256.toDouble()) / Math.log(2.0)).toInt()
        return zoom256 + x
    }

    fun getZoomLevel(circle: Circle?): Int {
        var zoomLevel = 11
        if (circle != null) {
            val radius = circle.radius + circle.radius / 2
            val scale = radius / 500
            zoomLevel = (16 - Math.log(scale) / Math.log(2.0)).toInt()
        }
        return zoomLevel
    }

    fun getZoomForMetersWide(
        googleMap: GoogleMap,
        mapViewWidth: Int,
        latLngPoint: LatLng,
        desiredMeters: Int
    ): Double {
        val metrics: DisplayMetrics =
            resources.getDisplayMetrics()
        val mapWidth = mapViewWidth / metrics.density
        val EQUATOR_LENGTH = 40075004
        val latitudinalAdjustment =
            Math.cos(Math.PI * latLngPoint.latitude / 180.0)
        val arg =
            EQUATOR_LENGTH * mapWidth * latitudinalAdjustment / (desiredMeters * 256.0)
        val valToZoom = Math.log(arg) / Math.log(2.0)
        return valToZoom
    }


    private fun callGetFeatureBusinessOfTheWeekApi(isRequireToShowLoader: Boolean) {
        if (isRequireToShowLoader) {
            CommonMethods.showProgress(this@PromationsListActivity)
        }
        val inputparam = HashMap<String, String>()

        inputparam.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputparam.put(ApiConstants.CATEGORY_ID, category.categoryId)
        inputparam.put(
            ApiConstants.LAT,
            CommonMethods.getDataFromSharePreference(
                this@PromationsListActivity,
                AppConstants.LATITUDE
            )
        )
        inputparam.put(
            ApiConstants.LON,
            CommonMethods.getDataFromSharePreference(
                this@PromationsListActivity,
                AppConstants.LONGITUDE
            )
        )

        val call = ApiClient.getRetrofitInterface()?.getFeatureBusinessOfTheWeekList(inputparam)
        WSClient<Response<List<FeaturedBusiness>>>().request(
            this@PromationsListActivity,
            100,
            false,
            call,
            object : ISuccessHandler<Response<List<FeaturedBusiness>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<FeaturedBusiness>>
                ) {
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null && mResponse.data!!.isNotEmpty()) {
                        promotionList = mResponse.data as MutableList<FeaturedBusiness>
                        featureWeekBusinessList = mResponse.data as MutableList<FeaturedBusiness>
                        modelList.clear()
                        val featuredBusiness = FeaturedBusiness()
                        featuredBusiness.header = getString(R.string.featured_this_week)
                        featuredBusiness.itemType = ItemType.Header
                        modelList.add(featuredBusiness)
                        promotionList.forEachIndexed { i, featuredBusinessModel ->
                            featuredBusinessModel.itemType = ItemType.Post
                            modelList.add(featuredBusinessModel)
                        }
                        promotionAdapter?.submitList(modelList)
                        promotionAdapter?.notifyDataSetChanged()
                        callFeaturedBusinessApi(false, isCallingFromWeekOFThisBusinessApi = true)
                    } else {
                        modelList.clear()
                        promotionAdapter?.submitList(modelList)
                        promotionAdapter?.notifyDataSetChanged()
                        callFeaturedBusinessApi(false, isCallingFromWeekOFThisBusinessApi = true)
                    }
                }
            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    modelList.clear()
                    promotionAdapter?.submitList(modelList)
                    promotionAdapter?.notifyDataSetChanged()
                    callFeaturedBusinessApi(false, isCallingFromWeekOFThisBusinessApi = true)
                }

            })
    }

    fun callBusinessApi(isRequireToShowLoader: Boolean) {
        if (activePromotionsOnly.equals("1") || subCategoryIds.isNotEmpty() || searchKey.isNotEmpty()) {
            callFeaturedBusinessApi(
                isRequireToShowLoader,
                isCallingFromWeekOFThisBusinessApi = false
            )
        } else {
            callGetFeatureBusinessOfTheWeekApi(isRequireToShowLoader)
        }
    }

    fun removeFeatureWeekBusinessFromFeatureBusiness(featurebusinessList: MutableList<FeaturedBusiness>): MutableList<FeaturedBusiness> {
        featureWeekBusinessList?.forEachIndexed { featureWeekBusinessIndex, featureWeekBusiness ->
            var repeatedFeatureBusinessIndex = -1
            featurebusinessList.forEachIndexed { featuredBusinessIndex, featuredBusiness ->
                if (featureWeekBusiness.businessPlaceId.equals(featuredBusiness.businessPlaceId)) {
                    repeatedFeatureBusinessIndex = featuredBusinessIndex
                }
            }
            if (repeatedFeatureBusinessIndex != -1) {
                featurebusinessList.removeAt(repeatedFeatureBusinessIndex)
            }

        }
        return featurebusinessList

    }


}


