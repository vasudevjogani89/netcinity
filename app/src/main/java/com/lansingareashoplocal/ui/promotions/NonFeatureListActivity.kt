package com.lansingareashoplocal.ui.promotions

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.databinding.ItemNonFeatureBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.model.NonFeature
import com.lansingareashoplocal.ui.tempareture.TemperatureActivity
import kotlinx.android.synthetic.main.activity_non_feature.*
import kotlinx.android.synthetic.main.toolbar_with_weather_details.view.*
import kotlin.math.roundToInt

class NonFeatureListActivity : BaseActivity() {

    var nonFeatureAdapter: GenericAdapter<NonFeature, ItemNonFeatureBinding>? = null
    val nonFeatureList = mutableListOf<NonFeature>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_non_feature)
        val nonFeature = intent.getParcelableExtra(AppConstants.SHARE_KEY) as NonFeature
        nonFeatureList.add(nonFeature)
        setAdapter()
        updateTemperature()
        nonFeatureToolbar.tvLeft.setOnClickListener {
            finish()
        }
      //  nonFeatureToolbar.ivLeft.setImageDrawable(this@NonFeatureListActivityCompat.getDrawable(this@NonFeatureListActivity,R.drawable.back_icon))
        nonFeatureToolbar.rlTemperature.setOnClickListener {
            startActivity(Intent(this@NonFeatureListActivity,TemperatureActivity::class.java))
        }


    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(this@NonFeatureListActivity)
        rvNonFeature.layoutManager = layoutManager
        nonFeatureAdapter =
            object : GenericAdapter<NonFeature, ItemNonFeatureBinding>(nonFeatureList) {
                override fun getLayoutId(): Int {
                    return R.layout.item_non_feature
                }

                override fun onBindData(
                    model: NonFeature?,
                    position: Int,
                    dataBinding: ItemNonFeatureBinding?
                ) {
                    dataBinding?.tvCommunityName?.text = model?.placeName
                    dataBinding?.tvCommunityAddress?.text = model?.address
                    dataBinding?.tvPhoneNumber?.text = model?.phone
                    if(position==nonFeatureList.size-1){
                        dataBinding?.btnListYourBusiness?.visibility=View.VISIBLE
                    }
                    else{
                        dataBinding?.btnListYourBusiness?.visibility=View.INVISIBLE
                    }
                    dataBinding?.btnListYourBusiness?.setOnClickListener {
                        CommonMethods.openCustomtab(this@NonFeatureListActivity,CommonMethods.business_link)
                    }

                    dataBinding?.rlpromotionAddress?.setOnClickListener {
                        if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)){
                            var geoUri: String? = null
                            if (CommonMethods.getDataFromSharePreference(
                                    this@NonFeatureListActivity,
                                    AppConstants.LATITUDE
                                ).isNotEmpty() && CommonMethods.getDataFromSharePreference(
                                    this@NonFeatureListActivity,
                                    AppConstants.LONGITUDE
                                ).isNotEmpty()
                            ) {
                                geoUri =
                                    "http://maps.google.com/maps?saddr=${CommonMethods.getDataFromSharePreference(this@NonFeatureListActivity, AppConstants.LATITUDE)},${CommonMethods.getDataFromSharePreference(this@NonFeatureListActivity, AppConstants.LONGITUDE)}&daddr=${model?.latitude},${model?.longitude}"
                            } else {
                                geoUri =
                                    "http://maps.google.com/maps?q=loc:${model?.latitude},${model?.longitude})"
                            }

                            if (model?.latitude?.isNotEmpty()!! && model?.longitude.isNotEmpty()) {
                                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
                                try {
                                    intent.setPackage("com.google.android.apps.maps");
                                    this@NonFeatureListActivity.startActivity(intent)
                                } catch (e: Exception) {
                                    Toast.makeText(
                                        this@NonFeatureListActivity,
                                        "Invalid link",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    e.printStackTrace()
                                }
                            } else {
                                Toast.makeText(
                                    this@NonFeatureListActivity,
                                    "Invalid",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }

                }
            }
        rvNonFeature.adapter = nonFeatureAdapter


    }


    fun updateTemperature() {
        if (CommonMethods.getFloatValve(
               this@NonFeatureListActivity,
                AppConstants.CURRENT_TEMPERATURE
            ) != 0.0F
        ) {
            nonFeatureToolbar.tvTemparature.text = "${CommonMethods.getFloatValve(this@NonFeatureListActivity, AppConstants.CURRENT_TEMPERATURE).roundToInt()}${getString(R.string.degree)}"
            Glide.with(this@NonFeatureListActivity).load(
                "${AppConstants.WETHER_ICON_URL}${CommonMethods.getDataFromSharePreference(
                    this@NonFeatureListActivity,
                    AppConstants.CURRENT_WEATHER_ICON_ID
                )}@2x.png"
            )
                .into(nonFeatureToolbar.ivCloud)

        }
    }
}