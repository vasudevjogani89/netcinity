package com.lansingareashoplocal.ui.communityevents

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.CalendarContract
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.home.HomeActivity
import com.lansingareashoplocal.ui.model.CommunityEvents
import com.lansingareashoplocal.utility.helper.TimeUtils
import kotlinx.android.synthetic.main.fragment_community_event_details.*
import org.jetbrains.anko.ctx
import java.util.*
import kotlin.collections.HashMap
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.single.PermissionListener
import com.karumi.dexter.Dexter
import android.provider.Settings
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.karumi.dexter.listener.*
import com.lansingareashoplocal.utility.helper.logs.Log
import kotlinx.android.synthetic.main.tool_bar_with_out_bg.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.okButton
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class CommunityEventDetailsActivity() :
    BaseActivity() {
    var mimeType = "text/html;charset=UTF-8"
    var encoding = "utf-8"
    var communityEvents: CommunityEvents? = null
    var is_from_push_notification = false


    var eventId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_community_event_details)
        val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        is_from_push_notification =
            intent.getBooleanExtra(AppConstants.IS_NEED_TO_CALL_API, false)


        if (is_from_push_notification) {
            eventId = intent.getStringExtra(AppConstants.EVENT_ID)
            callEventDetailsAi()
        } else {
            rlEventMain.visibility = View.VISIBLE
            communityEvents = intent.getSerializableExtra("details") as CommunityEvents
            setData()
            callEventViewApi()

        }
        llAddress.setOnClickListener {
            if (communityEvents != null) {
                var geoUri: String? = null

                if (CommonMethods.getDataFromSharePreference(
                        this@CommunityEventDetailsActivity,
                        AppConstants.LATITUDE
                    ).isNotEmpty() && CommonMethods.getDataFromSharePreference(
                        this@CommunityEventDetailsActivity,
                        AppConstants.LONGITUDE
                    ).isNotEmpty()
                ) {
                    geoUri =
                        "http://maps.google.com/maps?saddr=${CommonMethods.getDataFromSharePreference(this@CommunityEventDetailsActivity, AppConstants.LATITUDE)},${CommonMethods.getDataFromSharePreference(this@CommunityEventDetailsActivity, AppConstants.LONGITUDE)}&daddr=${communityEvents?.eventLatitude},${communityEvents?.eventLongitude}"
                } else {
                    geoUri =
                        "http://maps.google.com/maps?q=loc:${communityEvents!!.eventLatitude},${communityEvents!!.eventLongitude})"
                }

                if (communityEvents!!.eventLatitude.isNotEmpty() && communityEvents!!.eventLongitude.isNotEmpty()) {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
                    try {
                        intent.setPackage("com.google.android.apps.maps");
                        startActivity(intent)
                    } catch (e: Exception) {
                        Toast.makeText(
                            this@CommunityEventDetailsActivity,
                            "Invalid link",
                            Toast.LENGTH_SHORT
                        ).show()
                        e.printStackTrace()
                    }
                } else {
                    Toast.makeText(
                        this@CommunityEventDetailsActivity,
                        "Invalid",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        }
        rlAddEvent.setOnClickListener {
            Dexter.withActivity(this@CommunityEventDetailsActivity)
                .withPermission(Manifest.permission.WRITE_CALENDAR)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        Log.d("onPermissionGranted", response.toString())
                        // addEventToCalender()
                        addEventToCalenderNew()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        if (!response.isPermanentlyDenied) {
                            alert(CommonMethods.getCalenderPermissionAlert(this@CommunityEventDetailsActivity)) {
                                cancelButton {
                                }
                                okButton {
                                    openSettings()
                                }
                            }.show()
                        }


                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest,
                        token: PermissionToken
                    ) {
                        token.continuePermissionRequest();
                        Log.d("onPermissionRationaleShouldBeShown", "")

                    }
                })
                .withErrorListener(object : PermissionRequestErrorListener {
                    override fun onError(error: DexterError?) {
                        Log.d("error", error.toString())
                    }
                })
                .check()

        }
        ivShare.setOnClickListener {
            shareEventDetails()
        }


    }


    private fun shareEventDetails() {
        Glide.with(this@CommunityEventDetailsActivity).asBitmap().load(communityEvents?.eventImage)
            .into(object : SimpleTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    val intent = Intent(Intent.ACTION_SEND)
                    intent.type = "image/*"
                    intent.action = Intent.ACTION_SEND
                    intent.putExtra(
                        Intent.EXTRA_TEXT,
                        "Come and join us in the event!\n\n${communityEvents?.eventTitle}\n${communityEvents?.eventLocation}\n${TimeUtils.convertDateWithTimeForShowingApp(
                            communityEvents?.eventDateTime
                        )}\n${CommonMethods.getShareAppText(this@CommunityEventDetailsActivity)}"
                    )
                    intent.putExtra(Intent.EXTRA_STREAM, getBitmapFromView(resource))
                    startActivity(Intent.createChooser(intent, "Share Image"))
                }


            })

    }


    fun getBitmapFromView(bmp: Bitmap?): Uri? {
        var bmpUri: Uri? = null
        try {
            val file = File(externalCacheDir, System.currentTimeMillis().toString() + ".jpg")

            val out = FileOutputStream(file)
            bmp?.compress(Bitmap.CompressFormat.JPEG, 90, out)
            out.close()
            bmpUri = Uri.fromFile(file)

        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri
    }


    private fun checkEventDateIsFeatured() {
        val eventDate = CommonMethods.getDateInMillis(communityEvents?.event_start_date!!)
        val cal = Calendar.getInstance();
        if (eventDate > cal.timeInMillis) {
            rlAddEvent.visibility = View.VISIBLE
        } else {
            rlAddEvent.visibility = View.GONE
        }

    }


    fun setData() {
        if (communityEvents != null) {
            cummunityEventDetailstoolBar.tvLeft.setOnClickListener {
                if (isTaskRoot) {
                    startActivity(Intent(this@CommunityEventDetailsActivity, HomeActivity::class.java))
                    finish()
                } else {
                    finish()
                }
            }

            tvCommunityEventName.text = communityEvents?.eventTitle
            tvCommunityEventsAddress.text = communityEvents?.eventLocation
            Glide.with(this@CommunityEventDetailsActivity).load(communityEvents?.eventImage)
                .into(ivCommunityLogo)
            tvCommunityEventDate.text =
                "${TimeUtils.convertDateWithTimeForShowingApp(communityEvents?.event_start_date)} - ${TimeUtils.convertDateWithTimeForShowingApp(communityEvents?.event_end_date)}"
            var text =
                "<html><head>" + "<style type=\"text/css\">@font-face {font-family: poppins;src: url(\"file:///android_asset/museosans_300.otf\")}body{font-family: poppins;color: #21242c;font-size: 17px}" + "</style></head>" + "<body>" + communityEvents?.eventDescription + "</body></html>";
            tvCommunityEventDetails.loadDataWithBaseURL(null, text, mimeType, encoding, null);


            if (communityEvents?.eventLocation?.isEmpty()!!) {
                llAddress.visibility = View.GONE
            }
            if (communityEvents?.eventDateTime?.isEmpty()!!)
                llCommunityDate.visibility = View.GONE
        }
        checkEventDateIsFeatured()
    }


    private fun callEventDetailsAi() {

        var inputParams = HashMap<String, String>()
        inputParams.put(AppConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputParams.put(AppConstants.EVENT_ID, eventId)
        var call = ApiClient.getRetrofitInterface()?.eventsDetail(inputParams)


        WSClient<Response<List<CommunityEvents>>>().request(
            this@CommunityEventDetailsActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<CommunityEvents>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<CommunityEvents>>
                ) {

                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        rlEventMain.visibility = View.VISIBLE
                        communityEvents = mResponse.data?.get(0)
                        setData()
                    }
                }
            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                    alert(message) {
                        okButton { }
                    }.show()
                }
            })


    }


    @SuppressLint("MissingPermission")
    private fun addEventToCalender() {


        val cr = ctx.getContentResolver()
        val values = ContentValues()
        if (communityEvents?.eventDateTime!!.isNotEmpty()) {
            val eventTime = CommonMethods.getDateInMillis(communityEvents?.eventDateTime!!)
            if (eventTime != 0L) {
                values.put(CalendarContract.Events.DTSTART, eventTime)
            }
        }
        values.put(CalendarContract.Events.TITLE, communityEvents!!.eventTitle)
        values.put(CalendarContract.Events.DESCRIPTION, communityEvents!!.eventDescription)
        values.put(CalendarContract.Events.EVENT_LOCATION, communityEvents!!.eventLocation)

        val timeZone = TimeZone.getDefault()
        values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID())

// Default calendar
        values.put(CalendarContract.Events.CALENDAR_ID, 1)

        // values.put(CalendarContract.Events.RRULE, "FREQ=DAILY;UNTIL=$dtUntill")
// Set Period for 1 Hour
        values.put(CalendarContract.Events.DURATION, "+P1H")

        values.put(CalendarContract.Events.HAS_ALARM, 1)

// Insert event to calendar


        try {
            val uri = cr.insert(CalendarContract.Events.CONTENT_URI, values)
        } catch (e: Exception) {
            alert(e.toString()) {
                okButton { }
            }.show()
        }

        //val eventId = uri!!.lastPathSegment!!.toLong()
        alert("Event added succesfully") {
            okButton { }
        }.show()
    }


    @SuppressLint("MissingPermission")
    private fun addEventToCalenderNew() {
        val calID: Long = 1
        val timeZone = TimeZone.getDefault()
        if (communityEvents?.eventDateTime!!.isNotEmpty()) {
            val eventTime = CommonMethods.getDateInMillis(communityEvents?.eventDateTime!!)
            if (eventTime != 0L) {
                val values = ContentValues().apply {
                    put(CalendarContract.Events.DTSTART, eventTime)
                    put(
                        CalendarContract.Events.DTEND,
                        eventTime + AppConstants.ONE_HOUR_IN_MILLI_SEC
                    )
                    put(CalendarContract.Events.TITLE, communityEvents!!.eventTitle)
                    put(CalendarContract.Events.DESCRIPTION, communityEvents!!.eventDescription)
                    put(CalendarContract.Events.EVENT_LOCATION, communityEvents!!.eventLocation)
                    put(CalendarContract.Events.CALENDAR_ID, calID)
                    put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID())
                }
                try {
                    val uri: Uri? =
                        contentResolver.insert(CalendarContract.Events.CONTENT_URI, values)
                    val eventId = uri?.lastPathSegment!!.toLong()
                    if (eventId != 0L) {
                        alert("Event added succesfully") {
                            okButton { }
                        }.show()
                    }
                } catch (e: Exception) {

                }


            }
        }

    }


    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivity(intent)
    }

    override fun onBackPressed() {
        if (isTaskRoot) {
            startActivity(Intent(this@CommunityEventDetailsActivity, HomeActivity::class.java))
            finish()
        } else {
            finish()
        }
    }

    private fun callEventViewApi() {
        val inputParams = HashMap<String, String>()
        inputParams.put(AppConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputParams.put(AppConstants.EVENT_ID, communityEvents?.eventId!!)
        val call = ApiClient.getRetrofitInterface()?.eventsDetail(inputParams)


        WSClient<Response<List<CommunityEvents>>>().request(
            this@CommunityEventDetailsActivity,
            100,
            false,
            call,
            object : ISuccessHandler<Response<List<CommunityEvents>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<CommunityEvents>>
                ) {


                }
            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                }
            })


    }





}