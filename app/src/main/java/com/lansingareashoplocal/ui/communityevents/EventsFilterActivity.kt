package com.lansingareashoplocal.ui.communityevents

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.model.EventCategory
import com.lansingareashoplocal.utility.MyDatePickerDialog
import kotlinx.android.synthetic.main.activity_events_filter.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import java.util.*
import com.zhy.view.flowlayout.FlowLayout
import com.zhy.view.flowlayout.TagAdapter
import com.zhy.view.flowlayout.TagFlowLayout
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.*
import org.jetbrains.anko.collections.forEachReversedWithIndex
import java.text.SimpleDateFormat


class EventsFilterActivity : BaseActivity() {


    // var eventCategoryAdapter: GenericAdapter<EventCategory, ItemEventCategoryBinding>? = null
    var eventCategoryList = mutableListOf<EventCategory>()
    lateinit var dateFormate: SimpleDateFormat
    var categoriesIdList: List<String>? = null
    var timeType = ""
    var tagAdapter: TagAdapter<EventCategory>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_events_filter)
        dateFormate = SimpleDateFormat(AppConstants.ONLY_DATE_FORMATE_APP)
        callEventCategoryApi()

        tvStartDate.setOnClickListener {

            var seletedDate = ""
            if (!tvStartDate.equals(getString(R.string.date_hint))) {
                seletedDate = tvStartDate.text.toString()
            }


            val myDatePickerDialog = MyDatePickerDialog()
            myDatePickerDialog.showDatePickerWithSelectedDate(
                this@EventsFilterActivity,
                object : MyDatePickerDialog.DateSelection {
                    override fun getDate(date: String) {
                        if (date.isNotEmpty()) {
                            tvStartDate.text = date
                            resetDateSeletion()
                            timeType = ""

                            if (tvEndDate.text.equals(getString(R.string.date_hint))) {

                            } else {
                                if (CommonMethods.getDateOnlyInMillis(tvEndDate.text.toString()) < CommonMethods.getDateOnlyInMillis(
                                        date
                                    )
                                ) {
                                    tvEndDate.text = getString(R.string.date_hint)
                                }

                            }

                        }

                    }


                }, 0, CommonMethods.getDateOnlyInMillis(seletedDate)
            )

        }

        tvEndDate.setOnClickListener {
            var minDate = ""
            if (tvStartDate.equals(getString(R.string.date_hint))) {
                minDate = dateFormate.format(Calendar.getInstance().time)
            } else {
                minDate = tvStartDate.text.toString()
            }
            var selectedDate = ""
            if (!tvEndDate.equals(getString(R.string.date_hint))) {
                selectedDate = tvEndDate.text.toString()
            }
            val myDatePickerDialog = MyDatePickerDialog()
            myDatePickerDialog.showDatePickerWithSelectedDate(
                this@EventsFilterActivity,
                object : MyDatePickerDialog.DateSelection {
                    override fun getDate(date: String) {
                        if (date.isNotEmpty()) {
                            tvEndDate.text = date
                            resetDateSeletion()
                            timeType = ""
                        }

                    }


                },
                CommonMethods.getDateOnlyInMillis(minDate),
                CommonMethods.getDateOnlyInMillis(selectedDate)
            )
        }

        tvToday.setOnClickListener {
            timeType = getString(R.string.today)
            setTodayButtonAsSelected()
            tvEndDate.text = dateFormate.format(Calendar.getInstance().time)
            setMinimumDataAsToday()

        }

        tvThisWeek.setOnClickListener {
            timeType = getString(R.string.this_weak)
            setMinimumDataAsToday()
            setThisWeekButtonAsSelected()
            val c = Calendar.getInstance()
            c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
            c.set(Calendar.HOUR_OF_DAY, 0)
            c.set(Calendar.MINUTE, 0)
            c.set(Calendar.SECOND, 0)
            c.add(Calendar.DATE, 7)
            tvEndDate.text = dateFormate.format(c.time)
        }
        tvThisMonth.setOnClickListener {
            timeType = getString(R.string.this_month)
            setMinimumDataAsToday()
            val c = Calendar.getInstance()
            setThisMonthButtonAsSeleted()
            c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH))
            c.set(Calendar.HOUR_OF_DAY, 0)
            c.set(Calendar.MINUTE, 0)
            c.set(Calendar.SECOND, 0)

            tvEndDate.text = dateFormate.format(c.time)


        }
        tvLeft.setOnClickListener {
            finish()
        }

        setToolBar()

        tvRight.setOnClickListener {
            resetTheFilter()
        }
        btnApply.setOnClickListener {
            var selectCategories = ""
            val intent = Intent()
            if (!tvEndDate.text.equals(getString(R.string.date_hint))) {
                intent.putExtra(ApiConstants.TO_DATE, tvEndDate.text)
            }
            if (!tvStartDate.text.equals(getString(R.string.date_hint))) {
                intent.putExtra(ApiConstants.FROM_DATE, tvStartDate.text)
            }
            if (timeType.isNotEmpty()) {
                intent.putExtra(AppConstants.TIMETYPE, timeType)
            }

            eventCategoryList.forEachIndexed { index, eventCategory ->
                if (eventCategory.isSelected) {
                    selectCategories += ",${eventCategory.category_id}"
                }
            }
            if (!selectCategories.isEmpty()) {
                selectCategories = selectCategories.removePrefix(",")
                intent.putExtra(ApiConstants.CATEGORY_ID, selectCategories)
            }
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
        if (intent != null) {
            if (intent.getStringExtra(ApiConstants.FROM_DATE).isNotEmpty()) {
                tvStartDate.text = intent.getStringExtra(ApiConstants.FROM_DATE)
            } else {
                tvStartDate.text = getString(R.string.date_hint)
            }
            if (intent.getStringExtra(ApiConstants.TO_DATE).isNotEmpty()) {
                tvEndDate.text = intent.getStringExtra(ApiConstants.TO_DATE)
            } else {
                tvEndDate.text = getString(R.string.date_hint)
            }
            if (intent.getStringExtra(ApiConstants.CATEGORY_ID).isNotEmpty()) {
                var categoriesId = intent.getStringExtra(ApiConstants.CATEGORY_ID)
                categoriesIdList = categoriesId.split(",").map { it.trim() }


            }


            if (intent.getStringExtra(AppConstants.TIMETYPE).isNotEmpty()) {
                timeType = intent.getStringExtra(AppConstants.TIMETYPE)
                if (timeType.equals(getString(R.string.today))) {
                    setTodayButtonAsSelected()
                } else if (timeType.equals(getString(R.string.this_weak))) {
                    setThisWeekButtonAsSelected()
                } else if (timeType.equals(getString(R.string.this_month))) {
                    setThisMonthButtonAsSeleted()

                }

            }
        }


    }

    private fun setToolBar() {
        tvRight.text = getString(R.string.reset)
        tvRight.visibility=View.VISIBLE
        tvTitle.text = getString(R.string.filter)
        tvLeft.text=""
        tvLeft.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_close_popup,0,0,0)
    }

    private fun resetTheFilter() {
        timeType = ""
        clearDatePicker()
        resetDateSeletion()
        eventCategoryList.forEachIndexed { index, eventCategory ->
            eventCategory.isSelected = false
        }
        tagAdapter?.notifyDataChanged()
    }

    private fun callEventCategoryApi() {

        var call = ApiClient.getRetrofitInterface()?.getEventCategory()
        WSClient<Response<List<EventCategory>>>().request(
            this@EventsFilterActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<EventCategory>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<EventCategory>>
                ) {
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        eventCategoryList = mResponse.data as MutableList<EventCategory>

                        //eventCategoryAdapter?.setData(eventCategoryList)
                        eventCategoryList.forEachReversedWithIndex { i, eventCategory ->
                            categoriesIdList?.forEachIndexed { index, s ->
                                if (eventCategory.category_id.equals(s)) {
                                    eventCategoryList.get(i).isSelected = true
                                }
                            }
                        }
                        setUpEventCategoryAdapter()

                    }
                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert(message) { okButton { } }.show()
                }

            })

    }

    private fun setUpEventCategoryAdapter() {

        /* var gridLayoutManager = GridLayoutManager(this@EventsFilterActivity, 2)
         rvEventCategory.layoutManager = gridLayoutManager

         eventCategoryAdapter =
             object : GenericAdapter<EventCategory, ItemEventCategoryBinding>(eventCategoryList) {
                 override fun getLayoutId(): Int {
                     return R.layout.item_event_category
                 }

                 override fun onBindData(
                     model: EventCategory?,
                     position: Int,
                     dataBinding: ItemEventCategoryBinding?
                 ) {
                     dataBinding?.tvEventCategotyName?.text = model?.category



                     if (model?.isSelected!!) {
                         dataBinding?.tvEventCategotyName?.setBackgroundResource(R.drawable.corner_radius_with_primary_color)
                         dataBinding?.tvEventCategotyName?.setTextColor(
                             ContextCompat.getColor(
                                 this@EventsFilterActivity,
                                 R.color.colorWhite
                             )
                         )
                     } else {
                         dataBinding?.tvEventCategotyName?.setBackgroundResource(R.drawable.corner_radius_white_bg)
                         dataBinding?.tvEventCategotyName?.setTextColor(
                             ContextCompat.getColor(
                                 this@EventsFilterActivity,
                                 R.color.colorBlack
                             )
                         )
                     }

                     dataBinding?.tvEventCategotyName?.setOnClickListener {
                         eventCategoryList.get(position).isSelected =
                             !eventCategoryList.get(position).isSelected
                         notifyDataSetChanged()
                     }
                 }
             }
         rvEventCategory.adapter = eventCategoryAdapter



 */
        tagAdapter = object : TagAdapter<EventCategory>(eventCategoryList) {
            override fun getView(parent: FlowLayout, position: Int, s: EventCategory): View {
                val tv = LayoutInflater.from(this@EventsFilterActivity).inflate(
                    R.layout.item_event_category,
                    id_flowlayout,
                    false
                ) as AppCompatTextView
                tv.text = s.category
                return tv
            }

            override fun setSelected(position: Int, eventCategory: EventCategory?): Boolean {
                return eventCategory?.isSelected!!
            }
        }

        id_flowlayout.setAdapter(tagAdapter)
        id_flowlayout.setOnTagClickListener(TagFlowLayout.OnTagClickListener { view, position, parent ->
            eventCategoryList.get(position).isSelected =
                !eventCategoryList.get(position).isSelected
            tagAdapter?.notifyDataChanged()

            true
        })


    }


    private fun setAllBlackText() {
        tvToday.setTextColor(
            ContextCompat.getColor(
                this@EventsFilterActivity,
                R.color.colorBlack
            )
        )
        tvThisWeek.setTextColor(
            ContextCompat.getColor(
                this@EventsFilterActivity,
                R.color.colorBlack
            )
        )
        tvThisMonth.setTextColor(
            ContextCompat.getColor(
                this@EventsFilterActivity,
                R.color.colorBlack
            )
        )
    }

    private fun setAllBackgroundWhite() {
        tvThisMonth.setBackgroundColor(
            ContextCompat.getColor(
                this@EventsFilterActivity,
                R.color.colorWhite
            )
        )
        tvThisWeek.setBackgroundColor(
            ContextCompat.getColor(
                this@EventsFilterActivity,
                R.color.colorWhite
            )
        )
        tvToday.setBackgroundColor(
            ContextCompat.getColor(
                this@EventsFilterActivity,
                R.color.colorWhite
            )
        )
    }


    private fun setTodayButtonAsSelected() {
        setAllBackgroundWhite()
        tvToday.setBackgroundColor(
            ContextCompat.getColor(
                this@EventsFilterActivity,
                R.color.colorPrimary
            )
        )
        setAllBlackText()
        tvToday.setTextColor(
            ContextCompat.getColor(
                this@EventsFilterActivity,
                R.color.colorWhite
            )
        )
    }

    private fun setThisWeekButtonAsSelected() {
        setAllBackgroundWhite()
        tvThisWeek.setBackgroundColor(
            ContextCompat.getColor(
                this@EventsFilterActivity,
                R.color.colorPrimary
            )
        )
        setAllBlackText()
        tvThisWeek.setTextColor(
            ContextCompat.getColor(
                this@EventsFilterActivity,
                R.color.colorWhite
            )
        )
    }


    private fun setThisMonthButtonAsSeleted() {
        setAllBackgroundWhite()
        tvThisMonth.setBackgroundColor(
            ContextCompat.getColor(
                this@EventsFilterActivity,
                R.color.colorPrimary
            )
        )
        setAllBlackText()
        tvThisMonth.setTextColor(
            ContextCompat.getColor(
                this@EventsFilterActivity,
                R.color.colorWhite
            )
        )
    }


    private fun resetDateSeletion() {
        setAllBackgroundWhite()
        setAllBlackText()
    }

    private fun clearDatePicker() {
        tvEndDate.text = getString(R.string.date_hint)
        tvStartDate.text = getString(R.string.date_hint)
    }


    private fun setMinimumDataAsToday() {
        val c = Calendar.getInstance()
        tvStartDate.text = dateFormate.format(c.time)
    }
}