package com.lansingareashoplocal.ui.communityevents

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.DialogSortBinding
import com.lansingareashoplocal.databinding.ItemSortingBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.interfaces.CommonCallBack
import com.lansingareashoplocal.ui.model.SortModel

class DialogSort(var context: Context ,var sortType:String="", var commonCallBack: CommonCallBack) {


    lateinit var sortDailog: Dialog
    lateinit var binding: DialogSortBinding

    var sortAdapter: GenericAdapter<SortModel, ItemSortingBinding>? = null
    var sortingList: MutableList<SortModel>?=null




    fun showSortAlert() {
        sortDailog = Dialog(context, R.style.MyAlertDialogStyle)
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_sort,
            null,
            false
        )
        sortDailog.setContentView(binding.getRoot())
        sortDailog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        prepareSortList()
        setSortingAdapter()
        sortDailog.show()
         binding.ivCancel.setOnClickListener {
             sortDailog.dismiss()
         }
        binding.btnApply.setOnClickListener {
            commonCallBack.commonCallBack(sortType,"")
            sortDailog.dismiss()
        }

    }

    private fun prepareSortList() {

        val jsonFileString = CommonMethods.getJsonDataFromAsset(context, "sorting.json")


        val gson = Gson()
        val listPersonType = object : TypeToken<List<SortModel>>() {}.type
         sortingList = gson.fromJson(jsonFileString, listPersonType)






    }




    fun setSortingAdapter() {
        sortAdapter = object : GenericAdapter<SortModel, ItemSortingBinding>(sortingList) {
            override fun getLayoutId(): Int {
                return R.layout.item_sorting
            }

            override fun onBindData(
                model: SortModel?,
                position: Int,
                dataBinding: ItemSortingBinding?
            ) {

                dataBinding?.rbSortingName?.text = model?.sort_name

                if(sortType.equals(model?.sort_type)){
                    model!!.isSelected =true
                }
                dataBinding?.rbSortingName?.isChecked = model!!.isSelected
                dataBinding?.rbSortingName?.setOnClickListener {


                    sortingList?.forEachIndexed { index, sortModel ->
                        sortingList?.get(index)?.isSelected = false
                    }
                    sortingList?.get(position)?.isSelected = true
                    sortType=model.sort_type
                    notifyDataSetChanged()
                }

            }

        }
        binding.rvSorting.adapter=sortAdapter


    }
}