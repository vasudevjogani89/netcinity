package com.lansingareashoplocal.ui.communityevents

import ApiClient
import ApiConstants
import AppConstants
import CommonMethods
import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.databinding.ItemCummunityEventsBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.home.HomeActivity
import com.lansingareashoplocal.ui.interfaces.CommonCallBack
import com.lansingareashoplocal.ui.model.CommunityEvents
import com.lansingareashoplocal.ui.tempareture.TemperatureActivity
import com.lansingareashoplocal.utility.helper.TimeUtils
import com.lansingareashoplocal.utility.helper.customclass.CustomScrollListener
import com.shrikanthravi.collapsiblecalendarview.data.Event
import com.shrikanthravi.collapsiblecalendarview.view.OnSwipeTouchListener
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import kotlinx.android.synthetic.main.fragment_community_event_list.*
import kotlinx.android.synthetic.main.toolbar_with_weather_details.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAdjusters
import java.util.*
import kotlin.collections.HashMap
import kotlin.math.roundToInt

class CommunityEventListActivity : BaseActivity(), CommonCallBack, View.OnClickListener {


    var communityEventList = mutableListOf<CommunityEvents>()
    var communityEventsAdapter: GenericAdapter<CommunityEvents, ItemCummunityEventsBinding>? = null


    var communityEventsByDateAdapter: GenericAdapter<CommunityEvents, ItemCummunityEventsBinding>? =
        null
    var communityEventListByDates = mutableListOf<CommunityEvents>()
    private var hasNext = false
    private var pageCount = 0
    var currentPage = 1
    var sortByType = ""
    var fromDate = ""
    var toDate = ""
    var categoryId = ""
    var timeType = ""


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_community_event_list)

        //  communityEventToolbar.ivLeft.setDrawa(resources.getDrawable(R.drawable.back_icon))
        communityEventToolbar.tvLeft.setOnClickListener {
            if (isTaskRoot) {
                finish()
                startActivity(Intent(this@CommunityEventListActivity, HomeActivity::class.java))
            } else {
                finish()
            }

        }
        setCummunity()
        communityEventToolbar.rlTemperature.setOnClickListener {
            startActivity(Intent(this@CommunityEventListActivity, TemperatureActivity::class.java))
        }
        calleventListApi(true)
        swipeRefreshEventList.setOnRefreshListener {
            currentPage = 1
            hasNext = false
            calleventListApi(false)
        }
        val layoutManager = LinearLayoutManager(this@CommunityEventListActivity)
        rvCommunityEventList.layoutManager = layoutManager
        rvCommunityEventList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager != null && layoutManager.findLastCompletelyVisibleItemPosition() == layoutManager.itemCount - 1) {
                    if (hasNext) {
                        hasNext = false
                        rvProgressLayout?.visibility = View.VISIBLE
                        currentPage += 1
                        calleventListApi(false)
                    }
                }
            }

        })
        if (CommonMethods.getFloatValve(
                this@CommunityEventListActivity,
                AppConstants.CURRENT_TEMPERATURE
            ) != 0.0F
        ) {
            communityEventToolbar.tvTemparature.text = "${CommonMethods.getFloatValve(
                this@CommunityEventListActivity,
                AppConstants.CURRENT_TEMPERATURE
            ).roundToInt()}${getString(R.string.degree)}"

            Glide.with(this@CommunityEventListActivity).load(
                "${AppConstants.WETHER_ICON_URL}${CommonMethods.getDataFromSharePreference(
                    this@CommunityEventListActivity,
                    AppConstants.CURRENT_WEATHER_ICON_ID
                )}@2x.png"
            ).into(communityEventToolbar.ivCloud)

        }

        llSort.setOnClickListener {
            val dialogSort = DialogSort(this@CommunityEventListActivity, sortByType, this)
            dialogSort.showSortAlert()
        }
        llFilter.setOnClickListener {
            val intent = Intent(this@CommunityEventListActivity, EventsFilterActivity::class.java)
            intent.putExtra(ApiConstants.FROM_DATE, fromDate)
            intent.putExtra(ApiConstants.TO_DATE, toDate)
            intent.putExtra(ApiConstants.CATEGORY_ID, categoryId)
            intent.putExtra(AppConstants.TIMETYPE, timeType)
            startActivityForResult(intent, 100)

        }
        btnListYourEvent.setOnClickListener {
            CommonMethods.openCustomtab(this@CommunityEventListActivity, CommonMethods.event_link)
        }
        ivCalenderAndList.setOnClickListener(this)

        collapsibleCalendar.setOnTouchListener(object :
            OnSwipeTouchListener(this@CommunityEventListActivity) {
            override fun onSwipeRight() {
                collapsibleCalendar.nextDay()
            }

            override fun onSwipeLeft() {
                collapsibleCalendar.prevDay()
            }

            override fun onSwipeTop() {
                if (collapsibleCalendar.expanded) {
                    collapsibleCalendar.collapse(100)
                }
            }

            override fun onSwipeBottom() {
                if (!collapsibleCalendar.expanded) {
                    collapsibleCalendar.expand(100)
                }
            }
        })
        collapsibleCalendar.expand(0)


        //To hide or show expand icon
        collapsibleCalendar.setExpandIconVisible(false)
        collapsibleCalendar.params = CollapsibleCalendar.Params(0, 3650)
        collapsibleCalendar.setCalendarListener(object : CollapsibleCalendar.CalendarListener {
            override fun onDayChanged() {

            }

            override fun onClickListener() {
                if (collapsibleCalendar.expanded) {
                    collapsibleCalendar.collapse(200)
                } else {
                    collapsibleCalendar.expand(200)
                }
            }

            override fun onDaySelect() {
                var date: String = ""
                if ((collapsibleCalendar.selectedDay!!.month) + 1 < 10 || (collapsibleCalendar.selectedDay!!.day) < 10) {
                    if ((collapsibleCalendar.selectedDay!!.month) + 1 < 10 && !((collapsibleCalendar.selectedDay!!.day) < 10)) {
                        date =
                            (collapsibleCalendar.selectedDay!!.year).toString().plus("-").plus("0")
                                .plus((collapsibleCalendar.selectedDay!!.month) + 1).plus("-")
                                .plus((collapsibleCalendar.selectedDay!!.day))
                    } else if ((collapsibleCalendar.selectedDay!!.day) < 10 && !((collapsibleCalendar.selectedDay!!.month) + 1 < 10)) {
                        date = (collapsibleCalendar.selectedDay!!.year).toString().plus("-")
                            .plus((collapsibleCalendar.selectedDay!!.month) + 1).plus("-").plus("0")
                            .plus((collapsibleCalendar.selectedDay!!.day))
                    } else {
                        date =
                            (collapsibleCalendar.selectedDay!!.year).toString().plus("-").plus("0")
                                .plus((collapsibleCalendar.selectedDay!!.month) + 1).plus("-")
                                .plus("0").plus((collapsibleCalendar.selectedDay!!.day))
                    }

                } else
                    date = (collapsibleCalendar.selectedDay!!.year).toString().plus("-")
                        .plus((collapsibleCalendar.selectedDay!!.month) + 1).plus("-")
                        .plus((collapsibleCalendar.selectedDay!!.day))


                calleventListByDatesApi(true, date, date)

            }

            override fun onItemClick(v: View) {

            }

            override fun onDataUpdate() {

            }

            override fun onMonthChange() {
                collapsibleCalendar.clearOldEvents()

                Log.e("fsdf", collapsibleCalendar.month.toString())
                var startDate: String = ""

                if ((collapsibleCalendar.month) + 1 < 10) {
                    startDate = (collapsibleCalendar.year).toString().plus("-0").plus((collapsibleCalendar.month) + 1).plus("-01")

                } else {
                    startDate = (collapsibleCalendar.year).toString().plus("-").plus((collapsibleCalendar.month) + 1).plus("-01")
                }

                val endDate: java.time.LocalDate = java.time.LocalDate.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd")).with(TemporalAdjusters.lastDayOfMonth())
                if(Calendar.getInstance().get(Calendar.MONTH).equals(collapsibleCalendar.month)){
                    val sdf = SimpleDateFormat("yyyy-MM-dd")
                    startDate = sdf.format(Date())
                }
                calleventListByDatesApi(true, startDate, endDate.toString())

            }

            override fun onWeekChange(position: Int) {

            }
        })

        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val currentDate = sdf.format(Date())
        val lastDayOfMonth: java.time.LocalDate = java.time.LocalDate.parse(
            currentDate,
            DateTimeFormatter.ofPattern("yyyy-MM-dd")
        )
            .with(TemporalAdjusters.lastDayOfMonth())
        calleventListByDatesApi(true, currentDate, lastDayOfMonth.toString())
        setCummunityByDates()
        showEventView()
        calenderEventListOnScroll()

    }

    private fun calenderEventListOnScroll() {
        rv_calendar_event_list.addOnScrollListener(CustomScrollListener())
        val eventsWithCalenderLayoutManager = LinearLayoutManager(this)
        rv_calendar_event_list.layoutManager = eventsWithCalenderLayoutManager
        rv_calendar_event_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> {
                        println("The RecyclerView is not scrolling")
                    }

                    RecyclerView.SCROLL_STATE_DRAGGING -> {
                        if (collapsibleCalendar.expanded) {
                            collapsibleCalendar.collapse(200)
                        }
                        println("Scrolling now")
                    }
                    RecyclerView.SCROLL_STATE_SETTLING -> println("Scroll Settling")
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (eventsWithCalenderLayoutManager != null && eventsWithCalenderLayoutManager.findFirstCompletelyVisibleItemPosition() == 0 ) {
                    if (dy > 0) {
                        System.out.println("Scrolled Downwards");
                    } else if (dy < 0) {
                        if (!collapsibleCalendar.expanded) {
                            collapsibleCalendar.expand(200)
                        }
                        System.out.println("Scrolled Upwards");
                    }


                }


            }

        })

    }


    private fun setCummunity() {
        communityEventsAdapter = object :
            GenericAdapter<CommunityEvents, ItemCummunityEventsBinding>(communityEventList) {
            override fun getLayoutId(): Int {
                return R.layout.item_cummunity_events
            }

            override fun onBindData(
                model: CommunityEvents?,
                position: Int,
                dataBinding: ItemCummunityEventsBinding?
            ) {
                if (model?.category!!.isEmpty()) {
                    dataBinding?.tvEventName?.visibility = View.GONE
                } else {
                    dataBinding?.tvEventName?.visibility = View.VISIBLE
                }
                if (model?.eventLocation.isEmpty()) {
                    dataBinding?.eventAddress?.visibility = View.GONE
                } else {
                    dataBinding?.eventAddress?.visibility = View.VISIBLE
                }
                dataBinding?.tvEventTitle?.text = model?.eventTitle
                dataBinding?.eventAddress?.text = model?.eventLocation
                dataBinding?.tvEventName?.text = model?.category
                dataBinding?.tvEventDate?.text =
                    "${TimeUtils.convertDateWithTimeForShowingApp(model?.event_start_date)} - ${TimeUtils.convertDateWithTimeForShowingApp(
                        model?.event_end_date
                    )}"



                Glide.with(this@CommunityEventListActivity).load(model?.eventImage)
                    .into(dataBinding?.ivCommunityEvent!!)
                dataBinding?.root.setOnClickListener {

                    val intent = Intent(
                        this@CommunityEventListActivity,
                        CommunityEventDetailsActivity::class.java
                    )
                    intent.putExtra("details", model as CommunityEvents)
                    startActivity(intent)
                }



                dataBinding.eventAddress.setOnClickListener {
                    val geoUri =
                        "http://maps.google.com/maps?q=loc:${model.eventLatitude},${model.eventLongitude})"
                    if (model.eventLatitude.isNotEmpty() && model.eventLongitude.isNotEmpty()) {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
                        try {
                            startActivity(intent)
                        } catch (e: Exception) {
                            Toast.makeText(
                                this@CommunityEventListActivity,
                                "Invalid link",
                                Toast.LENGTH_SHORT
                            ).show()
                            e.printStackTrace()
                        }
                    } else {
                        Toast.makeText(
                            this@CommunityEventListActivity,
                            "Invalid",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            }

        }

        rvCommunityEventList.adapter = communityEventsAdapter


    }




    private fun calleventListByDatesApi(
        isRequireShowLoader: Boolean,
        fromDate: String,
        toDate: String
    ) {
        val call = ApiClient.getRetrofitInterface()?.getCommunityEventsListCaender(
            from_date = fromDate,
            to_date = toDate,
            lat = CommonMethods.getDataFromSharePreference(
                this@CommunityEventListActivity,
                AppConstants.LATITUDE
            ),
            lon = CommonMethods.getDataFromSharePreference(
                this@CommunityEventListActivity,
                AppConstants.LONGITUDE
            ),
            community_id = CommonMethods.cummunity_id
        )
        WSClient<Response<List<CommunityEvents>>>().request(
            this@CommunityEventListActivity,
            200,
            isRequireShowLoader,
            call,
            object : ISuccessHandler<Response<List<CommunityEvents>>> {
                @SuppressLint("NewApi")
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<CommunityEvents>>
                ) {
                    CommonMethods.setDataIntoSharePreference(
                        this@CommunityEventListActivity,
                        AppConstants.Last_Month, fromDate

                    )
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        communityEventListByDates =
                            (mResponse.data as MutableList<CommunityEvents>?)!!
                        communityEventsByDateAdapter!!.removeAll()
                        communityEventsByDateAdapter?.notifyDataSetChanged()
                        communityEventsByDateAdapter?.setData(communityEventListByDates)
                        communityEventsByDateAdapter?.notifyDataSetChanged()
                        //collapsibleCalendar.clearOldEvents()
                        for (schedule in communityEventListByDates) {
                            val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                            val format2 = SimpleDateFormat("yyyy-MM-dd")
                            /*val date = format1.parse(schedule.eventDateTime)
                            val dateString = format2.format(date)
                            val dateParts: List<String> = dateString.split("-")
                            val year = dateParts[0].toInt()
                            val month = dateParts[1].toInt()-1
                            val day = dateParts[2].toInt()*/
                            val event = Event()
                            event.strDate = schedule.event_start_date
                            event.endDate = schedule.event_end_date
                            event.mColor = getColor(R.color.colorPrimary)
                            collapsibleCalendar.addEventTag(event)

                        }

                        rv_calendar_event_list.visibility = View.VISIBLE
                        noCalenderEventsTxt.visibility = View.GONE


                    } else {
                        noCalenderEventsTxt.visibility = View.VISIBLE
                        rv_calendar_event_list.visibility = View.GONE
                    }

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    CommonMethods.hideProgress()
                    CommonMethods.setDataIntoSharePreference(
                        this@CommunityEventListActivity,
                        AppConstants.Last_Month, fromDate

                    )

                }

            })


    }

    private fun setCummunityByDates() {
        communityEventsByDateAdapter = object :
            GenericAdapter<CommunityEvents, ItemCummunityEventsBinding>(communityEventListByDates) {
            override fun getLayoutId(): Int {
                return R.layout.item_cummunity_events
            }

            override fun onBindData(
                model: CommunityEvents?,
                position: Int,
                dataBinding: ItemCummunityEventsBinding?
            ) {
                if (model?.category!!.isEmpty()) {
                    dataBinding?.tvEventName?.visibility = View.GONE
                } else {
                    dataBinding?.tvEventName?.visibility = View.VISIBLE
                }
                if (model?.eventLocation.isEmpty()) {
                    dataBinding?.eventAddress?.visibility = View.GONE
                } else {
                    dataBinding?.eventAddress?.visibility = View.VISIBLE
                }
                dataBinding?.tvEventTitle?.text = model?.eventTitle
                dataBinding?.eventAddress?.text = model?.eventLocation
                dataBinding?.tvEventName?.text = model?.category
                dataBinding?.tvEventDate?.text =
                    "${TimeUtils.convertDateWithTimeForShowingApp(model?.event_start_date)} - ${TimeUtils.convertDateWithTimeForShowingApp(
                        model?.event_end_date
                    )}"
                Glide.with(this@CommunityEventListActivity).load(model?.eventImage)
                    .into(dataBinding?.ivCommunityEvent!!)
                dataBinding?.root.setOnClickListener {

                    val intent = Intent(
                        this@CommunityEventListActivity,
                        CommunityEventDetailsActivity::class.java
                    )
                    intent.putExtra("details", model as CommunityEvents)
                    startActivity(intent)
                }



                dataBinding.eventAddress.setOnClickListener {
                    val geoUri =
                        "http://maps.google.com/maps?q=loc:${model.eventLatitude},${model.eventLongitude})"
                    if (model.eventLatitude.isNotEmpty() && model.eventLongitude.isNotEmpty()) {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
                        try {
                            startActivity(intent)
                        } catch (e: Exception) {
                            Toast.makeText(
                                this@CommunityEventListActivity,
                                "Invalid link",
                                Toast.LENGTH_SHORT
                            ).show()
                            e.printStackTrace()
                        }
                    } else {
                        Toast.makeText(
                            this@CommunityEventListActivity,
                            "Invalid",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            }

        }

        rv_calendar_event_list.adapter = communityEventsByDateAdapter


    }


    override fun onClick(p0: View?) {
        val id: Int = p0!!.getId()
        if (id == R.id.ivCalenderAndList) {
            if (swipeRefreshEventList.visibility == View.VISIBLE) {
                showCalenderView()
            } else {
                showEventView()
            }

        }

    }


    private fun calleventListApi(isRequireShowLoader: Boolean) {

        val inputParams = HashMap<String, String>()
        inputParams.put(ApiConstants.PAGE_INDEX, "" + currentPage)
        inputParams.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputParams.put(ApiConstants.SORT_BY, sortByType)
        inputParams.put(ApiConstants.FROM_DATE, TimeUtils.convertDateForSendToServer(fromDate))
        inputParams.put(ApiConstants.TO_DATE, TimeUtils.convertDateForSendToServer(toDate))
        inputParams.put(ApiConstants.CATEGORY_ID, categoryId)
        inputParams.put(
            ApiConstants.LAT,
            CommonMethods.getDataFromSharePreference(
                this@CommunityEventListActivity,
                AppConstants.LATITUDE
            )
        )
        inputParams.put(
            ApiConstants.LON,
            CommonMethods.getDataFromSharePreference(
                this@CommunityEventListActivity,
                AppConstants.LONGITUDE
            )
        )
        val call = ApiClient.getRetrofitInterface()
            ?.getCommunityEvents(inputParams)
        WSClient<Response<List<CommunityEvents>>>().request(
            this@CommunityEventListActivity,
            100,
            isRequireShowLoader,
            call,
            object : ISuccessHandler<Response<List<CommunityEvents>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<CommunityEvents>>
                ) {
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        rvProgressLayout?.visibility = View.GONE
                        hasNext = mResponse.settings?.nextPage == 1
                        pageCount = mResponse.settings?.perPage!!
                        tvNoEventsData.visibility = View.INVISIBLE
                        if (currentPage == 1) {
                            communityEventList = (mResponse.data as MutableList<CommunityEvents>?)!!
                            communityEventsAdapter?.setData(communityEventList)
                        } else {
                            communityEventsAdapter?.addItems(mResponse.data)
                        }


                    } else {
                        tvNoEventsData.visibility = View.VISIBLE
                        tvNoEventsData.text = mResponse.settings?.message
                        communityEventList.clear()
                        communityEventsAdapter?.setData(communityEventList)
                    }
                    if (swipeRefreshEventList.isRefreshing) {
                        swipeRefreshEventList.isRefreshing = false
                    }
                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    rvProgressLayout?.visibility = View.GONE
                    CommonMethods.hideProgress()
                    if (swipeRefreshEventList.isRefreshing) {
                        swipeRefreshEventList.isRefreshing = false
                    }
                    alert(message) {
                        okButton { }
                    }.show()
                }

            })


    }

    override fun commonCallBack(type: String, data: Any, position: Int) {

        if (sortByType.equals(type)) {

        } else {
            sortByType = type
            currentPage = 1
            hasNext = false
            calleventListApi(true)
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                if (data?.getStringExtra(ApiConstants.FROM_DATE) != null) {
                    fromDate = data.getStringExtra(ApiConstants.FROM_DATE)
                } else {
                    fromDate = ""
                }

                if (data?.getStringExtra(ApiConstants.TO_DATE) != null) {
                    toDate = data.getStringExtra(ApiConstants.TO_DATE)
                } else {
                    toDate = ""
                }

                if (data?.getStringExtra(ApiConstants.CATEGORY_ID) != null) {
                    categoryId = data.getStringExtra(ApiConstants.CATEGORY_ID)
                } else {
                    categoryId = ""
                }

                if (data?.getStringExtra(AppConstants.TIMETYPE) != null) {
                    timeType = data.getStringExtra(AppConstants.TIMETYPE)
                } else {
                    timeType = ""
                }

                hasNext = false
                currentPage = 1
                calleventListApi(true)
            }


        }
    }


    fun showCalenderView() {
        llCalender.visibility = View.VISIBLE
        rvProgressLayout.visibility=View.INVISIBLE
        swipeRefreshEventList.visibility = View.GONE
        llFilterAndSoring.visibility = View.GONE
        ivCalenderAndList.setImageDrawable(
            ContextCompat.getDrawable(
                this@CommunityEventListActivity,
                R.drawable.listicon
            )
        )
    }

    fun showEventView() {
        swipeRefreshEventList.visibility = View.VISIBLE
        llFilterAndSoring.visibility = View.VISIBLE
        llCalender.visibility = View.GONE
        ivCalenderAndList.setImageDrawable(
            ContextCompat.getDrawable(
                this@CommunityEventListActivity,
                R.drawable.calender_icon
            )
        )
    }


}