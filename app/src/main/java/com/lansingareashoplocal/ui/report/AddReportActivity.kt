package com.lansingareashoplocal.ui.report

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.interfaces.CommonCallBack
import com.lansingareashoplocal.ui.model.Report
import kotlinx.android.synthetic.main.activity_add_report.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton

class AddReportActivity : BaseActivity(), CommonCallBack {

    var reportTypeList = mutableListOf<Report>()
    var dialogReport: DialogReport? = null
    var selectedType = ""
    var selectTypeId = ""
    var businessPlaceId: String? = null
    var businessLogo: String? = null
    var businessName: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_report)
        businessPlaceId = intent.getStringExtra(AppConstants.BUSINESS_ID)
        businessLogo = intent.getStringExtra(AppConstants.BUSINESS_LOGO)
        businessName = intent.getStringExtra(AppConstants.BUSINESS_NAME)

        toolbarAddReport.tvLeft.setOnClickListener {
            finish()
        }
        toolbarAddReport.tvTitle.text = getString(R.string.report)
        tvReportType.setOnClickListener {
            if (reportTypeList.isEmpty()) {
                callGetReportTypelistApi()
            } else {
                dialogReport?.showSortAlert(reportTypeList, selectTypeId)
            }
        }
        btnReportSumbit.setOnClickListener {
            if (selectTypeId.isEmpty()) {
                alert(getString(R.string.select_the_report_alert)) {
                    okButton { }
                }.show()
            } else if (etReportComment.text?.trim()?.isEmpty()!!) {
                alert(getString(R.string.please_add_report_cooment_alert)) {
                    okButton { }
                }.show()
            } else {
                callSendReport()
            }
        }

        if (businessLogo != null) {
            Glide.with(this@AddReportActivity).load(businessLogo).into(ivBusinessLogo)
        } else {
            cvBusinessProfile.visibility = View.GONE
        }

        tvReportText.text = "${getString(R.string.report_the_inaccuracy_for)} ${businessName}"

    }

    override fun commonCallBack(type: String, data: Any, position: Int) {

    }


    fun callGetReportTypelistApi() {
        val call = ApiClient.getRetrofitInterface()?.getInaccuracyReportTypeList()
        WSClient<Response<List<Report>>>().request(
            this@AddReportActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<Report>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Report>>) {

                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        reportTypeList = mResponse.data as MutableList<Report>
                        dialogReport =
                            DialogReport(this@AddReportActivity, object : CommonCallBack {
                                override fun commonCallBack(
                                    type: String,
                                    data: Any,
                                    position: Int
                                ) {
                                    selectTypeId = type
                                    if (selectTypeId.isNotEmpty()) {
                                        tvReportType.text = getSelectReportType(selectTypeId)
                                    }

                                }

                            })
                        dialogReport?.showSortAlert(reportTypeList, selectTypeId)
                    }

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert(message) {
                        okButton { }
                    }.show()

                }

            })

    }

    fun callSendReport() {

        val inputparam = HashMap<String, String>()
        if (businessPlaceId != null) {
            inputparam.put(ApiConstants.BUSINESS_PLACE_ID, businessPlaceId!!)
        }
        inputparam.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputparam.put(ApiConstants.COMMENT, etReportComment.text?.trim().toString())
        inputparam.put(ApiConstants.REPORT_TYPE_ID, selectTypeId)

        if (CommonMethods.getUserId(this@AddReportActivity)!!.isNotEmpty()) {
            inputparam.put(ApiConstants.USER_ID, CommonMethods.getUserId(this@AddReportActivity)!!)
        } else {
            inputparam.put(ApiConstants.USER_ID, "0")
        }

        val call = ApiClient.getRetrofitInterface()?.reportBusinessInaccuracy(inputparam)
        WSClient<Response<List<Any>>>().request(
            this@AddReportActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<Any>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Any>>) {
                    val alerts = alert("" + mResponse.settings?.message) {
                        okButton {
                            finish()
                        }
                    }.show()
                    alerts.setCancelable(false)

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert(message) {
                        okButton { }
                    }.show()

                }

            })

    }

    fun getSelectReportType(reportedId: String): String {
        reportTypeList.forEachIndexed { index, report ->
            if (report.reportTypeId.equals(selectTypeId)) {
                selectedType = report.reportType
                return selectedType
            }
        }
        return ""
    }

}