package com.lansingareashoplocal.ui.report

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.DialogReportBinding
import com.lansingareashoplocal.databinding.ItemSortingBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.interfaces.CommonCallBack
import com.lansingareashoplocal.ui.model.Report

class DialogReport(var context: Context, var commonCallBack: CommonCallBack) {


    lateinit var reportDailog: Dialog
    lateinit var binding: DialogReportBinding

    var reportAdapter: GenericAdapter<Report, ItemSortingBinding>? = null
    var reportingList: MutableList<Report>?=null
    var reportId:String=""





    fun showSortAlert(reportTypeList: MutableList<Report>, reportId:String="") {
        this.reportId=reportId
        this.reportingList=reportTypeList
        reportDailog = Dialog(context, R.style.MyAlertDialogStyle)
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_report,
            null,
            false
        )
        reportDailog.setContentView(binding.getRoot())
        reportDailog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setReportingAdapter()
        reportDailog.show()
         binding.ivReportCancel.setOnClickListener {
             reportDailog.dismiss()
         }
        binding.btnReportApply.setOnClickListener {
            commonCallBack.commonCallBack(this.reportId,"")
            reportDailog.dismiss()
        }

    }






    fun setReportingAdapter() {
        if(reportId.isNotEmpty()){
            setPreviusReportAsSelected()
        }
        else{
            reportingList?.forEachIndexed { index, report ->
                reportingList?.get(index)?.isSelected =false
            }
        }
        reportAdapter = object : GenericAdapter<Report, ItemSortingBinding>(reportingList) {
            override fun getLayoutId(): Int {
                return R.layout.item_sorting
            }

            override fun onBindData(
                model: Report?,
                position: Int,
                dataBinding: ItemSortingBinding?
            ) {

                dataBinding?.rbSortingName?.text = model?.reportType
                if(reportId.equals(model?.reportTypeId)){
                    model!!.isSelected =true
                }
                dataBinding?.rbSortingName?.isChecked = model!!.isSelected
                dataBinding?.rbSortingName?.setOnClickListener {
                    reportingList?.forEachIndexed { index, sortModel ->
                        reportingList?.get(index)?.isSelected = false
                    }
                    reportingList?.get(position)?.isSelected = true
                    reportId=model.reportTypeId
                    notifyDataSetChanged()
                }

            }

        }
        binding.rvReporting.adapter=reportAdapter


    }


    fun setPreviusReportAsSelected(){
        reportingList?.forEachIndexed { index, report ->
            reportingList?.get(index)?.isSelected = reportId.equals(report.reportTypeId)
        }
    }
}