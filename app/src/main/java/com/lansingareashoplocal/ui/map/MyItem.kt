/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lansingareashoplocal.ui.map

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class MyItem : ClusterItem {
    private val mPosition: LatLng

    /**
     * Set the title of the marker
     * @param title string to be set as title
     */
    var title: String?

    /**
     * Set the description of the marker
     * @param snippet string to be set as snippet
     */
    var snippet: String?

    constructor(lat: Double, lng: Double) {
        mPosition = LatLng(lat, lng)
        title = null
        snippet = null
    }

    constructor(
        lat: Double,
        lng: Double,
        title: String?,
        snippet: String?
    ) {
        mPosition = LatLng(lat, lng)
        this.title = title
        this.snippet = snippet
    }

    override fun getPosition(): LatLng {
        return mPosition
    }

}