package com.lansingareashoplocal.ui.settings

import android.util.Patterns

class ContantUs {

    var name:String=""
    var mail :String=""
    var mobileNo :String=""
    var reason: String=""
    var message :String=""

    fun isNameEmpty():Boolean{
        return name.trim().isEmpty()
    }
    fun isMailEmpty():Boolean{
        return mail.trim().isEmpty()
    }
    fun isMessageEmpty():Boolean{
        return message.trim().isEmpty()
    }
    fun isMailValid():Boolean{
        return Patterns.EMAIL_ADDRESS.matcher(mail.trim()).matches()
    }
    fun isReasonEmpty():Boolean{
        return reason.trim().isEmpty()
    }
    fun isMobileNumberEmpty():Boolean{
        return mobileNo.trim().isEmpty()
    }
    fun  isMobileNumberValid():Boolean{
          return (mobileNo.length < 10 || mobileNo.length > 15)
    }



}