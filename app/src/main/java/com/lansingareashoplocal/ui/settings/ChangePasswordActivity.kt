package com.lansingareashoplocal.ui.settings

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import androidx.databinding.DataBindingUtil
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.ActivityChangePasswordBinding
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.interfaces.EditTextChangeCallBack
import com.lansingareashoplocal.utility.helper.others.CustomTextWatcher
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.activity_change_password.etConfirmPassWord
import kotlinx.android.synthetic.main.activity_change_password.etPassWord
import kotlinx.android.synthetic.main.activity_change_password.tInputLayoutConfirmPassword
import kotlinx.android.synthetic.main.activity_change_password.tInputLayoutPassword
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton

class ChangePasswordActivity : BaseActivity(), EditTextChangeCallBack {


    lateinit var binding: ActivityChangePasswordBinding
    override fun afterTextChanged(editText: AppCompatEditText) {
        if (etOldPassWord == editText) {
            tInputLayoutOldPassword.error = ""
        } else if (etPassWord == editText) {
            tInputLayoutPassword.error = ""
        } else if (etConfirmPassWord == editText) {
            tInputLayoutConfirmPassword.error = ""
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_change_password
        ) as ActivityChangePasswordBinding
        binding.changePassword = ChangePasswordModel()
        tvLeft.visibility = View.VISIBLE
        tvTitle.text = getString(R.string.change_password)
        tvLeft.setOnClickListener {
            finish()
        }
        etOldPassWord.addTextChangedListener(CustomTextWatcher(etOldPassWord, this))
        etPassWord.addTextChangedListener(CustomTextWatcher(etPassWord, this))
        etConfirmPassWord.addTextChangedListener(CustomTextWatcher(etConfirmPassWord, this))
        etOldPassWord.setOnFocusChangeListener(onFocusListener())
        etPassWord.setOnFocusChangeListener(onFocusListener())
        etConfirmPassWord.setOnFocusChangeListener(onFocusListener())



        tInputLayoutOldPassword.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutOldPassword.hint = requireField(getString(R.string.OLD_PASSWORD))

        tInputLayoutPassword.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutPassword.hint = requireField(getString(R.string.PASSWORD))
        tInputLayoutConfirmPassword.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutConfirmPassword.hint = requireField(getString(R.string.CONFIRM_PASSWORD))

        btnChangePassword.setOnClickListener {
            tInputLayoutOldPassword.error = ""
            tInputLayoutPassword.error = ""
            tInputLayoutConfirmPassword.error = ""
            if (checkValidations()) {
                 callChangePasswordApi()
            }
        }
    }


    private fun onFocusListener(): View.OnFocusChangeListener {
        val onFocusChangeListener = object : View.OnFocusChangeListener {
            override fun onFocusChange(v: View?, hasFocus: Boolean) {
                if (!hasFocus) {
                    if (v == etOldPassWord) {
                        if (binding?.changePassword?.isOldPasswordEmpty!!) {
                            tInputLayoutOldPassword.error =
                                getString(R.string.error_please_enter_old_password)
                        }
                    } else if (v == etPassWord) {
                        if (binding?.changePassword?.isNewPasswordEmpty!!) {
                            tInputLayoutPassword.error =
                                getString(R.string.error_please_enter_new_password)
                        } else if (binding?.changePassword?.isValidPassword()!!) {
                            tInputLayoutPassword.error =
                                getString(R.string.error_please_enter_strong_password)
                        } else if (binding?.changePassword?.compareOldPasswordAndNewPassword(this@ChangePasswordActivity)!!) {
                            tInputLayoutPassword.error =
                                getString(R.string.error_Old_password_new_password_not_equal)
                        }
                    } else if (v == etConfirmPassWord) {
                        if (binding?.changePassword?.isconfirmPasswordEmpty!!) {
                            tInputLayoutConfirmPassword.error =
                                getString(R.string.error_please_enter_confirm_password)
                        } else if (binding?.changePassword?.comparePasswordAndConfirm()!!) {
                            tInputLayoutConfirmPassword.error =
                                getString(R.string.error_new_password_confirm_password_not_equal)
                        }
                    }
                }


            }

        }
        return onFocusChangeListener as View.OnFocusChangeListener
    }

    fun requireField(text: String): SpannableStringBuilder {
        val mSpannableStringBuilder = SpannableStringBuilder(text)
        val star = SpannableString("*")
        star.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorRed)),
            0, star.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        mSpannableStringBuilder.append(star)
        return mSpannableStringBuilder
    }

    private fun checkValidations(): Boolean {
        var count = 6
        if (binding?.changePassword?.isOldPasswordEmpty!!) {
            tInputLayoutOldPassword.error = getString(R.string.error_please_enter_old_password)
            count -= 1
        }
        if (binding?.changePassword?.isNewPasswordEmpty!!) {
            tInputLayoutPassword.error = getString(R.string.error_please_enter_new_password)
            count -= 1
        } else if (binding?.changePassword?.isValidPassword()!!) {
            tInputLayoutPassword.error = getString(R.string.error_please_enter_strong_password)
            count -= 1
        } else if (binding?.changePassword?.compareOldPasswordAndNewPassword(this@ChangePasswordActivity)!!) {
            tInputLayoutPassword.error =
                getString(R.string.error_Old_password_new_password_not_equal)
            count -= 1
        }

        if (binding?.changePassword?.isconfirmPasswordEmpty!!) {
            tInputLayoutConfirmPassword.error =
                getString(R.string.error_please_enter_confirm_password)
            count -= 1
        } else if (binding?.changePassword?.comparePasswordAndConfirm()!!) {
            tInputLayoutConfirmPassword.error =
                getString(R.string.error_new_password_confirm_password_not_equal)
            count -= 1
        }
        return count == 6
    }


    private fun callChangePasswordApi() {
        val inputParam = HashMap<String, String>()
        inputParam.put(ApiConstants.OLD_PASSWORD, binding.changePassword?.oldPassword!!.trim())
        inputParam.put(ApiConstants.NEW_PASSWORD, binding.changePassword?.password!!.trim())
        inputParam.put(ApiConstants.USER_ID, CommonMethods.getUserId(this@ChangePasswordActivity)!!)
        val call = ApiClient.getRetrofitInterface()?.changePassword(inputParam)





        WSClient<Response<List<Any>>>().request(
            this@ChangePasswordActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<Any>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Any>>) {
                    if (mResponse.settings?.success.equals("1")) {
                        CommonMethods.setDataIntoSharePreference(this@ChangePasswordActivity, AppConstants.PASSWORD, binding.changePassword?.password)
                        val alert = alert("" + mResponse.settings?.message) {
                            okButton {
                                finish()
                            }


                        }.show()
                        alert.setCancelable(false)
                    } else {
                        alert("" + mResponse.settings?.message) {
                            okButton {

                            }
                        }.show()
                    }
                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert(message) {
                        okButton { }
                    }.show()
                }

            })
    }
}