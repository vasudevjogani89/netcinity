package com.lansingareashoplocal.ui.settings

import ApiClient
import AppConstants
import CommonMethods
import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.adapter.CustomExpandableListAdapter
import com.lansingareashoplocal.ui.interfaces.CommonCallBack
import com.lansingareashoplocal.ui.interfaces.EditTextChangeCallBack
import com.lansingareashoplocal.ui.model.FAQ
import com.lansingareashoplocal.ui.settings.ExpandableListData.data
import com.lansingareashoplocal.utility.helper.others.CustomTextWatcher
import kotlinx.android.synthetic.main.activity_faq_list.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton

class FAQListActivity : BaseActivity() ,EditTextChangeCallBack,CommonCallBack{


    private var adapter: CustomExpandableListAdapter? = null
    private var questionsList: MutableList<String>? = null
    val expandableListDetail = HashMap<String, List<String>>()
    private var faqList = mutableListOf<FAQ>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_faq_list)
        tvTitle.text=getString(R.string.faq_s)
        tvLeft.setOnClickListener { finish() }
        callFAQApi()
        searchFAQ()
        etFAQListSearch.addTextChangedListener(CustomTextWatcher(etFAQListSearch, this))
        clearText()
    }


    fun callFAQApi() {
        val inputParams = HashMap<String, String>()
        inputParams.put(AppConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        val call = ApiClient.getRetrofitInterface()?.getfaqList(inputParams)
        WSClient<Response<List<FAQ>>>().request(
            this@FAQListActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<FAQ>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<FAQ>>) {
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null && mResponse.data!!.isNotEmpty()) {
                        tvNoFAQData.visibility=View.GONE
                        rlSearchBarFAQ.visibility=View.VISIBLE
                        faqList= mResponse.data as MutableList<FAQ>
                        faqList.forEachIndexed { index, faq ->
                            questionsList?.add(faq.question)
                            val answer: MutableList<String> = ArrayList()
                            answer.add(faq.answer)
                            expandableListDetail[faq.question] = answer
                        }
                        setFAQData()


                    }
                    else{
                        rlSearchBarFAQ.visibility=View.GONE
                        tvNoFAQData.visibility=View.VISIBLE
                        tvNoFAQData.text=mResponse.settings?.message
                    }

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert(message) {
                        okButton { }
                    }.show()
                }

            })

    }

    fun setFAQData(){
        val listData = data
        questionsList = ArrayList(expandableListDetail.keys)
        adapter = CustomExpandableListAdapter(this, questionsList as ArrayList<String>, expandableListDetail,this)
        expandableListViewFAQ!!.setAdapter(adapter)
       /* expandableListViewFAQ!!.setOnGroupExpandListener { groupPosition ->
            Toast.makeText(
                applicationContext,
                (questionsList as ArrayList<String>)[groupPosition] + " List Expanded.",
                Toast.LENGTH_SHORT
            ).show()
        }
        expandableListViewFAQ!!.setOnGroupCollapseListener { groupPosition ->
            Toast.makeText(
                applicationContext,
                (questionsList as ArrayList<String>)[groupPosition] + " List Collapsed.",
                Toast.LENGTH_SHORT
            ).show()
        }
        expandableListViewFAQ!!.setOnChildClickListener { _, _, groupPosition, childPosition, _ ->
            Toast.makeText(
                applicationContext,
                "Clicked: " + (questionsList as ArrayList<String>)[groupPosition] + " -> " + listData[(
                        questionsList as
                                ArrayList<String>
                        )
                        [groupPosition]]!!.get(
                    childPosition
                ),
                Toast.LENGTH_SHORT
            ).show()
            false
        }*/
    }

    private fun searchFAQ() {
        etFAQListSearch.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                   // isSearchDone = true
                    if (v?.text != null) {
                        adapter?.filter?.filter(v.text)
                        CommonMethods.hideKeypad(this@FAQListActivity, etFAQListSearch)
                    }

                    return true
                }
                return false
            }
        })
    }
    private fun clearText() {

        ivFAQSearch.setOnClickListener {
            if (etFAQListSearch.text!!.isNotEmpty()) {
                CommonMethods.hideKeypad(this@FAQListActivity,etFAQListSearch)
                etFAQListSearch.setText("")
                adapter?.filter?.filter("")
                etFAQListSearch.clearFocus()
            } else {

            }
            tvNoFAQData.visibility=View.GONE
        }
    }

    override fun afterTextChanged(editText: AppCompatEditText) {
        if (editText.text != null) {
            if (editText.text!!.isNotEmpty()) {
                Glide.with(this@FAQListActivity).load(R.drawable.cancel)
                    .into(ivFAQSearch)
            } else {
                Glide.with(this@FAQListActivity).load(R.drawable.ic_search)
                    .into(ivFAQSearch)
            }
        }

    }

    override fun commonCallBack(type: String, data: Any, position: Int) {
       val isDataEmpty=data as Boolean
        if(isDataEmpty){
            tvNoFAQData.text=getString(R.string.no_faq_found)
            tvNoFAQData.visibility=View.VISIBLE
        }
        else{
            tvNoFAQData.visibility=View.GONE
        }

    }
}
