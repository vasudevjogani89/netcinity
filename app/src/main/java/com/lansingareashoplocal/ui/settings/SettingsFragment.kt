package com.lansingareashoplocal.ui.settings

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.ActivitySettingsBinding
import com.lansingareashoplocal.core.BaseFragment
import com.lansingareashoplocal.room.AppDataBase
import com.lansingareashoplocal.ui.review.MyReviewsListActivity
import com.lansingareashoplocal.ui.user.LoginActivity
import com.lansingareashoplocal.ui.user.UpdateProfileActivity
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.view.*
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.okButton
import org.jetbrains.anko.support.v4.alert


class SettingsFragment : BaseFragment<ActivitySettingsBinding>() {


    override fun onBackPressed(): Boolean {

        return false
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_settings
    }

    override fun init() {
        binding.toolBarSettings.tvLeft.visibility=View.INVISIBLE
        binding.toolBarSettings.tvTitle.text = getString(R.string.settings)
        binding.tvUserStatus.setOnClickListener {
            if (tvUserStatus.text.equals(getString(R.string.sign_out))) {
            var signOutAlert=    alert(getString(R.string.signout_alert)) {
                    positiveButton(getString(R.string.sign_out)) {
                        CommonMethods.logout(activity!!)
                    }
                    negativeButton(R.string.cancel) {
                    }
                }.show()
                signOutAlert.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                signOutAlert.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false

            } else {
                startActivity(Intent(activity!!, LoginActivity::class.java))
            }
        }
        binding.tvAboutUs.setOnClickListener {


            openStaticPage(ApiConstants.ABOUT_US)
        }
        binding.tvLogger.setOnClickListener {
            //rateMe(binding.tvLogger)
            //   Logger.launchActivity()
        }
        binding.tvPrivacyPolicy.setOnClickListener {
            openStaticPage(ApiConstants.PRIVACY_POLICY)
        }
        binding.tvTermsConditions.setOnClickListener {
            openStaticPage(ApiConstants.TERMS_CONDITION)
        }

        binding.tvFAQ.setOnClickListener {
          startActivity(Intent(activity!!,FAQListActivity::class.java))
        }
        binding.tvChangePassword.setOnClickListener {
            startActivity(Intent(activity!!, ChangePasswordActivity::class.java))
        }
        binding.tvContactUs.setOnClickListener {
            startActivity(Intent(activity!!,ContantUsActivity::class.java))
        }
        binding.tvManuallySearch.setOnClickListener {

            val builder =
                alert(getString(R.string.becon_reset_alert)) {
                    positiveButton(getString(R.string.yes)) {
                        AppDataBase.getAppDatabase(activity!!).Dao().deleteAllBecons()
                        alert(
                            getString(R.string.becon_reset_done_alert)
                        ) {
                            okButton { }
                        }.show()
                    }
                    negativeButton(getString(R.string.no)) {

                    }
                }.show()
            builder.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
            builder.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false


        }



        binding.rlUserType.setOnClickListener {
            if (CommonMethods.getUserId(activity!!)!!.isNotEmpty()) {
                startActivityForResult(
                    Intent(activity!!, UpdateProfileActivity::class.java),
                    AppConstants.PROFILE_UPDATE_REQUEST
                )
            } else {
                startActivity(Intent(activity!!, LoginActivity::class.java))
            }

        }

        if (CommonMethods.getUserId(activity!!)!!.isNotEmpty() && CommonMethods.getUserData(activity!!)!!.facebook_id.isEmpty() && CommonMethods.getUserData(
                activity!!
            )!!.google_id.isEmpty()
        ) {
            binding.tvChangePassword.visibility = View.VISIBLE
            //   tvUserType.text=
        } else {
            binding.tvChangePassword.visibility = View.GONE

        }
        binding.tvMyReviews.setOnClickListener {

            if (CommonMethods.getUserId(activity!!)?.isEmpty()!!) {
                CommonMethods.isComeFromSplashScreen=false

                startActivityForResult(Intent(activity!!, LoginActivity::class.java),AppConstants.MY_REVIEWS_REQUEST)
            } else {
                startActivity(Intent(activity!!, MyReviewsListActivity::class.java))
            }

        }

        if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
            changeColorForStayLocal()
        }
    }

    private fun changeColorForStayLocal() {
        binding.tvUserStatus.setTextColor(ContextCompat.getColor(activity!!,R.color.colorDarkBlue))
    }

    private fun openStaticPage(pageCode: String) {
        val intent = Intent(activity!!, StaticsPageActivity::class.java)
        intent.putExtra(AppConstants.PAGE_CODE, pageCode)
        startActivity(intent)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setData()

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode.equals(AppConstants.PROFILE_UPDATE_REQUEST) && resultCode == Activity.RESULT_OK) {
            binding.tvUserType.text =
                "${CommonMethods.getUserData(activity!!)?.firstName} ${CommonMethods.getUserData(
                    activity!!
                )?.lastName}"
        }
        else if(requestCode.equals(AppConstants.MY_REVIEWS_REQUEST) && resultCode== Activity.RESULT_OK){
            setData()
            startActivity(Intent(activity!!, MyReviewsListActivity::class.java))
        }
    }


    fun rateMe(view: View) {
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + context!!.packageName)
                )
            )
        } catch (e: android.content.ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context!!.packageName)
                )
            )
        }

    }

   fun setData(){
       if (CommonMethods.getUserId(activity!!)?.isEmpty()!!) {
           binding.tvMyReviews.visibility=View.GONE
           binding.tvUserStatus.text = getString(R.string.signin)
           binding.tvUserType.text = getString(R.string.guest)
       } else {
           binding.tvMyReviews.visibility=View.VISIBLE
           binding.tvUserStatus.text = getString(R.string.sign_out)
           binding.tvUserType.text =
               "${CommonMethods.getUserData(activity!!)?.firstName} ${CommonMethods.getUserData(
                   activity!!
               )?.lastName}"
       }
       binding.tvAppVersion.text = BuildConfig.VERSION_NAME
   }
}