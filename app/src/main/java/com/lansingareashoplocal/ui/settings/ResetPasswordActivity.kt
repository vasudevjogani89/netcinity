package com.lansingareashoplocal.ui.settings

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.graphics.Typeface
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.View
import kotlinx.android.synthetic.main.activity_reset_password.*
import androidx.appcompat.widget.AppCompatEditText
import androidx.databinding.DataBindingUtil
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.ActivityResetPasswordBinding
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.interfaces.EditTextChangeCallBack
import com.lansingareashoplocal.utility.helper.others.CustomTextWatcher
import kotlinx.android.synthetic.main.activity_reset_password.etConfirmPassWord
import kotlinx.android.synthetic.main.activity_reset_password.etPassWord
import kotlinx.android.synthetic.main.activity_reset_password.tInputLayoutConfirmPassword
import kotlinx.android.synthetic.main.activity_reset_password.tInputLayoutPassword
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton


class ResetPasswordActivity() : BaseActivity(), EditTextChangeCallBack, Parcelable {

    var binding: ActivityResetPasswordBinding? = null
    var passwordState = true
    var confirmPassword = true

    constructor(parcel: Parcel) : this() {
        passwordState = parcel.readByte() != 0.toByte()
        confirmPassword = parcel.readByte() != 0.toByte()
    }

    override fun afterTextChanged(mEditText: AppCompatEditText) {
        if (mEditText == etPassWord) {
            tInputLayoutPassword.error = ""

        } else if (mEditText == etConfirmPassWord) {
            tInputLayoutConfirmPassword.error = ""
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_reset_password
        ) as ActivityResetPasswordBinding
        binding?.resetPassword = ChangePasswordModel()
        tvLeft.visibility = View.VISIBLE
        tvTitle.text = getString(R.string.reset_password)
        tvLeft.setOnClickListener {
            finish()
        }
        etPassWord.addTextChangedListener(CustomTextWatcher(etPassWord, this));
        etConfirmPassWord.addTextChangedListener(CustomTextWatcher(etConfirmPassWord, this));
        etPassWord.onFocusChangeListener = onFocusListener()
        etConfirmPassWord.onFocusChangeListener = onFocusListener()

        btnResetPassword.setOnClickListener {
            tInputLayoutPassword.error = ""
            tInputLayoutConfirmPassword.error = ""
            if (checkValidation()) {
                  callResetPasswordApi()
            }

        }
        tInputLayoutPassword.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutPassword.hint = requireField(getString(R.string.PASSWORD))
        tInputLayoutConfirmPassword.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutConfirmPassword.hint = requireField(getString(R.string.CONFIRM_PASSWORD))

    }


    private fun checkValidation(): Boolean {
        var validCount = 4
        if (binding?.resetPassword?.isNewPasswordEmpty!!) {
            tInputLayoutPassword.error = getString(R.string.error_please_enter_new_password)
            validCount -= 1
        } else if (binding?.resetPassword?.isValidPassword()!!) {
            tInputLayoutPassword.error = getString(R.string.error_please_enter_strong_password)
            validCount -= 1
        }


        if (binding?.resetPassword?.isconfirmPasswordEmpty!!) {
            tInputLayoutConfirmPassword.error =
                getString(R.string.error_please_enter_confirm_password)
            validCount -= 1
        } else if (binding?.resetPassword?.comparePasswordAndConfirm()!!) {
            tInputLayoutConfirmPassword.error =
                getString(R.string.error_new_password_confirm_password_not_equal)
            validCount -= 1
        }

        return validCount == 4


    }

    private fun onFocusListener(): View.OnFocusChangeListener {
        val onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                if (v == etPassWord) {
                    if (binding?.resetPassword?.isNewPasswordEmpty!!) {
                        tInputLayoutPassword.error =
                            getString(R.string.error_please_enter_new_password)
                    } else if (binding?.resetPassword?.isValidPassword()!!) {
                        tInputLayoutPassword.error =
                            getString(R.string.error_please_enter_strong_password)
                    }

                } else if (v == etConfirmPassWord) {
                    if (binding?.resetPassword?.isconfirmPasswordEmpty!!) {
                        tInputLayoutConfirmPassword.error =
                            getString(R.string.error_please_enter_confirm_password)
                    } else if (binding?.resetPassword?.comparePasswordAndConfirm()!!) {
                        tInputLayoutConfirmPassword.error =
                            getString(R.string.error_new_password_confirm_password_not_equal)
                    }
                }
            }
        }
        return onFocusChangeListener as View.OnFocusChangeListener
    }

    fun requireField(text: String): SpannableStringBuilder {
        val mSpannableStringBuilder = SpannableStringBuilder(text)
        val star = SpannableString("*")
        star.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorRed)),
            0, star.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        mSpannableStringBuilder.append(star)
        return mSpannableStringBuilder
    }

    private fun callResetPasswordApi() {
        val inputParam = HashMap<String, String>()
        inputParam.put(ApiConstants.PASSWORD, binding?.resetPassword?.password!!.trim())
        inputParam.put(ApiConstants.USER_ID, intent.getStringExtra(AppConstants.USER_ID))
        val call = ApiClient.getRetrofitInterface()?.resetPassword(inputParam)

        WSClient<Response<List<Any>>>().request(
            this@ResetPasswordActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<Any>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Any>>) {
                    if (mResponse.settings?.success.equals("1")) {
                        CommonMethods.setDataIntoSharePreference(
                            this@ResetPasswordActivity,
                            AppConstants.PASSWORD,
                            binding?.resetPassword?.password
                        )
                        val alert = alert("" + mResponse.settings?.message) {
                            okButton {
                                CommonMethods.logout(this@ResetPasswordActivity)
                            }
                        }.show()
                        alert.setCancelable(false)
                    } else {
                        alert("" + mResponse.settings?.message) {
                            okButton {

                            }
                        }.show()
                    }
                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert(message) {
                        okButton { }
                    }.show()
                }

            })
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (passwordState) 1 else 0)
        parcel.writeByte(if (confirmPassword) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ResetPasswordActivity> {
        override fun createFromParcel(parcel: Parcel): ResetPasswordActivity {
            return ResetPasswordActivity(parcel)
        }

        override fun newArray(size: Int): Array<ResetPasswordActivity?> {
            return arrayOfNulls(size)
        }
    }

}
