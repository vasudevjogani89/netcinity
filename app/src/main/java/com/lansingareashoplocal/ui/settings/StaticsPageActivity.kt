package com.lansingareashoplocal.ui.settings

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.model.StaticPageModel
import kotlinx.android.synthetic.main.activity_static_pages.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton

class StaticsPageActivity : BaseActivity() {
    var pageCode: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_static_pages)
        pageCode = intent.getStringExtra(AppConstants.PAGE_CODE)
        tvLeft.visibility = View.VISIBLE
        tvLeft.setOnClickListener {
            finish()
        }
        prograssBar.visibility = View.VISIBLE
        wv.webViewClient = myWebClient()
        wv.getSettings().setJavaScriptEnabled(true);
        callStaticPageApi()

    }

    private fun callStaticPageApi() {
        val call = ApiClient.getRetrofitInterface()?.staticPagesApi("${CommonMethods.cummunity_id}_${pageCode}")

        WSClient<Response<List<StaticPageModel>>>().request(this@StaticsPageActivity, 100, true, call, object : ISuccessHandler<Response<List<StaticPageModel>>> {
            override fun successResponse(requestCode: Int, mResponse: Response<List<StaticPageModel>>) {
                if (mResponse.settings?.success.equals("1")) {
                    val staticPageModel = mResponse.data?.get(0)
                    wv.loadUrl(staticPageModel?.page_url)
                    if(pageCode.equals(ApiConstants.ABOUT_US)){
                        tvTitle.text =getString(R.string.about_us)
                    }
                    else if(pageCode.equals(ApiConstants.TERMS_CONDITION)){
                        tvTitle.text =getString(R.string.terms_conditions)
                    }
                    else if(pageCode.equals(ApiConstants.PRIVACY_POLICY)){
                        tvTitle.text =getString(R.string.privacy_policy)
                    }
                    else if(pageCode.equals(ApiConstants.FAQ)){
                        tvTitle.text =getString(R.string.faq_s)
                    }


                }

            }

        }, object : IFailureHandler {
            override fun failureResponse(requestCode: Int, message: String) {
                prograssBar.visibility = View.INVISIBLE
                alert(message) {
                    okButton { }
                }.show()
            }

        })
    }

    inner class myWebClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true

        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            prograssBar.visibility = View.GONE
        }

    }


}