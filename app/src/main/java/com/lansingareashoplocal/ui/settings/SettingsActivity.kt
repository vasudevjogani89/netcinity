package com.lansingareashoplocal.ui.settings

import android.os.Bundle
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity

class SettingsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

    }
}