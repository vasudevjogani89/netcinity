package com.lansingareashoplocal.ui.settings

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.widget.AppCompatEditText
import androidx.databinding.DataBindingUtil
import com.google.android.material.textfield.TextInputLayout
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.databinding.ActivityContantUsBinding
import com.lansingareashoplocal.ui.interfaces.EditTextChangeCallBack
import com.lansingareashoplocal.ui.model.Reason
import com.lansingareashoplocal.utility.helper.others.CustomTextWatcher
import kotlinx.android.synthetic.main.activity_contant_us.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton

class ContantUsActivity : BaseActivity(), EditTextChangeCallBack {

    var reasonList = mutableListOf<Reason>()
    var reasonTitleList = mutableListOf<String>()
    var binding: ActivityContantUsBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contant_us)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_contant_us
        ) as ActivityContantUsBinding
        binding?.contactUsModel = ContantUs()

        tvLeft.setOnClickListener {
            finish()
        }
        tvTitle.text = getString(R.string.contact_us)
        etContactName.onFocusChangeListener = onFocusListener()
        etContactMail.onFocusChangeListener = onFocusListener()
        etContactNumber.onFocusChangeListener = onFocusListener()
        etContactMessage.onFocusChangeListener = onFocusListener()
        etContactName.addTextChangedListener(CustomTextWatcher(etContactName, this))
        etContactMail.addTextChangedListener(CustomTextWatcher(etContactMessage, this))
        etContactNumber.addTextChangedListener(CustomTextWatcher(etContactNumber, this))
        etContactMessage.addTextChangedListener(CustomTextWatcher(etContactMessage, this))
        btnContactUs.setOnClickListener {
            tInputLayoutContactName.error = ""
            tInputLayoutContactEmail.error = ""
            tInputLayoutContactNumber.error = ""
            tInputLayoutContactMessage.error = ""
            tInputLayoutContactReason.error = ""
            if (validateForm()) {
                callContactUsApi()
            }

        }
        setfontAndErrorMessages(tInputLayoutContactName, getString(R.string.name), true)
        setfontAndErrorMessages(tInputLayoutContactEmail, getString(R.string.EMAIL_ID), true)
        setfontAndErrorMessages(tInputLayoutContactReason, getString(R.string.reason), true)
        setfontAndErrorMessages(tInputLayoutContactMessage, getString(R.string.message), true)
        setfontAndErrorMessages(tInputLayoutContactNumber, getString(R.string.phone_number))
        etReason.setOnClickListener {
            spinnerReasons.performClick()
        }
        setData()
        callReportList()
    }

    private fun setData() {

        if (CommonMethods.getUserId(this@ContantUsActivity)!!.isNotEmpty()) {
            binding?.contactUsModel?.name =
                "${CommonMethods.getUserData(this@ContantUsActivity)!!.firstName} ${CommonMethods.getUserData(
                    this@ContantUsActivity
                )!!.lastName}"
            if (CommonMethods.getUserData(this@ContantUsActivity)!!.email.isNotEmpty()) {
                binding?.contactUsModel?.mail =
                    CommonMethods.getUserData(this@ContantUsActivity)!!.email
            }
        }

    }

    private fun setfontAndErrorMessages(
        textInputLayout: TextInputLayout,
        hintName: String,
        isRequire: Boolean = false
    ) {
        textInputLayout.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))

        if (isRequire) {
            textInputLayout.hint = requireField(hintName)
        } else {
            textInputLayout.hint = hintName
        }

    }

    private fun callContactUsApi() {

        val inputParams = HashMap<String, String>()
        inputParams.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputParams.put(ApiConstants.EMAIL, binding?.contactUsModel?.mail!!)
        inputParams.put(ApiConstants.MESSAGE, binding?.contactUsModel?.message!!)
        inputParams.put(ApiConstants.MOBILE, binding?.contactUsModel?.mobileNo!!)
        inputParams.put(ApiConstants.NAME, binding?.contactUsModel?.name!!)
        reasonList.forEachIndexed { index, reason ->
            if (binding?.contactUsModel?.reason.equals(reason.reason)) {
                inputParams.put(ApiConstants.REASON_ID, reason.reasonId)
            }
        }

        if (CommonMethods.getUserId(this@ContantUsActivity)!!.isNotEmpty()) {
            inputParams.put(ApiConstants.USER_ID, CommonMethods.getUserId(this@ContantUsActivity)!!)
        } else {
            inputParams.put(ApiConstants.USER_ID, "0")
        }


        val call = ApiClient.getRetrofitInterface()?.sendContactUsRequest(inputParams)
        WSClient<Response<Any>>().request(this@ContantUsActivity,100,true,call,object :ISuccessHandler<Response<Any>>{
            override fun successResponse(requestCode: Int, mResponse: Response<Any>) {
                if(mResponse.settings?.success.equals("1")){
                val alert=    alert(""+mResponse.settings?.message) {
                        okButton {
                            finish()
                        }
                    }.show()
                    alert.setCancelable(false)
                }

            }

        },object :IFailureHandler{
            override fun failureResponse(requestCode: Int, message: String) {
                alert(message) {
                    okButton {  }
                }.show()
            }

        })


    }

    fun requireField(text: String): SpannableStringBuilder {
        val mSpannableStringBuilder = SpannableStringBuilder(text)
        val star = SpannableString("*")
        star.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorRed)),
            0,
            star.length,
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        mSpannableStringBuilder.append(star)
        return mSpannableStringBuilder
    }

    private fun callReportList() {
        val call = ApiClient.getRetrofitInterface()?.getReasonList()
        WSClient<Response<List<Reason>>>().request(
            this@ContantUsActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<Reason>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Reason>>) {

                    if (mResponse.settings?.success.equals("1") && mResponse.data != null && mResponse.data!!.isNotEmpty()) {
                        reasonList = mResponse.data as MutableList<Reason>
                        reasonTitleList.add("Reason")
                        if (reasonList.isNotEmpty()) {
                            reasonList.forEachIndexed { index, reason ->
                                reasonTitleList.add(reason.reason)
                            }
                            setSpinner(reasonTitleList, spinnerReasons)
                            spinnerReasons.onItemSelectedListener =
                                object : AdapterView.OnItemSelectedListener {
                                    override fun onItemSelected(
                                        parent: AdapterView<*>?,
                                        view: View?,
                                        position: Int,
                                        id: Long
                                    ) {
                                        tInputLayoutContactReason.error=""
                                        etReason.setText(reasonTitleList[position])
                                        tInputLayoutContactReason.hint =
                                            requireField(getString(R.string.reason))
                                        tInputLayoutContactReason.typeface =
                                            Typeface.createFromAsset(
                                                assets,
                                                getString(R.string.museasans_100)
                                            )
                                        if (position == 0) {
                                            etReason.setText("")
                                        }

                                        /* if (position != 0) {

                                         } else {
                                             tInputLayoutGender.hint =
                                                 getString(R.string.gender)
                                             tInputLayoutGender.typeface =
                                                 Typeface.createFromAsset(assets, getString(R.string.museosans_700))
                                         }*/

                                    }

                                    override fun onNothingSelected(parent: AdapterView<*>) {

                                        Log.e("onNothingSelected", "onNothingSelected")

                                    }
                                }
                        }
                    }

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                }

            })


    }

    private fun validateForm(): Boolean {
        var validCount = 6
        if (binding?.contactUsModel?.isNameEmpty()!!) {
            tInputLayoutContactName.error = getString(R.string.error_please_enter_name)
            validCount -= 1
        }
        if (binding?.contactUsModel?.isMailEmpty()!!) {
            tInputLayoutContactEmail.error = getString(R.string.error_please_enter_email_address)
            validCount -= 1
        } else if (!binding?.contactUsModel?.isMailValid()!!) {
            tInputLayoutContactEmail.error =
                getString(R.string.error_please_enter_valid_email_address)
            validCount -= 1
        }
        if (!binding?.contactUsModel?.isMobileNumberEmpty()!! && binding?.contactUsModel?.isMobileNumberValid()!!) {
            tInputLayoutContactNumber.error =
                getString(R.string.error_enter_valid_phone_number)
            validCount -= 1

        }
         if (binding?.contactUsModel?.isReasonEmpty()!!) {
             tInputLayoutContactReason.error = getString(R.string.error_please_select_reason)
             validCount -= 1
         }
        if (binding?.contactUsModel?.isMessageEmpty()!!) {
            tInputLayoutContactMessage.error =
                getString(R.string.error_please_enter_message)
            validCount -= 1
        }
        return validCount == 6
    }

    private fun onFocusListener(): View.OnFocusChangeListener {
        val onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                if (v == etContactName) {
                    if (binding?.contactUsModel?.isNameEmpty()!!) {
                        tInputLayoutContactName.error =
                            getString(R.string.error_please_enter_name)
                    } else {
                        tInputLayoutContactName.error = ""
                    }
                } else if (v == etContactMail) {
                    if (binding?.contactUsModel?.isMailEmpty()!!) {
                        tInputLayoutContactEmail.error =
                            getString(R.string.error_please_enter_email_address)
                    } else if (!binding?.contactUsModel?.isMailValid()!!) {
                        tInputLayoutContactEmail.error =
                            getString(R.string.error_please_enter_valid_email_address)
                    } else {
                        tInputLayoutContactEmail.error = ""
                    }
                } else if (v == etContactNumber) {
                    if (!binding?.contactUsModel?.isMobileNumberEmpty()!! && binding?.contactUsModel?.isMobileNumberValid()!!) {
                        tInputLayoutContactNumber.error =
                            getString(R.string.error_enter_valid_phone_number)
                    } else {
                        tInputLayoutContactNumber.error = ""
                    }
                } else if (v == etContactMessage) {
                    if (binding?.contactUsModel?.isMessageEmpty()!!) {
                        tInputLayoutContactMessage.error =
                            getString(R.string.error_please_enter_message)
                    } else {
                        tInputLayoutContactMessage.error = ""
                    }
                }
            }
        }
        return onFocusChangeListener as View.OnFocusChangeListener
    }

    override fun afterTextChanged(editText: AppCompatEditText) {
        if (etContactName == editText) {
            tInputLayoutContactName.error = ""
        } else if (etContactMail == editText) {
            tInputLayoutContactEmail.error = ""
        } else if (etContactMessage == editText) {
            tInputLayoutContactMessage.error = ""
        } else if (etContactNumber == editText) {
            tInputLayoutContactNumber.error = ""
        }

    }

    private fun setSpinner(spinnerData: MutableList<String>, spinner: Spinner) {
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerData)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = arrayAdapter

    }
}