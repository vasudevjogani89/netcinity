package com.lansingareashoplocal.ui.settings

import android.content.Context
import android.text.TextUtils
import java.util.regex.Matcher
import java.util.regex.Pattern

class ChangePasswordModel {
    var oldPassword: String? = null

    var password: String? = null
    var confirmPassword: String? = null


    val isOldPasswordEmpty: Boolean
        get() = TextUtils.isEmpty(oldPassword?.trim())

    val isNewPasswordEmpty: Boolean
        get() = TextUtils.isEmpty(password?.trim())

    val isconfirmPasswordEmpty: Boolean
        get() = TextUtils.isEmpty(confirmPassword?.trim())



    val isPasswordValidMinLength: Boolean
        get() = password!!.length < 8


    fun comparePasswordAndConfirm(): Boolean {
        return if (password != null && confirmPassword != null) {
            !password?.trim()!!.equals(confirmPassword?.trim()!!, ignoreCase = true)
        } else {
            true
        }

    }

    fun compareOldPasswordAndNewPassword(context: Context): Boolean {
        return if (password != null && oldPassword != null) {
            password?.trim()!!.equals(oldPassword?.trim()!!, ignoreCase = true)
        } else {
            true
        }

    }
    fun isValidPassword(): Boolean {

        val pattern: Pattern
        val matcher: Matcher
        val PASSWORD_PATTERN =  "^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*?[@\$!%*#?&]).{6,}\$$"
        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)

        return !matcher.matches()

    }


    fun isEnterCorrectOldCorrect(context: Context): Boolean {
        return CommonMethods.getDataFromSharePreference(context, AppConstants.PASSWORD)!!.equals(oldPassword)
    }
}