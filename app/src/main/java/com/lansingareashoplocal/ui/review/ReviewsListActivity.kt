package com.lansingareashoplocal.ui.review

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.databinding.ItemReviewsListBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.model.Review
import com.lansingareashoplocal.ui.model.ReviewList
import com.lansingareashoplocal.ui.model.ReviewSummary
import com.lansingareashoplocal.ui.user.LoginActivity
import com.lansingareashoplocal.utility.helper.TimeUtils
import kotlinx.android.synthetic.main.activity_my_reviews_list.*
import kotlinx.android.synthetic.main.activity_review_list.*
import kotlinx.android.synthetic.main.tool_bar_reviews.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton

class ReviewsListActivity : BaseActivity() {
    var businessPlaceId: String? = null
    var businessLogo: String? = null
    private var hasNext = true
    private var pageCount = 0
    var currentPage = 1
    var businessName: String? = null
    var reviewsAdapter: GenericAdapter<Review, ItemReviewsListBinding>? = null
    var reviewList = mutableListOf<Review>()
    var reviewSummary: ReviewSummary? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review_list)
        businessPlaceId = intent.getStringExtra(AppConstants.BUSINESS_ID)
        businessName = intent.getStringExtra(AppConstants.SHARE_KEY)
        businessLogo = intent.getStringExtra(AppConstants.BUSINESS_LOGO)
        tvBusinessName.text = businessName
        val layoutManager = LinearLayoutManager(this@ReviewsListActivity)
        rvReviewsList.setLayoutManager(layoutManager)
        rvReviewsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager != null && layoutManager.findLastCompletelyVisibleItemPosition() == reviewList.size - 1) {
                    if (hasNext) {
                        hasNext = false
                        rlReviewListProgressLayout?.visibility = View.VISIBLE
                        currentPage += 1
                        callReviewListApi(false)
                    }
                }
            }
        })
        setupRatingSection()
        setReviewAdapter()
        if (businessPlaceId != null) {
            callReviewListApi(true)
        }
        tvLeft.setOnClickListener {
            finish()
        }
        tvAddReviewInReviewList.setOnClickListener {
            if (CommonMethods.getUserId(this@ReviewsListActivity)?.isEmpty()!!) {
                CommonMethods.isComeFromSplashScreen = false
                startActivityForResult(
                    Intent(this@ReviewsListActivity, LoginActivity::class.java),
                    AppConstants.LOG_IN_REQUEST_FOR_ADD_REVIEW
                )
            } else {
                redirectToAddReview()
            }


        }
        swipeRefreshReviews.setOnRefreshListener {
            currentPage = 1
            hasNext = false
            callReviewListApi(false)
        }


    }

    private fun isRequireToShowAddReview() {
        if (reviewSummary?.is_review_added.equals("0")) {
            tvAddReviewInReviewList.visibility = View.VISIBLE
        } else {
            tvAddReviewInReviewList.visibility = View.GONE
        }
    }

    private fun setupRatingSection() {
        ratingBarFiveStars.rating = 5f
        ratingBarFourStars.rating = 4f
        ratingBarThreeStars.rating = 3f
        ratingBarTwoStars.rating = 2f
        ratingBarOneStars.rating = 1f
    }


    private fun setReviewAdapter() {
        reviewsAdapter = object : GenericAdapter<Review, ItemReviewsListBinding>(reviewList) {
            override fun getLayoutId(): Int {
                return R.layout.item_reviews_list
            }

            override fun onBindData(
                model: Review?,
                position: Int,
                dataBinding: ItemReviewsListBinding?
            ) {
                Glide.with(this@ReviewsListActivity).load(model?.profileImage)
                    .into(dataBinding?.ivReviewer!!)
                dataBinding?.tvReviewerName.text = model?.userName
                dataBinding?.tvReview.text = model?.review
                dataBinding?.tvReviewDate.text =
                    TimeUtils.convertDateWithTimeForShowingApp(
                        model?.reviewDate
                    )
                if (model?.rating?.isNotEmpty()!!) {
                    dataBinding?.ratingBar.rating = model?.rating?.toFloat()
                }

                if (position == reviewList.size - 1) {
                    dataBinding?.viewReviewSeparator.visibility = View.GONE
                }
                if (model.publish_status.equals("Pending", true)) {
                    dataBinding?.ivEditReview.visibility = View.VISIBLE
                } else {
                    dataBinding?.ivEditReview.visibility = View.GONE
                }

                dataBinding?.ivEditReview.setOnClickListener {
                    var intent = Intent(this@ReviewsListActivity, AddReviewActivity::class.java)
                    intent.putExtra(AppConstants.BUSINESS_ID, businessPlaceId)
                    intent.putExtra(AppConstants.BUSINESS_LOGO, businessLogo)
                    intent.putExtra(AppConstants.BUSINESS_NAME, businessName)
                    intent.putExtra(AppConstants.RATING_ID, model.reviewId)
                    intent.putExtra(AppConstants.REVIEW, model.review)
                    intent.putExtra(AppConstants.RATING, model.rating)
                    intent.putExtra(AppConstants.IS_EDITING, true)
                    startActivityForResult(intent, AppConstants.UPDATE_REVIEW_REQUEST)
                }
            }
        }
        rvReviewsList.adapter = reviewsAdapter

    }


    fun callReviewListApi(isLoaderRequired: Boolean) {
        val inputparams = HashMap<String, String>()
        inputparams.put(ApiConstants.BUSINESS_PLACE_ID, businessPlaceId!!)
        inputparams.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputparams.put(
            ApiConstants.USER_ID,
            CommonMethods.getUserId(this@ReviewsListActivity)!!
        )
        inputparams.put(ApiConstants.PAGE_INDEX, "" + currentPage)

        val call = ApiClient.getRetrofitInterface()?.getReviewsList(inputparams)

        WSClient<Response<ReviewList>>().request(
            this@ReviewsListActivity,
            100,
            isLoaderRequired,
            call,
            object : ISuccessHandler<Response<ReviewList>> {

                override fun successResponse(requestCode: Int, mResponse: Response<ReviewList>) {
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        rlReviewListProgressLayout?.visibility = View.GONE
                        hasNext = mResponse.settings?.nextPage == 1
                        pageCount = mResponse.settings?.perPage!!

                        if (currentPage == 1) {
                            if (mResponse.data?.reviewListing != null) {
                                reviewList = mResponse.data!!.reviewListing as MutableList<Review>
                                reviewsAdapter?.setData(reviewList)
                                if ( mResponse.data?.reviewSummary!=null && mResponse.data?.reviewSummary?.isNotEmpty()!!) {
                                    reviewSummary = mResponse.data!!.reviewSummary[0]
                                    setReviewSummary()
                                }

                            }

                        } else {
                            if (mResponse.data?.reviewSummary != null && mResponse.data?.reviewSummary?.isNotEmpty()!!) {
                                if (mResponse.data?.reviewSummary?.isNotEmpty()!!) {
                                    reviewsAdapter?.addItems(mResponse.data!!.reviewSummary[0] as MutableList<Review>)
                                }


                            }
                        }

                        if (swipeRefreshReviews.isRefreshing) {
                            swipeRefreshReviews.isRefreshing = false
                        }

                    }
                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    rlMyReviewListProgressLayout?.visibility = View.GONE
                    if (swipeRefreshReviews.isRefreshing) {
                        swipeRefreshReviews.isRefreshing = false
                    }
                    alert(message) { okButton { } }.show()

                }

            })

    }

    @SuppressLint("SetTextI18n")
    private fun setReviewSummary() {
        tvFiveStartCount.text = "(${reviewSummary?.starRatingCountFive})"
        tvFourStartCount.text = "(${reviewSummary?.starRatingCountFour})"
        tvThreeStartCount.text = "(${reviewSummary?.starRatingCountThree})"
        tvTwoStartCount.text = "(${reviewSummary?.starRatingCountTwo})"
        tvOneStartCount.text = "(${reviewSummary?.starRatingCountOne})"
        tvRatingListCount.text = "(${reviewSummary?.totalReviewCount})"
        tvAvgRating.text = "(${reviewSummary?.avgRatingCount})"
        isRequireToShowAddReview()
        try {
            ratingBarAvg.rating = reviewSummary?.avgRatingCount?.toFloat()!!
        } catch (e: Exception) {

        }


    }


    fun redirectToAddReview() {
        var intent = Intent(this@ReviewsListActivity, AddReviewActivity::class.java)
        intent.putExtra(AppConstants.BUSINESS_ID, businessPlaceId)
        intent.putExtra(AppConstants.BUSINESS_LOGO, businessLogo)
        intent.putExtra(AppConstants.BUSINESS_NAME, businessName)
        intent.putExtra(AppConstants.IS_EDITING, false)
        startActivityForResult(intent, AppConstants.ADD_REVIEW_REQUEST)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode.equals(AppConstants.LOG_IN_REQUEST_FOR_ADD_REVIEW) && resultCode == Activity.RESULT_OK) {
            redirectToAddReview()
        } else if (requestCode.equals(AppConstants.ADD_REVIEW_REQUEST) && resultCode == Activity.RESULT_OK) {
            callReviewListApi(true)
        } else if (requestCode.equals(AppConstants.UPDATE_REVIEW_REQUEST) && resultCode == Activity.RESULT_OK) {
            callReviewListApi(true)
        }
    }
}