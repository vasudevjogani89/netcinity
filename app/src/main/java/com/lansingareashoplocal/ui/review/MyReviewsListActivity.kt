package com.lansingareashoplocal.ui.review

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.databinding.ItemMyReviewsBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.model.MyReviews
import kotlinx.android.synthetic.main.activity_my_reviews_list.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import java.lang.Exception

class MyReviewsListActivity : BaseActivity() {
    private var hasNext = true
    private var pageCount = 0
    var currentPage = 1
    var myReviewAdapter: GenericAdapter<MyReviews, ItemMyReviewsBinding>? = null
    var myReviewList = mutableListOf<MyReviews>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_reviews_list)
        toolBarMyReviews.tvLeft.setOnClickListener {
            finish()
        }
        toolBarMyReviews.tvTitle.text = getString(R.string.my_reviews)
        val layoutManager = LinearLayoutManager(this@MyReviewsListActivity)
        rvMyReviews.setLayoutManager(layoutManager)
        rvMyReviews.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager != null && layoutManager.findLastCompletelyVisibleItemPosition() == myReviewList.size - 1) {
                    if (hasNext) {
                        hasNext = false
                        rlMyReviewListProgressLayout?.visibility = View.VISIBLE
                        currentPage += 1
                        callMyReviewApi(false)
                    }
                }
            }
        })
        setMyReviewsAdapter()
        callMyReviewApi(true)
        swipeRefreshMyReviews.setOnRefreshListener {
            currentPage = 1
            hasNext = false
            callMyReviewApi(false)
        }

    }

    private fun callMyReviewApi(isLoaderRequired: Boolean) {
        var inputParams = HashMap<String, String>()
        inputParams.put(ApiConstants.USER_ID, CommonMethods.getUserId(this@MyReviewsListActivity)!!)
        inputParams.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputParams.put(ApiConstants.PAGE_INDEX,""+currentPage)


        var call = ApiClient.getRetrofitInterface()?.getMyReview(inputParams)
        WSClient<Response<List<MyReviews>>>().request(
            this@MyReviewsListActivity,
            100,
            isLoaderRequired,
            call,
            object : ISuccessHandler<Response<List<MyReviews>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<MyReviews>>
                ) {
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        rlMyReviewListProgressLayout?.visibility = View.GONE
                        tvNoMyReviews.visibility=View.GONE
                        hasNext = mResponse.settings?.nextPage == 1
                        pageCount = mResponse.settings?.perPage!!
                        if (currentPage == 1) {
                            myReviewList = mResponse.data!!as MutableList<MyReviews>
                            myReviewAdapter?.setData(myReviewList)
                        } else {
                            myReviewAdapter?.addItems(mResponse.data!! as MutableList<MyReviews>)
                        }

                    }
                    else{
                        tvNoMyReviews.visibility=View.VISIBLE
                        tvNoMyReviews.text =mResponse.settings?.message
                    }
                    if (swipeRefreshMyReviews.isRefreshing) {
                        swipeRefreshMyReviews.isRefreshing = false
                    }
                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    if (swipeRefreshMyReviews.isRefreshing) {
                        swipeRefreshMyReviews.isRefreshing = false
                    }
                    rlMyReviewListProgressLayout?.visibility = View.GONE
                    alert(message) {
                        okButton {  }
                    }.show()
                }

            })
    }

    private fun setMyReviewsAdapter() {
        myReviewAdapter = object : GenericAdapter<MyReviews, ItemMyReviewsBinding>(myReviewList) {
            override fun getLayoutId(): Int {
                return R.layout.item_my_reviews
            }

            override fun onBindData(
                model: MyReviews?,
                position: Int,
                dataBinding: ItemMyReviewsBinding?
            ) {
                Glide.with(this@MyReviewsListActivity).load(model?.icon)
                    .into(dataBinding?.ivMyReviewBusinessLogo!!)
                dataBinding?.tvReviewBusinessName.text = model?.place_name
                try {
                    dataBinding?.ratingBarMyReview.rating = model?.rating?.toFloat()!!
                } catch (e: Exception) {

                }
                dataBinding?.tvMyReviews.text = model?.review
                if (model?.publishStatus.equals("Pending", true)) {
                    dataBinding?.ivEditMyReview.visibility=View.VISIBLE
                } else {
                    dataBinding?.ivEditMyReview.visibility=View.GONE
                }
                dataBinding?.ivEditMyReview.setOnClickListener {
                    val intent = Intent(this@MyReviewsListActivity, AddReviewActivity::class.java)
                    intent.putExtra(AppConstants.BUSINESS_ID, model?.businessPlaceId)
                    intent.putExtra(AppConstants.BUSINESS_LOGO,model?.icon)
                    intent.putExtra(AppConstants.BUSINESS_NAME, model?.place_name)
                    intent.putExtra(AppConstants.RATING_ID,model?.reviewId)
                    intent.putExtra(AppConstants.REVIEW,model?.review)
                    intent.putExtra(AppConstants.RATING,model?.rating)
                    intent.putExtra(AppConstants.IS_EDITING, true)
                    startActivityForResult(intent,AppConstants.UPDATE_REVIEW_REQUEST)
                }


            }


        }
        rvMyReviews.adapter=myReviewAdapter

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==AppConstants.UPDATE_REVIEW_REQUEST&& resultCode== Activity.RESULT_OK){
            callMyReviewApi(true)
        }
    }
}