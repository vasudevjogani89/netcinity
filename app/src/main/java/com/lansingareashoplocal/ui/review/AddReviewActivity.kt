package com.lansingareashoplocal.ui.review

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.app.Activity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import kotlinx.android.synthetic.main.activity_add_review.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton

class AddReviewActivity : BaseActivity() {
    var businessLogo: String? = null
    var businessId: String? = null
    var businessName: String? = null
    var isEditing: Boolean? = null
    var reviewId: String? = null
    var review: String? = null
    var rating: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_review)
        tool_bar_add_review.tvLeft.setOnClickListener {
            finish()
        }

        businessName = intent.getStringExtra(AppConstants.BUSINESS_NAME)
        businessLogo = intent.getStringExtra(AppConstants.BUSINESS_LOGO)
        businessId = intent.getStringExtra(AppConstants.BUSINESS_ID)
        isEditing = intent.getBooleanExtra(AppConstants.IS_EDITING, false)
        if (isEditing != null && isEditing!!) {
            reviewId = intent.getStringExtra(AppConstants.RATING_ID)
            review = intent.getStringExtra(AppConstants.REVIEW)
            rating = intent.getStringExtra(AppConstants.RATING)
        }
        setData()
        btnReviewSumbit.setOnClickListener {


            if (ratingBarMyReview.rating < 1) {
                alert(getString(R.string.select_the_rating_alert)) {
                    okButton { }
                }.show()
            } else if (ratingBarMyReview.rating < 4 && etComment.text?.trim()?.isEmpty()!!) {
                alert(getString(R.string.please_add_review_cooment_alert)) {
                    okButton { }
                }.show()

            } else {

                if (btnReviewSumbit.text.equals(getString(R.string.submit))) {
                    callAddReviewApi()
                } else {
                    callUpdateReviewApi()

                }

            }
        }
    }

    private fun setData() {
        if (isEditing != null) {
            if (isEditing!!) {
                tool_bar_add_review.tvTitle.text = getString(R.string.edit_review)
                btnReviewSumbit.text = getString(R.string.update)
                prefillData()
            } else {
                btnReviewSumbit.text = getString(R.string.submit)
                tool_bar_add_review.tvTitle.text = getString(R.string.add_review)
            }
        }

        tvLastOrder.text = "${getString(R.string.how_was_your_last_order_n_from)} ${businessName}?"
        Glide.with(this@AddReviewActivity).load(businessLogo).into(ivMyReviewBusinessLogo)
    }


    fun callAddReviewApi() {

        val inputParams = HashMap<String, String>()
        if (businessId != null) {
            inputParams.put(ApiConstants.BUSINESS_PLACE_ID, businessId!!)
        }

        var userGivenRating = ratingBarMyReview.rating.toInt()
        inputParams.put(ApiConstants.RATING, userGivenRating.toString())
        inputParams.put(ApiConstants.USER_ID, CommonMethods.getUserId(this@AddReviewActivity)!!)
        inputParams.put(ApiConstants.REVIEW, etComment.text?.trim().toString())
        var call = ApiClient.getRetrofitInterface()?.addReview(inputParams)

        WSClient<Response<List<Any>>>().request(
            this@AddReviewActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<Any>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Any>>) {
                    if (mResponse.settings?.success.equals("1")) {
                        val alert = alert(mResponse.settings?.message.toString()) {
                            okButton {
                                setResult(Activity.RESULT_OK)
                                finish()
                            }
                        }.show()
                        alert.setCancelable(false)

                    } else {
                        alert(mResponse.settings?.message.toString()) {
                            okButton {   setResult(Activity.RESULT_OK)
                                finish() }
                        }.show()
                    }

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert(message) { okButton { } }.show()
                }

            })
    }


    fun callUpdateReviewApi() {
        val inputParams = HashMap<String, String>()
        if (businessId != null) {
            inputParams.put(ApiConstants.BUSINESS_PLACE_ID, businessId!!)
        }
        var userGivenRating = ratingBarMyReview.rating.toInt()
        inputParams.put(ApiConstants.RATING, userGivenRating.toString())
        inputParams.put(ApiConstants.USER_ID, CommonMethods.getUserId(this@AddReviewActivity)!!)
        inputParams.put(ApiConstants.REVIEW, etComment.text?.trim().toString())
        if (reviewId != null) {
            inputParams.put(ApiConstants.REVIEW_ID, reviewId!!)
        }
        var call = ApiClient.getRetrofitInterface()?.editReview(inputParams)

        WSClient<Response<List<Any>>>().request(
            this@AddReviewActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<Any>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Any>>) {
                    if (mResponse.settings?.success.equals("1")) {
                        val alert = alert(mResponse.settings?.message.toString()) {
                            okButton {
                                setResult(Activity.RESULT_OK)
                                finish()
                            }
                        }.show()
                        alert.setCancelable(false)

                    } else {
                        alert(mResponse.settings?.message.toString()) {
                            okButton {   setResult(Activity.RESULT_OK)
                                finish()}
                        }.show()
                    }

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert(message) { okButton { } }.show()
                }

            })


    }


    fun prefillData() {
        if (review != null) {
            etComment.setText(review)
        }
        if (rating != null) {
            try {
                ratingBarMyReview.rating = rating?.toFloat()!!
            } catch (e: Exception) {

            }
        }


    }
}