package com.lansingareashoplocal.ui.intro

import ApiClient
import ApiConstants
import AppConstants
import CommonMethods
import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.TextUtils
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.tasks.OnFailureListener
import com.google.firebase.remoteconfig.BuildConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.service.BackgroundScanService
import com.lansingareashoplocal.ui.communityevents.CommunityEventDetailsActivity
import com.lansingareashoplocal.ui.home.HomeActivity
import com.lansingareashoplocal.ui.model.UserDetails
import com.lansingareashoplocal.ui.newsandalert.NewsAndAlertDetailsActivity
import com.lansingareashoplocal.ui.promotions.PromotionDetailsActivity
import com.lansingareashoplocal.utility.helper.others.GpsUtils
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.okButton

class SplashActivity : BaseActivity() {
    //  lateinit var logger: Logger
    private val SPLASH_DURATION = 2000L



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
        callTrackAppDownload()
        makeFullScreen()
        CommonMethods.printKeyHash(this@SplashActivity)
        appUpdateManager = AppUpdateManagerFactory.create(this@SplashActivity)
        // Returns an intent object that you use to check for an update.
        appUpdateInfoTask = appUpdateManager?.appUpdateInfo
        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask?.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                showAlertForRestartApp()
            }
            else  if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                val firebaseRemoteConfig: FirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
                val appVersionCode =
                    firebaseRemoteConfig.getString(AppConstants.ANDROID_MINIMUM_REQUIRED_VERSION_CODE)
                try {
                    if (appVersionCode != null && appVersionCode.isNotEmpty()) {
                        if (appVersionCode.toInt() < BuildConfig.VERSION_CODE) {
                            immediateRequest(appUpdateInfo)
                        } else {
                            appUpdateManager?.registerListener(listener)
                            flexibleUpdateRequest(appUpdateInfo)

                        }
                    } else {
                        appUpdateManager?.registerListener(listener)
                        flexibleUpdateRequest(appUpdateInfo)
                    }
                } catch (e: Exception) {
                    if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                        showAlertForRestartApp()
                    } else {
                        //Register the listener
                        appUpdateManager?.registerListener(listener)
                        flexibleUpdateRequest(appUpdateInfo)
                    }
                }


            } else {
                checkBluetoothAndLocationPermissions()
            }
        }
        appUpdateInfoTask?.addOnFailureListener(object : OnFailureListener {
            override fun onFailure(e: java.lang.Exception?) {
                checkBluetoothAndLocationPermissions()
            }

        })


    }


    fun checkBluetoothAndLocationPermissions() {
        when {
            !CommonMethods.isBluetoothEnabled() -> {
                val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(intent, 100)
            }

            !isLocationAppPermissionAvalible() -> {
                checkPermissionAndStart()
            }

            !CommonMethods.isLocationEnabled(this@SplashActivity) -> {
                showGpsLocationRequireAlert()


            }


            else -> {
                moveForward()
            }

        }
    }


    //make screen with status bar
    private fun makeFullScreen() {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

    //once user is already login we calling the login api and user is not login we are redirecting to login screen

    private fun moveForward() {
        if (intent != null && intent!!.extras != null && intent.getStringExtra(AppConstants.TYPE) != null) {
            notificationRedirection(intent)
        } else {
            if (isLocationAppPermissionAvalible() && CommonMethods.isBluetoothEnabled() && CommonMethods.isLocationEnabled(
                    this@SplashActivity
                )
            ) {
                startBackgroundService()
            } else {
                if (!isLocationAppPermissionAvalible()) {
                    //  logger.debugEvent("App Location permission not avalible", "")
                } else if (!CommonMethods.isLocationEnabled(this@SplashActivity)) {
                    //  logger.debugEvent("Device Location permission not avalible", "")
                } else if (!CommonMethods.isBluetoothEnabled()) {
                    // logger.debugEvent("Bluetooth permission not avalible", "")
                }

            }

            if (TextUtils.isEmpty(CommonMethods.getUserId(this@SplashActivity))) {
                Handler().postDelayed({
                    startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                    finish()
                }, SPLASH_DURATION)
            } else {
                if (CommonMethods.checkInternetConnection(this@SplashActivity)) {
                    if (CommonMethods.getUserData(this@SplashActivity)?.facebook_id?.isEmpty()!! && CommonMethods.getUserData(
                            this@SplashActivity
                        )?.google_id?.isEmpty()!!
                    ) {
                        callLoginApi()
                    } else {
                        callSocialSignIn()
                    }

                } else {
                    Handler().postDelayed({
                        startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                        finish()
                    }, SPLASH_DURATION)
                }


            }
        }


    }

    private fun notificationRedirection(intent: Intent?) {


        val type = intent!!.getStringExtra(AppConstants.TYPE)

        if (type.equals("local")) {
            // logger.dumpCustomEvent("LocalNotificationClick", "")
            val business_id = intent.getStringExtra(AppConstants.BUSINESS_ID)
            if (business_id != null) {
                val intent = Intent(
                    this@SplashActivity,
                    PromotionDetailsActivity::class.java
                )
                intent.putExtra(AppConstants.BUSINESS_ID, business_id)
                intent.putExtra(AppConstants.IS_FROM_BECON_NOTIFICATION,true)
                Handler().postDelayed({
                    startActivity(intent)
                    finish()
                }, SPLASH_DURATION)


            }

        }

        else if(type.equals("update")){
            appUpdateManager?.completeUpdate()
        }

        else {
            val notificationObject =
                intent!!.getSerializableExtra("BundleData") as HashMap<String, String>

            if (notificationObject["code"]!!.contains(AppConstants.EVENT)!!) {
                if (notificationObject.containsKey("others")) {
                    val event = notificationObject["others"]
                    if (event != null) {
                        val map = Gson().fromJson<HashMap<String, String>>(
                            event,
                            object : TypeToken<HashMap<String, String>>() {
                            }.type
                        )
                        if (map != null) {
                            if (event.contains(AppConstants.COMMUNITY_ID) && event.contains(
                                    AppConstants.EVENT_ID
                                )
                            ) {
                                val community_id = map[AppConstants.COMMUNITY_ID]
                                val event_id = map[AppConstants.EVENT_ID]
                                if (community_id != null && event_id != null) {
                                    val intent = Intent(
                                        this@SplashActivity,
                                        CommunityEventDetailsActivity::class.java
                                    )
                                    intent.putExtra(AppConstants.COMMUNITY_ID, community_id)
                                    intent.putExtra(AppConstants.EVENT_ID, event_id)
                                    intent.putExtra(AppConstants.IS_NEED_TO_CALL_API, true)
                                    Handler().postDelayed({
                                        startActivity(intent)
                                        finish()
                                    }, SPLASH_DURATION)


                                }

                            }
                        }
                    }


                }


            }
            else if (notificationObject["code"]!!.contains(AppConstants.ALERT)!!) {

                if (notificationObject.containsKey("others")) {
                    val alert = notificationObject["others"]
                    if (alert != null) {
                        val map = Gson().fromJson<HashMap<String, String>>(
                            alert,
                            object : TypeToken<HashMap<String, String>>() {
                            }.type
                        )
                        if (map != null) {
                            if (alert.contains(AppConstants.COMMUNITY_ID) && alert.contains(
                                    AppConstants.NOTIFICATION_ID
                                )
                            ) {
                                val notification_id = map[AppConstants.NOTIFICATION_ID]
                                if ( notification_id != null) {
                                    Handler().postDelayed({
                                        val intentNewAndAlert = Intent(this@SplashActivity, NewsAndAlertDetailsActivity::class.java)
                                        intentNewAndAlert.putExtra(AppConstants.NOTIFICATION_ID,notification_id)
                                        startActivity(intentNewAndAlert)
                                        finish()

                                    }, SPLASH_DURATION)


                                }

                            }
                        }
                    }
                }


            }
            else if (notificationObject["code"]!!.contains(AppConstants.DIRECT)!!) {

                val message = notificationObject["message"]
                val imageUrl = notificationObject["image"]

                Handler().postDelayed({
                    startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                    if (message != null && message.isNotEmpty()) {
                        CommonMethods.setDataIntoSharePreference(
                            this@SplashActivity,
                            AppConstants.DIRECT_MESSAGE,
                            message
                        )
                    }

                    if (imageUrl != null && imageUrl.isNotEmpty()) {
                        CommonMethods.setDataIntoSharePreference(
                            this@SplashActivity,
                            AppConstants.IMAGE_URL,
                            imageUrl
                        )
                    }

                    finish()
                }, SPLASH_DURATION)


                /*     if (message != null) {
                         alert(message) {
                             okButton {

                             }
                         }.show()
                     }*/


            }
            else if (notificationObject["code"]!!.contains(AppConstants.B_DIR)!!) {

                if (notificationObject.containsKey("others")) {
                    val businessDetails = notificationObject["others"]
                    if (businessDetails != null) {
                        val map = Gson().fromJson<HashMap<String, String>>(
                            businessDetails,
                            object : TypeToken<HashMap<String, String>>() {
                            }.type
                        )
                        if (map != null) {
                            if (businessDetails.contains(AppConstants.BUSINESS_ID)) {
                                val business_id = map[AppConstants.BUSINESS_ID]
                                if (business_id != null) {
                                    val intent = Intent(
                                        this@SplashActivity,
                                        PromotionDetailsActivity::class.java
                                    )
                                    intent.putExtra(AppConstants.BUSINESS_ID, business_id)
                                    Handler().postDelayed({
                                        startActivity(intent)
                                        finish()
                                    }, SPLASH_DURATION)


                                }

                            }
                        }
                    }


                }


            }

        }


    }

    fun callLoginApi() {
        val inputParams = HashMap<String, String>()
        inputParams[ApiConstants.EMAIL] = CommonMethods.getUserData(this@SplashActivity)?.email!!
        inputParams[ApiConstants.PASSWORD] =
            CommonMethods.getDataFromSharePreference(this@SplashActivity, AppConstants.PASSWORD)
        inputParams[ApiConstants.DEVICE_TOKEN] =
            CommonMethods.getDataFromSharePreference(this@SplashActivity, AppConstants.DEVICE_TOKEN)
        inputParams[ApiConstants.DEVICE_TYPE] = getString(R.string.android)
        inputParams[ApiConstants.DEVICE_OS] = getString(R.string.android)
        inputParams[ApiConstants.DEVICE_NAME] = CommonMethods.getDeviceName()
        inputParams[ApiConstants.APP_VERSION] = BuildConfig.VERSION_NAME
        inputParams[ApiConstants.COMMUNITY_ID] = CommonMethods.cummunity_id
        inputParams[ApiConstants.DEVICE_UDID] =
            Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
        var call = ApiClient.getRetrofitInterface()?.logIn(inputParams)


        WSClient<Response<List<UserDetails>>>().request(
            this@SplashActivity,
            100,
            false,
            call,
            object : ISuccessHandler<Response<List<UserDetails>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<UserDetails>>
                ) {
                    if (mResponse?.settings?.success.equals("1")) {
                        if (mResponse.data?.get(0) != null) {
                            CommonMethods.setUserData(this@SplashActivity, mResponse.data?.get(0))
                            val intent = Intent(this@SplashActivity, HomeActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent)
                            finish()
                        }
                    } else {
                        alert(mResponse?.settings?.message.toString()) {
                            okButton { }
                        }.show()
                    }


                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                    alert(message) {
                        okButton { }
                    }.show()
                }

            })
    }

    private fun callSocialSignIn() {



        val inputparams = HashMap<String, String>()

        if (CommonMethods.getUserData(this@SplashActivity)?.social_type.equals(ApiConstants.FACEBOOK)) {
            inputparams.put(
                ApiConstants.FACEBOOK_ID,
                CommonMethods.getUserData(this@SplashActivity)?.facebook_id!!
            )
            inputparams.put(ApiConstants.SOCIAL_TYPE, ApiConstants.FACEBOOK)
        } else {
            inputparams.put(
                ApiConstants.GOOGLE_ID,
                CommonMethods.getUserData(this@SplashActivity)?.google_id!!
            )
            inputparams.put(ApiConstants.SOCIAL_TYPE, ApiConstants.GOOGLE)
        }


        inputparams.put(ApiConstants.DEVICE_TOKEN, CommonMethods.getDataFromSharePreference(this@SplashActivity, AppConstants.DEVICE_TOKEN))
        inputparams.put(ApiConstants.DEVICE_OS, getString(R.string.android))
        inputparams.put(ApiConstants.DEVICE_NAME, CommonMethods.getDeviceName())
        inputparams.put(ApiConstants.DEVICE_TYPE, getString(R.string.android))
        inputparams.put(ApiConstants.APP_VERSION, BuildConfig.VERSION_NAME)
        inputparams.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputparams.put(ApiConstants.DEVICE_UDID, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID))
        val call = ApiClient.getRetrofitInterface()?.socialSignUp(inputparams)


        WSClient<Response<List<UserDetails>>>().request(
            this@SplashActivity,
            100,
            false,
            call,
            object : ISuccessHandler<Response<List<UserDetails>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<UserDetails>>
                ) {
                    if (mResponse?.settings?.success.equals("1")) {
                        if (mResponse.data?.get(0) != null) {

                            CommonMethods.setUserData(this@SplashActivity, mResponse.data?.get(0))
                            CommonMethods.setDataIntoSharePreference(
                                this@SplashActivity,
                                AppConstants.USER_ID,
                                mResponse.data?.get(0)?.userId
                            )
                            CommonMethods.setDataIntoSharePreference(
                                this@SplashActivity,
                                AppConstants.FACEBOOK_ID,
                                mResponse.data!!.get(0).facebook_id
                            )
                            val intent = Intent(this@SplashActivity, HomeActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent)
                            finish()
                        }
                    } else {
                        alert(mResponse?.settings?.message!!) {
                            okButton {

                            }
                        }.show()
                    }


                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                    alert(message) {
                        okButton {

                        }
                    }.show()
                }

            })

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == AppConstants.LOCATION_REQUEST && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && CommonMethods.isLocationEnabled(
                this@SplashActivity
            ) && CommonMethods.isLocationEnabled(this@SplashActivity)
        ) {
            moveForward()
        } else {
            if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                showExplanationDialog()
            } else {
                if (!CommonMethods.isLocationEnabled(this@SplashActivity)) {
                    showGpsLocationRequireAlert()
                }
            }
        }

    }

    private fun checkPermissionAndStart(): Boolean {
        val checkSelfPermissionResult = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        if (PackageManager.PERMISSION_GRANTED == checkSelfPermissionResult) {
            //already granted
            return true
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                showExplanationDialog()
                //we should show some explanation for user here
            } else {
                //request permission
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), AppConstants.LOCATION_REQUEST)
            }
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            100 -> {
                if (isLocationAppPermissionAvalible() && CommonMethods.isLocationEnabled(this@SplashActivity)
                ) {
                    moveForward()
                } else {
                    if (!isLocationAppPermissionAvalible()) {
                        checkPermissionAndStart()
                    } else if (!CommonMethods.isLocationEnabled(this@SplashActivity)) {
                        showGpsLocationRequireAlert()

                    }
                }

            }
            AppConstants.GPS_REQUEST -> {
                moveForward()


            }
            AppConstants.LOCATION_REQUEST -> {
                if (CommonMethods.isLocationEnabled(this@SplashActivity) && isLocationAppPermissionAvalible()
                ) {
                    moveForward()
                } else {
                    if (isLocationAppPermissionAvalible()) {
                        if (!CommonMethods.isLocationEnabled(this@SplashActivity)) {
                            showGpsLocationRequireAlert()
                        }
                    } else {
                        moveForward()
                    }


                }

            }
            AppConstants.FLEXIBLE_UPDATE_REQUEST_CODE -> {
                checkBluetoothAndLocationPermissions()

            }
            AppConstants.IMMEDIATE_UPDATE_REQUEST_CODE -> {
                checkBluetoothAndLocationPermissions()
            }
        }
    }


    //calling login api

    private fun showExplanationDialog() {
        val alert=   alert(CommonMethods.getLocationPermissionAlert(this@SplashActivity)) {
            okButton {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                val uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, AppConstants.LOCATION_REQUEST);
            }
            cancelButton {
                moveForward()

            }
        }.show()
        alert.setCancelable(false)
    }


    private fun isLocationAppPermissionAvalible(): Boolean {


        return (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION))
    }

    private fun startBackgroundService() {
        startService(Intent(this@SplashActivity, BackgroundScanService::class.java))
    }


    public fun showGpsLocationRequireAlert() {
        GpsUtils(this@SplashActivity).turnGPSOn {
        }
    }


    fun callTrackAppDownload(){
        val inputParams= HashMap<String,String>()
        inputParams[ApiConstants.COMMUNITY_ID] = CommonMethods.cummunity_id
        inputParams[ApiConstants.DEVICE_TYPE] = getString(R.string.android)
        inputParams[ApiConstants.DEVICE_UDID] = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
        val call =ApiClient.getRetrofitInterface()?.trackAppDownload(inputParams)
        WSClient<Response<Any>>().request(this@SplashActivity,100,false,call,null,null)


    }










}


