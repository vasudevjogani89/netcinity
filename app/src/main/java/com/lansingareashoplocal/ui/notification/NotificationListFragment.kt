package com.lansingareashoplocal.ui.notification

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.content.Intent
import android.provider.Settings
import android.text.format.DateUtils
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseFragment
import com.lansingareashoplocal.databinding.FragmentUserNotificationBinding
import com.lansingareashoplocal.databinding.ItemUserNotificationBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.communityevents.CommunityEventDetailsActivity
import com.lansingareashoplocal.ui.direct_message.DialogDirectMessage
import com.lansingareashoplocal.ui.model.BusinessDetails
import com.lansingareashoplocal.ui.model.NonFeature
import com.lansingareashoplocal.ui.model.UserNotification
import com.lansingareashoplocal.ui.newsandalert.NewsAndAlertDetailsActivity
import com.lansingareashoplocal.ui.promotions.NonFeatureListActivity
import com.lansingareashoplocal.ui.promotions.PromotionDetailsActivity
import org.jetbrains.anko.okButton
import org.jetbrains.anko.support.v4.alert
import java.util.*
import kotlin.collections.HashMap
class NotificationListFragment : BaseFragment<FragmentUserNotificationBinding>() {
    private var hasNext = true
    private var pageCount = 0
    var currentPage = 1
    var useNotificationAdapter: GenericAdapter<UserNotification, ItemUserNotificationBinding>? =
        null
    var userNotificationList = mutableListOf<UserNotification>()
    override fun onBackPressed(): Boolean {
        return false
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_user_notification
    }

    override fun init() {
        setAdapter()
        callUserNotification(true)
        val layoutManager = LinearLayoutManager(activity)
        binding.rvUserNotification.setLayoutManager(layoutManager)
        binding.rvUserNotification.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager != null && layoutManager.findLastCompletelyVisibleItemPosition() == userNotificationList.size - 1) {
                    if (hasNext) {
                        hasNext = false
                        binding.rlUserNotificationProgressLayout?.visibility = View.VISIBLE
                        currentPage += 1
                        callUserNotification(false)
                    }
                }
            }
        })

        binding.swipeRefreshUserNotification.setOnRefreshListener {
            currentPage = 1
            hasNext = false
            callUserNotification(false)
        }
    }

    private fun callUserNotification(isRequireShowLoader: Boolean) {
        val inputParams = HashMap<String, String>()
        inputParams.put(ApiConstants.PAGE_INDEX, "" + currentPage)
        inputParams.put(ApiConstants.COMMUNITY_ID,CommonMethods.cummunity_id)
        if (CommonMethods.getUserId(activity!!)!!.isEmpty()) {
            inputParams.put(ApiConstants.USER_ID, "0")
        } else {
            inputParams.put(ApiConstants.USER_ID, CommonMethods.getUserId(activity!!)!!)
        }

        inputParams.put(ApiConstants.DEVICE_UDID, Settings.Secure.getString(activity!!.getContentResolver(), Settings.Secure.ANDROID_ID)
        )
        val call = ApiClient.getRetrofitInterface()?.getUserNotification(inputParams)
        WSClient<Response<List<UserNotification>>>().request(
            activity!!,
            100,
            isRequireShowLoader,
            call,
            object : ISuccessHandler<Response<List<UserNotification>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<UserNotification>>
                ) {
                    if (mResponse.settings?.success.equals("1")) {
                        binding.rlUserNotificationProgressLayout?.visibility = View.GONE
                        hasNext = mResponse.settings?.nextPage == 1
                        pageCount = mResponse.settings?.perPage!!
                        if (currentPage == 1) {
                            userNotificationList = mResponse.data as MutableList<UserNotification>
                            useNotificationAdapter?.setData(userNotificationList)
                        } else {
                            useNotificationAdapter?.addItems(mResponse.data as MutableList<UserNotification>)
                        }
                    }
                    if (binding.swipeRefreshUserNotification.isRefreshing) {
                        binding.swipeRefreshUserNotification.isRefreshing = false
                    }
                    if (userNotificationList.isEmpty()) {
                        binding.tvNoData.text = mResponse.settings?.message
                        binding.tvNoData.visibility = View.VISIBLE
                    } else {
                        binding.tvNoData.visibility = View.INVISIBLE
                    }
                }

            }, object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    binding.rlUserNotificationProgressLayout?.visibility = View.GONE
                    alert(message) {
                        okButton {  }
                    }.show()
                }

            })


    }

    private fun setAdapter() {
        useNotificationAdapter = object :
            GenericAdapter<UserNotification, ItemUserNotificationBinding>(userNotificationList) {
            override fun onBindData(
                model: UserNotification?,
                position: Int,
                dataBinding: ItemUserNotificationBinding?
            ) {
                dataBinding?.tvNotification?.text = model?.message
                if (model?.notiDate!!.isNotEmpty()) {
                    val cal = Calendar.getInstance()
                    dataBinding?.tvTime?.text = DateUtils.getRelativeTimeSpanString(CommonMethods.getDateInMillis(model?.notiDate!!), cal.timeInMillis, DateUtils.MINUTE_IN_MILLIS
                    )
                }


                if (model.type.equals(AppConstants.EVENT)) {
                    Glide.with(this@NotificationListFragment).load(R.drawable.ic_events)
                        .into(dataBinding!!.ivMessageType)
                } else if (model.type.equals(AppConstants.ALERT)) {
                    Glide.with(this@NotificationListFragment).load(R.drawable.ic_newsalerts)
                        .into(dataBinding!!.ivMessageType)
                } else if (model.type.equals(AppConstants.DIRECT)) {
                    Glide.with(this@NotificationListFragment).load(R.drawable.ic_directnotification)
                        .into(dataBinding!!.ivMessageType)
                }

                dataBinding?.rlNotification?.setOnClickListener {
                    if (model?.type.equals(AppConstants.ALERT)) {
                        if (model?.userNotificationId!!.isNotEmpty()) {
                            val intentNewAndAlert = Intent(activity!!, NewsAndAlertDetailsActivity::class.java)
                            intentNewAndAlert.putExtra(AppConstants.NOTIFICATION_ID,model?.notificationId)
                            startActivity(intentNewAndAlert)

                        }


                    } else if (model?.type.equals(AppConstants.EVENT)) {
                        if (model?.eventId!!.isNotEmpty()) {
                            val intent = Intent(activity!!, CommunityEventDetailsActivity::class.java)
                            intent.putExtra(AppConstants.EVENT_ID, model?.eventId)
                            intent.putExtra(AppConstants.IS_NEED_TO_CALL_API, true)
                            startActivity(intent)
                            CommonMethods.hideKeypad(activity!!, dataBinding?.tvNotification)
                        }


                    } else if (model?.type.equals(AppConstants.DIRECT)) {
                        CommonMethods.hideKeypad(activity!!, dataBinding?.tvNotification)
                        if(model?.image?.isNotEmpty()!! && model?.message!!.isNotEmpty()){
                            val dialogDirectMessage= DialogDirectMessage(activity!!,model.image,model.message)
                            dialogDirectMessage.showDirectMessageAlert()
                        }
                        else{
                            if (model?.message!!.isNotEmpty()) {
                                alert(model.message) {
                                    okButton { }
                                }.show()
                            }
                        }




                    } else if (model?.type.equals(AppConstants.BUSINESS)|| model?.type.equals(AppConstants.OWNER)|| model?.type.equals(AppConstants.PROMOTION)) {
                        if (model?.businessPlaceId!!.isNotEmpty()) {
                            CommonMethods.hideKeypad(activity!!, dataBinding?.tvNotification)
                            callBusinessDetailsApi(model.businessPlaceId)
                        }
                    }
                }


            }

            override fun getLayoutId(): Int {

                return R.layout.item_user_notification

            }


        }
        binding.rvUserNotification.adapter = useNotificationAdapter
    }
    private fun callBusinessDetailsApi(businessId: String) {

        val inputparam = HashMap<String, String>()
        inputparam.put(ApiConstants.USER_ID, CommonMethods.getUserId(activity!!)!!)
        inputparam.put(ApiConstants.BUSINESS_PLACE_ID, businessId)
        inputparam.put(ApiConstants.COMMUNITY_ID,CommonMethods.cummunity_id)
        inputparam.put(
            ApiConstants.DEVICE_UDID,
            Settings.Secure.getString(activity!!.getContentResolver(), Settings.Secure.ANDROID_ID)
        )
        val call = ApiClient.getRetrofitInterface()?.getBusinessDetails(inputparam)

        WSClient<Response<List<BusinessDetails>>>().request(activity!!, 100, true, call,
            object : ISuccessHandler<Response<List<BusinessDetails>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<BusinessDetails>>
                ) {
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        val businessDetails = mResponse.data!!.get(0)
                        if (businessDetails.is_featured.equals("1")) {
                            val intent = Intent(activity!!, PromotionDetailsActivity::class.java)
                            intent.putExtra(AppConstants.BUSINESS_ID, businessId)
                            intent.putExtra(AppConstants.BUSINESS_DETAILS_MODEL, businessDetails)
                            intent.putExtra(AppConstants.IS_FROM_SEARCH, true)
                            startActivity(intent)
                        } else {
                            val nonFeature = NonFeature()
                            nonFeature.address = businessDetails.address
                            nonFeature.phone = businessDetails.phone
                            nonFeature.placeName = businessDetails.placeName
                            val intent = Intent(activity!!, NonFeatureListActivity::class.java)
                            intent.putExtra(AppConstants.SHARE_KEY, nonFeature)
                            startActivity(intent)


                        }

                    }
                }

            }, object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {


                    alert(message) {

                        okButton { }

                    }.show()

                }

            })
    }
}
