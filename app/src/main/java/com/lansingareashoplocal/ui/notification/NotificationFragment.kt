package com.lansingareashoplocal.ui.notification

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseFragment
import com.lansingareashoplocal.databinding.FragmentNotificationsBinding
import com.lansingareashoplocal.ui.adapter.ViewPagerAdapter
import com.lansingareashoplocal.ui.home.HomeActivity
import com.lansingareashoplocal.ui.tempareture.TemperatureActivity
import kotlinx.android.synthetic.main.toolbar_with_weather_details.view.*
import kotlin.math.roundToInt

class NotificationFragment : BaseFragment<FragmentNotificationsBinding>() {


    override fun onBackPressed(): Boolean {
        return false
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_notifications
    }

    override fun init() {
        setUpViewPager()
        binding.tabLayout.setupWithViewPager(binding.vwPagerNotification)
        binding.notificationToolbar.rlTemperature.setOnClickListener {
            startActivity(Intent(activity!!, TemperatureActivity::class.java))
        }
        setBackImage()
       binding.notificationToolbar.tvLeft.setOnClickListener {
           (activity as HomeActivity).setHomebBar()
       }

    }

    private fun setUpViewPager() {
        val viewPagerAdapter = ViewPagerAdapter(childFragmentManager)
        viewPagerAdapter.addFragment(PromotionNotificationListFragment(), "Promotions")
        viewPagerAdapter.addFragment(NotificationListFragment(), "Notifications")
        binding.vwPagerNotification.adapter = viewPagerAdapter
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    override fun onResume() {
        super.onResume()
        updateTemperature()

    }


    fun updateTemperature() {
        if (CommonMethods.getFloatValve(
                activity!!,
                AppConstants.CURRENT_TEMPERATURE
            ) != 0.0F
        ) {
            binding.notificationToolbar.tvTemparature.text ="${CommonMethods.getFloatValve(activity!!, AppConstants.CURRENT_TEMPERATURE).roundToInt()}${getString(R.string.degree)}"
            Glide.with(activity!!).load(
                "${AppConstants.WETHER_ICON_URL}${CommonMethods.getDataFromSharePreference(
                    activity!!,
                    AppConstants.CURRENT_WEATHER_ICON_ID
                )}@2x.png"
            )
                .into(binding.notificationToolbar.ivCloud)

        }
    }


    private fun setBackImage() {
      //  binding.notificationToolbar.ivLeft.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.back_icon))

    }


}