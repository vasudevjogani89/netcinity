package com.lansingareashoplocal.ui.notification

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.content.Intent
import android.provider.Settings
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.ItemPromotionsBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.core.BaseFragment
import com.lansingareashoplocal.databinding.FragmentPromotionNotificationBinding
import com.lansingareashoplocal.ui.model.FeaturedBusiness
import com.lansingareashoplocal.ui.promotions.PromotionDetailsActivity
import com.lansingareashoplocal.utility.helper.TimeUtils
import kotlinx.android.synthetic.main.fragment_user_notification.*
import kotlinx.android.synthetic.main.item_promotions.view.*
import org.jetbrains.anko.okButton
import org.jetbrains.anko.support.v4.alert

class PromotionNotificationListFragment : BaseFragment<FragmentPromotionNotificationBinding>() {
    private var hasNext = true
    private var pageCount = 20
    var currentPage = 1
    var categoryId = ""
    var promotionNotificationList = mutableListOf<FeaturedBusiness>()
    var promotionNotificationAdapter: GenericAdapter<FeaturedBusiness, ItemPromotionsBinding>? =
        null

    override fun onBackPressed(): Boolean {
        return false
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_promotion_notification
    }

    override fun init() {
        setAdapter()
        callNotificationList(true)
        val layoutManager = LinearLayoutManager(activity)
        binding.rvPromotionNotification.setLayoutManager(layoutManager)
        binding.rvPromotionNotification.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager != null && layoutManager.findLastCompletelyVisibleItemPosition() == promotionNotificationList.size - 1) {
                    if (hasNext) {
                        hasNext = false
                        binding.rvNotificationListProgressLayout?.visibility = View.VISIBLE
                        currentPage += 1
                        callNotificationList(false)
                    }
                }
            }
        })

        binding.swipeRefreshPromotionNotification.setOnRefreshListener {
            currentPage = 1
            hasNext = false
            callNotificationList(false)
        }


    }

    private fun setAdapter() {
        promotionNotificationAdapter =
            object :
                GenericAdapter<FeaturedBusiness, ItemPromotionsBinding>(promotionNotificationList) {
                override fun getLayoutId(): Int {
                    return R.layout.item_promotions
                }

                override fun onBindData(
                    model: FeaturedBusiness?,
                    position: Int,
                    dataBinding: ItemPromotionsBinding?
                ) {
                    if (com.lansingareashoplocal.BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
                        dataBinding?.tvSubCategories?.setTextColor(ContextCompat.getColor(activity!!, R.color.colorDarkBlue))
                    }


                    if (model!!.address.isEmpty()) {
                        dataBinding!!.rlpromotionAddress.visibility = View.GONE
                    } else {
                        dataBinding!!.rlpromotionAddress.visibility = View.VISIBLE
                    }
                    if (model!!.phone.isEmpty()) {
                        dataBinding!!.rlPhone.visibility = View.GONE
                    } else {
                        dataBinding!!.rlPhone.visibility = View.VISIBLE
                    }
                    dataBinding?.tvCommunityName?.text = model?.placeName
                    dataBinding?.tvCommunityAddress?.text = model?.address
                    Glide.with(activity!!).load(model.owner_profile_image)
                        .into(dataBinding?.ivOwner)

                    if (model?.getPromotionList?.isNotEmpty()!!) {
                        if (model?.getPromotionList.get(0).set_expiration.equals("Yes")) {
                            dataBinding?.tvEventExpireDate?.text =
                                "Exp.-${TimeUtils.convertDateWithTimeForShowingApp(
                                    model.getPromotionList.get(
                                        0
                                    ).peroidEnd
                                )}"
                        }

                        dataBinding.tvEventName.text =
                            model?.getPromotionList.get(0)?.priceDiscount
                        dataBinding.rlEventName?.visibility = View.VISIBLE
                        dataBinding.vwDummySpace.visibility=View.VISIBLE
                        dataBinding.tvEventExpireDate?.visibility = View.VISIBLE
                    } else {
                        dataBinding.rlEventName?.visibility = View.GONE
                        dataBinding.vwDummySpace.visibility=View.GONE
                        dataBinding?.tvEventExpireDate?.visibility = View.INVISIBLE
                    }
                    dataBinding.tvPhoneNumber.text = model.phone


                    Glide.with(activity!!).load(model.bannerImage)
                        .into(dataBinding?.ivCommunity!!)

                    if (model.owner_profile_image.isEmpty()) {
                        dataBinding?.ivOwner.visibility = View.GONE
                    } else {
                        dataBinding?.ivOwner.visibility = View.VISIBLE
                    }
                    if(model.sub_category.isEmpty()){
                        dataBinding.tvSubCategories.visibility=View.GONE
                    }else{
                        dataBinding.tvSubCategories.visibility=View.VISIBLE
                        try {
                            dataBinding.tvSubCategories?.text =CommonMethods.getCapsSentences(model.sub_category)
                        }
                        catch (e :Exception){
                            dataBinding.tvSubCategories?.text=model.sub_category
                        }

                    }
                    if (model.featured.equals("Yes")) {
                        dataBinding.triangleViewFeature.visibility = View.VISIBLE
                    } else {
                        dataBinding.triangleViewFeature.visibility = View.GONE
                    }


                    dataBinding.cvMain.setOnClickListener {
                        val intent = Intent(activity!!, PromotionDetailsActivity::class.java)
                        intent.putExtra(AppConstants.BUSINESS_ID, model.businessPlaceId)
                        startActivity(intent)
                    }
                }

            }
        binding.rvPromotionNotification.adapter = promotionNotificationAdapter


    }


    private fun callNotificationList(isRequireToShowLoader: Boolean) {
        val inputParam = HashMap<String, String>()
        inputParam.put(
            ApiConstants.DEVICE_UDID,
            Settings.Secure.getString(activity!!.getContentResolver(), Settings.Secure.ANDROID_ID)
        )
        inputParam.put(ApiConstants.PAGE_INDEX, "" + currentPage)
        inputParam.put(
            ApiConstants.USER_ID,
            CommonMethods.getUserId(activity!!)!!
        )
        inputParam.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        val call = ApiClient.getRetrofitInterface()?.getNotificationList(inputParam)
        WSClient<Response<List<FeaturedBusiness>>>().request(
            activity!!,
            100,
            isRequireToShowLoader,
            call,
            object : ISuccessHandler<Response<List<FeaturedBusiness>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<FeaturedBusiness>>
                ) {
                    if (mResponse.settings?.success.equals("1")) {
                        binding.rvNotificationListProgressLayout?.visibility = View.GONE
                        hasNext = mResponse.settings?.nextPage == 1
                        pageCount = mResponse.settings?.perPage!!
                        if (currentPage == 1) {
                            promotionNotificationList =
                                mResponse.data as MutableList<FeaturedBusiness>
                            promotionNotificationAdapter?.setData(promotionNotificationList)
                        } else {
                            promotionNotificationAdapter?.addItems(mResponse.data as MutableList<FeaturedBusiness>)
                        }
                    }
                    if (binding.swipeRefreshPromotionNotification.isRefreshing) {
                        binding.swipeRefreshPromotionNotification.isRefreshing = false
                    }
                    if (promotionNotificationList.isEmpty()) {
                        binding.tvNoData.text = mResponse.settings?.message
                        binding.tvNoData.visibility = View.VISIBLE
                    } else {
                        binding.tvNoData.visibility = View.INVISIBLE
                    }

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    CommonMethods.hideProgress()
                    if (binding.swipeRefreshPromotionNotification.isRefreshing) {
                        binding.swipeRefreshPromotionNotification.isRefreshing = false
                    }
                    binding.rvNotificationListProgressLayout?.visibility = View.GONE
                    alert(message) {
                        okButton { }
                    }.show()
                }

            })


    }


}