package com.lansingareashoplocal.ui.newsandalert

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.text.Html
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.DialogNewAlertDetailsBinding
import com.lansingareashoplocal.ui.model.News
import com.lansingareashoplocal.utility.helper.TimeUtils

class DialogNewAlertDetails(
    var context: Context,
    var news: News? = null,

    var notificationId: String = ""
) {


    lateinit var newAndAlertDailog: Dialog
    lateinit var dialogAddsBinding: DialogNewAlertDetailsBinding
    var mimeType = "text/html;charset=UTF-8"
    var encoding = "utf-8"


    fun showAlertAndNews() {
        newAndAlertDailog = Dialog(context, R.style.MyAlertDialogStyle)
        dialogAddsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_new_alert_details,
            null,
            false
        )
        newAndAlertDailog.setContentView(dialogAddsBinding.getRoot())
        newAndAlertDailog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
      /*  if (sortDailog.window != null) {
            sortDailog.window!!.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
            );
        }*/
        dialogAddsBinding.ivCancel.setOnClickListener {
            newAndAlertDailog.dismiss()
        }




        if (news == null) {
            callNewsAndAlertApi()
        } else {
            newAndAlertDailog.show()
            setData()
        }


    }

    private fun callNewsAndAlertApi() {

        var inputparams = HashMap<String, String>()
        inputparams[AppConstants.COMMUNITY_ID] = CommonMethods.cummunity_id
        inputparams[AppConstants.NOTIFICATION_ID] = notificationId


        val call = ApiClient.getRetrofitInterface()?.newDetails(inputparams)
        WSClient<Response<List<News>>>().request(
            context,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<News>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<News>>) {

                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        newAndAlertDailog.show()
                        news = mResponse.data?.get(0)
                        setData()
                    }

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                }

            })


    }


    fun setData() {
        dialogAddsBinding.tvNewsTitle?.text = news?.title
        dialogAddsBinding.tvNewTime?.text =
            TimeUtils.convertDateWithTimeForShowingApp(news?.addedDate)
        Glide.with(context).load(news?.image).into(dialogAddsBinding?.ivNews)

        var text =
            "<html><head>" + "<style type=\"text/css\">@font-face {font-family: font;src: url(\"file:///android_asset/museosans_300.otf\")}body{font-family: poppins;color: #21242c;font-size: 17px}" + "</style></head>" + "<body>" + news?.description + "</body></html>";
        dialogAddsBinding.tvNewsDetails.loadDataWithBaseURL(null, text, mimeType, encoding, null);
    }
}