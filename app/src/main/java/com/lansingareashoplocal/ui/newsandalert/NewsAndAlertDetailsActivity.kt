package com.lansingareashoplocal.ui.newsandalert

import ApiClient
import AppConstants
import CommonMethods
import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.text.Html
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.home.HomeActivity
import com.lansingareashoplocal.ui.model.News
import com.lansingareashoplocal.utility.helper.TimeUtils
import kotlinx.android.synthetic.main.activity_news_and_alert_details.*
import kotlinx.android.synthetic.main.tool_bar_with_out_bg.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class NewsAndAlertDetailsActivity : BaseActivity() {
    var mimeType = "text/html;charset=UTF-8"
    var encoding = "utf-8"
    var newsAndAlert: News? = null
    var newsId: String? = null
    var communityId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_and_alert_details)
        val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        tvLeft.setOnClickListener {
            if (isTaskRoot) {
                startActivity(Intent(this@NewsAndAlertDetailsActivity, HomeActivity::class.java))
                finish()
            } else {
                finish()
            }
        }
        if(intent!=null){
            newsId = intent.getStringExtra(AppConstants.NOTIFICATION_ID)
            if (newsId!= null) {
                callNewsAndAlertApi()
            } else {
                rlNewsAndAlertMain.visibility = View.VISIBLE
                newsAndAlert = intent.getSerializableExtra("news") as News
                setData()
                if (newsAndAlert != null && newsAndAlert?.notificationId!=null){
                    callNewsAndAlertViewApi()
                }



            }
        }
        ivNewsShare.setOnClickListener {
            shareNewsAndAlertDetails()
        }



    }

    override fun onBackPressed() {
        if (isTaskRoot) {
            startActivity(Intent(this@NewsAndAlertDetailsActivity, HomeActivity::class.java))
            finish()
        } else {
            finish()
        }

    }

    private fun callNewsAndAlertApi() {

        val inputparams = HashMap<String, String>()
        inputparams[AppConstants.COMMUNITY_ID] = CommonMethods.cummunity_id
        if (newsId != null) {
            inputparams[AppConstants.NOTIFICATION_ID] = newsId!!
        }


        val call = ApiClient.getRetrofitInterface()?.newDetails(inputparams)
        WSClient<Response<List<News>>>().request(
            this@NewsAndAlertDetailsActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<News>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<News>>) {

                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        rlNewsAndAlertMain.visibility = View.VISIBLE
                        newsAndAlert = mResponse.data?.get(0)
                        setData()
                    }

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert (message){
                        okButton {  }
                    }.show()
                }

            })


    }


    fun setData() {
        tvNewsTitleName.text = newsAndAlert?.title
        tvNewsAddedDate.text =
            TimeUtils.convertDateWithTimeForShowingApp(newsAndAlert?.addedDate)
        Glide.with(this@NewsAndAlertDetailsActivity).load(newsAndAlert?.image).into(ivNewsLogo)

        val text =
            "<html><head>" + "<style type=\"text/css\">@font-face {font-family: font;src: url(\"file:///android_asset/museosans_300.otf\")}body{font-family: poppins;color: #21242c;font-size: 17px}" + "</style></head>" + "<body>" + newsAndAlert?.description + "</body></html>";
        wvNewDetails.loadDataWithBaseURL(null, text, mimeType, encoding, null);
    }
    private fun callNewsAndAlertViewApi() {
        val inputparams = HashMap<String, String>()
        inputparams[AppConstants.COMMUNITY_ID] = CommonMethods.cummunity_id
        if (newsAndAlert != null && newsAndAlert?.notificationId!=null) {
            inputparams[AppConstants.NOTIFICATION_ID] = newsAndAlert?.notificationId!!
        }


        val call = ApiClient.getRetrofitInterface()?.newDetails(inputparams)
        WSClient<Response<List<News>>>().request(this@NewsAndAlertDetailsActivity, 100, false, call,
            object : ISuccessHandler<Response<List<News>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<News>>) {



                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                }

            })


    }


    private fun shareNewsAndAlertDetails() {
        Glide.with(this@NewsAndAlertDetailsActivity).asBitmap().load(newsAndAlert?.image)
            .into(object : SimpleTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    val intent = Intent(Intent.ACTION_SEND)
                    intent.type = "image/*"
                    intent.action = Intent.ACTION_SEND
                    intent.putExtra(
                        Intent.EXTRA_TEXT,
                        "${newsAndAlert?.title}\n\n${TimeUtils.convertDateWithTimeForShowingApp(newsAndAlert?.addedDate)}\n${stripHtml(newsAndAlert?.description)}\n${CommonMethods.getShareAppText(this@NewsAndAlertDetailsActivity)}"
                    )
                    intent.putExtra(Intent.EXTRA_STREAM, getBitmapFromView(resource))
                    startActivity(Intent.createChooser(intent, "Share Image"))
                }
            })

    }
    fun getBitmapFromView(bmp: Bitmap?): Uri? {
        var bmpUri: Uri? = null
        try {
            val file = File(externalCacheDir, System.currentTimeMillis().toString() + ".jpg")

            val out = FileOutputStream(file)
            bmp?.compress(Bitmap.CompressFormat.JPEG, 90, out)
            out.close()
            bmpUri = Uri.fromFile(file)

        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri
    }
    fun stripHtml(html: String?): String? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString()
        } else {
            Html.fromHtml(html).toString()
        }
    }

}