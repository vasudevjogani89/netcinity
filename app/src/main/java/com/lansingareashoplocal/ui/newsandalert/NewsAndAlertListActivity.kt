package com.lansingareashoplocal.ui.newsandalert

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.content.Intent
import android.os.Bundle
import android.text.format.DateUtils
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.ItemNewsAlertBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.model.News
import com.lansingareashoplocal.ui.tempareture.TemperatureActivity
import kotlinx.android.synthetic.main.fragment_new_and_alert_list.*
import kotlinx.android.synthetic.main.fragment_new_and_alert_list.rvProgressLayout
import kotlinx.android.synthetic.main.toolbar_with_weather_details.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import java.util.*
import kotlin.math.roundToInt

class NewsAndAlertListActivity() : BaseActivity() {
    var newsList = mutableListOf<News>()
    var newsAdapter: GenericAdapter<News, ItemNewsAlertBinding>? = null
    private var hasNext = true
    var currentPage = 1
    private var pageCount = 20


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_new_and_alert_list)
    //    newAndAlertToolBar.ivLeft.setImageDrawable(resources.getDrawable(R.drawable.back_icon))
        newAndAlertToolBar.tvLeft.setOnClickListener {
            pop()
        }
        setNewsAndAlert()
        newAndAlertToolBar.rlTemperature.setOnClickListener {
            startActivity(Intent(this@NewsAndAlertListActivity, TemperatureActivity::class.java))
        }
        callNewsListAPi(true)


        swipeRefreshNewsAndAlertList.setOnRefreshListener {
            currentPage = 1
            hasNext = false
            callNewsListAPi(false)
        }
        val layoutManager = LinearLayoutManager(this@NewsAndAlertListActivity)
        rvNewsAndAlert.setLayoutManager(layoutManager)
        rvNewsAndAlert.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager != null && layoutManager.findLastCompletelyVisibleItemPosition() == newsList.size - 1) {
                    if (hasNext) {
                        hasNext = false
                        rvProgressLayout?.visibility = View.VISIBLE
                        currentPage += 1
                        callNewsListAPi(false)
                    }
                }
            }
        })
        if (CommonMethods.getFloatValve(
                this@NewsAndAlertListActivity,
                AppConstants.CURRENT_TEMPERATURE
            ) != 0.0F
        ) {
            newAndAlertToolBar.tvTemparature.text = "${CommonMethods.getFloatValve(this@NewsAndAlertListActivity, AppConstants.CURRENT_TEMPERATURE).roundToInt()}${getString(R.string.degree)}"

            Glide.with(this@NewsAndAlertListActivity).load(
                "${AppConstants.WETHER_ICON_URL}${CommonMethods.getDataFromSharePreference(
                    this@NewsAndAlertListActivity,
                    AppConstants.CURRENT_WEATHER_ICON_ID
                )}@2x.png"
            ).into(newAndAlertToolBar.ivCloud)


        }
    }


    private fun setNewsAndAlert() {
        newsAdapter = object :
            GenericAdapter<News, ItemNewsAlertBinding>(newsList) {
            override fun onBindData(
                model: News?,
                position: Int,
                dataBinding: ItemNewsAlertBinding?
            ) {
                val cal = Calendar.getInstance()
                dataBinding?.tvTime?.text = DateUtils.getRelativeTimeSpanString(
                    CommonMethods.getDateInMillis(model?.addedDate!!),
                    cal.timeInMillis,
                    DateUtils.MINUTE_IN_MILLIS
                )
                dataBinding?.tvNewTitle?.text = model?.title
                Glide.with(this@NewsAndAlertListActivity).load(model?.image)
                    .into(dataBinding?.ivNews!!)
                if (position == 0) {
                    dataBinding?.ivTopLine.visibility = View.GONE
                } else {
                    dataBinding?.ivTopLine.visibility = View.VISIBLE
                }


                dataBinding?.root.setOnClickListener {
                    val intent = Intent(this@NewsAndAlertListActivity, NewsAndAlertDetailsActivity::class.java)
                    intent.putExtra("news", model)
                    startActivity(intent)
                }
            }

            override fun getLayoutId(): Int {
                return R.layout.item_news_alert
            }


        }

        rvNewsAndAlert.adapter = newsAdapter

    }




    private fun callNewsListAPi(isRequireLoader: Boolean) {


        val call = ApiClient.getRetrofitInterface()?.getNewsAndAlert(
            community_id = CommonMethods.cummunity_id,

            page_index = "" + currentPage
        )
        WSClient<Response<List<News>>>().request(
            this@NewsAndAlertListActivity,
            100,
            isRequireLoader,
            call,
            object : ISuccessHandler<Response<List<News>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<News>>) {
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        rvProgressLayout?.visibility = View.GONE
                        hasNext = mResponse.settings?.nextPage == 1
                        pageCount = mResponse.settings?.perPage!!
                        if (currentPage == 1) {
                            newsList = mResponse.data as MutableList<News>
                            newsAdapter?.setData(newsList)
                        } else {
                            newsAdapter?.addItems(mResponse.data)
                        }


                    }
                    if (swipeRefreshNewsAndAlertList.isRefreshing) {
                        swipeRefreshNewsAndAlertList.isRefreshing = false
                    }

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    rvProgressLayout?.visibility = View.GONE
                    CommonMethods.hideProgress()
                    if (swipeRefreshNewsAndAlertList.isRefreshing) {
                        swipeRefreshNewsAndAlertList.isRefreshing = false
                    }
                    alert(message) {
                        okButton { }
                    }.show()
                }


            })
    }


}