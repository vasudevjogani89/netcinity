package com.lansingareashoplocal.ui.user

import AppConstants
import CommonMethods
import IFailureHandler
import ISuccessHandler
import WSClient
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.databinding.ItemUnSplashPhotoBinding
import com.lansingareashoplocal.retrofit.PhotosEndpointInterface
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.interfaces.EditTextChangeCallBack
import com.lansingareashoplocal.ui.model.Photo
import com.lansingareashoplocal.ui.model.SearchResults
import com.lansingareashoplocal.utility.helper.HeaderInterceptor
import com.lansingareashoplocal.utility.helper.logs.Log
import com.lansingareashoplocal.utility.helper.others.CustomTextWatcher
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_un_splash.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.view.*
import okhttp3.OkHttpClient
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.*
import java.net.MalformedURLException
import java.net.URL
import java.net.URLConnection
import java.util.concurrent.TimeUnit

class UnSplashActivity : BaseActivity(), EditTextChangeCallBack {

    private var hasNext = true
    private var pageCount = 0
    var currentPage = 1
    var photosList = mutableListOf<Photo>()
    var unSplashPhotoAdapter: GenericAdapter<Photo, ItemUnSplashPhotoBinding>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_un_splash)
        toolBar.tvLeft.setOnClickListener { finish() }
        toolBar.tvTitle.text = getString(R.string.un_splash_screen_title)
        etSearchUnSplash.addTextChangedListener(CustomTextWatcher(etSearchUnSplash, this))
        searchFAQ()
        clearText()
        val layoutManager = GridLayoutManager(this@UnSplashActivity, 2)
        recyclerViewUnSplash.setLayoutManager(layoutManager)
        recyclerViewUnSplash.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager != null && layoutManager.findLastCompletelyVisibleItemPosition() == photosList.size - 1) {
                    if (hasNext) {
                        hasNext = false
                        rlUnsplashProgressLayout?.visibility = View.VISIBLE
                        currentPage += 1
                        callUnSplashApi(false)
                    }
                }
            }
        })
        setData()
    }

    private fun callUnSplashApi(b: Boolean=true) {
        val client = OkHttpClient.Builder()
        client.addInterceptor(HeaderInterceptor(AppConstants.UN_SPLASH_CLIENT_ID))
        client.callTimeout(5, TimeUnit.MINUTES)
        client.readTimeout(5, TimeUnit.MINUTES)
        client.writeTimeout(5, TimeUnit.MINUTES)

        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://api.unsplash.com/")
            .client(client.build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val photosApiService = retrofit.create(PhotosEndpointInterface::class.java)

        val call = photosApiService?.searchPhotos(
            etSearchUnSplash.text?.trim().toString(),
            currentPage,
            20,
            null
        )
        WSClient<SearchResults>().request(
            this@UnSplashActivity,
            100,
            true,
            call,
            object : ISuccessHandler<SearchResults> {
                override fun successResponse(requestCode: Int, mResponse: SearchResults) {
                    if (mResponse != null && mResponse.photoList.isNotEmpty()) {
                       rlUnsplashProgressLayout?.visibility = View.GONE
                        if(currentPage>mResponse.totalPages){
                            hasNext=true
                        }

                       // hasNext = mResponse.settings?.nextPage == 1
                        pageCount = mResponse.totalPages
                        if (currentPage == 1) {
                            photosList = mResponse.photoList as MutableList<Photo>
                            tvNoUnSplashData.visibility = View.GONE
                            unSplashPhotoAdapter?.setData(photosList)
                        } else {
                            unSplashPhotoAdapter?.addItems(mResponse.photoList as MutableList<Photo>)
                        }

                    } else {
                        tvNoUnSplashData.visibility = View.VISIBLE
                        tvNoUnSplashData.text = getString(R.string.no_image_found)

                    }

                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert(message) {
                        okButton { }
                    }.show()

                }

            })
    }

    private fun setData() {

        unSplashPhotoAdapter =
            object : GenericAdapter<Photo, ItemUnSplashPhotoBinding>(photosList) {
                override fun getLayoutId(): Int {
                    return R.layout.item_un_splash_photo
                }

                override fun onBindData(
                    model: Photo?,
                    position: Int,
                    dataBinding: ItemUnSplashPhotoBinding?
                ) {
                    if (model != null && model.urls.small != null) {
                        dataBinding?.progressUnSplash?.visibility = View.VISIBLE
                        Glide.with(this@UnSplashActivity).asBitmap().load(model.urls.small)
                            .into(object : SimpleTarget<Bitmap>() {
                                override fun onResourceReady(
                                    resource: Bitmap,
                                    transition: Transition<in Bitmap>?
                                ) {
                                    dataBinding?.imageView?.setImageBitmap(resource)
                                    dataBinding?.progressUnSplash?.visibility = View.GONE
                                }

                            })
                        dataBinding?.root?.setOnClickListener {
                            /*val intent=Intent()
                            intent.putExtra(AppConstants.RESULT_URL,model.urls.small)
                            setResult(Activity.RESULT_OK,intent)
                            finish()*/

                            val folder =
                                File(
                                    Environment.getExternalStorageDirectory().getAbsolutePath(),
                                    "unsplash"
                                )
                            val outputFile = File(
                                folder,
                                "${CommonMethods.getFileNameFromURl(model.urls.small)}.jpg"
                            )
                            val path: String = outputFile.toString()
                            val filePath = File(path)
                            if (filePath.exists()) {
                                try {
                                    CropImage.activity(Uri.fromFile(filePath.absoluteFile))
                                        .start(this@UnSplashActivity);
                                } catch (e: Exception) {
                                }
                            } else {
                                DownloadTask().execute(model.urls.small)
                            }


                        }
                    }


                }

            }
        recyclerViewUnSplash.adapter = unSplashPhotoAdapter
    }


    private fun searchFAQ() {
        etSearchUnSplash.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (v?.text != null) {
                        searchPerform()
                        CommonMethods.hideKeypad(this@UnSplashActivity, etSearchUnSplash)
                    }

                    return true
                }
                return false
            }
        })
    }

    private fun searchPerform() {
        currentPage = 1
        hasNext = false
        callUnSplashApi(false)
    }

    private fun clearText() {

        ivSearchUnSplash.setOnClickListener {
            if (etSearchUnSplash.text!!.isNotEmpty()) {
                CommonMethods.hideKeypad(this@UnSplashActivity, etSearchUnSplash)
                etSearchUnSplash.setText("")
                etSearchUnSplash.clearFocus()
            } else {

            }
            tvNoUnSplashData.visibility = View.GONE
        }
    }

    override fun afterTextChanged(editText: AppCompatEditText) {
        if (editText.text != null) {
            if (editText.text!!.isNotEmpty()) {
                Glide.with(this@UnSplashActivity).load(R.drawable.cancel)
                    .into(ivSearchUnSplash)
            } else {
                Glide.with(this@UnSplashActivity).load(R.drawable.ic_search)
                    .into(ivSearchUnSplash)
            }
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK && result != null) {
                val intent = Intent()
                intent.putExtra(AppConstants.RESULT_URL, result.uri.path)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
    }


    inner class DownloadTask : AsyncTask<String, Int, String>() {
        var progressDialog: ProgressDialog? = null

        /**
         * Set up a ProgressDialog
         */
        protected override fun onPreExecute() {
            /* progressDialog = ProgressDialog(this@UnSplashActivity)
             progressDialog?.setTitle("Download in progress...")
             progressDialog?.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
             progressDialog?.setMax(100)
             progressDialog?.setProgress(0)
             progressDialog?.show()*/
            CommonMethods.showProgress(this@UnSplashActivity)
        }

        /**
         * Background task
         */
        protected override fun doInBackground(vararg params: String): String {
            val path = params[0]
            val fileLength: Int
            try {
                val url = URL(path)
                val urlConnection: URLConnection = url.openConnection()
                urlConnection.connect()
                fileLength = urlConnection.getContentLength()
                /**
                 * Create a folder
                 */
                val newFolder = File(Environment.getExternalStorageDirectory(), "unsplash")
                if (!newFolder.exists()) {
                    if (newFolder.mkdir()) {
                        Log.i("Info", "Folder succesfully created")
                    } else {
                        Log.i("Info", "Failed to create folder")
                    }
                } else {
                    Log.i("Info", "Folder already exists")
                }
                /**
                 * Create an output file to store the image for download
                 */
                val output_file = File(newFolder, "${CommonMethods.getFileNameFromURl(path)}.jpg")
                val outputStream: OutputStream = FileOutputStream(output_file)
                val inputStream: InputStream = BufferedInputStream(url.openStream(), 8192)
                val data = ByteArray(1024)
                var total = 0
                var count: Int
                while (inputStream.read(data).also { count = it } != -1) {
                    total += count
                    outputStream.write(data, 0, count)
                    val progress = 100 * total / fileLength
                    publishProgress(progress)
                    Log.i("Info", "Progress: " + Integer.toString(progress))
                }
                inputStream.close()
                outputStream.close()
                Log.i("Info", "file_length: " + Integer.toString(fileLength))
            } catch (e: MalformedURLException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return path
        }

        protected override fun onProgressUpdate(vararg values: Int?) {
            /* values[0]?.let { progressDialog?.setProgress(it) }*/
        }

        protected override fun onPostExecute(result: String) {
            CommonMethods.hideProgress()

            val folder =
                File(Environment.getExternalStorageDirectory().getAbsolutePath(), "unsplash")
            val output_file = File(folder, "${CommonMethods.getFileNameFromURl(result)}.jpg")
            val path: String = output_file.toString()
            val filePath = File(path)
            try {
                CropImage.activity(Uri.fromFile(filePath.absoluteFile))
                    .start(this@UnSplashActivity);
            } catch (e: Exception) {

            }


            Log.i("Info", "Path: $path")
        }
    }
}