package com.lansingareashoplocal.ui.user

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.DialogImageDetailBinding

class ProfileImageDetail(var context: Context, var imagePath :String?) {

    lateinit var imageDailog: Dialog
    lateinit var dialogProfileImageBinding: DialogImageDetailBinding


    fun showImageDetail() {
        imageDailog = Dialog(context, R.style.MyAlertDialogStyle)
        dialogProfileImageBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_image_detail, null, false)
        imageDailog.setContentView(dialogProfileImageBinding.getRoot())
        imageDailog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        imageDailog.show()
        Glide.with(context).load(imagePath).into(dialogProfileImageBinding.ivProfile)

        dialogProfileImageBinding.ivCancel.setOnClickListener {
            imageDailog.dismiss()
        }

    }
}