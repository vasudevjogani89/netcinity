package com.lansingareashoplocal.ui.user

import ApiClient
import ApiConstants
import AppConstants
import CommonMethods
import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.databinding.ActivitySignUpBinding
import com.lansingareashoplocal.photopicker.UnsplashPhotoPicker
import com.lansingareashoplocal.presentation.UnsplashPickerActivity
import com.lansingareashoplocal.ui.home.HomeActivity
import com.lansingareashoplocal.ui.interfaces.EditTextChangeCallBack
import com.lansingareashoplocal.ui.model.UserDetails
import com.lansingareashoplocal.utility.MyDatePickerDialog
import com.lansingareashoplocal.utility.helper.TimeUtils
import com.lansingareashoplocal.utility.helper.others.CustomTextWatcher
import com.lansingareashoplocal.utility.helper.setDifferentColor
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_sign_up.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.okButton
import org.jetbrains.anko.selector
import java.io.File
import kotlin.collections.ArrayList

class SignUpActivity : BaseActivity(), EditTextChangeCallBack {
    var flag = 0


    override fun afterTextChanged(editText: AppCompatEditText) {


        if (editText == etEditableFirstName) {
            tInputLayoutFirstName.error = ""


        } else if (editText == etMail) {
            tInputLayoutEmail.error = ""


        } else if (editText == etPassWord) {
            tInputLayoutPassword.error = ""

        } else if (editText == etConfirmPassWord) {
            tInputLayoutConfirmPassword.error = ""
        }
    }

    var imagePath: String? = null
    var liveImagePath: String? = null
    var genderOptions = arrayListOf<String>(
        "Gender","Male","Female","Other","Prefer not to say"," It's complicated"
    )
    var selectionList = ArrayList<String>()
    lateinit var binding: ActivitySignUpBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectionList.add(getString(R.string.cameraAndGallery))
        selectionList.add(getString(R.string.online_library))
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_sign_up) as ActivitySignUpBinding
        binding.signupModel = SignUpModel()

        etDateOfBirth.setOnClickListener {
            val myDatePickerDialog = MyDatePickerDialog()
            myDatePickerDialog.getDate(
                this@SignUpActivity,
                object : MyDatePickerDialog.DateSelection {
                    override fun getDate(date: String) {
                        if (date.isNotEmpty()) {
                            etDateOfBirth.setText(date)
                            tInputLayoutDateOfBirth.hint =
                                requireField(getString(R.string.birthday_require_13_year_of_age_or_older))
                            tInputLayoutDateOfBirth.typeface =
                                Typeface.createFromAsset(assets, getString(R.string.museasans_100))
                        }

                    }


                })
        }
        btnSignUP.setOnClickListener {
            tInputLayoutFirstName.error = ""
            tInputLayoutEmail.error = ""
            tInputLayoutPassword.error = ""
            if (validateForm()) {
                callSignUpApi()
            }

        }
        etEditableFirstName.onFocusChangeListener = onFocusListener()
        etEditableLastName.onFocusChangeListener = onFocusListener()
        etMail.onFocusChangeListener = onFocusListener()
        etDateOfBirth.onFocusChangeListener = onFocusListener()
        etPassWord.onFocusChangeListener = onFocusListener()
        etConfirmPassWord.onFocusChangeListener = onFocusListener()
        etEditableFirstName.addTextChangedListener(CustomTextWatcher(etEditableFirstName, this))
        etMail.addTextChangedListener(CustomTextWatcher(etMail, this))
        etPassWord.addTextChangedListener(CustomTextWatcher(etPassWord, this))
        etConfirmPassWord.addTextChangedListener(CustomTextWatcher(etConfirmPassWord, this))
        tvAlreadyHaveAccount.setOnClickListener {
            finish()
        }

        tvSkip.setOnClickListener {
            val intent = Intent(this@SignUpActivity, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent)
            finish()
        }
        setPrivacyPolicyAndTermsAndCondtionTextWithMultiColors()
        setSpinner(genderOptions, spinnerGender)
        spinnerGender.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                etGender.setText(genderOptions[position])
                tInputLayoutGender.hint =
                    getString(R.string.GENDER)
                tInputLayoutGender.typeface =
                    Typeface.createFromAsset(assets, getString(R.string.museasans_100))
                if (position == 0) {
                    etGender.setText("")
                }

                /* if (position != 0) {

                 } else {
                     tInputLayoutGender.hint =
                         getString(R.string.gender)
                     tInputLayoutGender.typeface =
                         Typeface.createFromAsset(assets, getString(R.string.museosans_700))
                 }*/

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

                Log.e("onNothingSelected", "onNothingSelected")

            }
        }
        tInputLayoutPassword.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museosans_700))
        tvAlreadyHaveAccount.setDifferentColor(
            getString(R.string.already_have_an_account),
            " ${getString(R.string.signin)}.",
            this@SignUpActivity,
            tvAlreadyHaveAccount,
            Typeface.createFromAsset(assets, getString(R.string.museosans_500)),
            if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) ContextCompat.getColor(this, R.color.colorDarkBlue) else ContextCompat.getColor(this, R.color.colorPrimary)
        )
        etGender.setOnClickListener {
            spinnerGender.performClick()
        }
        tInputLayoutFirstName.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutFirstName.hint = requireField(getString(R.string.FIRST_NAME))
        tInputLayoutLastName.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutEmail.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutEmail.hint = requireField(getString(R.string.EMAIL_ID_ALSO_YOUR_USER_NAME))
        tInputLayoutDateOfBirth.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutGender.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutPassword.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutPassword.hint = requireField(getString(R.string.PASSWORD))
        tInputLayoutConfirmPassword.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutConfirmPassword.hint = requireField(getString(R.string.CONFIRM_PASSWORD))
        ivOPenCamera.setOnClickListener {
            requestCameraPermission()

        }
        ivUserProfile.setOnClickListener {
            if (imagePath != null) {
                val profileImageDetail = ProfileImageDetail(this@SignUpActivity, imagePath)
                profileImageDetail.showImageDetail()
            }
        }


    }


    private fun validateForm(): Boolean {
        var validCount = 7
        if (binding.signupModel?.isFirstNameEmpty!!) {
            tInputLayoutFirstName.error = getString(R.string.error_please_enter_first_name)
            validCount -= 1
        }
        if (binding.signupModel?.isEmailEmpty!!) {
            tInputLayoutEmail.error = getString(R.string.error_please_enter_email_address)
            validCount -= 1
        } else if (binding.signupModel?.isEmailValid!!) {
            tInputLayoutEmail.error = getString(R.string.error_please_enter_valid_email_address)
            validCount -= 1
        }
        if (binding.signupModel?.isPasswordEmpty!!) {
            tInputLayoutPassword.error = getString(R.string.error_please_enter_password)
            validCount -= 1
        } else if (binding.signupModel?.isValidPassword()!!) {
            tInputLayoutPassword.error =
                getString(R.string.error_please_enter_strong_password)
            validCount -= 1
        }
        if (binding?.signupModel?.isConfirmPasswordEmpty!!) {
            tInputLayoutConfirmPassword.error =
                getString(R.string.error_please_enter_confirm_password)
            validCount -= 1
        } else if (binding?.signupModel?.comparePasswordAndConfirm()!!) {
            tInputLayoutConfirmPassword.error =
                getString(R.string.error_new_password_confirm_password_not_equal)
            validCount -= 1
        }



        return validCount == 7


    }


    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    private fun setPrivacyPolicyAndTermsAndCondtionTextWithMultiColors() {


        val mSpannableStringBuilder =
            SpannableStringBuilder(getString(R.string.i_agree_t_amp_c_and_privacy_policy))
        val terms = SpannableString(" " + getString(R.string.terms_and_conditions))
        terms.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {

            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }, 0, terms.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        terms.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorRed)),
            0, terms.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )


        mSpannableStringBuilder.append(terms)
        val and = SpannableString(" & ")
        and.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorBlack)),
            0, and.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        mSpannableStringBuilder.append(and)

        val privacy = SpannableString(getString(R.string.privacy_policy))
        privacy.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {

            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }, 0, privacy.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        privacy.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorRed)),
            0, privacy.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )

        mSpannableStringBuilder.append(privacy)
        tvTermsConditionsAndPrivacyPolicy.movementMethod = LinkMovementMethod.getInstance()
        tvTermsConditionsAndPrivacyPolicy.text = mSpannableStringBuilder

    }


    private fun onFocusListener(): View.OnFocusChangeListener {
        val onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                if (v == etEditableFirstName) {
                    if (binding.signupModel?.isFirstNameEmpty!!) {
                        tInputLayoutFirstName.error =
                            getString(R.string.error_please_enter_first_name)
                    } else {
                        tInputLayoutFirstName.error = ""
                    }
                } else if (v == etMail) {
                    if (binding.signupModel?.isEmailEmpty!!) {
                        tInputLayoutEmail.error =
                            getString(R.string.error_please_enter_email_address)
                    } else if (binding.signupModel?.isEmailValid!!) {
                        tInputLayoutEmail.error =
                            getString(R.string.error_please_enter_valid_email_address)
                    } else {
                        tInputLayoutEmail.error = ""
                    }
                } else if (v == etPassWord) {
                    if (binding.signupModel?.isPasswordEmpty!!) {
                        tInputLayoutPassword.error =
                            getString(R.string.error_please_enter_password)
                    } else if (binding.signupModel?.isValidPassword()!!) {
                        tInputLayoutPassword.error =
                            getString(R.string.error_please_enter_strong_password)
                    } else {
                        tInputLayoutPassword.error = ""
                    }
                } else if (v == etConfirmPassWord) {
                    if (binding?.signupModel?.isConfirmPasswordEmpty!!) {
                        tInputLayoutConfirmPassword.error =
                            getString(R.string.error_please_enter_confirm_password)
                    } else if (binding?.signupModel?.comparePasswordAndConfirm()!!) {
                        tInputLayoutConfirmPassword.error =
                            getString(R.string.error_new_password_confirm_password_not_equal)
                    }
                }
            }
        }
        return onFocusChangeListener as View.OnFocusChangeListener
    }

    private fun setSpinner(spinnerData: ArrayList<String>, spinner: Spinner) {
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerData)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = arrayAdapter

    }


    fun requireField(text: String): SpannableStringBuilder {
        val mSpannableStringBuilder = SpannableStringBuilder(text)
        val star = SpannableString("*")
        star.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorRed)),
            0, star.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        mSpannableStringBuilder.append(star)
        return mSpannableStringBuilder
    }


    fun callSignUpApi() {
        CommonMethods.hideKeypad(this@SignUpActivity, btnSignUP)



        if(binding.signupModel!=null){
            val firstName = MultipartBody.Part.createFormData(ApiConstants.FIRST_NAME, binding.signupModel!!.firstName.trim())

            val lastName = MultipartBody.Part.createFormData(
                ApiConstants.LAST_NAME,
                binding.signupModel!!.lastName.trim()
            )
            val email =
                MultipartBody.Part.createFormData(ApiConstants.EMAIL, binding.signupModel!!.email)
            val password = MultipartBody.Part.createFormData(
                ApiConstants.PASSWORD,
                binding.signupModel!!.password.trim()
            )
            val DOB = MultipartBody.Part.createFormData(
                ApiConstants.DOB,
                TimeUtils.convertDateForSendToServer(binding.signupModel!!.DOB)
            )
            val gender =
                MultipartBody.Part.createFormData(ApiConstants.GENDER, binding.signupModel!!.gender)

            val deviceToken = MultipartBody.Part.createFormData(
                ApiConstants.DEVICE_TOKEN,
                CommonMethods.getDataFromSharePreference(
                    this@SignUpActivity,
                    AppConstants.DEVICE_TOKEN
                )
            )
            val device_os = MultipartBody.Part.createFormData(ApiConstants.DEVICE_OS, getString(R.string.android))
            val deviceName = MultipartBody.Part.createFormData(ApiConstants.DEVICE_NAME, CommonMethods.getDeviceName())
            val deviceType =
                MultipartBody.Part.createFormData(
                    ApiConstants.DEVICE_TYPE,
                    getString(R.string.android)
                )
            val versionName =
                MultipartBody.Part.createFormData(
                    ApiConstants.APP_VERSION,
                    BuildConfig.VERSION_NAME
                )
            val community_id =
                MultipartBody.Part.createFormData(
                    ApiConstants.COMMUNITY_ID,
                    CommonMethods.cummunity_id
                )
            val device_udid = MultipartBody.Part.createFormData(
                ApiConstants.DEVICE_UDID,
                Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
            )


            if (imagePath != null) {
                val imgFile = File(imagePath)
                val requestFile =
                    RequestBody.create("multipart/form-data".toMediaTypeOrNull(), imgFile)
                val body = MultipartBody.Part.createFormData(ApiConstants.AVATAR, imgFile.getName(), requestFile)
                val call = ApiClient.getRetrofitInterface()?.signup(
                    firstName,
                    lastName,
                    email,
                    password,
                    DOB,
                    gender,
                    deviceToken,
                    device_os,
                    deviceName,
                    deviceType,
                    versionName,
                    community_id,
                    device_udid,
                    body
                )
                WSClient<Response<List<UserDetails>>>().request(
                    this@SignUpActivity,
                    100,
                    true,
                    call,
                    object : ISuccessHandler<Response<List<UserDetails>>> {
                        override fun successResponse(
                            requestCode: Int,
                            mResponse: Response<List<UserDetails>>
                        ) {
                            var alert = alert(mResponse?.settings?.message.toString()) {
                                okButton {
                                    if (mResponse?.settings?.success.equals("1")) {

                                        finish()
                                    }


                                }
                            }.show()
                            alert.setCancelable(false)


                        }

                    },
                    object : IFailureHandler {
                        override fun failureResponse(requestCode: Int, message: String) {

                            alert(message) {
                                okButton {

                                }
                            }.show()
                        }

                    })
            } else {
                val call = ApiClient.getRetrofitInterface()?.signup(
                    firstName,
                    lastName,
                    email,
                    password,
                    DOB,
                    gender,
                    deviceToken,
                    device_os,
                    deviceName,
                    deviceType,
                    versionName,
                    community_id,
                    device_udid
                )
                WSClient<Response<List<UserDetails>>>().request(
                    this@SignUpActivity,
                    100,
                    true,
                    call,
                    object : ISuccessHandler<Response<List<UserDetails>>> {
                        override fun successResponse(
                            requestCode: Int,
                            mResponse: Response<List<UserDetails>>
                        ) {
                            var alert = alert(mResponse?.settings?.message.toString()) {
                                okButton {
                                    if (mResponse?.settings?.success.equals("1")) {

                                        finish()
                                    }


                                }
                            }.show()
                            alert.setCancelable(false)


                        }

                    },
                    object : IFailureHandler {
                        override fun failureResponse(requestCode: Int, message: String) {

                            alert(message) {
                                okButton {

                                }
                            }.show()
                        }

                    })
            }
        }


    }

    private fun requestCameraPermission() {
        Dexter.withActivity(this@SignUpActivity)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        selector(getString(R.string.select_picture_from), selectionList) { _, i ->
                            when (i) {
                                0 -> {

                                    CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON)
                                        .start(this@SignUpActivity)
                                }
                                1 -> {
                                    UnsplashPhotoPicker.init(application,   AppConstants.UNSPLASH_ACCESS_KEY, AppConstants.UNSPLASH_SECRET_KEY,20)
                                    startActivityForResult(UnsplashPickerActivity.getStartingIntent(this@SignUpActivity, false), AppConstants.UN_SPLASH_REQUEST_CODE)
                                }
                            }
                        }

                    }
                    // check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        if(PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this@SignUpActivity, Manifest.permission.CAMERA)){
                            val builder =
                                alert(CommonMethods.getCameraPermissionAlert(this@SignUpActivity)) {
                                    positiveButton(getString(R.string.settings)) {
                                        openSettings()
                                    }
                                    cancelButton {

                                    }
                                }.show()
                            builder.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                            builder.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                        }
                        else if(PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this@SignUpActivity, Manifest.permission.READ_EXTERNAL_STORAGE) && PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this@SignUpActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE )){
                            val builder =
                                alert(CommonMethods.getStoragePermissionAlert(this@SignUpActivity)) {
                                    positiveButton(getString(R.string.settings)) {
                                        openSettings()
                                    }
                                    cancelButton {

                                    }
                                }.show()
                            builder.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                            builder.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                        }



                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener { }
            .onSameThread()
            .check()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                imagePath = result.uri.path
                if (imagePath != null) {
                    Glide.with(this@SignUpActivity)
                        .load(imagePath)/*.placeholder(R.drawable.my_profile)*/
                        .into(ivUserProfile)
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
            }
        } else if (AppConstants.UN_SPLASH_REQUEST_CODE == requestCode) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                imagePath = data.getStringExtra(AppConstants.RESULT_URL)
                if (imagePath != null) {
                    Glide.with(this@SignUpActivity)
                        .load(imagePath)/*.placeholder(R.drawable.my_profile)*/
                        .into(ivUserProfile)
                }

            }

        }
    }


}