package com.lansingareashoplocal.ui.user

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.provider.Settings
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.ActivityLoginBinding
import com.lansingareashoplocal.databinding.DialogForgotPasswordBinding
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.home.HomeActivity
import com.lansingareashoplocal.ui.model.UserDetails
import com.lansingareashoplocal.utility.helper.Contants.others.FaceBookData
import com.lansingareashoplocal.utility.helper.getTrimmedText
import com.lansingareashoplocal.utility.helper.setDifferentColor
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.etMail
import kotlinx.android.synthetic.main.activity_login.etPassWord
import kotlinx.android.synthetic.main.activity_login.tInputLayoutEmail
import kotlinx.android.synthetic.main.activity_login.tInputLayoutPassword
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import org.jetbrains.anko.toast
import org.json.JSONObject
import kotlin.collections.HashMap

class
LoginActivity : BaseActivity() {

    lateinit var binding: ActivityLoginBinding
    private var callbackManager: CallbackManager? = null
    private var mGoogleSignInClient: GoogleSignInClient? = null

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_login) as ActivityLoginBinding
        binding.loginModel = LoginModel()
        callbackManager = CallbackManager.Factory.create()

//google login
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        // setDontHaveAccountAndSignUp()
        btnLogin.setOnClickListener {
            tInputLayoutEmail.error = ""
            tInputLayoutPassword.error = ""
            if (validateForm()) {
                callLoginApi()
            }
        }
        tvDontHaveAccount.setOnClickListener {
            startActivity(Intent(this@LoginActivity, SignUpActivity::class.java))
        }
        etMail.setOnFocusChangeListener(object : View.OnFocusChangeListener {
            override fun onFocusChange(v: View?, hasFocus: Boolean) {
                if (!hasFocus) {
                    if (binding.loginModel?.isEmailEmpty!!) {
                        tInputLayoutEmail.error = getString(R.string.error_please_enter_email_address)
                    } else if (binding.loginModel?.isEmailValid!!) {
                        tInputLayoutEmail.error =
                            getString(R.string.error_please_enter_valid_email_address)
                    } else {
                        tInputLayoutEmail.error = ""
                    }
                }

            }

        })
        etMail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                tInputLayoutEmail.error = ""
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })


        etPassWord.setOnFocusChangeListener(object : View.OnFocusChangeListener {
            override fun onFocusChange(v: View?, hasFocus: Boolean) {
                if (!hasFocus) {
                    if (binding.loginModel?.isPasswordEmpty!!) {
                        tInputLayoutPassword.error = getString(R.string.error_please_enter_password)
                    } else {
                        tInputLayoutPassword.error = ""
                    }
                }

            }

        })
        etPassWord.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                tInputLayoutPassword.error = ""
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })


        tvForgotPassword.setOnClickListener {
            showForgotPasswordDialog()
        }

        btnFacebook.setOnClickListener {
            doFacebookSignUp()
        }
        btnGoolge.setOnClickListener {
            doGoogleSignUp()
        }

        tvSkip.setOnClickListener {


            if (isTaskRoot) {
                val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent)
                finish()
            } else {
                finish()
            }

        }

        tInputLayoutPassword.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museosans_700))
        tvDontHaveAccount.setDifferentColor(getString(R.string.don_t_have_an_account), " ${getString(R.string.sign_up)}.", this@LoginActivity, tvDontHaveAccount, Typeface.createFromAsset(assets, getString(R.string.museosans_500)), if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) ContextCompat.getColor(this, R.color.colorDarkBlue) else ContextCompat.getColor(this, R.color.colorPrimary))

        tInputLayoutEmail.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutPassword.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))


        tvForgotPassword.setTextColor(if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) ContextCompat.getColor(this, R.color.colorDarkBlue) else ContextCompat.getColor(this, R.color.colorPrimary))

    }

    private fun validateForm(): Boolean {
        var validCount = 3
        if (binding.loginModel?.isEmailEmpty!!) {
            tInputLayoutEmail.error = getString(R.string.error_please_enter_email_address)
            validCount -= 1
        } else if (binding.loginModel?.isEmailValid!!) {
            tInputLayoutEmail.error = getString(R.string.error_please_enter_valid_email_address)
            validCount -= 1
        }
        if (binding.loginModel?.isPasswordEmpty!!) {
            tInputLayoutPassword.error = getString(R.string.error_please_enter_password)
            validCount -= 1
        }


        return validCount == 3

    }

    private fun setDontHaveAccountAndSignUp() {
        val mSpannableStringBuilder =
            SpannableStringBuilder(getString(R.string.don_t_have_an_account))
        val terms = SpannableString(" ${getString(R.string.sign_up)}.")
        terms.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorPrimary)),
            0, terms.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        mSpannableStringBuilder.append(terms)
        tvDontHaveAccount.movementMethod = LinkMovementMethod.getInstance()
        tvDontHaveAccount.text = mSpannableStringBuilder
    }

    private fun doFacebookSignUp() {
        LoginManager.getInstance()
            .logInWithReadPermissions(this, listOf("public_profile", "email"))
        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    val request = GraphRequest.newMeRequest(loginResult.accessToken, object : GraphRequest.GraphJSONObjectCallback {
                            override fun onCompleted(`object`: JSONObject, response: GraphResponse) {
                                val gson = Gson()
                                val jsonInString = `object`.toString()
                                val faceBookData = gson.fromJson<FaceBookData>(
                                    jsonInString,
                                    FaceBookData::class.java
                                )
                                if (faceBookData != null) {
                                    callFacebookSignUp(faceBookData)
                                    Log.e("FacebookData", jsonInString)
                                }
                            }
                        })
                    val parameters = Bundle()
                    parameters.putString(
                        "fields",
                        "id,first_name,last_name,middle_name,name_format,picture,short_name,email"
                    )
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                }

                override fun onError(exception: FacebookException) {
                    //  Toast.makeText(this@LoginActivity, exception.toString(),Toast.LENGTH_SHORT).show()

                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                    Log.d("exception", "" + exception)
                }
            })
    }

    private fun callFacebookSignUp(faceBookData: FaceBookData) {

        val inputparams = HashMap<String, String>()
        inputparams.put(ApiConstants.FIRST_NAME, faceBookData.first_name)
        if (faceBookData.last_name.isNotEmpty()) {
            inputparams.put(ApiConstants.LAST_NAME, faceBookData.last_name)
        }
        if (faceBookData.email.isNotEmpty()) {
            inputparams.put(ApiConstants.EMAIL, faceBookData.email)
        }
        if(faceBookData.picture!=null && faceBookData.picture!!.data!=null){
            inputparams.put(ApiConstants.AVATAR,"https://graph.facebook.com/${faceBookData.id}/picture?width=600&height=600")
        }
        /*if (binding.signupModel?.phoneNumber != null) {
            inputparams.put(ApiConstants.MOBILE, binding.signupModel?.phoneNumber!!)
        }*/
        /* if (binding.signupModel?.DOB != null) {
             inputparams.put(
                 ApiConstants.DOB,
                 TimeUtils.convertDateForSendToServer(binding.signupModel?.DOB)
             )
         }
         if (binding.signupModel?.gender != null) {
             inputparams.put(ApiConstants.GENDER, binding.signupModel?.gender!!)
         }*/
        inputparams.put(ApiConstants.FACEBOOK_ID, faceBookData.id)
        inputparams.put(ApiConstants.SOCIAL_TYPE, ApiConstants.FACEBOOK)
        inputparams.put(
            ApiConstants.DEVICE_TOKEN,
            CommonMethods.getDataFromSharePreference(this@LoginActivity, AppConstants.DEVICE_TOKEN)
        )
        inputparams.put(ApiConstants.DEVICE_OS, getString(R.string.android))
        inputparams.put(ApiConstants.DEVICE_NAME, CommonMethods.getDeviceName())
        inputparams.put(ApiConstants.DEVICE_TYPE, getString(R.string.android))
        inputparams.put(ApiConstants.APP_VERSION, BuildConfig.VERSION_NAME)
        inputparams[ApiConstants.COMMUNITY_ID] = CommonMethods.cummunity_id
        inputparams.put(
            ApiConstants.DEVICE_UDID,
            Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
        )


        val call = ApiClient.getRetrofitInterface()?.socialSignUp(inputparams)


        WSClient<Response<List<UserDetails>>>().request(
            this@LoginActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<UserDetails>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<UserDetails>>
                ) {

                    if (mResponse?.settings?.success.equals("1")) {
                        if (mResponse.data?.get(0) != null) {
                            CommonMethods.setUserData(this@LoginActivity, mResponse.data?.get(0))
                            CommonMethods.setDataIntoSharePreference(
                                this@LoginActivity,
                                AppConstants.USER_ID,
                                mResponse.data?.get(0)?.userId
                            )
                            if (CommonMethods.getDataFromSharePreference(
                                    this@LoginActivity,
                                    AppConstants.BUSINESS_ID
                                ).isNotEmpty()
                            ) {
                                setResult(Activity.RESULT_OK)
                                finish()
                            }
                            else if(!CommonMethods.isComeFromSplashScreen){
                                CommonMethods.isComeFromSplashScreen=false
                                setResult(Activity.RESULT_OK)
                                finish()
                            }


                            else {
                                val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent)
                                finish()
                            }

                        }
                    }


                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                    alert(message) {
                        okButton {

                        }
                    }.show()
                }

            })

    }

    private fun callGoogleSignUp(acct: GoogleSignInAccount) {
        val inputparams = HashMap<String, String>()


        if (acct.givenName != null && acct.givenName!!.isNotEmpty()) {
            inputparams.put(ApiConstants.FIRST_NAME, acct.givenName!!)
        }
        if (acct.familyName != null && acct.familyName!!.isNotEmpty()) {
            inputparams.put(ApiConstants.LAST_NAME, acct.familyName!!)
        }
        if (acct.email != null && acct.email!!.isNotEmpty()) {
            inputparams.put(ApiConstants.EMAIL, acct.email!!)
        }
        if (acct.id != null) {
            inputparams.put(ApiConstants.GOOGLE_ID, acct.id!!)
        }
        if(acct.photoUrl!=null && acct.photoUrl!!.isAbsolute){
            inputparams.put(ApiConstants.AVATAR,acct.photoUrl.toString())
        }
        inputparams.put(ApiConstants.SOCIAL_TYPE, ApiConstants.GOOGLE)
        inputparams.put(
            ApiConstants.DEVICE_TOKEN,
            CommonMethods.getDataFromSharePreference(this@LoginActivity, AppConstants.DEVICE_TOKEN)
        )
        inputparams.put(ApiConstants.DEVICE_OS, getString(R.string.android))
        inputparams.put(ApiConstants.DEVICE_NAME, CommonMethods.getDeviceName())
        inputparams.put(ApiConstants.DEVICE_TYPE, getString(R.string.android))
        inputparams.put(ApiConstants.APP_VERSION, BuildConfig.VERSION_NAME)
        inputparams[ApiConstants.COMMUNITY_ID] = CommonMethods.cummunity_id
        inputparams.put(
            ApiConstants.DEVICE_UDID,
            Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)
        )


        val call = ApiClient.getRetrofitInterface()?.socialSignUp(inputparams)


        WSClient<Response<List<UserDetails>>>().request(
            this@LoginActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<UserDetails>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<UserDetails>>
                ) {

                    if (mResponse?.settings?.success.equals("1")) {
                        if (mResponse.data?.get(0) != null) {
                            CommonMethods.setUserData(this@LoginActivity, mResponse.data?.get(0))
                            CommonMethods.setDataIntoSharePreference(
                                this@LoginActivity,
                                AppConstants.USER_ID,
                                mResponse.data?.get(0)?.userId
                            )
                            if (CommonMethods.getDataFromSharePreference(
                                    this@LoginActivity,
                                    AppConstants.BUSINESS_ID
                                ).isNotEmpty()
                            ) {
                                setResult(Activity.RESULT_OK)
                                finish()
                            }
                            else if(!CommonMethods.isComeFromSplashScreen){
                                CommonMethods.isComeFromSplashScreen=false
                                setResult(Activity.RESULT_OK)
                                finish()
                            }


                            else {
                                val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent)
                                finish()
                            }

                        }
                    }


                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                    alert(message) {
                        okButton {

                        }
                    }.show()
                }

            })

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppConstants.GOOGLE_DETAILS_REQUEST) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleGoogleSignInResult(task)
        }
        callbackManager?.onActivityResult(requestCode, resultCode, data)
        LoginManager.getInstance().logOut();
    }


    fun showForgotPasswordDialog() {
        val context = this
        val builder = AlertDialog.Builder(context)
        val dataBinding = DataBindingUtil.inflate(
            LayoutInflater.from(this@LoginActivity),
            R.layout.dialog_forgot_password,
            null,
            false
        ) as DialogForgotPasswordBinding
        builder.setView(dataBinding.root);
        val mAlertDialog = builder.create()
        dataBinding.tInputLayoutForgotEmail.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))




        if (!binding.loginModel?.isEmailValid!!) {
            dataBinding.etForgotMail.setText(binding.loginModel?.email)
        }

        dataBinding.tvOk.setOnClickListener {
            if (dataBinding.etForgotMail?.getTrimmedText()?.isEmpty()!!) {
                dataBinding.tInputLayoutForgotEmail.error =
                    getString(R.string.error_please_enter_email_address)
            } else if (!Patterns.EMAIL_ADDRESS.matcher(dataBinding.etForgotMail.getTrimmedText()).matches()) {
                dataBinding.tInputLayoutForgotEmail.error =
                    getString(R.string.error_please_enter_valid_email_address)
            } else {
                dataBinding.tInputLayoutForgotEmail.error = ""
            }
            if (!TextUtils.isEmpty(dataBinding.etForgotMail.getTrimmedText()) &&
                Patterns.EMAIL_ADDRESS.matcher(dataBinding.etForgotMail.getTrimmedText()).matches()
            ) {
                CommonMethods.hideKeypad(applicationContext, it)
                mAlertDialog.dismiss()
                callForgotPasswordApi(dataBinding.etForgotMail.getTrimmedText(), mAlertDialog)
            }
        }

        dataBinding.tvCancel.setOnClickListener {

            CommonMethods.hideKeypad(applicationContext, it)
            mAlertDialog.cancel()
        }

        dataBinding.etForgotMail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                dataBinding.tInputLayoutForgotEmail.error = ""
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
        mAlertDialog.show()

    }

    fun callLoginApi() {
        CommonMethods.hideKeypad(this@LoginActivity, btnLogin)
        val inputParams = HashMap<String, String>()
        inputParams[ApiConstants.EMAIL] = binding.loginModel?.email!!.trim()
        inputParams[ApiConstants.PASSWORD] = binding.loginModel?.password!!.trim()
        inputParams[ApiConstants.DEVICE_TOKEN] =
            CommonMethods.getDataFromSharePreference(this@LoginActivity, AppConstants.DEVICE_TOKEN)
        inputParams[ApiConstants.DEVICE_TYPE] = getString(R.string.android)
        inputParams[ApiConstants.DEVICE_OS] = getString(R.string.android)
        inputParams[ApiConstants.DEVICE_NAME] = CommonMethods.getDeviceName()
        inputParams[ApiConstants.APP_VERSION] = BuildConfig.VERSION_NAME
        inputParams[ApiConstants.DEVICE_UDID] =
            Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)

        inputParams[ApiConstants.COMMUNITY_ID] =  CommonMethods.cummunity_id
        var call = ApiClient.getRetrofitInterface()?.logIn(inputParams)



        WSClient<Response<List<UserDetails>>>().request(
            this@LoginActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<UserDetails>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<UserDetails>>
                ) {
                    if (mResponse?.settings?.success.equals("1")) {
                        if (mResponse.data?.get(0) != null) {
                            CommonMethods.setUserData(this@LoginActivity, mResponse.data?.get(0))
                            CommonMethods.setDataIntoSharePreference(this@LoginActivity, AppConstants.USER_ID, mResponse.data?.get(0)?.userId)
                            CommonMethods.setDataIntoSharePreference(this@LoginActivity, AppConstants.PASSWORD, binding.loginModel?.password)
                            if (CommonMethods.getDataFromSharePreference(this@LoginActivity, AppConstants.BUSINESS_ID).isNotEmpty()) {
                                setResult(Activity.RESULT_OK)
                                finish()
                            }
                            else if(!CommonMethods.isComeFromSplashScreen){
                                CommonMethods.isComeFromSplashScreen=false
                                setResult(Activity.RESULT_OK)
                                finish()
                            }
                            else {
                                val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent)
                                finish()
                            }

                        }
                    } else if (mResponse?.settings?.success.equals("-1")) {

                        var alert = alert(mResponse.settings?.message.toString()) {
                            positiveButton(getString(R.string.resend)) {

                                callResendEmailVerfication(mResponse.data?.get(0)?.userId!!)
                            }

                            negativeButton(getString(R.string.cancel)) { }
                        }.show()
                        alert.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                        alert.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false

                    } else {
                        alert(mResponse?.settings?.message.toString()) {
                            okButton { }
                        }.show()
                    }


                }


            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {

                    alert(message) {
                        okButton { }
                    }.show()
                }

            })
    }

    private fun callResendEmailVerfication(loginID: String) {

        var call = ApiClient.getRetrofitInterface()?.resendEmailVerification(loginID)

        WSClient<Response<List<Any>>>().request(
            this@LoginActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<Any>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Any>>) {
                    alert(mResponse.settings?.message.toString()) {
                        okButton { }
                    }.show()
                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert(message) {
                        okButton { }
                    }.show()
                }

            })
    }


    private fun callForgotPasswordApi(emailId: String, mAlertDialog: AlertDialog) {
        val call = ApiClient.getRetrofitInterface()?.forgotPassword(emailId.trim())
        WSClient<Response<List<Any>>>().request(
            this@LoginActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<Any>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Any>>) {
                    if (mResponse.settings?.success.equals("1", true)) {
                        mAlertDialog?.dismiss()
                        alert("" + mResponse.settings?.message) {
                            okButton {
                            }
                        }.show().setOnDismissListener {
                            val intent = Intent(this@LoginActivity, PinViewActivity::class.java)
                            intent.putExtra(AppConstants.MAIL, emailId.trim())
                            // intent.putExtra(AppConstants.USER_ID, mResponse.data?.get(0)?.userId)
                            startActivity(intent)
                        }
                    } else {
                        alert("" + mResponse.settings?.message) {
                            okButton { }
                        }.show()
                    }
                }
            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    //mAlertDialog?.dismiss()
                    alert(message) {
                        okButton { }
                    }.show()
                }
            })
    }


    override fun onBackPressed() {


        if (isTaskRoot) {
            startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
        } else {
            super.onBackPressed()
        }

    }


    private fun doGoogleSignUp() {
        //googleSignOut()
        val signInIntent = mGoogleSignInClient?.signInIntent
        startActivityForResult(signInIntent, AppConstants.GOOGLE_DETAILS_REQUEST)
    }

    private fun handleGoogleSignInResult(completedTask: Task<GoogleSignInAccount>) {

        val acct = GoogleSignIn.getLastSignedInAccount(this@LoginActivity)
        if (acct != null) {
            callGoogleSignUp(acct)
        }


    }
    private fun googleSignOut() {
        val account = GoogleSignIn.getLastSignedInAccount(this)
        if (account != null) {
            mGoogleSignInClient?.signOut()

        }
    }

}
