package com.lansingareashoplocal.ui.user

import android.text.TextUtils
import android.util.Patterns

class LoginModel {

    var email: String=""
    var password: String=""

    val isEmailEmpty: Boolean
        get() = TextUtils.isEmpty(email?.trim { it <= ' ' })

    val isEmailValid: Boolean
        get() = !Patterns.EMAIL_ADDRESS.matcher(email).matches()

    val isPasswordEmpty: Boolean
        get() = TextUtils.isEmpty(password?.trim { it <= ' ' })

    val isPasswordValidMinLength: Boolean
        get() = password!!.length < 4

    val isPasswordValidMaxLength: Boolean
        get() = password!!.length > 11
}
