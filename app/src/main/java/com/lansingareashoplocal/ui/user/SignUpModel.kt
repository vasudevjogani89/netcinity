package com.lansingareashoplocal.ui.user

import android.text.TextUtils
import android.util.Patterns
import java.util.regex.Matcher
import java.util.regex.Pattern

class SignUpModel {

    var firstName: String=""
    var lastName: String=""
    var email: String=""
    var password: String=""
    var confirmPassword=""
    var phoneNumber=""
    var gender: String=""
    var termsAndConditionsState: Boolean = false

    var DOB: String=""

    val isFirstNameEmpty: Boolean
        get() = TextUtils.isEmpty(firstName?.trim())


    val isLastNameEmpty: Boolean
        get() = TextUtils.isEmpty(lastName?.trim())

    val isPhoneEmpty: Boolean
        get() = TextUtils.isEmpty(phoneNumber?.trim())
    val isPhoneSizeValidation: Boolean
        get() = phoneNumber!!.length < 8

    val isEmailEmpty: Boolean
        get() = TextUtils.isEmpty(email?.trim())
    val isGenderEmpty: Boolean
        get() = TextUtils.isEmpty(gender?.trim())
    val isEmailValid: Boolean
        get() = !Patterns.EMAIL_ADDRESS.matcher(email).matches()

    val isPasswordEmpty: Boolean
        get() = TextUtils.isEmpty(password?.trim())


    val isConfirmPasswordEmpty: Boolean
        get() = TextUtils.isEmpty(confirmPassword?.trim())

    val isPasswordValidMinLength: Boolean
        get() = password!!.length < 8

    val isPasswordValidMaxLength: Boolean
        get() = password!!.length > 15


    fun isPasswordValid(): Boolean {
        if (password != null) {
            return (password!!.length < 8 || password!!.length > 15)
        } else {
            return false
        }

    }

    fun comparePasswordAndConfirm(): Boolean {
        return if (password != null && confirmPassword != null) {
            !password!!.equals(confirmPassword!!, ignoreCase = true)
        } else {
            true
        }

    }


    fun isDOBEmpty(): Boolean {
        return TextUtils.isEmpty(DOB)
    }

    fun isValidPassword(): Boolean {

        val pattern: Pattern
        val matcher: Matcher
        val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*?[!\"#\$%&'()*+,-./:;<=>?@\\[\\]^_`{|}~]).{6,}\$$"




        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)

        return !matcher.matches()

    }

}