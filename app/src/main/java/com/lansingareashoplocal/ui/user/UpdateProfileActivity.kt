package com.lansingareashoplocal.ui.user

import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.widget.AppCompatEditText
import androidx.databinding.DataBindingUtil
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.ActivityUpdateProfileBinding
import com.lansingareashoplocal.utility.MyDatePickerDialog
import com.lansingareashoplocal.utility.helper.others.CustomTextWatcher
import kotlinx.android.synthetic.main.activity_update_profile.*
import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.photopicker.UnsplashPhotoPicker
import com.lansingareashoplocal.presentation.UnsplashPickerActivity
import com.lansingareashoplocal.ui.interfaces.EditTextChangeCallBack
import com.lansingareashoplocal.ui.model.UserDetails
import com.lansingareashoplocal.utility.helper.TimeUtils
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_update_profile.etDateOfBirth
import kotlinx.android.synthetic.main.activity_update_profile.etEditableFirstName
import kotlinx.android.synthetic.main.activity_update_profile.etEditableLastName
import kotlinx.android.synthetic.main.activity_update_profile.etGender
import kotlinx.android.synthetic.main.activity_update_profile.etMail
import kotlinx.android.synthetic.main.activity_update_profile.spinnerGender
import kotlinx.android.synthetic.main.activity_update_profile.tInputLayoutDateOfBirth
import kotlinx.android.synthetic.main.activity_update_profile.tInputLayoutEmail
import kotlinx.android.synthetic.main.activity_update_profile.tInputLayoutFirstName
import kotlinx.android.synthetic.main.activity_update_profile.tInputLayoutGender
import kotlinx.android.synthetic.main.activity_update_profile.tInputLayoutLastName
import kotlinx.android.synthetic.main.activity_update_profile.tvTermsConditionsAndPrivacyPolicy
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.okButton
import org.jetbrains.anko.selector
import java.io.File


class UpdateProfileActivity : BaseActivity(), EditTextChangeCallBack {

    var flag = 0
    var myDatePickerDialog: MyDatePickerDialog? = null
    var localImagePath: String? = null
    var liveImagePath:String?=null
    var selectionList = ArrayList<String>()
    override fun afterTextChanged(editText: AppCompatEditText) {


        if (editText == etEditableFirstName)
            tInputLayoutFirstName.error = ""
        else if (editText == etMail) {
            tInputLayoutEmail.error = ""
        }
    }

    var genderOptions = arrayListOf<String>(
        "Gender", "Male","Female","Other","Prefer not to say"," It's complicated"
    )
    lateinit var binding: ActivityUpdateProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectionList.add(getString(R.string.cameraAndGallery))
        selectionList.add(getString(R.string.online_library))
        binding =
            DataBindingUtil.setContentView(
                this,
                R.layout.activity_update_profile
            ) as ActivityUpdateProfileBinding
        binding.signupModel = SignUpModel()

        etDateOfBirth.setOnClickListener {
            if (CommonMethods.getUserData(this@UpdateProfileActivity)!!.dob.isNotEmpty()) {
                val split = CommonMethods.getUserData(this@UpdateProfileActivity)!!.dob.split("-")
                if (split != null && split.size > 2) {
                    val day = Integer.valueOf(split[2])
                    val month = Integer.valueOf(split[1])
                    val year = Integer.valueOf(split[0])
                    myDatePickerDialog = MyDatePickerDialog(year, month, day)
                } else {
                    myDatePickerDialog = MyDatePickerDialog()
                }
                myDatePickerDialog?.getDate(
                    this@UpdateProfileActivity,
                    object : MyDatePickerDialog.DateSelection {
                        override fun getDate(date: String) {
                            if (date.isNotEmpty()) {
                                etDateOfBirth.setText(date)
                                tInputLayoutDateOfBirth.hint =
                                    requireField(getString(R.string.birthday_require_13_year_of_age_or_older))
                                tInputLayoutDateOfBirth.typeface =
                                    Typeface.createFromAsset(
                                        assets,
                                        getString(R.string.museasans_100)
                                    )
                            }

                        }


                    })

            } else {
                val myDatePickerDialog = MyDatePickerDialog()
                myDatePickerDialog.getDate(
                    this@UpdateProfileActivity,
                    object : MyDatePickerDialog.DateSelection {
                        override fun getDate(date: String) {
                            if (date.isNotEmpty()) {
                                if (date.isNotEmpty()) {
                                    etDateOfBirth.setText(date)
                                    tInputLayoutDateOfBirth.hint =
                                        requireField(getString(R.string.birthday_require_13_year_of_age_or_older))
                                    tInputLayoutDateOfBirth.typeface =
                                        Typeface.createFromAsset(
                                            assets,
                                            getString(R.string.museasans_100)
                                        )
                                }
                            }

                        }


                    })
            }

        }

        tvLeft.setOnClickListener {
            finish()
        }


        btnUpdate.setOnClickListener {
            tInputLayoutFirstName.error = ""
            tInputLayoutEmail.error = ""
            if (validateForm()) {
                callUpdateProfile()
            }

        }
        etEditableFirstName.onFocusChangeListener = onFocusListener()
        etEditableLastName.onFocusChangeListener = onFocusListener()
        etMail.onFocusChangeListener = onFocusListener()
        etMail.addTextChangedListener(CustomTextWatcher(etMail, this))
        etEditableFirstName.addTextChangedListener(CustomTextWatcher(etEditableFirstName, this))
        setPrivacyPolicyAndTermsAndCondtionTextWithMultiColors()
        setSpinner(genderOptions, spinnerGender)
        spinnerGender.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                etGender.setText(genderOptions[position])
                tInputLayoutGender.hint =
                    getString(R.string.GENDER)
                tInputLayoutGender.typeface =
                    Typeface.createFromAsset(assets, getString(R.string.museasans_100))
                if (position == 0) {
                    etGender.setText("")
                }

                /* if (position != 0) {

                 } else {
                     tInputLayoutGender.hint =
                         getString(R.string.gender)
                     tInputLayoutGender.typeface =
                         Typeface.createFromAsset(assets, getString(R.string.museosans_700))
                 }*/

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

                Log.e("onNothingSelected", "onNothingSelected")

            }
        }


        etGender.setOnClickListener {
            spinnerGender.performClick()
        }
        tInputLayoutFirstName.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutFirstName.hint = requireField(getString(R.string.FIRST_NAME))
        tInputLayoutLastName.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutEmail.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutEmail.hint = requireField(getString(R.string.EMAIL_ID_ALSO_YOUR_USER_NAME))
        tInputLayoutDateOfBirth.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tInputLayoutGender.typeface =
            Typeface.createFromAsset(assets, getString(R.string.museasans_100))
        tvTitle.text = getString(R.string.my_profile)



        setData()
        ivUpdateOPenCamera.setOnClickListener {
         requestCameraPermission()
        }
        ivUserUpdateProfile.setOnClickListener {
            if (localImagePath != null ) {
                val profileImageDetail = ProfileImageDetail(this@UpdateProfileActivity, localImagePath)
                profileImageDetail.showImageDetail()
            }
           else if(liveImagePath != null){
                val profileImageDetail = ProfileImageDetail(this@UpdateProfileActivity, liveImagePath)
                profileImageDetail.showImageDetail()
            }
        }

    }

    private fun setData() {
        binding.signupModel?.firstName =
            CommonMethods.getUserData(this@UpdateProfileActivity)!!.firstName
        binding.signupModel?.lastName =
            CommonMethods.getUserData(this@UpdateProfileActivity)!!.lastName
        binding.signupModel?.email = CommonMethods.getUserData(this@UpdateProfileActivity)!!.email
        tInputLayoutEmail.isFocusable = binding.signupModel?.email?.isEmpty()!!
        etMail.isFocusable = binding.signupModel?.email?.isEmpty()!!
        val gender = CommonMethods.getUserData(this@UpdateProfileActivity)!!.gender
        spinnerGender.setSelection(getSpinnerPosition(gender))
        val dateOfBirth = CommonMethods.getUserData(this@UpdateProfileActivity)!!.dob
        if (dateOfBirth.isNotEmpty()) {
            binding.signupModel?.DOB = TimeUtils.convertDateForShowingApp(dateOfBirth)

        }

        if (CommonMethods.getUserData(this@UpdateProfileActivity)!!.avatar.isNotEmpty()) {
            liveImagePath=CommonMethods.getUserData(this@UpdateProfileActivity)!!.avatar
            progressImage.visibility = View.VISIBLE
            Glide.with(this@UpdateProfileActivity)
                .load(CommonMethods.getUserData(this@UpdateProfileActivity)?.avatar!!)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progressImage.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progressImage.visibility = View.GONE
                        return false
                    }

                }
                )
                .placeholder(R.drawable.img_profilepic)
                .error(R.drawable.img_profilepic)
                .into(ivUserUpdateProfile)

        }


    }


    private fun getSpinnerPosition(gender: String): Int {

        genderOptions.forEachIndexed { index, s ->
            if (s.equals(gender)) {
                return index
            }
        }
        return 0

    }

    private fun validateForm(): Boolean {

        if (binding.signupModel?.isFirstNameEmpty!!) {
            tInputLayoutFirstName.error = getString(R.string.error_please_enter_first_name)
            return false
        } else if (!binding.signupModel?.isEmailEmpty!! && binding.signupModel?.isEmailValid!!) {
            tInputLayoutEmail.error = getString(R.string.error_please_enter_valid_email_address)
            return false
        } else {
            return true
        }


    }


    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    private fun setPrivacyPolicyAndTermsAndCondtionTextWithMultiColors() {


        val mSpannableStringBuilder =
            SpannableStringBuilder(getString(R.string.i_agree_t_amp_c_and_privacy_policy))
        val terms = SpannableString(" " + getString(R.string.terms_and_conditions))
        terms.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {

            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }, 0, terms.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        terms.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorRed)),
            0, terms.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )


        mSpannableStringBuilder.append(terms)
        val and = SpannableString(" & ")
        and.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorBlack)),
            0, and.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        mSpannableStringBuilder.append(and)

        val privacy = SpannableString(getString(R.string.privacy_policy))
        privacy.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {

            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }, 0, privacy.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        privacy.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorRed)),
            0, privacy.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )

        mSpannableStringBuilder.append(privacy)
        tvTermsConditionsAndPrivacyPolicy.movementMethod = LinkMovementMethod.getInstance()
        tvTermsConditionsAndPrivacyPolicy.text = mSpannableStringBuilder

    }


    private fun onFocusListener(): View.OnFocusChangeListener {
        val onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                if (v == etEditableFirstName) {
                    if (binding.signupModel?.isFirstNameEmpty!!) {
                        tInputLayoutFirstName.error =
                            getString(R.string.error_please_enter_first_name)
                    } else {
                        tInputLayoutFirstName.error = ""
                    }
                } else if (v == etMail) {
                    if (binding.signupModel?.isEmailEmpty!!) {
                        tInputLayoutEmail.error =
                            getString(R.string.error_please_enter_email_address)
                    } else if (binding.signupModel?.isEmailValid!!) {
                        tInputLayoutEmail.error =
                            getString(R.string.error_please_enter_valid_email_address)
                    } else {
                        tInputLayoutEmail.error = ""
                    }
                }
            }
        }
        return onFocusChangeListener as View.OnFocusChangeListener
    }

    private fun setSpinner(spinnerData: ArrayList<String>, spinner: Spinner) {
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerData)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = arrayAdapter

    }


    fun requireField(text: String): SpannableStringBuilder {
        val mSpannableStringBuilder = SpannableStringBuilder(text)
        val star = SpannableString("*")
        star.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorRed)),
            0, star.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        mSpannableStringBuilder.append(star)
        return mSpannableStringBuilder
    }


    fun callUpdateProfile() {

        if (binding.signupModel != null) {
            val firstName = MultipartBody.Part.createFormData(
                ApiConstants.FIRST_NAME, binding.signupModel!!.firstName.trim()
            )
            val lastName = MultipartBody.Part.createFormData(
                ApiConstants.LAST_NAME,
                binding.signupModel!!.lastName.trim()
            )
            val email =
                MultipartBody.Part.createFormData(ApiConstants.EMAIL, binding.signupModel!!.email)


            val DOB = MultipartBody.Part.createFormData(
                ApiConstants.DOB,
                TimeUtils.convertDateForSendToServer(binding.signupModel!!.DOB)
            )
            val gender =
                MultipartBody.Part.createFormData(ApiConstants.GENDER, binding.signupModel!!.gender)

            val userID = MultipartBody.Part.createFormData(
                ApiConstants.USER_ID,
                CommonMethods.getUserId(this@UpdateProfileActivity)!!
            )

            if (localImagePath != null) {
                val imgFile = File(localImagePath)
                val requestFile =
                    RequestBody.create("multipart/form-data".toMediaTypeOrNull(), imgFile)
                val body = MultipartBody.Part.createFormData(
                    ApiConstants.AVATAR,
                    imgFile.getName(),
                    requestFile
                )
                val call = ApiClient.getRetrofitInterface()
                    ?.updateProfile(firstName, lastName, email, gender, DOB, userID, body)
                WSClient<Response<List<UserDetails>>>().request(
                    this@UpdateProfileActivity,
                    100,
                    true,
                    call,
                    object : ISuccessHandler<Response<List<UserDetails>>> {
                        override fun successResponse(
                            requestCode: Int,
                            mResponse: Response<List<UserDetails>>
                        ) {
                            if (mResponse.settings?.success.equals("1")) {
                                var alert = alert(mResponse.settings?.message!!) {
                                    okButton {
                                        setResult(Activity.RESULT_OK)
                                        finish()
                                    }
                                }.show()
                                alert.setCancelable(false)
                                val userDetails = CommonMethods.getUserData(this@UpdateProfileActivity)

                                if (binding?.signupModel?.firstName != null) {
                                    userDetails?.firstName = binding?.signupModel?.firstName!!
                                }

                                if (binding?.signupModel?.lastName != null) {
                                    userDetails?.lastName = binding?.signupModel?.lastName!!
                                }
                                if (binding?.signupModel?.DOB != null) {
                                    userDetails?.dob =
                                        TimeUtils.convertDateForSendToServer(binding?.signupModel?.DOB)
                                }
                                if (binding?.signupModel?.gender != null) {
                                    userDetails?.gender = binding?.signupModel?.gender!!
                                }
                                if (binding?.signupModel?.email != null) {
                                    userDetails?.email = binding?.signupModel?.email!!
                                }
                                if (mResponse.data != null && mResponse.data!!.isNotEmpty()) {
                                    userDetails?.avatar = mResponse.data!![0].avatar
                                }

                                CommonMethods.setUserData(this@UpdateProfileActivity, userDetails)


                            }
                        }

                    },
                    object : IFailureHandler {
                        override fun failureResponse(requestCode: Int, message: String) {
                        }

                    })
            } else {
                val call = ApiClient.getRetrofitInterface()
                    ?.updateProfile(firstName, lastName, email, gender, DOB, userID)
                WSClient<Response<List<UserDetails>>>().request(
                    this@UpdateProfileActivity,
                    100,
                    true,
                    call,
                    object : ISuccessHandler<Response<List<UserDetails>>> {
                        override fun successResponse(
                            requestCode: Int,
                            mResponse: Response<List<UserDetails>>
                        ) {
                            if (mResponse.settings?.success.equals("1")) {
                                var alert = alert(mResponse.settings?.message!!) {
                                    okButton {
                                        setResult(Activity.RESULT_OK)
                                        finish()
                                    }
                                }.show()
                                alert.setCancelable(false)
                                val userDetails =
                                    CommonMethods.getUserData(this@UpdateProfileActivity)

                                if (binding?.signupModel?.firstName != null) {
                                    userDetails?.firstName = binding?.signupModel?.firstName!!
                                }

                                if (binding?.signupModel?.lastName != null) {
                                    userDetails?.lastName = binding?.signupModel?.lastName!!
                                }
                                if (binding?.signupModel?.DOB != null) {
                                    userDetails?.dob =
                                        TimeUtils.convertDateForSendToServer(binding?.signupModel?.DOB)
                                }
                                if (binding?.signupModel?.gender != null) {
                                    userDetails?.gender = binding?.signupModel?.gender!!
                                }
                                if (binding?.signupModel?.email != null) {
                                    userDetails?.email = binding?.signupModel?.email!!
                                }


                                CommonMethods.setUserData(this@UpdateProfileActivity, userDetails)


                            }
                        }

                    },
                    object : IFailureHandler {
                        override fun failureResponse(requestCode: Int, message: String) {
                        }

                    })
            }


        }


    }

    private fun requestCameraPermission() {
        Dexter.withActivity(this@UpdateProfileActivity)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        selector(getString(R.string.select_picture_from), selectionList) { _, i ->
                            when (i) {
                                0 -> {
                                    CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON)
                                        .start(this@UpdateProfileActivity)
                                }
                                1 -> {
                                    UnsplashPhotoPicker.init(application,   AppConstants.UNSPLASH_ACCESS_KEY, AppConstants.UNSPLASH_SECRET_KEY,20)
                                    startActivityForResult(UnsplashPickerActivity.getStartingIntent(this@UpdateProfileActivity, false), AppConstants.UN_SPLASH_REQUEST_CODE)
                                }
                            }
                        }

                    }
                    // check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        if(PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this@UpdateProfileActivity, Manifest.permission.CAMERA)){
                            val builder =
                                alert(CommonMethods.getCameraPermissionAlert(this@UpdateProfileActivity)) {
                                    positiveButton(getString(R.string.settings)) {
                                        openSettings()
                                    }
                                    cancelButton {

                                    }
                                }.show()
                            builder.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                            builder.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                        }
                        else if(PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this@UpdateProfileActivity, Manifest.permission.READ_EXTERNAL_STORAGE) && PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this@UpdateProfileActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE )){
                            val builder =
                                alert(CommonMethods.getStoragePermissionAlert(this@UpdateProfileActivity)) {
                                    positiveButton(getString(R.string.settings)) {
                                        openSettings()
                                    }
                                    cancelButton {

                                    }
                                }.show()
                            builder.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                            builder.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                        }



                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener { }
            .onSameThread()
            .check()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK && result!=null) {
                localImagePath = result.uri.path
                if (localImagePath != null) {
                    Glide.with(this@UpdateProfileActivity)
                        .load(localImagePath)/*.placeholder(R.drawable.my_profile)*/
                        .into(ivUserUpdateProfile)
                }
            }
        }

        else if (AppConstants.UN_SPLASH_REQUEST_CODE == requestCode) {
            if (resultCode == Activity.RESULT_OK && data!=null) {
                localImagePath=data.getStringExtra(AppConstants.RESULT_URL)
                if (localImagePath != null) {
                    Glide.with(this@UpdateProfileActivity)
                        .load(localImagePath)/*.placeholder(R.drawable.my_profile)*/
                        .into(ivUserUpdateProfile)
                }

            }

        }

    }
}