package com.lansingareashoplocal.ui.user

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.content.Intent
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.lansingareashoplocal.R
import com.lansingareashoplocal.ui.model.OtpModel
import com.lansingareashoplocal.ui.settings.ResetPasswordActivity
import kotlinx.android.synthetic.main.activity_pin_view.*
import kotlinx.android.synthetic.main.tool_bar_with_out_weather_details.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton

class PinViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin_view)
        tvLeft.visibility = View.VISIBLE

        tvTitle.text = getString(R.string.verify_email)
        tvLeft.setOnClickListener { onBackPressed() }
        btnLogin.setOnClickListener {
            if (pinView.value.isNotEmpty() && pinView.value.length > 3) {
                callOtpVerifyApi()
            } else {
                alert(getString(R.string.error_enter_valid_otp)) {
                    okButton { }

                }.show()
            }


        }
        DontRecieveOtp()

        tvDontRecieveOtp.setOnClickListener {
            callForgotPasswordApi()
        }
    }

    private fun callOtpVerifyApi() {
        val inputParams = HashMap<String, String>()
        inputParams[ApiConstants.EMAIL] = intent.getStringExtra(AppConstants.MAIL)
        inputParams[ApiConstants.VERIFY_CODE] = pinView.value

        val call = ApiClient.getRetrofitInterface()?.verifyOtp(inputParams)

        WSClient<Response<List<OtpModel>>>().request(this@PinViewActivity, 100, true, call, object :
            ISuccessHandler<Response<List<OtpModel>>> {
            override fun successResponse(requestCode: Int, mResponse: Response<List<OtpModel>>) {
                if (mResponse.settings?.success.equals("1", true)) {
                    val intentMain = Intent(applicationContext, ResetPasswordActivity::class.java)
                    intentMain.putExtra(
                        ApiConstants.EMAIL,
                        getIntent().getStringExtra(ApiConstants.EMAIL)
                    )
                    intentMain.putExtra(AppConstants.USER_ID, mResponse.data?.get(0)?.userId)
                    startActivity(intentMain)

                } else {
                    alert("" + mResponse.settings?.message) {
                        okButton { }
                    }.show()
                }
            }
        }, object : IFailureHandler {
            override fun failureResponse(requestCode: Int, message: String) {
                alert(message) {
                    okButton { }
                }.show()
            }
        })
    }


    private fun DontRecieveOtp() {
        val mSpannableStringBuilder =
            SpannableStringBuilder(getString(R.string.don_t_received_verification_otp))
        val terms = SpannableString(" " + getString(R.string.resend))
        terms.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorRed)),
            0, terms.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        mSpannableStringBuilder.append(terms)
        tvDontRecieveOtp.movementMethod = LinkMovementMethod.getInstance()
        tvDontRecieveOtp.text = mSpannableStringBuilder

    }


    private fun callForgotPasswordApi() {
        val call = ApiClient.getRetrofitInterface()
            ?.forgotPassword(intent.getStringExtra(AppConstants.MAIL))
        WSClient<Response<List<Any>>>().request(
            this@PinViewActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Response<List<Any>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<Any>>) {
                    alert("" + mResponse.settings?.message) {
                        okButton { }
                    }.show()
                }
            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    //mAlertDialog?.dismiss()
                    alert(message) {
                        okButton { }
                    }.show()
                }
            })
    }
}
