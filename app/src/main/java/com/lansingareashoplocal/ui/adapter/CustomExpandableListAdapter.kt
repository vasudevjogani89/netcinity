package com.lansingareashoplocal.ui.adapter

import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.lansingareashoplocal.R
import com.lansingareashoplocal.ui.interfaces.CommonCallBack
import java.util.*

public  class CustomExpandableListAdapter(
    var context: Context,
    var titleList: List<String>,
    var dataList: HashMap<String, List<String>>, var commonCallBack: CommonCallBack) : BaseExpandableListAdapter(), Filterable {

    var beforeFilterFAQList = HashMap<String, List<String>>()
    var afterFilterFAQList = HashMap<String, List<String>>()
    var beforeTitleList= mutableListOf<String>()
    var afterTitleList= mutableListOf<String>()
    private var valueFilter: ValueFilter? = null
    init {
        this.beforeFilterFAQList = dataList
        this.afterFilterFAQList=dataList
        this.beforeTitleList= titleList as MutableList<String>
        this.afterTitleList= titleList as MutableList<String>


    }


    override fun getChild(listPosition: Int, expandedListPosition: Int): Any {
        return this.afterFilterFAQList[this.afterTitleList[listPosition]]!![expandedListPosition]
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(
        listPosition: Int,
        expandedListPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = convertView
        val expandedListText = getChild(listPosition, expandedListPosition) as String
        if (convertView == null) {
            val layoutInflater =
                this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.item_faq, null)
        }
        val expandedListTextView = convertView!!.findViewById<TextView>(R.id.listView)
        expandedListTextView.text = expandedListText
        return convertView
    }

    override fun getChildrenCount(listPosition: Int): Int {
        return this.afterFilterFAQList[this.afterTitleList[listPosition]]!!.size
    }

    override fun getGroup(listPosition: Int): Any {
        return this.afterTitleList[listPosition]
    }

    override fun getGroupCount(): Int {
        return this.afterTitleList.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getGroupView(
        listPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = convertView
        val listTitle = getGroup(listPosition) as String
        if (convertView == null) {
            val layoutInflater =
                this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.item_faq, null)
        }
        val listTitleTextView = convertView!!.findViewById<TextView>(R.id.listView)
        val face = Typeface.createFromAsset(context.getAssets(), "museosans_500.otf")
        listTitleTextView.typeface = face
        listTitleTextView.text = listTitle
        return convertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        return true
    }

    override public fun getFilter(): Filter {
        if (valueFilter == null) {
            valueFilter = ValueFilter()
        }
        return valueFilter as ValueFilter
    }

    public inner class ValueFilter : Filter() {
        @RequiresApi(Build.VERSION_CODES.N)
        override fun performFiltering(constraint: CharSequence): FilterResults {
            val results = FilterResults()
            if (constraint != null && constraint.length > 0) {
                val filterList: HashMap<String, List<String>> = HashMap<String, List<String>>()

                beforeFilterFAQList.forEach { title, answerList ->
                    if (title.toUpperCase(Locale.ROOT).contains(constraint.toString().toUpperCase(Locale.ROOT))) {
                        val answer: MutableList<String> = ArrayList()
                        answerList.forEachIndexed { index, s ->
                            answer.add(s)
                            filterList[title] = answer
                        }
                        //  questionsList?.add(faq.question)

                    } else {
                        val answerList: MutableList<String> = ArrayList()
                        answerList.forEachIndexed { index, answer ->
                            if (answer.toUpperCase(Locale.ROOT).contains(constraint.toString().toUpperCase(Locale.ROOT))) {
                                answerList.add(answer)
                                filterList[title] = answerList
                            }
                        }

                    }
                }

                results.count = filterList.size
                results.values = filterList
            } else {
                results.count = beforeFilterFAQList.size
                results.values = beforeFilterFAQList
            }
            return results
        }

        @RequiresApi(Build.VERSION_CODES.N)
        override fun publishResults(
            constraint: CharSequence,
            results: FilterResults
        ) {
            afterTitleList.clear()
            afterFilterFAQList = results.values as HashMap<String, List<String>>
            afterFilterFAQList.forEach { t, u ->
                afterTitleList.add(t)
            }
            if (afterFilterFAQList.isEmpty()) {
                commonCallBack.commonCallBack("",true)
            } else {
                commonCallBack.commonCallBack("",false)
            }
            notifyDataSetChanged()
        }
    }
}



