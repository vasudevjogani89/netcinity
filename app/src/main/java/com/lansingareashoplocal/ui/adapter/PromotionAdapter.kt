package com.lansingareashoplocal.ui.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.Glide
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.sticky_header.ItemType
import com.lansingareashoplocal.sticky_header.KmStickyListener
import com.lansingareashoplocal.ui.interfaces.CommonCallBack
import com.lansingareashoplocal.ui.model.FeaturedBusiness
import com.lansingareashoplocal.ui.report.AddReportActivity
import com.lansingareashoplocal.utility.helper.TimeUtils
import kotlinx.android.synthetic.main.item_promotions.view.*

open class PromotionAdapter(
    var context: Context, var commonCallBack: CommonCallBack
) : ListAdapter<FeaturedBusiness, RecyclerView.ViewHolder>(ModelDiffUtilCallback),
    KmStickyListener {


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View
        if (viewType == ItemType.Header) {
            itemView = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_header, viewGroup, false)
            return HeaderViewHolder(itemView)
        } else {
            val dataBinding = DataBindingUtil.inflate<ViewDataBinding>(
                LayoutInflater.from(viewGroup.getContext()),
                R.layout.item_promotions,
                viewGroup,
                false
            )
            return PostViewHolder(dataBinding)
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        if (getItemViewType(i) == ItemType.Header) {
            (viewHolder as HeaderViewHolder).bind(getItem(i))
        } else {
            (viewHolder as PostViewHolder).bind(getItem(i), i)
        }
    }


    inner class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView

        init {
            title = itemView.findViewById<View>(R.id.title_header) as TextView
        }

        fun bind(model: FeaturedBusiness) {
            title.text = model.header
        }
    }

    inner class PostViewHolder(itemView: ViewDataBinding) : RecyclerView.ViewHolder(itemView.root) {
        fun bind(model: FeaturedBusiness, position: Int) {

            if (com.lansingareashoplocal.BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
                changeColorForStayLocal()
            }
            itemView?.tvCommunityName?.text = model.placeName
            itemView?.tvCommunityAddress?.text = model.address
            Glide.with(context).load(model.owner_profile_image).into(itemView.ivOwner)
            if (model.owner_profile_image.isEmpty()) {
                itemView.ivOwner.visibility = View.GONE
            } else {
                itemView.ivOwner.visibility = View.VISIBLE
            }
            if (model?.getPromotionList.isNotEmpty()) {
                if (model.getPromotionList.get(0).set_expiration.equals("Yes")) {
                    itemView?.tvEventExpireDate.text =
                        "Exp.-${TimeUtils.convertDateWithTimeForShowingApp(
                            model.getPromotionList.get(
                                0
                            ).peroidEnd
                        )}"
                }
                itemView?.tvEventName.text = model?.getPromotionList?.get(0)?.priceDiscount
                itemView?.rlEventName.visibility = View.VISIBLE
                itemView?.tvEventExpireDate.visibility = View.VISIBLE
                itemView?.vwDummySpace.visibility = View.VISIBLE
            } else {
                itemView?.rlEventName.visibility = View.GONE
                itemView?.vwDummySpace.visibility = View.GONE
                itemView?.tvEventExpireDate.visibility = View.INVISIBLE
            }
            itemView?.tvPhoneNumber?.text = model.phone
            if (model.phone.isEmpty()) {
                itemView.rlPhone.visibility = View.GONE
            } else {
                itemView.rlPhone.visibility = View.VISIBLE
            }
            if (model.distance.isEmpty()) {

            } else {
                itemView.tvDistance.text = "${model.distance} mi"
            }
            itemView?.tvPhoneNumber?.text = model.phone
            if (model.address.isEmpty()) {
                itemView.rlpromotionAddress.visibility = View.GONE
            } else {
                itemView.rlpromotionAddress.visibility = View.VISIBLE
            }

            Glide.with(context).load(model.bannerImage).into(itemView?.ivCommunity!!)

            if (model.hasNext) {
                if (model.isFeatureBusiness) {
                    itemView?.tvShowMore?.text =
                        "show Me ${model.remainingCount} More Featured Business"
                } else {
                    itemView?.tvShowMore?.text =
                        "show Me ${model.remainingCount} More Non Featured Business"
                }
                itemView?.tvShowMore?.visibility = View.VISIBLE
            } else {
                itemView?.tvShowMore?.visibility = View.GONE
            }

            if (model.isFeatureBusiness) {
                itemView.rlCommunityImage.visibility = View.VISIBLE
                itemView.tvReportNonBusiness.visibility = View.GONE
                itemView.tvCommunityName.setCompoundDrawablesWithIntrinsicBounds(
                    null, null,
                    ContextCompat.getDrawable(context, R.drawable.ic_right_arrow), null
                )
            } else {
                itemView.tvReportNonBusiness.visibility = View.VISIBLE
                itemView.tvCommunityName.setCompoundDrawables(null, null, null, null)
                itemView.rlCommunityImage.visibility = View.GONE
                itemView.rlpromotionAddress.setOnClickListener {
                    if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
                        var geoUri: String? = null
                        if (CommonMethods.getDataFromSharePreference(
                                context,
                                AppConstants.LATITUDE
                            ).isNotEmpty() && CommonMethods.getDataFromSharePreference(
                                context,
                                AppConstants.LONGITUDE
                            ).isNotEmpty()
                        ) {
                            geoUri =
                                "http://maps.google.com/maps?saddr=${CommonMethods.getDataFromSharePreference(
                                    context,
                                    AppConstants.LATITUDE
                                )},${CommonMethods.getDataFromSharePreference(
                                    context,
                                    AppConstants.LONGITUDE
                                )}&daddr=${model?.latitude},${model?.longitude}"
                        } else {
                            geoUri =
                                "http://maps.google.com/maps?q=loc:${model.latitude},${model.longitude})"
                        }

                        if (model.latitude.isNotEmpty() && model.longitude.isNotEmpty()) {
                            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
                            try {
                                intent.setPackage("com.google.android.apps.maps");
                                context.startActivity(intent)
                            } catch (e: Exception) {
                                Toast.makeText(
                                    context,
                                    "Invalid link",
                                    Toast.LENGTH_SHORT
                                ).show()
                                e.printStackTrace()
                            }
                        } else {
                            Toast.makeText(
                                context,
                                "Invalid",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

            }
            if (model.sub_category.isEmpty()) {
                itemView.tvSubCategories.visibility = View.GONE
            } else {
                try {
                    itemView?.tvSubCategories?.text = CommonMethods.getCapsSentences(model.sub_category)
                } catch (e: Exception) {
                    itemView?.tvSubCategories?.text = model.sub_category
                }

                itemView.tvSubCategories.visibility = View.VISIBLE
            }
            if (position == itemCount - 1) {
                itemView.btnListYourBusiness.visibility = View.VISIBLE
            } else {
                itemView.btnListYourBusiness.visibility = View.GONE
            }

            if (model.featured.equals("Yes")) {
                itemView.triangleViewFeature.visibility = View.VISIBLE
            } else {
                itemView.triangleViewFeature.visibility = View.GONE
            }
            itemView?.cvMain?.setOnClickListener {


                if (model.isFeatureBusiness) {
                    commonCallBack.commonCallBack(AppConstants.ITEM_CLICK, model)
                }
            }
            itemView?.tvReportNonBusiness.setOnClickListener {
                val intent = Intent(context, AddReportActivity::class.java)
                intent.putExtra(AppConstants.BUSINESS_ID, model.businessPlaceId)
                intent.putExtra(AppConstants.BUSINESS_NAME, model.placeName)
                context.startActivity(intent)
            }


            if (model.emptyData) {
                itemView.cvMain.visibility = View.GONE
                itemView.tvNoData.visibility = View.VISIBLE
                itemView.tvNoData.text = model.emptyMessage
            } else {
                itemView.cvMain.visibility = View.VISIBLE
                itemView.tvNoData.visibility = View.GONE
            }



            itemView?.tvShowMore?.setOnClickListener {
                if (model.isFeatureBusiness) {
                    commonCallBack.commonCallBack(
                        AppConstants.FEATURED_BUSINESS,
                        adapterPosition + 1
                    )
                } else {
                    commonCallBack.commonCallBack(
                        AppConstants.NON_FEATURED_BUSINESS,
                        adapterPosition + 1
                    )
                }

            }


            itemView?.btnListYourBusiness?.setOnClickListener {
                /*val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(AppConstants.BUSINESS_LINK)
                try {
                    context.startActivity(i)
                } catch (e: Exception) {
                    Toast.makeText(context, "Invalid link", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }*/

                CommonMethods.openCustomtab(context, CommonMethods.business_link)

            }
        }

        private fun changeColorForStayLocal() {
            itemView.tvSubCategories.setTextColor(ContextCompat.getColor(context, R.color.colorDarkBlue))
            itemView.tvShowMore.setTextColor(ContextCompat.getColor(context, R.color.colorDarkBlue))
            itemView.tvReportNonBusiness.setTextColor(ContextCompat.getColor(context, R.color.colorDarkBlue))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).itemType!!
    }

    override fun getHeaderPositionForItem(itemPosition: Int?): Int? {
        var headerPosition: Int? = 0
        if (itemPosition != null) {
            for (i in itemPosition downTo 1) {
                if (isHeader(i)!!) {
                    headerPosition = i
                    return headerPosition
                }
            }
        }
        return headerPosition
    }

    override fun getHeaderLayout(headerPosition: Int?): Int? {
        return R.layout.item_header
    }

    override fun bindHeaderData(header: View, headerPosition: Int?) {
        val tv = header.findViewById<TextView>(R.id.title_header)
        tv.text = getItem(headerPosition!!).header
    }

    override fun isHeader(itemPosition: Int?): Boolean? {
        return getItem(itemPosition!!).itemType == ItemType.Header
    }

    companion object {

        val ModelDiffUtilCallback: DiffUtil.ItemCallback<FeaturedBusiness> =
            object : DiffUtil.ItemCallback<FeaturedBusiness>() {
                override fun areItemsTheSame(
                    model: FeaturedBusiness,
                    t1: FeaturedBusiness
                ): Boolean {
                    return model.header == t1.header
                }

                override fun areContentsTheSame(
                    model: FeaturedBusiness,
                    t1: FeaturedBusiness
                ): Boolean {
                    return model == t1
                }
            }
    }

}
