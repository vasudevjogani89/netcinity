package com.lansingareashoplocal.ui.home

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.core.home.HomeFragment
import com.lansingareashoplocal.ui.notification.NotificationFragment
import com.lansingareashoplocal.ui.settings.SettingsFragment
import com.lansingareashoplocal.utility.helper.customclass.TabCustomView
import kotlinx.android.synthetic.main.activity_home.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.okButton
import com.lansingareashoplocal.utility.helper.logs.Log
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import android.net.Uri
import android.os.Handler


class HomeActivity : BaseActivity(), GoogleApiClient.OnConnectionFailedListener {


    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.e("onConnectionFailed", "onConnectionFailed")
    }

    var notificationView: TabCustomView? = null
    var currentTab = 0



    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        if (CommonMethods.isLocationEnabled(this@HomeActivity)) {
            getLastLocation()
        }
        setTabs()


        // here we are saving latitude and longitude for feature purpose


        getDynamicLink()


    }

    private fun getDynamicLink() {
        FirebaseDynamicLinks.getInstance()
            .getDynamicLink(intent)
            .addOnSuccessListener(this) { pendingDynamicLinkData ->
                // Get deep link from result (may be null if no link is found)
                var deepLink: Uri? = null
                if (pendingDynamicLinkData != null) {
                    deepLink = pendingDynamicLinkData!!.link
                    Log.e("getDynamicLink", deepLink.toString())
                }


                // Handle the deep link. For example, open the linked
                // content, or apply promotional credit to the user's
                // account.
                // ...

                // ...
            }
            .addOnFailureListener(this, object : OnFailureListener {
                override fun onFailure(e: Exception) {
                    Log.e("getDynamicLink", "OnFailure")
                }
            })
    }


    private fun setTabs() {
        tabHost?.setup(this, supportFragmentManager, R.id.mainFrameLayout)

        tabHost?.addTab(
            tabHost!!.newTabSpec("tab1").setIndicator(
                TabCustomView(this, R.drawable.menu_home, R.drawable.menu_home, "", false)), HomeFragment()::class.java, null
        )

        tabHost?.addTab(
            tabHost!!.newTabSpec("tab2").setIndicator(
                TabCustomView(
                    this,
                    R.drawable.menu_message, R.drawable.menu_message,
                    "", false
                )
            ), NotificationFragment()::class.java, null
        )

        tabHost?.addTab(
            tabHost!!.newTabSpec("tab3").setIndicator(
                TabCustomView(
                    this,
                    R.drawable.menu_search, R.drawable.menu_search,
                    "", false
                )
            ), SearchFragment()::class.java, null
        )

        notificationView = TabCustomView(
            this,
            R.drawable.ic_share_gray, R.drawable.ic_share_gray,
            "", true
        )

        tabHost?.addTab(
            tabHost!!.newTabSpec("tab4").setIndicator(notificationView),
            SettingsFragment::class.java, null
        )

        tabHost?.addTab(
            tabHost!!.newTabSpec("tab5").setIndicator(
                TabCustomView(
                    this,
                    R.drawable.meu_toggle, R.drawable.meu_toggle,
                    "", false
                )
            ),
            SettingsFragment::class.java, null
        )

        tabHost!!.currentTab = 0

        tabHost!!.tabClickableListener { index ->

            if (currentRunningFragment != null) {
                when (index) {
                    0 -> {
                        currentTab = 0
                        /* if (homeFragment == null) {
                             homeFragment = currentRunningFragment as HomeFragment
                         }

                         if (homeFragment != null) {
                             homeFragment?.updateTemperature()
                             homeFragment?.hideSearchBar()
                             if (isOnSearchTab) { //
                                 isOnSearchTab =
                                     false //on click on search tab this condtion only for we dont want change isSearchShowing status
                             } else {
                                 isSearchShowing =
                                     false //onclick on Home tab  every time we change isSearchShowing status "flase" since if  click on search tab on next time  we need to show search bar
                             }

                         }*/

                    }
                    1 -> {


                        currentTab = 1
                        /*  isSearchShowing = false
                          val notificationListFragment =
                              currentRunningFragment as NotificationFragment
                          notificationListFragment?.updateTemperature()
  */
                    }
                    2 -> {

                        currentTab = 2
                        /*tabHost!!.currentTab = 0
                        isOnSearchTab = true
                        isSearchShowing =
                            !isSearchShowing // if search is showing  we will hide it  if search is not showing we will show it
                        homeFragment?.hideAndShowSearchBar(isSearchShowing)*/
                    }
                    3 -> {
                        tabHost.currentTab = currentTab
                        /* isSearchShowing = false
                         if (currentTab == 2) {
                             isOnSearchTab = true
                         }*/
                        CommonMethods.shareApp(this@HomeActivity)

                    }
                    4 -> {
                        currentTab = 4
                        /* isSearchShowing =
                             false

                         val settingFragment = currentRunningFragment as SettingsFragment
                         settingFragment?.init()*/
                    }
                }
            }

        }


    }


    override fun onBackPressed() {
        alert(getString(R.string.exit_app_alert)) {
            okButton {
                finish()
            }
            cancelButton {


            }
        }.show()


    }











    public fun setHomebBar() {
        tabHost.currentTab = 0

    }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        /*if (requestCode == AppConstants.WEATHER_REQUEST && resultCode == Activity.RESULT_OK) {
            homeFragment = currentRunningFragment as HomeFragment
            if(homeFragment!=null){
                homeFragment?.updateTemperature()
                homeFragment?.setCityName()
                homeFragment?.setCityNameEmpty()
            }
        }*/
    }


}