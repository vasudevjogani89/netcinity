package com.lansingareashoplocal.core.home


import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.content.Intent
import android.graphics.Bitmap
import android.text.format.DateUtils
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.gson.Gson
import com.kobakei.ratethisapp.RateThisApp
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.communityevents.CommunityEventDetailsActivity
import com.lansingareashoplocal.ui.communityevents.CommunityEventListActivity
import com.lansingareashoplocal.core.BaseFragment
import com.lansingareashoplocal.databinding.*
import com.lansingareashoplocal.ui.direct_message.DialogDirectMessage
import com.lansingareashoplocal.ui.model.*
import com.lansingareashoplocal.ui.promotions.PromationsListActivity
import com.lansingareashoplocal.ui.newsandalert.NewsAndAlertDetailsActivity
import com.lansingareashoplocal.ui.newsandalert.NewsAndAlertListActivity
import com.lansingareashoplocal.ui.promotions.FavouriteListActivity
import com.lansingareashoplocal.ui.promotions.PromotionDetailsActivity
import com.lansingareashoplocal.ui.tempareture.TemperatureActivity
import com.lansingareashoplocal.ui.user.LoginActivity
import com.lansingareashoplocal.utility.helper.AlertUtils
import com.lansingareashoplocal.utility.helper.TimeUtils
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.toolbar_with_weather_details.view.ivCloud
import kotlinx.android.synthetic.main.toolbar_with_weather_details.view.rlTemperature
import kotlinx.android.synthetic.main.toolbar_with_weather_details.view.tvTemparature
import kotlinx.android.synthetic.main.toolbar_with_weather_details.view.tvTitle
import org.jetbrains.anko.okButton
import org.jetbrains.anko.support.v4.alert
import java.util.*
import kotlin.collections.HashMap
import kotlin.math.roundToInt


public class HomeFragment() : BaseFragment<FragmentHomeBinding>() {


    var categoryList = mutableListOf<Category>()
    var cummunityEventList = mutableListOf<CommunityEvents>()
    var newsList = mutableListOf<News>()
    var featureBusinessOfTheWeekList = mutableListOf<FeaturedBusinessWeek>()
    var featureBusinessOfTheWeekAdapter: GenericAdapter<FeaturedBusinessWeek, ItemFeatureBusinessOfTheWeekBinding>? =
        null
    var categoryListWithBitmap = mutableListOf<CategoryBitMap>()

    override fun onBackPressed(): Boolean {


        return false
    }


    var categoryIcons = arrayListOf<Int>(
        R.drawable.ic_shop_local,
        R.drawable.ic_fav
    )


    var categotyAdapter: GenericAdapter<Category, ItemCategoryBinding>? = null
    var dummyCategotyAdapter: GenericAdapter<Category, ItemDummyCategoryBinding>? = null
    var newsAdapter: GenericAdapter<News, ItemNewsAlertBinding>? = null

    var communityEventsAdapter: GenericAdapter<CommunityEvents, ItemCummunityEventsBinding>? = null


    override fun getLayoutId(): Int {
        return R.layout.fragment_home

    }

    override fun init() {
        showRateAppIFNeed()
        showShareAppIfNeed()
        Log.d(
            "tokenHomeFragmnet",
            CommonMethods.getDataFromSharePreference(activity!!, AppConstants.DEVICE_TOKEN)
        )
        setCategories()
        setDummyCategories()
        setCummunity()
        setNewsAndAlert()
        binding.rlSearchBar.visibility = View.GONE
        binding.tvShowAllCommunity.setOnClickListener {

            if (cummunityEventList.size > 0) {
                startActivity(Intent(activity, CommunityEventListActivity::class.java))
            }

        }
        binding.tvShowNewAndAlert.setOnClickListener {

            if (newsList.size > 0) {

                startActivity(Intent(activity, NewsAndAlertListActivity::class.java))
            }
        }
        binding.toolBarHome.rlTemperature.setOnClickListener {
            startActivityForResult(
                Intent(activity!!, TemperatureActivity::class.java),
                AppConstants.WEATHER_REQUEST
            )
        }
        binding.swipeRefreshHome.setOnRefreshListener {
            callCaterotyApi(false)
        }
        clearText()
        callCaterotyApi(true)
        updateTemperature()
        setCityName()
        setCityNameEmpty()
        if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
            changeColorForStayLocal()
        }
        binding.btnListYourEventHomeScreen.setOnClickListener {
            CommonMethods.openCustomtab(activity!!, CommonMethods.event_link)
        }
    }

    private fun changeColorForStayLocal() {
        binding.tvShowAllCommunity.setTextColor(ContextCompat.getColor(activity!!,R.color.colorDarkBlue))
        binding.tvShowNewAndAlert.setTextColor(ContextCompat.getColor(activity!!,R.color.colorDarkBlue))
    }

    private fun setDummyCategories() {


        dummyCategotyAdapter =
            object : GenericAdapter<Category, ItemDummyCategoryBinding>(categoryList) {
                override fun getLayoutId(): Int {
                    return R.layout.item_dummy_category
                }

                override fun onBindData(
                    model: Category?,
                    position: Int,
                    dataBinding: ItemDummyCategoryBinding?
                ) {

                    if (model?.isLocal!!) {
                        Glide.with(activity!!).asBitmap().load(model?.localImage)
                            .into(object : SimpleTarget<Bitmap>() {
                                override fun onResourceReady(
                                    resource: Bitmap,
                                    transition: Transition<in Bitmap>?
                                ) {
                                    var categoryBitMap = CategoryBitMap()
                                    categoryBitMap.bitMap =
                                        Bitmap.createScaledBitmap(resource, 80, 80, false)
                                    categoryBitMap.category = model.category
                                    categoryBitMap.categoryId = model.categoryId

                                    categoryListWithBitmap.add(categoryBitMap)
                                    val gson = Gson()
                                    val json = gson.toJson(categoryListWithBitmap)
                                    CommonMethods.setDataIntoSharePreference(
                                        activity!!,
                                        AppConstants.CATEGORY_BITMAP_DATA,
                                        json
                                    )
                                }

                            })

                    } else {
                        Glide.with(activity!!).asBitmap().load(model?.icon)
                            .into(object : SimpleTarget<Bitmap>() {
                                override fun onResourceReady(
                                    resource: Bitmap,
                                    transition: Transition<in Bitmap>?
                                ) {
                                    var categoryBitMap = CategoryBitMap()
                                    categoryBitMap.bitMap =
                                        Bitmap.createScaledBitmap(resource, 80, 80, false)
                                    categoryBitMap.category = model.category
                                    categoryBitMap.categoryId = model.categoryId

                                    categoryListWithBitmap.add(categoryBitMap)
                                    val gson = Gson()
                                    val json = gson.toJson(categoryListWithBitmap)
                                    CommonMethods.setDataIntoSharePreference(
                                        activity!!,
                                        AppConstants.CATEGORY_BITMAP_DATA,
                                        json
                                    )
                                }

                            })

                    }


                }

            }
        binding.rvDummyategories.adapter = dummyCategotyAdapter
    }


    private fun showShareAppIfNeed() {
        val date_in_milli_sec = (CommonMethods.getLongKeyDataFromSharePreference(
            activity!!,
            AppConstants.DATE_IN_MILLI_SEC
        ))
        if (date_in_milli_sec == 0L) {
            saveShareDateInSharePrefrence(7)

        } else {
            if (date_in_milli_sec != null) {
                val date = Date(date_in_milli_sec)
                val currentDate = Date(System.currentTimeMillis())
                if (CommonMethods.getZeroTimeDate(date) == CommonMethods.getZeroTimeDate(currentDate)) {
                    AlertUtils.showMessageWithCancelOk(
                        activity!!,
                        getString(R.string.share_app_message),
                        "SHARE IT NOW ",
                        "NO THANKS",
                        "Share this app",
                        true,
                        object : AlertUtils.IAlertCallback {
                            override fun onOKClick() {
                                saveShareDateInSharePrefrence(-1)
                                CommonMethods.shareApp(activity!!)

                            }

                            override fun onCancelClick() {
                                saveShareDateInSharePrefrence(7)
                            }
                        }
                    )
                }
            }


        }

    }

    private fun showRateAppIFNeed() {
        // Monitor launch times and interval from installation
        RateThisApp.onCreate(activity);
        // If the condition is satisfied, "Rate this app" dialog will be shown
        RateThisApp.showRateDialogIfNeeded(activity);
// Custom condition: 3 days and 5 launches
        val config = RateThisApp.Config(0, 7);

        //config.setTitle(R.string.my_own_title);
        //config.setMessage(R.string.my_own_message);
        //config.setYesButtonText(R.string.my_own_rate);
        // config.setNoButtonText(R.string.my_own_thanks);
        // config.setCancelButtonText(R.string.my_own_cancel);
        RateThisApp.init(config);
    }

    private fun callNewsListAPi() {
        val call = ApiClient.getRetrofitInterface()
            ?.getNewsAndAlert(
                community_id = CommonMethods.cummunity_id
            )
        WSClient<Response<List<News>>>().request(
            activity!!,
            100,
            false,
            call,
            object : ISuccessHandler<Response<List<News>>> {
                override fun successResponse(requestCode: Int, mResponse: Response<List<News>>) {
                    setCityName()
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        newsList = mResponse.data as MutableList<News>
                        if (newsList.size <= 3) {
                            binding.tvShowNewAndAlert.visibility = View.INVISIBLE
                        } else {
                            binding.tvShowNewAndAlert.visibility = View.VISIBLE
                        }
                        if (newsList.size > 3) {
                            newsList = newsList.subList(0, 3)
                        }
                        newsAdapter?.setData(newsList)
                        if (checkIsRequireToCallWeatherApi()) {
                            callWeatherReportAPi()
                        } else {
                            CommonMethods.hideProgress()
                            if (binding.swipeRefreshHome.isRefreshing) {
                                binding.swipeRefreshHome.isRefreshing = false
                            }
                            showDirectMessageIfRequired()
                        }
                    } else {
                        if (checkIsRequireToCallWeatherApi()) {
                            callWeatherReportAPi()
                        } else {
                            CommonMethods.hideProgress()
                            if (binding.swipeRefreshHome.isRefreshing) {
                                binding.swipeRefreshHome.isRefreshing = false
                            }
                            showDirectMessageIfRequired()
                        }
                    }

                    if (newsList.size < 1) {
                        binding.tvShowNewAndAlert.text = mResponse.settings?.message
                        binding.tvShowNewAndAlert.visibility = View.VISIBLE
                        binding.tvShowNewAndAlert.setTextColor(resources.getColor(R.color.colorBlack))
                    }


                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    setCityName()
                    CommonMethods.hideProgress()
                    if (binding.swipeRefreshHome.isRefreshing) {
                        binding.swipeRefreshHome.isRefreshing = false
                    }
                    alert(message) { okButton { } }.show()

                }


            })
    }

    private fun calleventListApi() {
        var inputParams = HashMap<String, String>()
        inputParams.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        inputParams.put(ApiConstants.PAGE_INDEX, "1")
        if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY) || BuildConfig.FLAVOR.equals(
                AppConstants.SARASOTA
            )
        ) {
            inputParams.put(
                ApiConstants.LAT,
                CommonMethods.getDataFromSharePreference(activity!!, AppConstants.LATITUDE)
            )
            inputParams.put(
                ApiConstants.LON,
                CommonMethods.getDataFromSharePreference(activity!!, AppConstants.LONGITUDE)
            )
            inputParams.put(ApiConstants.SORT_BY, AppConstants.DISTANCE_ASC)
        }
        var call = ApiClient.getRetrofitInterface()
            ?.getCommunityEvents(inputParams)
        WSClient<Response<List<CommunityEvents>>>().request(
            activity!!,
            100,
            false,
            call,
            object : ISuccessHandler<Response<List<CommunityEvents>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<CommunityEvents>>
                ) {
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null) {
                        cummunityEventList = (mResponse.data as MutableList<CommunityEvents>?)!!
                        if (cummunityEventList.isNotEmpty()) {
                            binding.tvShowAllCommunity.visibility = View.VISIBLE
                            binding.btnListYourEventHomeScreen.visibility = View.GONE
                        }
                        if (cummunityEventList.size > 3) {
                            cummunityEventList = cummunityEventList.subList(0, 3)
                        }
                        communityEventsAdapter?.setData(cummunityEventList)
                        callNewsListAPi()

                    } else {
                        callNewsListAPi()
                    }
                    if (cummunityEventList.size < 1) {
                        binding.btnListYourEventHomeScreen.visibility = View.VISIBLE
                        binding.tvShowAllCommunity.text = mResponse.settings?.message
                        binding.tvShowAllCommunity.visibility = View.VISIBLE
                        binding.tvShowAllCommunity.setTextColor(resources.getColor(R.color.colorBlack))
                    }
                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    callNewsListAPi()

                }

            })


    }

    private fun clearText() {

        binding.ivSearch.setOnClickListener {
            if (binding.etSearch.text!!.isNotEmpty()) {
                CommonMethods.hideKeypad(activity!!, binding.etSearch)
                binding.etSearch.setText("")
                binding.etSearch.clearFocus()
            } else {

            }
        }
    }

    private fun callCaterotyApi(loaderRequire: Boolean) {
        if (loaderRequire) {
            CommonMethods.showProgress(activity!!)
        }

        var call = ApiClient.getRetrofitInterface()?.getCategory(CommonMethods.cummunity_id)

        WSClient<Response<List<Category>>>().request(
            activity!!,
            100,
            false,
            call,
            object : ISuccessHandler<Response<List<Category>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<Category>>
                ) {
                    if (mResponse.settings?.success.equals("1")) {
                        if (mResponse.data != null) {
                            categoryList.clear()
                            addShopLocalAndMyFav()
                            categoryList.addAll(mResponse.data!!)
                            categotyAdapter?.setData(categoryList)
                            categoryListWithBitmap.clear()
                            dummyCategotyAdapter?.setData(categoryList)
                            calleventListApi()

                        }
                    } else {
                        categoryList.clear()
                        addShopLocalAndMyFav()
                        categotyAdapter?.setData(categoryList)
                        calleventListApi()
                    }
                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    calleventListApi()

                }

            })
    }


    fun updateTemperature() {
        if (CommonMethods.getFloatValve(activity!!, AppConstants.CURRENT_TEMPERATURE) != 0.0F) {

            binding.toolBarHome.tvTemparature.text =
                "${CommonMethods.getFloatValve(activity!!, AppConstants.CURRENT_TEMPERATURE)
                    .roundToInt()}${getString(R.string.degree)}"



            Glide.with(activity!!).load(
                "${AppConstants.WETHER_ICON_URL}${CommonMethods.getDataFromSharePreference(
                    activity!!,
                    AppConstants.CURRENT_WEATHER_ICON_ID
                )}@2x.png"
            )
                .into(binding.toolBarHome.ivCloud)

        }


    }


    fun hideAndShowSearchBar(isSearchShowing: Boolean) {


        if (!isSearchShowing) {
            binding.rlSearchBar.visibility = View.GONE
            binding.etSearch.setText("")

        } else {
            binding.scrollVwHome.smoothScrollTo(0, 0)
            binding.rlSearchBar.visibility = View.VISIBLE


        }

    }

    fun hideSearchBar() {

        binding.rlSearchBar.visibility = View.GONE
    }

    private fun setNewsAndAlert() {
        newsAdapter = object :
            GenericAdapter<News, ItemNewsAlertBinding>(newsList) {
            override fun onBindData(
                model: News?,
                position: Int,
                dataBinding: ItemNewsAlertBinding?
            ) {
                val cal = Calendar.getInstance()
                dataBinding?.tvTime?.text = DateUtils.getRelativeTimeSpanString(
                    CommonMethods.getDateInMillis(model?.addedDate!!),
                    cal.timeInMillis,
                    DateUtils.MINUTE_IN_MILLIS
                )
                dataBinding?.tvNewTitle?.text = model?.title
                Glide.with(activity!!).load(model?.image).into(dataBinding?.ivNews!!)
                if (position == 0) {
                    dataBinding?.ivTopLine.visibility = View.GONE
                } else {
                    dataBinding?.ivTopLine.visibility = View.VISIBLE
                }
                dataBinding?.root.setOnClickListener {
                    val intent = Intent(activity!!, NewsAndAlertDetailsActivity::class.java)
                    intent.putExtra("news", model)
                    startActivity(intent)
                }
            }

            override fun getLayoutId(): Int {
                return R.layout.item_news_alert
            }


        }

        binding.rvNewsAndAlert.adapter = newsAdapter

    }

    private fun setCummunity() {
        communityEventsAdapter = object :
            GenericAdapter<CommunityEvents, ItemCummunityEventsBinding>(cummunityEventList) {
            override fun getLayoutId(): Int {
                return R.layout.item_cummunity_events
            }

            override fun onBindData(
                model: CommunityEvents?,
                position: Int,
                dataBinding: ItemCummunityEventsBinding?
            ) {
                dataBinding?.tvEventTitle?.text = model?.eventTitle
                dataBinding?.eventAddress?.text = model?.eventLocation
                dataBinding?.tvEventName?.text = model?.category
                dataBinding?.tvEventDate?.text =
                    "${TimeUtils.convertDateWithTimeForShowingApp(model?.event_start_date)} - ${TimeUtils.convertDateWithTimeForShowingApp(
                        model?.event_end_date
                    )}"
                Glide.with(activity!!).load(model?.eventImage)
                    .into(dataBinding?.ivCommunityEvent!!)
                if (model?.category!!.isEmpty()) {
                    dataBinding?.tvEventName.visibility = View.GONE
                } else {
                    dataBinding?.tvEventName.visibility = View.VISIBLE
                }

                if (model?.eventLocation.isEmpty()) {
                    dataBinding?.eventAddress.visibility = View.GONE
                } else {
                    dataBinding?.eventAddress.visibility = View.VISIBLE
                }

                dataBinding?.root.setOnClickListener {


                    val intent = Intent(activity!!, CommunityEventDetailsActivity::class.java)
                    intent.putExtra(AppConstants.IS_NEED_TO_CALL_API, false)
                    intent.putExtra("details", model)
                    startActivity(intent)


                }


            }

        }

        binding.rvCommunityEvent.adapter = communityEventsAdapter


    }

    private fun setCategories() {
        val linearLayoutManager =
            LinearLayoutManager(activity!!, RecyclerView.HORIZONTAL, false)
        binding.rvCategories.layoutManager = linearLayoutManager
        categotyAdapter = object : GenericAdapter<Category, ItemCategoryBinding>(categoryList) {
            override fun getLayoutId(): Int {
                return R.layout.item_category
            }

            override fun onBindData(
                model: Category?,
                position: Int,
                dataBinding: ItemCategoryBinding?
            ) {

                if (model?.isLocal!!) {
                    Glide.with(activity!!).load(model?.localImage)
                        .into(dataBinding?.ivCategoryIcon!!)

                } else {
                    Glide.with(activity!!).load(model?.icon)
                        .into(dataBinding?.ivCategoryIcon!!)

                }

                dataBinding?.tvCategoryName?.text = model?.category
                dataBinding?.root!!.setOnClickListener {
                    if (model.category.equals(
                            "My Fav",
                            true
                        ) && CommonMethods.getUserId(activity!!)!!.isEmpty()
                    ) {
                        startActivity(Intent(activity!!, LoginActivity::class.java))
                    } else {


                        if (model.category.equals(getString(R.string.my_fav))) {
                            val intent = Intent(activity, FavouriteListActivity::class.java)
                            intent.putExtra(
                                AppConstants.SHARE_KEY,
                                etSearch?.text?.trim().toString()
                            )
                            startActivity(intent)
                        } else {
                            val intent = Intent(activity, PromationsListActivity::class.java)
                            intent.putExtra("category", model)
                            intent.putExtra(
                                AppConstants.SHARE_KEY, etSearch?.text?.trim().toString()
                            )
                            startActivity(intent)
                        }

                    }


                }
            }

        }
        binding.rvCategories.adapter = categotyAdapter


    }


    private fun callWeatherReportAPi() {


        val inputParams = HashMap<String, String>()
        if ((BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY) || BuildConfig.FLAVOR.equals(
                AppConstants.SARASOTA
            )) && CommonMethods.getDataFromSharePreference(activity!!, AppConstants.LATITUDE)
                .isNotEmpty() && CommonMethods.getDataFromSharePreference(
                activity!!,
                AppConstants.LONGITUDE
            ).isNotEmpty()
        ) {
            inputParams.put(
                ApiConstants.LAT,
                CommonMethods.getDataFromSharePreference(activity!!, AppConstants.LATITUDE)
            )
            inputParams.put(
                ApiConstants.LON,
                CommonMethods.getDataFromSharePreference(activity!!, AppConstants.LONGITUDE)
            )
        } else {
            inputParams.put(ApiConstants.LAT, CommonMethods.lat)
            inputParams.put(ApiConstants.LON, CommonMethods.lon)
        }


        inputParams.put(ApiConstants.APPID, CommonMethods.weather_api_id)
        inputParams.put(ApiConstants.UNITS, AppConstants.IMPERIAL)


        val call = ApiClient.getRetrofitInterface()?.getWetherReport(inputParams)


        WSClient<Temperature>().request(
            activity!!,
            100,
            false,
            call,
            object : ISuccessHandler<Temperature> {
                override fun successResponse(requestCode: Int, mResponse: Temperature) {

                    CommonMethods.hideProgress()
                    if (binding.swipeRefreshHome.isRefreshing) {
                        binding.swipeRefreshHome.isRefreshing = false
                    }
                    val gson = Gson()
                    val json = gson.toJson(mResponse)
                    CommonMethods.setDataIntoSharePreference(
                        activity!!,
                        AppConstants.WEATHER_DATA,
                        json
                    )
                    CommonMethods.setDataIntoSharePreference(
                        activity!!,
                        AppConstants.LAST_API_CALL_TIME,
                        System.currentTimeMillis()
                    )
                    CommonMethods.setFloatValve(
                        activity!!,
                        AppConstants.CURRENT_TEMPERATURE,
                        mResponse.current.temp
                    )
                    CommonMethods.setDataIntoSharePreference(
                        activity!!,
                        AppConstants.CURRENT_WEATHER_ICON_ID,
                        mResponse.current.weather?.get(0)?.icon
                    )


                    if (CommonMethods.getFloatValve(
                            activity!!,
                            AppConstants.CURRENT_TEMPERATURE
                        ) != 0.0F
                    ) {

                        binding.toolBarHome.tvTemparature.text = "${CommonMethods.getFloatValve(
                            activity!!,
                            AppConstants.CURRENT_TEMPERATURE
                        ).roundToInt()}${getString(R.string.degree)}"
                        Glide.with(activity!!).load(
                            "${AppConstants.WETHER_ICON_URL}${CommonMethods.getDataFromSharePreference(
                                activity!!,
                                AppConstants.CURRENT_WEATHER_ICON_ID
                            )}@2x.png"
                        )
                            .into(binding.toolBarHome.ivCloud)

                    }
                    showDirectMessageIfRequired()
                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    CommonMethods.hideProgress()
                    if (binding.swipeRefreshHome.isRefreshing) {
                        binding.swipeRefreshHome.isRefreshing = false
                    }
                    alert(message) { okButton { } }.show()
                }
            })


    }

    fun addShopLocalAndMyFav() {
        val categoryLocal = Category()
        categoryLocal.isLocal = true
        categoryLocal.category = getString(R.string.local_shop)
        categoryLocal.localImage = R.drawable.ic_shop_local

        categoryList.add(categoryLocal)
        val categoryMyFav = Category()

        categoryMyFav.isLocal = true
        categoryMyFav.category = getString(R.string.my_fav)
        categoryMyFav.localImage = R.drawable.ic_fav
        categoryList.add(categoryMyFav)
    }


    fun saveShareDateInSharePrefrence(noOfDays: Int) {
        val timeInMillSecondsAfter =
            System.currentTimeMillis() + AppConstants.ONE_DAY_IN_MILLI_SECONDS * noOfDays
        CommonMethods.setDataIntoSharePreference(
            activity!!,
            AppConstants.DATE_IN_MILLI_SEC,
            timeInMillSecondsAfter
        )
    }

    fun setCityName() {
        if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY) || BuildConfig.FLAVOR.equals(
                AppConstants.SARASOTA
            )
        ) {
            if (CommonMethods.getDataFromSharePreference(activity!!, AppConstants.USER_CITY_NAME)
                    .isNotEmpty()
            ) {
                binding.toolBarHome.tvTitle.text = CommonMethods.getCityName(activity!!)
            } else {
                binding.toolBarHome.tvTitle.text = getString(R.string.app_name_full_name)
            }

        }
    }

    fun setCityNameEmpty() {
        if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY) || BuildConfig.FLAVOR.equals(
                AppConstants.SARASOTA
            )
        ) {
            if (CommonMethods.getDataFromSharePreference(
                    activity!!,
                    AppConstants.USER_CITY_NAME
                ).isEmpty()
            ) {
                binding.toolBarHome.tvTitle.text = ""
            }
        }


    }

    override fun onResume() {
        super.onResume()
        if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY) || BuildConfig.FLAVOR.equals(
                AppConstants.SARASOTA
            )
        ) {
            updateTemperature()
            if (!binding.toolBarHome.tvTitle.text.equals(
                    CommonMethods.getDataFromSharePreference(
                        activity!!,
                        AppConstants.USER_CITY_NAME
                    )
                ) && CommonMethods.getDataFromSharePreference(
                    activity!!,
                    AppConstants.USER_CITY_NAME
                ).isNotEmpty()
            ) {
                CommonMethods.showProgress(activity!!)
                calleventListApi()

                setCityName()
            }
        }


    }

    fun showDirectMessageIfRequired() {
        if (CommonMethods.getDataFromSharePreference(activity!!, AppConstants.DIRECT_MESSAGE)
                .isNotEmpty()
        ) {
            val directMessage =
                CommonMethods.getDataFromSharePreference(activity!!, AppConstants.DIRECT_MESSAGE)
            val imageUrl =
                CommonMethods.getDataFromSharePreference(activity!!, AppConstants.IMAGE_URL)

            if (imageUrl.isNotEmpty() && directMessage.isNotEmpty()) {
                val dialogDirectMessage = DialogDirectMessage(activity!!, imageUrl, directMessage)
                dialogDirectMessage.showDirectMessageAlert()
            } else {
                if (directMessage.isNotEmpty()) {
                    alert(directMessage) {
                        okButton { }
                    }.show()
                }
            }
            CommonMethods.setDataIntoSharePreference(activity!!, AppConstants.DIRECT_MESSAGE, "")
            CommonMethods.setDataIntoSharePreference(activity!!, AppConstants.IMAGE_URL, "")
        }
    }


    fun checkIsRequireToCallWeatherApi(): Boolean {
        return CommonMethods.getLongKeyDataFromSharePreference(
            activity!!,
            AppConstants.LAST_API_CALL_TIME
        ) == 0L || CommonMethods.getLongKeyDataFromSharePreference(
            activity!!,
            AppConstants.LAST_API_CALL_TIME
        )?.plus(AppConstants.TEN_MINUTES)!! < System.currentTimeMillis()
    }


}