package com.lansingareashoplocal.ui.home

import IFailureHandler
import ISuccessHandler
import Response
import WSClient
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.provider.Settings
import android.text.*
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.api_call_with_ui.TrackPromotion
import com.lansingareashoplocal.core.BaseFragment
import com.lansingareashoplocal.databinding.FragmentSearchBinding
import com.lansingareashoplocal.databinding.ItemSuggestionBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.ui.communityevents.CommunityEventDetailsActivity
import com.lansingareashoplocal.ui.direct_message.DialogDirectMessage
import com.lansingareashoplocal.ui.model.*
import com.lansingareashoplocal.ui.newsandalert.NewsAndAlertDetailsActivity
import com.lansingareashoplocal.ui.promotions.NonFeatureListActivity
import com.lansingareashoplocal.ui.promotions.PromotionDetailsActivity
import com.lansingareashoplocal.ui.tempareture.TemperatureActivity
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.toolbar_with_weather_details.view.*
import org.jetbrains.anko.okButton
import org.jetbrains.anko.support.v4.alert
import kotlin.math.roundToInt

class SearchFragment : BaseFragment<FragmentSearchBinding>() {
    private var hasNext = false
    private var pageCount = 20
    var currentPage = 1
    private var cntr: CountDownTimer? = null
    private val waitingTime = 300
    var museosan_500: Typeface? = null

    var suggestionAdapter: GenericAdapter<GlobalSearch, ItemSuggestionBinding>? = null
    var suggestionsList = mutableListOf<GlobalSearch>()

    override fun onBackPressed(): Boolean {
        return false
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_search
    }

    override fun init() {
        setAdapter()
        clearText()
        ontextTyping()
        searchUseSearchOption()
        setPagination()
        binding.searchToolbar.tvTitle.visibility=View.VISIBLE
        binding.searchToolbar.tvTitle.text=getString(R.string.search)
        museosan_500 = Typeface.createFromAsset(activity!!.assets, "museosans_500.otf")
        // binding.searchToolbar.ivLeft.setImageDrawable(ContextCompat.getDrawable(activity!!,R.drawable.back_icon))
         binding.searchToolbar.tvLeft.setOnClickListener {
             (activity as HomeActivity).setHomebBar()
         }
        binding.searchToolbar.rlTemperature.setOnClickListener {
            startActivity(Intent(activity!!,TemperatureActivity::class.java))
        }
    }

    private fun setPagination() {
        val layoutManager = LinearLayoutManager(activity!!)
        binding.rvGlobalSuggestions.layoutManager = layoutManager
        binding.rvGlobalSuggestions.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager != null && layoutManager.findLastCompletelyVisibleItemPosition() == layoutManager.itemCount - 1) {
                    if (hasNext) {
                        hasNext = false
                        binding.rlGlobalSearchProgressLayout?.visibility = View.VISIBLE
                        currentPage += 1
                        callSuggestionApi(false)
                    }
                }
            }
        })

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    override fun onResume() {
        super.onResume()
        updateTemperature()

    }




    private fun ontextTyping() {

        binding.etSuggestion.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                currentPage=1
                hasNext = false
                if (s != null) {
                    if (s.isNotEmpty()) {
                        Glide.with(this@SearchFragment).load(R.drawable.cancel)
                            .into(binding.ivSuggestionSearch)
                    } else {
                        Glide.with(this@SearchFragment).load(R.drawable.ic_search)
                            .into(binding.ivSuggestionSearch)
                    }

                    if (cntr != null) {
                        cntr?.cancel()
                    }
                    cntr = object : CountDownTimer(waitingTime.toLong(), 500) {
                        override fun onTick(millisUntilFinished: Long) {
                            Log.d("TIME", "seconds remaining: " + millisUntilFinished / 1000)
                        }

                        override fun onFinish() {
                            Log.d("FINISHED", binding.etSuggestion.getText().toString())
                            if (binding.etSuggestion.length() > 2) {
                                callSuggestionApi(true)
                            }

                        }
                    }
                    cntr?.start()
                    if (binding.etSuggestion.length() < 3) {
                        suggestionsList.clear()
                        binding.tvNoSuggestion.visibility = View.GONE
                        suggestionAdapter?.setData(suggestionsList)
                    } else {

                    }


                }
            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.CUPCAKE)
    private fun searchUseSearchOption() {
        binding.etSuggestion.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (v?.text != null) {
                        if (etSuggestion.text?.length!! > 2) {
                            callSuggestionApi(true)
                        }

                    }

                    return true
                }
                return false
            }
        })
    }


    private fun clearText() {

        binding.ivSuggestionSearch.setOnClickListener {
            if (binding.etSuggestion.text!!.isNotEmpty()) {
                binding.etSuggestion.setText("")
            } else {

            }
        }
    }


    private fun setAdapter() {

        suggestionAdapter =
            object : GenericAdapter<GlobalSearch, ItemSuggestionBinding>(suggestionsList) {
                override fun getLayoutId(): Int {
                    return R.layout.item_suggestion
                }

                @SuppressLint("DefaultLocale")
                override fun onBindData(
                    model: GlobalSearch?,
                    position: Int,
                    dataBinding: ItemSuggestionBinding?
                ) {
                    dataBinding?.tvSuggestion?.text = model?.title

                    /*       val highlightedKey = etSuggestion?.text.toString().toLowerCase()
                           val startingIndex = model?.title?.toLowerCase()?.indexOf(highlightedKey)
                           val endingIndex = highlightedKey.length?.plus(startingIndex!!)
                           val mSpannableStringBuilder = SpannableStringBuilder(model?.title)
                           if (startingIndex != null && startingIndex >= 0) {
                               mSpannableStringBuilder.setSpan(
                                   ForegroundColorSpan(ContextCompat.getColor(activity!!, R.color.black)),
                                   startingIndex, endingIndex, Spannable.SPAN_INCLUSIVE_INCLUSIVE
                               )
                               mSpannableStringBuilder.setSpan(
                                   CustomTypefaceSpan("", museosan_500),
                                   startingIndex,
                                   endingIndex,
                                   Spanned.SPAN_EXCLUSIVE_INCLUSIVE
                               )
                           }
                           dataBinding?.tvSuggestion?.movementMethod = LinkMovementMethod.getInstance()
                           dataBinding?.tvSuggestion?.text = mSpannableStringBuilder*/
                    if (model?.entityType.equals(AppConstants.ALERT)) {
                        Glide.with(activity!!).load(R.drawable.ic_newsalerts)
                            .into(dataBinding?.ivSuggestionType!!)
                    } else if (model?.entityType.equals(AppConstants.EVENT)) {
                        Glide.with(activity!!).load(R.drawable.ic_events)
                            .into(dataBinding?.ivSuggestionType!!)
                    } else if (model?.entityType.equals(AppConstants.PUSH)) {
                        Glide.with(activity!!).load(R.drawable.ic_directnotification)
                            .into(dataBinding?.ivSuggestionType!!)
                    } else if (model?.entityType.equals(AppConstants.BUSINESS)) {
                        Glide.with(activity!!).load(R.drawable.ic_business)
                            .into(dataBinding?.ivSuggestionType!!)
                    }
                    else if (model?.entityType.equals(AppConstants.OWNER)) {
                        Glide.with(activity!!).load(R.drawable.ic_owner)
                            .into(dataBinding?.ivSuggestionType!!)
                    }
                    else if (model?.entityType.equals(AppConstants.PROMOTION)) {
                        Glide.with(activity!!).load(R.drawable.ic_promotion)
                            .into(dataBinding?.ivSuggestionType!!)
                    }



                    dataBinding?.tvSuggestion?.setOnClickListener {
                        if (model?.entityType.equals(AppConstants.ALERT)) {
                            if (model?.communitiyNotificationsId!!.isNotEmpty()) {
                                val intentNewAndAlert = Intent(activity!!, NewsAndAlertDetailsActivity::class.java)
                                intentNewAndAlert.putExtra(AppConstants.NOTIFICATION_ID,model?.communitiyNotificationsId)
                                startActivity(intentNewAndAlert)

                            }


                        } else if (model?.entityType.equals(AppConstants.EVENT)) {
                            if (model?.eventId!!.isNotEmpty()) {
                                val intent =
                                    Intent(activity!!, CommunityEventDetailsActivity::class.java)
                                intent.putExtra(AppConstants.EVENT_ID, model?.eventId)
                                intent.putExtra(AppConstants.IS_NEED_TO_CALL_API, true)
                                startActivity(intent)
                                CommonMethods.hideKeypad(activity!!, dataBinding?.tvSuggestion)
                            }


                        } else if (model?.entityType.equals(AppConstants.PUSH)) {
                            CommonMethods.hideKeypad(activity!!, dataBinding?.tvSuggestion)

                            if(model?.icon?.isNotEmpty()!! && model?.title!!.isNotEmpty()){
                                val dialogDirectMessage= DialogDirectMessage(activity!!,model.icon,model.title)
                                dialogDirectMessage.showDirectMessageAlert()
                            }
                            else{
                                if (model?.title!!.isNotEmpty()) {
                                    alert(model.title) {
                                        okButton { }
                                    }.show()
                                }
                            }




                        } else if (model?.entityType.equals(AppConstants.BUSINESS)|| model?.entityType.equals(AppConstants.OWNER)|| model?.entityType.equals(AppConstants.PROMOTION)) {
                            if (model?.businessPlaceId!!.isNotEmpty()) {
                                CommonMethods.hideKeypad(activity!!, dataBinding?.tvSuggestion)
                                callBusinessDetailsApi(model.businessPlaceId)
                            }
                        }
                    }


                }

            }
        binding.rvGlobalSuggestions.adapter = suggestionAdapter


    }

    private fun callSuggestionApi(isRequireToShowLoader: Boolean) {
        val inputParam = HashMap<String, String>()
        inputParam.put(ApiConstants.PAGE_INDEX, "" + currentPage)
        inputParam.put(ApiConstants.KEYWORD, etSuggestion.text.toString().trim())
        inputParam.put(ApiConstants.COMMUNITY_ID, CommonMethods.cummunity_id)
        val cal = ApiClient.getRetrofitInterface()?.getGlobalSearchResult(inputParam)

        WSClient<Response<List<GlobalSearch>>>().request(
            activity!!,
            100,
            isRequireToShowLoader,
            cal,
            object : ISuccessHandler<Response<List<GlobalSearch>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<GlobalSearch>>
                ) {
                    binding.rlGlobalSearchProgressLayout?.visibility = View.GONE
                    if (mResponse.settings?.success.equals("1")) {
                        binding.tvNoSuggestion.visibility = View.INVISIBLE
                        if(binding.etSuggestion.length()>2){
                            hasNext = mResponse.settings?.nextPage == 1
                            pageCount = mResponse.settings?.perPage!!
                            if (currentPage == 1) {
                                suggestionsList = mResponse.data as MutableList<GlobalSearch>
                                suggestionAdapter?.setData(suggestionsList)
                            } else {
                                suggestionAdapter?.addItems(mResponse.data as MutableList<GlobalSearch>)
                            }
                        }

                    } else {
                      // binding.rlGlobalSearchProgressLayout?.visibility = View.GONE
                        suggestionsList.clear()
                        suggestionAdapter?.setData(suggestionsList)
                        binding.tvNoSuggestion.visibility = View.VISIBLE
                        binding.tvNoSuggestion.text = mResponse.settings?.message
                    }


                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    binding.rlGlobalSearchProgressLayout?.visibility = View.GONE

                    alert(message) {
                        okButton { }
                    }.show()


                }

            })

    }

    fun updateTemperature() {
        if (CommonMethods.getFloatValve(
                activity!!,
                AppConstants.CURRENT_TEMPERATURE
            ) != 0.0F
        ) {
            binding.searchToolbar.tvTemparature.text =  "${CommonMethods.getFloatValve(activity!!, AppConstants.CURRENT_TEMPERATURE).roundToInt()}${getString(R.string.degree)}"

            Glide.with(activity!!).load(
                "${AppConstants.WETHER_ICON_URL}${CommonMethods.getDataFromSharePreference(
                    activity!!,
                    AppConstants.CURRENT_WEATHER_ICON_ID
                )}@2x.png"
            )
                .into(binding.searchToolbar.ivCloud)

        }
    }


    private fun callBusinessDetailsApi(businessId: String) {

        val inputparam = HashMap<String, String>()
        inputparam.put(ApiConstants.USER_ID, CommonMethods.getUserId(activity!!)!!)
        inputparam.put(ApiConstants.BUSINESS_PLACE_ID, businessId)
        inputparam.put(ApiConstants.COMMUNITY_ID,CommonMethods.cummunity_id)
        inputparam.put(
            ApiConstants.DEVICE_UDID,
            Settings.Secure.getString(activity!!.getContentResolver(), Settings.Secure.ANDROID_ID)
        )
        val call = ApiClient.getRetrofitInterface()?.getBusinessDetails(inputparam)

        WSClient<Response<List<BusinessDetails>>>().request(activity!!, 100, true, call,
            object : ISuccessHandler<Response<List<BusinessDetails>>> {
                override fun successResponse(
                    requestCode: Int,
                    mResponse: Response<List<BusinessDetails>>
                ) {
                    if (mResponse.settings?.success.equals("1") && mResponse.data != null && mResponse.data!!.isNotEmpty()) {
                        val businessDetails = mResponse.data!![0]
                        if (businessDetails.is_featured.equals("1")) {
                            if(businessDetails.getPromotionList.isNotEmpty()){
                                val trackPromotion= TrackPromotion(businessDetails.businessPlaceId,activity!!,businessDetails.getPromotionList[0].promotionId,"Search")
                                trackPromotion.callPromotionTrackApi()
                            }
                            val intent = Intent(activity!!, PromotionDetailsActivity::class.java)
                            intent.putExtra(AppConstants.BUSINESS_ID, businessId)
                            intent.putExtra(AppConstants.BUSINESS_DETAILS_MODEL, businessDetails)
                            intent.putExtra(AppConstants.IS_FROM_SEARCH, true)
                            startActivity(intent)
                        } else {
                            val trackPromotion= TrackPromotion(businessDetails.businessPlaceId,activity!!,"0","Search")
                            trackPromotion.callPromotionTrackApi()
                            val nonFeature = NonFeature()
                            nonFeature.address = businessDetails.address
                            nonFeature.phone = businessDetails.phone
                            nonFeature.placeName = businessDetails.placeName
                            nonFeature.latitude=businessDetails.latitude
                            nonFeature.longitude=businessDetails.longitude
                            val intent = Intent(activity!!, NonFeatureListActivity::class.java)
                            intent.putExtra(AppConstants.SHARE_KEY, nonFeature)
                            startActivity(intent)


                        }

                    }
                }

            }, object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {


                    alert(message) {

                        okButton { }

                    }.show()

                }

            })
    }


}