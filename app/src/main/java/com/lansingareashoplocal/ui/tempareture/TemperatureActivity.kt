package com.lansingareashoplocal.ui.tempareture

import IFailureHandler
import ISuccessHandler
import WSClient
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.ItemTemperatureBinding
import com.lansingareashoplocal.ui.adapter.GenericAdapter
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.ui.model.Temperature
import kotlinx.android.synthetic.main.activity_temperature.*
import kotlinx.android.synthetic.main.tool_bar_with_out_bg.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import kotlin.math.roundToInt


class TemperatureActivity : BaseActivity() {


    var weeklyTempList: List<Temperature.Daily> = listOf()
    var weatherData: Temperature? = null
    var weeklyTemperatueAdapter: GenericAdapter<Temperature.Daily, ItemTemperatureBinding>? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_temperature)
        if (CommonMethods.getLongKeyDataFromSharePreference(this@TemperatureActivity, AppConstants.LAST_API_CALL_TIME) == 0L || CommonMethods.getLongKeyDataFromSharePreference(this@TemperatureActivity, AppConstants.LAST_API_CALL_TIME)?.plus(AppConstants.TEN_MINUTES)!! < System.currentTimeMillis()
        ) {
            callWeatherReportAPi()

        } else {
            weatherData = CommonMethods.getWeatherData(this@TemperatureActivity)
            if (weatherData != null) {
                weeklyTempList = weatherData!!.daily
                setdata()
                setCurrentWether()
            }

        }


        tvLeft.setOnClickListener {
            finish()
        }
        tvLansingArea.text = CommonMethods.getCityName(this@TemperatureActivity)

    }

    private fun setCurrentWether() {
        tvCurrentTime.text = weatherData?.current?.sunrise?.let {
            CommonMethods.getTimeFromUnixTime(
                it
            )
        }
        tvFeelLike.text = weatherData?.current?.feelsLike


        val pressureInInHg = weatherData?.current?.pressure?.times(0.02953)
        tvPressure.text = "${"%.1f".format(pressureInInHg)} inhg"
        tvHumidity.text = "${weatherData?.current?.humidity} %"
        tvWind.text = "${"%.1f".format(weatherData?.current?.windSpeed)} mph"
        val visibilityinKm = weatherData?.current?.visibility?.times(0.00062137)
        tvVisibility.text = "${"%.1f".format(visibilityinKm)} mi"
        tvCurrentTemparatureNmae.text = weatherData?.current?.weather?.get(0)?.main
        try {
            tvCurrentTemperatureValve.text = weatherData?.current?.temp?.toInt().toString()
        }
        catch (e:Exception){
            tvCurrentTemperatureValve.text = weatherData?.current?.temp?.toString()
        }
        Glide.with(this@TemperatureActivity).load(getWeatherIcon()).into(ivCurrentWeather)
        if(getWeatherIcon()==R.drawable.thunderstorm || getWeatherIcon()== R.drawable.drizzle){
            tvCurrentTemparatureNmae.setTextColor(ContextCompat.getColor(this@TemperatureActivity,R.color.colorWhite))
            tvCurrentTemperatureValve.setTextColor(ContextCompat.getColor(this@TemperatureActivity,R.color.colorWhite))
            tvTemparatureUnits.setTextColor(ContextCompat.getColor(this@TemperatureActivity,R.color.colorWhite))
        }


    }

    private fun callWeatherReportAPi() {



        val inputParams=HashMap<String,String>()
        if ((BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY)  || BuildConfig.FLAVOR.equals(AppConstants.SARASOTA))&& CommonMethods.getDataFromSharePreference(this@TemperatureActivity,AppConstants.LATITUDE).isNotEmpty() && CommonMethods.getDataFromSharePreference(this@TemperatureActivity,AppConstants.LONGITUDE).isNotEmpty()){
            inputParams.put(ApiConstants.LAT,CommonMethods.getDataFromSharePreference(this@TemperatureActivity,AppConstants.LATITUDE))
            inputParams.put(ApiConstants.LON,CommonMethods.getDataFromSharePreference(this@TemperatureActivity,AppConstants.LONGITUDE))
        }
        else{
            inputParams.put(ApiConstants.LAT,CommonMethods.lat)
            inputParams.put(ApiConstants.LON,CommonMethods.lon)
        }
        inputParams.put(ApiConstants.APPID,CommonMethods.weather_api_id)
        inputParams.put(ApiConstants.UNITS,AppConstants.IMPERIAL)

        val call = ApiClient.getRetrofitInterface()?.getWetherReport(inputParams)


        WSClient<Temperature>().request(
            this@TemperatureActivity,
            100,
            true,
            call,
            object : ISuccessHandler<Temperature> {
                override fun successResponse(requestCode: Int, mResponse: Temperature) {
                    AppConstants.WEATHER_API_LAST_CALL_TIME = System.currentTimeMillis()
                    weatherData = mResponse
                    if (weatherData != null) {
                        weeklyTempList = weatherData!!.daily
                    }
                    setdata()
                    setCurrentWether()

                    val gson = Gson()
                    val json = gson.toJson(mResponse)
                    CommonMethods.setDataIntoSharePreference(
                        this@TemperatureActivity,
                        AppConstants.WEATHER_DATA,
                        json
                    )
                    CommonMethods.setDataIntoSharePreference(
                        this@TemperatureActivity,
                        AppConstants.LAST_API_CALL_TIME,
                        System.currentTimeMillis()
                    )
                    CommonMethods.setFloatValve(
                        this@TemperatureActivity,
                        AppConstants.CURRENT_TEMPERATURE,
                        mResponse.current.temp
                    )
                    CommonMethods.setDataIntoSharePreference(
                        this@TemperatureActivity,
                        AppConstants.CURRENT_WEATHER_ICON_ID,
                        mResponse.current.weather?.get(0)?.icon
                    )


                }

            },
            object : IFailureHandler {
                override fun failureResponse(requestCode: Int, message: String) {
                    alert(message) {
                        okButton {  }
                    }.show()
                }
            })


    }

    private fun setdata() {
        weeklyTemperatueAdapter =
            object : GenericAdapter<Temperature.Daily, ItemTemperatureBinding>(weeklyTempList) {
                override fun getLayoutId(): Int {
                    return R.layout.item_temperature
                }

                override fun onBindData(
                    model: Temperature.Daily?,
                    position: Int,
                    dataBinding: ItemTemperatureBinding?
                ) {

                    dataBinding?.tvDay?.text = CommonMethods.getdayFromUnixTime(model?.dt!!)
                    dataBinding?.tvDate?.text = CommonMethods.getdateFromUnixTime(model?.dt!!)

                    if (model?.temp.max > 1) {
                        dataBinding?.tvHighTemp?.text = "${model?.temp.max.roundToInt()}"
                    } else {
                        dataBinding?.tvHighTemp?.text = "-${model?.temp.max.roundToInt()}"
                    }
                    if (model?.temp.min > 1) {
                        dataBinding?.tvLowTemp?.text = "${model?.temp.min.roundToInt()}"
                    } else {
                        dataBinding?.tvLowTemp?.text = "-${model?.temp.min.roundToInt()}"
                    }


                    dataBinding?.tvWetherDes?.text = model?.weather?.get(0)?.main

                    Glide.with(this@TemperatureActivity)
                        .load("http://openweathermap.org/img/wn/${model.weather?.get(0)?.icon}@2x.png")


                        .into(
                            dataBinding?.ivWeather!!
                        )
                }

            }
        rvTemperature.adapter = weeklyTemperatueAdapter


    }


    private fun getWeatherIcon(): Int {
        if (weatherData?.current?.weather?.get(0)?.main.equals("clouds", true)) {
            return R.drawable.scattered_clouds
        } else if (weatherData?.current?.weather?.get(0)?.main.equals("clearsky", true))
            return R.drawable.clearsky
        else if (weatherData?.current?.weather?.get(0)?.main.equals("mist", true))
            return R.drawable.mist
        else if (weatherData?.current?.weather?.get(0)?.main.equals("snow", true))
            return R.drawable.snow
        else if (weatherData?.current?.weather?.get(0)?.main.equals("rain", true))
            return R.drawable.rain
        else if (weatherData?.current?.weather?.get(0)?.main.equals("thunderstorm", true))
            return R.drawable.thunderstorm
        else if (weatherData?.current?.weather?.get(0)?.main.equals("drizzle", true))
            return R.drawable.drizzle
        else
            return R.drawable.scattered_clouds


    }
}