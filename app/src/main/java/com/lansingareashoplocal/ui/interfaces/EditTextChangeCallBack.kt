package com.lansingareashoplocal.ui.interfaces

import androidx.appcompat.widget.AppCompatEditText


interface EditTextChangeCallBack {

    fun afterTextChanged(editText: AppCompatEditText)
}