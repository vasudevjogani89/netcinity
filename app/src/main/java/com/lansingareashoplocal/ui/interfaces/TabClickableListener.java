package com.lansingareashoplocal.ui.interfaces;

public interface TabClickableListener {
    void clickedTabId(int index);
}
