package com.lansingareashoplocal.ui.interfaces;

public interface AsyncResponse {
    void processFinish( );
}