package com.lansingareashoplocal.ui.direct_message

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.lansingareashoplocal.R
import com.lansingareashoplocal.databinding.DialogDirectMessageBinding
import com.lansingareashoplocal.databinding.DialogNewAlertDetailsBinding
import com.lansingareashoplocal.utility.helper.TimeUtils

class DialogDirectMessage(
    var context: Context,
    var imageUrl: String? = null,
    var directMessage: String = ""

) {


    lateinit var directMessageDailog: Dialog
    lateinit var dialoDirectMessageBinding: DialogDirectMessageBinding
    var mimeType = "text/html;charset=UTF-8"
    var encoding = "utf-8"


    fun showDirectMessageAlert() {
        directMessageDailog = Dialog(context, R.style.MyAlertDialogStyle)
        dialoDirectMessageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_direct_message,
            null,
            false
        )
        directMessageDailog.setContentView(dialoDirectMessageBinding.getRoot())
        directMessageDailog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialoDirectMessageBinding.ivCancel.setOnClickListener {
            directMessageDailog.dismiss()
        }


        setData()
        directMessageDailog.show()


    }


    fun setData() {
        // dialoDirectMessageBinding.tvNewsTitle?.text = "Direct Message"
        Glide.with(context).load(imageUrl).into(dialoDirectMessageBinding?.ivDirect)
        dialoDirectMessageBinding.tvDirectMessage.text = directMessage

        /* var text =*/
        /*     "<html><head>" + "<style type=\"text/css\">@font-face {font-family: font;src: url(\"file:///android_asset/museosans_300.otf\")}body{font-family: poppins;color: #21242c;font-size: 17px}" + "</style></head>" + "<body>" + directMessage + "</body></html>";*/
        /* dialoDirectMessageBinding.tvDirectMessage.loadDataWithBaseURL(null, text, mimeType, encoding, null);*/
    }
}