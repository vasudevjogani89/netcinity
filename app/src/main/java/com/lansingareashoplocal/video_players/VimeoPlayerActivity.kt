package com.lansingareashoplocal.video_players

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.ct7ct7ct7.androidvimeoplayer.model.PlayerState
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerActivity
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import kotlinx.android.synthetic.main.activity_vimeo_player1.*

class VimeoPlayerActivity : BaseActivity() {
    var REQUEST_CODE = 1234

    var videoId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vimeo_player1)
        videoId =  intent.getIntExtra(AppConstants.VIMEO_ID,0)
        setupView()
    }


    private fun setupView() {
        if (videoId != null) {
            lifecycle.addObserver(vimeoPlayer1)
            try {
                vimeoPlayer1.initialize(true, videoId!!.toInt())
            } catch (e: Exception) {

            }
            vimeoPlayer1.setFullscreenVisibility(true)
            startActivityForResult(
                VimeoPlayerActivity.createIntent(
                    this,
                    VimeoPlayerActivity.REQUEST_ORIENTATION_AUTO,
                    vimeoPlayer1
                ), REQUEST_CODE
            )


        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}