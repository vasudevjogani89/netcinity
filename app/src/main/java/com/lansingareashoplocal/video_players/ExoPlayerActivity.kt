package com.lansingareashoplocal.video_players

import android.app.Dialog
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.*
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity


class ExoPlayerActivity : BaseActivity() {
    private val STATE_RESUME_WINDOW = "resumeWindow"
    private val STATE_RESUME_POSITION = "resumePosition"
    private val STATE_PLAYER_FULLSCREEN = "playerFullscreen"
    var Video:String?=null
    private var playerView: PlayerView? = null
    private var mVideoSource: MediaSource? = null
    private var mExoPlayerFullscreen = false
    private var mFullScreenButton: FrameLayout? = null
    private var mFullScreenIcon: ImageView? = null
    private var mFullScreenDialog: Dialog? = null
    private var dataSourceFactory: DataSource.Factory? = null
    private var player: SimpleExoPlayer? = null
    private var mResumeWindow: Int = 0
    private var mResumePosition: Long = 0
    private var videoProgress: ProgressBar? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exoplayer)
        Video= intent.getStringExtra(AppConstants.CUSTOM_URL)
        dataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, getString(R.string.app_name)))
        if (savedInstanceState != null) {
            mResumeWindow = savedInstanceState.getInt(STATE_RESUME_WINDOW)
            mResumePosition = savedInstanceState.getLong(STATE_RESUME_POSITION)
            mExoPlayerFullscreen = savedInstanceState.getBoolean(STATE_PLAYER_FULLSCREEN)
        }
        if (playerView == null) {
            playerView = findViewById(R.id.exoplayer)
            videoProgress = findViewById(R.id.progress_bar)
            initFullscreenDialog()
            initFullscreenButton()
        }
        initExoPlayer()

        if (mExoPlayerFullscreen) {
            (playerView?.getParent() as ViewGroup).removeView(playerView)
            mFullScreenDialog?.addContentView(playerView!!, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
            mFullScreenIcon?.setImageDrawable(ContextCompat.getDrawable(this@ExoPlayerActivity, R.drawable.ic_fullscreen_skrink))
            mFullScreenDialog?.show()
        }
    }


    private fun initFullscreenDialog() {

        mFullScreenDialog = object : Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            override fun onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog()
                super.onBackPressed()
            }
        }
    }


    private fun openFullscreenDialog() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        (playerView?.getParent() as ViewGroup).removeView(playerView)
        mFullScreenDialog?.addContentView(playerView!!, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        mFullScreenIcon?.setImageDrawable(ContextCompat.getDrawable(this@ExoPlayerActivity, R.drawable.ic_fullscreen_skrink))
        mExoPlayerFullscreen = true
        mFullScreenDialog?.show()

    }

    public override fun onSaveInstanceState(outState: Bundle) {

        outState.putInt(STATE_RESUME_WINDOW, mResumeWindow)
        outState.putLong(STATE_RESUME_POSITION, mResumePosition)
        outState.putBoolean(STATE_PLAYER_FULLSCREEN, mExoPlayerFullscreen)

        super.onSaveInstanceState(outState)
    }


    private fun closeFullscreenDialog() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        (playerView?.getParent() as ViewGroup).removeView(playerView)
        (findViewById<View>(R.id.main_media_frame) as FrameLayout).addView(playerView)
        mExoPlayerFullscreen = false
        mFullScreenDialog?.dismiss()
        mFullScreenIcon?.setImageDrawable(ContextCompat.getDrawable(this@ExoPlayerActivity, R.drawable.ic_fullscreen_expand))

    }


    private fun initFullscreenButton() {

        val controlView = playerView?.findViewById<PlayerControlView>(R.id.exo_controller)
        mFullScreenIcon = controlView?.findViewById(R.id.exo_fullscreen_icon)
        mFullScreenButton = controlView?.findViewById(R.id.exo_fullscreen_button)
        mFullScreenButton?.setOnClickListener(View.OnClickListener {
            if (!mExoPlayerFullscreen)
                openFullscreenDialog()
            else
                closeFullscreenDialog()
        })
        val seekbar = controlView?.findViewById<DefaultTimeBar>(R.id.exo_progress)
        seekbar?.addListener(object : TimeBar.OnScrubListener {
            override fun onScrubStart(timeBar: TimeBar, position: Long) {
                Log.d("onScrubStart", "" + position)

            }

            override fun onScrubMove(timeBar: TimeBar, position: Long) {
                Log.d("onScrubMove", "" + position)

            }

            override fun onScrubStop(timeBar: TimeBar, position: Long, canceled: Boolean) {
                Log.d("onScrubMove", "" + position)

            }
        })



    }

    private fun initExoPlayer() {

        player = ExoPlayerFactory.newSimpleInstance(this)
        playerView?.setPlayer(player)
        playerView?.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM)
        val haveResumePosition = mResumeWindow != C.INDEX_UNSET

        if (haveResumePosition) {
            Log.i("DEBUG", " haveResumePosition ")
            player?.seekTo(mResumeWindow, mResumePosition)
        }
        mVideoSource = buildMediaSource(Uri.parse(Video))
        Log.i("DEBUG", " mVideoSource $mVideoSource")
        player?.prepare(mVideoSource)
        player?.setPlayWhenReady(true)
        player?.addListener(object : Player.EventListener {
            override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {
                Log.e("Timeline", "")
            }

            override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
                Log.e("TrackGroupArray", "")
            }

            override fun onLoadingChanged(isLoading: Boolean) {
                Log.e("isLoading", "")
            }

            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    Player.STATE_ENDED -> {

                    }
                    Player.STATE_READY -> {
                        videoProgress?.setVisibility(View.INVISIBLE)
                    }
                    Player.STATE_BUFFERING -> {
                        videoProgress?.setVisibility(View.VISIBLE)
                    }
                    Player.STATE_IDLE -> {
                        Log.d("STATE_IDLE","STATE_IDLE")
                    }
                }
            }

            override fun onRepeatModeChanged(repeatMode: Int) {
                Log.e("", "")
            }

            override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {
                Log.e("", "")
            }

            override fun onPlayerError(error: ExoPlaybackException?) {
                Log.e("", "")
            }

            override fun onPositionDiscontinuity(reason: Int) {
                Log.e("", "")
            }

            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {
                Log.e("", "")
            }

            override fun onSeekProcessed() {
                Log.e("", "")
            }
        })


    }


    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        if (playerView !=null) {
            player?.setPlayWhenReady(true)


        }


    }

    override fun onPause() {
        super.onPause()
        if (playerView !=null){
            player?.setPlayWhenReady(false)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }
    private fun buildMediaSource(uri: Uri): MediaSource {
        @C.ContentType val type = Util.inferContentType(uri)
        when (type) {
            C.TYPE_DASH -> return DashMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
            C.TYPE_SS -> return SsMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
            C.TYPE_HLS -> return HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
            C.TYPE_OTHER -> return ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
            else -> throw IllegalStateException("Unsupported type: $type") as Throwable
        }
    }



}