package com.lansingareashoplocal.video_players

import android.content.pm.ActivityInfo
import android.os.Bundle
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import com.lansingareashoplocal.R
import com.lansingareashoplocal.core.BaseActivity
import com.lansingareashoplocal.utility.helper.YouTubeHelper
import com.lansingareashoplocal.utility.helper.logs.Log
import kotlinx.android.synthetic.main.activity_youtube_player.*

class YoutubePlayerActivity : YouTubeBaseActivity(), YouTubePlayer.OnInitializedListener {

    var youTubePlayer: YouTubePlayer? = null

    var isFullScreen = false
    var youtubeId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_youtube_player)
        youtubeId = intent.getStringExtra(AppConstants.YOUTUBE_ID)
        youtubeVw?.initialize(AppConstants.YOUTUBE_KEY, this)


    }

    override fun onInitializationSuccess(
        p0: YouTubePlayer.Provider?,
        p1: YouTubePlayer?,
        p2: Boolean
    ) {
        this.youTubePlayer = youTubePlayer
        //  val youtube_id = YouTubeHelper().getYoutubeId("")


        if (youtubeId != null && youtubeId?.isNotEmpty()!!) {
            youTubePlayer?.cueVideo(youtubeId)
            youTubePlayer?.setFullscreen(true)
        }
        youTubePlayer?.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT)
    }

    override fun onInitializationFailure(
        p0: YouTubePlayer.Provider?,
        p1: YouTubeInitializationResult?
    ) {

    }

    override fun onBackPressed() {
        super.onBackPressed()

    }


}