package com.lansingareashoplocal.utility

import android.app.DatePickerDialog
import android.content.Context
import android.widget.DatePicker

import com.lansingareashoplocal.R

import java.text.SimpleDateFormat
import java.time.Month
import java.time.Year
import java.util.Calendar
import java.util.Date

class MyDatePickerDialog ( var mYear: Int = 0,  var mMonth: Int = 0,   var mDay: Int = 0){



    var startDateFrom: Long = 0
    var endDateForm: Long = 0
    var datePickerDialog: DatePickerDialog? = null
   lateinit var  format : SimpleDateFormat
    // public int updateYear, updateMonth, updateDay;


    fun getDate(mContext: Context, listener: DateSelection ) {
        format = SimpleDateFormat(AppConstants.APP_DATE_FORMATE)

        val c = Calendar.getInstance()
        if (mYear == 0) {
            mYear = c.get(Calendar.YEAR)
            mMonth = c.get(Calendar.MONTH)
            mDay = c.get(Calendar.DAY_OF_MONTH)
        }
        else{

        }

        datePickerDialog = DatePickerDialog(mContext, R.style.DialogTheme,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val date = ""
                val calendar = Calendar.getInstance()
                calendar.set(year, monthOfYear, dayOfMonth)
                val format = SimpleDateFormat(AppConstants.APP_DATE_FORMATE)

                val strDate = format.format(calendar.time)
                listener.getDate(strDate)
            }, mYear, mMonth, mDay
        )
        endDateForm = Date().time
        c.add(Calendar.YEAR, -13)
        endDateForm = c.timeInMillis
        /*if (startDateFrom != 0L) {
            datePickerDialog.datePicker.minDate = startDateFrom
        }*/
        if (endDateForm != 0L) {
            datePickerDialog!!.datePicker.maxDate = endDateForm
        }
        /* if(updateYear !=0 && updateMonth != 0 && updateDay!=0){
            datePickerDialog.updateDate(updateYear,updateMonth,updateDay);
        }*/


        datePickerDialog!!.show()
    }



    fun showDatePickerWithSelectedDate(mContext: Context, listener: DateSelection, minDate:Long,selectDate:Long) {
        format = SimpleDateFormat(AppConstants.APP_DATE_FORMATE)
        val c = Calendar.getInstance()
        c.timeInMillis=selectDate
        if (selectDate != 0L) {
            mYear = c.get(Calendar.YEAR)
            mMonth = c.get(Calendar.MONTH)
            mDay = c.get(Calendar.DAY_OF_MONTH)

        }
        else{

        }

        datePickerDialog = DatePickerDialog(mContext, R.style.DialogTheme,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val date = ""
                val calendar = Calendar.getInstance()
                calendar.set(year, monthOfYear, dayOfMonth)


                val strDate = format.format(calendar.time)
                listener.getDate(strDate)
            }, mYear, mMonth, mDay
        )



        if(minDate==0L){
            datePickerDialog!!.datePicker.minDate=System.currentTimeMillis()-1000
        }
        else{
            datePickerDialog!!.datePicker.minDate=minDate
        }


        /*if (startDateFrom != 0L) {
            datePickerDialog.datePicker.minDate = startDateFrom
        }*/



        datePickerDialog!!.show()
    }




    interface DateSelection {
        fun getDate(date: String)
    }
}
