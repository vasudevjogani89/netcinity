import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.content.*
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.location.Geocoder
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.ui.model.CategoryBitMap
import com.lansingareashoplocal.ui.model.Temperature
import com.lansingareashoplocal.ui.model.UserDetails
import com.lansingareashoplocal.ui.user.LoginActivity
import com.lansingareashoplocal.utility.helper.others.GpsUtils
import com.wang.avi.AVLoadingIndicatorView
import java.io.IOException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


object CommonMethods {

    private var mProgressDialog: ProgressDialog? = null
    public var isComeFromSplashScreen = true


    @SuppressLint("ResourceAsColor")
    fun showProgress(mContext: Context) {
        if (mProgressDialog == null) {

            mProgressDialog = ProgressDialog(mContext, R.style.AppTheme)
            if (mProgressDialog?.getWindow() != null)
                mProgressDialog?.getWindow()!!
                    .setBackgroundDrawable(ColorDrawable(R.color.color_bg_progres_bar))
            mProgressDialog?.window?.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
            mProgressDialog?.setIndeterminate(true)
            mProgressDialog?.setCancelable(false)
            mProgressDialog?.setCanceledOnTouchOutside(false)
            mProgressDialog?.show()
            mProgressDialog?.setContentView(R.layout.layout_loader_progress_bar)

            mProgressDialog?.setOnDismissListener(DialogInterface.OnDismissListener {
                // Log.e("mProgressDialog3 ",mProgressDialog.toString());
                //mProgressDialog = null;
            })
        }

        if (!mProgressDialog!!.isShowing()) {
            mProgressDialog?.show()
        }

    }

    fun getSpannableHtmlText(content: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(content, Html.FROM_HTML_MODE_COMPACT);
        } else {
            Html.fromHtml(content)
        }
    }


    fun checkInternetConnection(mActivity: Context): Boolean {

        // <uses-permission android:firstName="android.permission.ACCESS_NETWORK_STATE"></uses-permission>

        val manager =
            mActivity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mNetworkInfo = manager.activeNetworkInfo
        return mNetworkInfo?.isConnectedOrConnecting ?: false

    }

    fun hideProgress() {

        if (mProgressDialog != null && mProgressDialog!!.isShowing()) {
            mProgressDialog?.dismiss()
            WSClient.requestQue?.clear()
        }
        mProgressDialog = null
    }

    fun getDeviceName(): String {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.startsWith(manufacturer)) {
            capitalize(model)
        } else capitalize(manufacturer) + " " + model
    }

    private fun capitalize(str: String): String {
        if (TextUtils.isEmpty(str)) {
            return str
        }
        val arr = str.toCharArray()
        var capitalizeNext = true

        val phrase = StringBuilder()
        for (c in arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c))
                capitalizeNext = false
                continue
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true
            }
            phrase.append(c)
        }

        return phrase.toString()
    }


    fun hideKeypad(mContext: Context, view: View) {
        val inputMethodManager =
            mContext.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun setDataIntoSharePreference(context: Context, key: String, valve: String?) {
        val sharedpreferences =
            context.getSharedPreferences(AppConstants.MyPREFERENCES, Context.MODE_PRIVATE)
        val editor = sharedpreferences.edit()
        editor.putString(key, valve)
        editor.apply()

    }

    fun setDataIntoSharePreference(context: Context, key: String, valve: Int) {
        val sharedpreferences =
            context.getSharedPreferences(AppConstants.MyPREFERENCES, Context.MODE_PRIVATE)
        val editor = sharedpreferences.edit()
        editor.putInt(key, valve)
        editor.apply()

    }

    fun setDataIntoSharePreference(context: Context, key: String, valve: Long) {
        val sharedpreferences =
            context.getSharedPreferences(AppConstants.MyPREFERENCES, Context.MODE_PRIVATE)
        val editor = sharedpreferences.edit()
        editor.putLong(key, valve)
        editor.apply()

    }

    fun getDataFromSharePreference(context: Context, key: String): String {
        val userDetails = getSharePreferenceObject(context)
        val valve = userDetails.getString(key, "")


        if (valve != null) {
            return valve
        } else return ""


    }

    fun getIntDataFromSharePreference(context: Context, key: String): Int {
        val userDetails = getSharePreferenceObject(context)
        val valve = userDetails.getInt(key, 0)
        return valve


    }


    fun getFloatValve(context: Context, key: String): Float {
        val userDetails = getSharePreferenceObject(context)

        try {
            val valve = userDetails.getFloat(key, 0.0F)
            if (valve != null) {
                return valve
            } else return 0.0F
        } catch (e: Exception) {
            return 0.0F
        }
    }


    fun setFloatValve(context: Context, key: String, valve: Float) {
        val sharedpreferences =
            context.getSharedPreferences(AppConstants.MyPREFERENCES, Context.MODE_PRIVATE)
        val editor = sharedpreferences.edit()
        editor.putFloat(key, valve)
        editor.apply()
    }

    fun getLongKeyDataFromSharePreference(context: Context, key: String): Long? {
        val userDetails = getSharePreferenceObject(context)
        val valve = userDetails.getLong(key, 0)
        return valve

    }

    fun getUserData(context: Context): UserDetails? {
        val userDetails = CommonMethods.getSharePreferenceObject(context)
        val gson = Gson()
        val json = userDetails.getString(AppConstants.USER_DATA, "")
        val userData = gson.fromJson<UserDetails>(json, UserDetails::class.java)


        if (userData != null) {
            return userData
        } else {
            return UserDetails()
        }

    }

    fun getWeatherData(context: Context): Temperature? {
        val userDetails = CommonMethods.getSharePreferenceObject(context)
        val gson = Gson()
        val json = userDetails.getString(AppConstants.WEATHER_DATA, "")
        val userData = gson.fromJson<Temperature>(json, Temperature::class.java)
        return userData
    }

    /*  fun updateUserData(context: Context, userDetails: UserDetails?) {
          val gson = Gson()
          val json = gson.toJson(userDetails)
          setDataIntoSharePreference(context, AppConstants.USER_DATA, json)
      }*/

    fun getSharePreferenceObject(context: Context): SharedPreferences {
        return context.getSharedPreferences(AppConstants.MyPREFERENCES, Context.MODE_PRIVATE)
    }

    fun logout(context: Context) {
        setUserData(context, UserDetails())
        val intent = Intent(context, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent)
    }

    fun clearPreferences(context: Context) {
        context.getSharedPreferences(AppConstants.MyPREFERENCES, Context.MODE_PRIVATE).edit()
            .clear().commit()
        //  AppDataBase.getAppDatabase(context).Dao().deleteAllNews()

    }

    fun getUserId(context: Context): String? {

        return getUserData(context)?.userId

    }


    fun hideSoftKeyboard(mActivity: Activity) {
        try {
            val focusedView = mActivity?.getCurrentFocus();
            if (focusedView != null) {
                val inputMethodManager =
                    mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(focusedView.windowToken, 0);
            }
        } catch (e: Exception) {
            e.printStackTrace();
        }
    }


    fun getMimeType(url: String): String? {
        var type: String? = null
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)

        return extension
    }


    fun showGlideImageUsingLoader(
        mContext: Context,
        imageUrl: String,
        imageView: ImageView,
        progressBar: AVLoadingIndicatorView?
    ) {
        if (progressBar != null) {
            progressBar.visibility = View.VISIBLE
        }

        Glide.with(mContext)
            .load(imageUrl)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    @Nullable e: GlideException?, model: Any,
                    target: Target<Drawable>,
                    isFirstResource: Boolean
                ): Boolean {
                    progressBar!!.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable,
                    model: Any,
                    target: Target<Drawable>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    progressBar!!.visibility = View.GONE
                    return false
                }
            })
            .into(imageView)

    }

    fun printKeyHash(context: Activity): String? {
        val packageInfo: PackageInfo
        var key: String? = null
        try {
            //getting application package firstName, as defined in manifest
            val packageName = BuildConfig.APPLICATION_ID
            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )

            Log.e("Package Name=", context.getApplicationContext().getPackageName())

            for (signature in packageInfo.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                key = String(Base64.encode(md.digest(), 0))

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key)
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("Name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("No such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("Exception", e.toString())
        }

        return key
    }

    fun isBluetoothEnabled(): Boolean {
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        return bluetoothAdapter != null && bluetoothAdapter.isEnabled
    }

    fun isLocationEnabled(context: Context): Boolean {
        val manager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val isGpsProviderEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        val isNetworkProviderEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        return isGpsProviderEnabled || isNetworkProviderEnabled
    }

    fun isValidPassword(password: String): Boolean {

        val pattern: Pattern
        val matcher: Matcher
        val PASSWORD_PATTERN = "^(?=.*\\d)(?=.*[a-zA-Z]).{4,8}\$$"
        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)

        return matcher.matches()

    }

    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    fun getOpenFacebookIntent(context: Context): Intent {

        try {
            return Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.facebook.com/appetizerandroid")

            )
            // context.getPackageManager().getPackageInfo("com.facebook.katana", 0)
            //   return Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/426253597411506"))
            // add your page id
        } catch (e: Exception) {
            return Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.facebook.com/appetizerandroid")
            )
        }

    }


    fun openFacebookApp(context: Context, name: String, facebookID: String) {
        val facebookUrl = "https://www.facebook.com/${name}"
        try {
            val versionCode = context.getApplicationContext().getPackageManager()
                .getPackageInfo("com.facebook.katana", 0).versionCode

            if (!facebookID.isEmpty()) {
                // open the Facebook app using facebookID (fb://profile/facebookID or fb://page/facebookID)
                val uri = Uri.parse("fb://page/$facebookID")
                context.startActivity(Intent(Intent.ACTION_VIEW, uri))
            } else if (versionCode >= 3002850 && !facebookUrl.isEmpty()) {
                // open Facebook app using facebook url
                val uri = Uri.parse("fb://facewebmodal/f?href=$facebookUrl")
                context.startActivity(Intent(Intent.ACTION_VIEW, uri))
            } else {
                // Facebook is not installed. Open the browser
                val uri = Uri.parse(facebookUrl)
                context.startActivity(Intent(Intent.ACTION_VIEW, uri))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            // Facebook is not installed. Open the browser
            val uri = Uri.parse(facebookUrl)
            context.startActivity(Intent(Intent.ACTION_VIEW, uri))
        }

    }

    fun twitter(context: Context, name: String) {

        var intent: Intent? = null
        try {
            // Get Twitter app
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
            intent = Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=${name}"))

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent)
        } catch (e: PackageManager.NameNotFoundException) {
            // If no Twitter app found, open on browser
            intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/${name}"))
            context.startActivity(intent)
        }
    }

    fun openInsta(context: Context, name: String) {

        val likeIng = Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/${name}"))

        likeIng.setPackage("com.instagram.android");

        try {
            context.startActivity(likeIng);
        } catch (e: ActivityNotFoundException) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/${name}")
                )
            );
        }
    }


    fun openYoutube(context: Context, name: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("https://www.youtube.com/${name}")
        intent.setPackage("com.google.android.youtube")
        context.startActivity(intent)
    }

    fun getdayFromUnixTime(unixSeconds: Long): String {
// convert seconds to milliseconds
        val date = java.util.Date(unixSeconds * 1000L);
// the format of your date
        val sdf = java.text.SimpleDateFormat("EEEE");
// give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
        var formattedDate = sdf.format(date);
        return formattedDate
    }

    fun getdateFromUnixTime(unixSeconds: Long): String {
// convert seconds to milliseconds
        val date = java.util.Date(unixSeconds * 1000L);
// the format of your date
        val sdf = java.text.SimpleDateFormat("MMM dd");
// give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
        var formattedDate = sdf.format(date);
        return formattedDate
    }

    fun getTimeFromUnixTime(unixSeconds: Long): String {
// convert seconds to milliseconds
        val date = java.util.Date(unixSeconds * 1000L);
// the format of your date
        val sdf = java.text.SimpleDateFormat("hh:mm a")
// give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
        var formattedDate = sdf.format(date);
        return formattedDate
    }


    fun setUserData(context: Context, valve: UserDetails?) {
        val sharedpreferences =
            context.getSharedPreferences(AppConstants.MyPREFERENCES, Context.MODE_PRIVATE)
        val editor = sharedpreferences.edit()
        val myObject = valve
        val gson = Gson()
        val json = gson.toJson(myObject)
        editor.putString(AppConstants.USER_DATA, json)
        editor.apply()

    }


    fun getDateInMillis(srcDate: String): Long {
        val desiredFormat = SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss"
        )

        var dateInMillis: Long = 0
        try {
            val date = desiredFormat.parse(srcDate)
            dateInMillis = date.getTime()
            return dateInMillis
        } catch (e: ParseException) {

            e.printStackTrace()
        }

        return 0
    }

    fun getDateOnlyInMillis(srcDate: String): Long {
        val desiredFormat = SimpleDateFormat(
            AppConstants.ONLY_DATE_FORMATE_APP
        )

        var dateInMillis: Long = 0
        try {
            val date = desiredFormat.parse(srcDate)
            dateInMillis = date.getTime()
            return dateInMillis
        } catch (e: ParseException) {

            e.printStackTrace()
        }

        return 0
    }

    fun isYoutubeUrl(youTubeURl: String): Boolean {
        val success: Boolean
        val pattern = "^(http(s)?:\\/\\/)?((w){3}.)?youtu(be|.be)?(\\.com)?\\/.+"
        success = !youTubeURl.isEmpty() && youTubeURl.matches(pattern.toRegex())
        return success
    }


    fun pregMatch(pattern: String, content: String): Boolean {
        return content.matches(pattern.toRegex())
    }


    fun getSearchkey(key: String): Int {
        val items = key.split("/")
        val lastKey = items.get(items.size - 1)


        if (lastKey.isNotEmpty()) {
            if (lastKey.contains("?", true)) {
                val idWIthParam = lastKey.split("?")
                val id = idWIthParam.get(0)
                return id.toInt()
            } else {
                return lastKey.toInt()
            }

        }
        return 0
    }

    fun getWeatherIcon(iconName: String): Int {
        if (iconName.equals("clouds")) {
            return R.drawable.scattered_clouds
        } else if (iconName.equals("clearsky"))
            return R.drawable.clearsky
        else if (iconName.equals("mist"))
            return R.drawable.mist
        else if (iconName.equals("snow"))
            return R.drawable.snow
        else if (iconName.equals("rain"))
            return R.drawable.rain
        else if (iconName.equals("thunderstorm"))
            return R.drawable.thunderstorm
        else if (iconName.equals("drizzle"))
            return R.drawable.drizzle
        else

            return R.drawable.scattered_clouds

    }


    var playStoreLink =
        if (BuildConfig.FLAVOR.equals(AppConstants.LANSING_AREA_SHOP_LOCAL)) {
            AppConstants.LANSING_AREA_SHOP_LOCAL_STORE_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.SAN_ANGELO_SHOP_LOCAL)) {
            AppConstants.SAN_ANGELO_SHOP_LOCAL_PLAY_STORE_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.TALLAHASSEE_SMART_CITY)) {
            AppConstants.TALLAHASSEE_SMART_CITY_PLAY_STORE_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY)) {
            AppConstants.STALKEYES_SMART_CITY_PLAY_STORE_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.EVERETT_WASHINGTON)) {
            AppConstants.EVERETT_WASHINGTON_PLAY_STORE_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.NASHVILE)) {
            AppConstants.NASHVILE_PLAY_STORE_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
            AppConstants.SARASOTA_PLAY_STORE_LINK
        } else {
            ""
        }
    var cummunity_id = if (BuildConfig.FLAVOR.equals(AppConstants.LANSING_AREA_SHOP_LOCAL)) {
        AppConstants.COMMUNITY_ID_FOR_LANSING_AREA_SHOP_LOCAL
    } else if (BuildConfig.FLAVOR.equals(AppConstants.SAN_ANGELO_SHOP_LOCAL)) {
        AppConstants.COMMUNITY_ID_SANANGELO_SHOP_LOCAL
    } else if (BuildConfig.FLAVOR.equals(AppConstants.TALLAHASSEE_SMART_CITY)) {
        AppConstants.COMMUNITY_ID_TALLAHASSEE_SMART_CITY
    } else if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY)) {
        AppConstants.COMMUNITY_ID_STALKEYES_SMART_CITY
    } else if (BuildConfig.FLAVOR.equals(AppConstants.EVERETT_WASHINGTON)) {
        AppConstants.COMMUNITY_ID_EVERETT_WASHINGTON
    } else if (BuildConfig.FLAVOR.equals(AppConstants.NASHVILE)) {
        AppConstants.COMMUNITY_ID_NASHVILE
    }
    else if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
        AppConstants.COMMUNITY_ID_SARASOTA
    }

    else {
        ""
    }


    var lat = if (BuildConfig.FLAVOR.equals(AppConstants.LANSING_AREA_SHOP_LOCAL)) {
        AppConstants.LAT_LANSING_AREA_SHOP_LOCAL
    } else if (BuildConfig.FLAVOR.equals(AppConstants.SAN_ANGELO_SHOP_LOCAL)) {
        AppConstants.LAT_SAN_ANGELO_SHOP_LOCAL
    } else if (BuildConfig.FLAVOR.equals(AppConstants.TALLAHASSEE_SMART_CITY)) {
        AppConstants.LAT_TALLAHASSEE_SMART_CITY
    } else if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY)) {
        AppConstants.LAT_STALKEYES_SMART_CITY
    } else if (BuildConfig.FLAVOR.equals(AppConstants.EVERETT_WASHINGTON)) {
        AppConstants.LAT_EVERETT_WASHINGTON
    } else if (BuildConfig.FLAVOR.equals(AppConstants.NASHVILE)) {
        AppConstants.LAT_NASHVILE
    }

    else if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
        AppConstants.LAT_SARASOTA
    }
    else {
        ""
    }


    var lon = if (BuildConfig.FLAVOR.equals(AppConstants.LANSING_AREA_SHOP_LOCAL)) {
        AppConstants.LOG_LANSING_AREA_SHOP_LOCAL
    } else if (BuildConfig.FLAVOR.equals(AppConstants.SAN_ANGELO_SHOP_LOCAL)) {
        AppConstants.LOG_SAN_ANGELO_SHOP_LOCAL
    } else if (BuildConfig.FLAVOR.equals(AppConstants.TALLAHASSEE_SMART_CITY)) {
        AppConstants.LOG_TALLAHASSEE_SMART_CITY
    } else if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY)) {
        AppConstants.LOG_STALKEYES_SMART_CITY
    } else if (BuildConfig.FLAVOR.equals(AppConstants.EVERETT_WASHINGTON)) {
        AppConstants.LOG_EVERETT_WASHINGTON
    } else if (BuildConfig.FLAVOR.equals(AppConstants.NASHVILE)) {
        AppConstants.LOG_NASHVILE
    }

    else if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
        AppConstants.LOG_SARASOTA
    }
    else {
        ""
    }


    var weather_api_id = if (BuildConfig.FLAVOR.equals(AppConstants.LANSING_AREA_SHOP_LOCAL)) {
        AppConstants.LANSING_AREA_SHOP_LOCAL_WEATHER_API_ID
    } else if (BuildConfig.FLAVOR.equals(AppConstants.SAN_ANGELO_SHOP_LOCAL)) {
        AppConstants.SAN_ANGELO_SHOP_WEATHER_API_ID
    } else if (BuildConfig.FLAVOR.equals(AppConstants.TALLAHASSEE_SMART_CITY)) {
        AppConstants.TALLAHASSEE_SMART_CITY_WEATHER_API_ID
    } else if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY)) {
        AppConstants.STALKEYES_SMART_CITY_WEATHER_API_ID
    } else if (BuildConfig.FLAVOR.equals(AppConstants.EVERETT_WASHINGTON)) {
        AppConstants.EVERETT_WASHINGTON_WEATHER_API_ID
    } else if (BuildConfig.FLAVOR.equals(AppConstants.NASHVILE)) {
        AppConstants.NASHVILE_WEATHER_API_ID
    }
    else if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
        AppConstants.SARASOTA_WEATHER_API_ID
    }
    else {
        ""
    }


    var business_link =
        if (BuildConfig.FLAVOR.equals(AppConstants.LANSING_AREA_SHOP_LOCAL)) {
            AppConstants.LANSING_AREA_SHOP_LOCAL_BUSINESS_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.SAN_ANGELO_SHOP_LOCAL)) {
            AppConstants.SAN_ANGELO_SHOP_BUSINESS_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.TALLAHASSEE_SMART_CITY)) {
            AppConstants.TALLAHASSEE_SMART_CITY_BUSINESS_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY)) {
            AppConstants.STALKEYES_SMART_CITY_BUSINESS_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.EVERETT_WASHINGTON)) {
            AppConstants.EVERETT_WASHINGTON_BUSINESS_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.NASHVILE)) {
            AppConstants.NASHVILE_BUSINESS_LINK
        }
        else if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
            AppConstants.SARASOTA_BUSINESS_LINK
        }
        else {
            ""
        }


    fun getCityName(context: Context): String {

        if (BuildConfig.FLAVOR.equals(AppConstants.LANSING_AREA_SHOP_LOCAL)) {
            return AppConstants.LANSING_AREA_CITY_NAME
        } else if (BuildConfig.FLAVOR.equals(AppConstants.SAN_ANGELO_SHOP_LOCAL)) {
            return AppConstants.SAN_ANGELO_CITY_NAME
        } else if (BuildConfig.FLAVOR.equals(AppConstants.TALLAHASSEE_SMART_CITY)) {
            return AppConstants.TALLAHASSEE_CITY_NAME
        } else if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY)) {
            if (getDataFromSharePreference(context, AppConstants.USER_CITY_NAME).isNotEmpty()) {
                return getDataFromSharePreference(
                    context,
                    AppConstants.USER_CITY_NAME
                )

            } else {
                return AppConstants.STALKEYES_CITY_NAME
            }

        } else if (BuildConfig.FLAVOR.equals(AppConstants.EVERETT_WASHINGTON)) {
            return AppConstants.EVERETT_WASHINGTON_CITY_NAME
        } else if (BuildConfig.FLAVOR.equals(AppConstants.NASHVILE)) {
            return AppConstants.NASHVILE_CITY_NAME
        }
        else if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {

            if (getDataFromSharePreference(context, AppConstants.USER_CITY_NAME).isNotEmpty()) {
                return getDataFromSharePreference(
                    context,
                    AppConstants.USER_CITY_NAME
                )

            } else {
                return AppConstants.SARASOTA_CITY_NAME
            }
        }

        return ""
    }

    var proximate_uuid =
        if (BuildConfig.FLAVOR.equals(AppConstants.LANSING_AREA_SHOP_LOCAL)) {
            AppConstants.LANSING_AREA_SHOP_LOCAL_PROXIMATE_ID
        } else if (BuildConfig.FLAVOR.equals(AppConstants.SAN_ANGELO_SHOP_LOCAL)) {
            AppConstants.SAN_ANGELO_SHOP_PROXIMATE_ID
        } else if (BuildConfig.FLAVOR.equals(AppConstants.TALLAHASSEE_SMART_CITY)) {
            AppConstants.TALLAHASSEE_SMART_CITY_PROXIMATE_ID
        } else if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY)) {
            AppConstants.STALKEYES_SMART_CITY_PROXIMATE_ID
        } else if (BuildConfig.FLAVOR.equals(AppConstants.EVERETT_WASHINGTON)) {
            AppConstants.EVERETT_WASHINGTON_PROXIMATE_ID
        } else if (BuildConfig.FLAVOR.equals(AppConstants.NASHVILE)) {
            AppConstants.NASHVILE_PROXIMATE_ID
        } else if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
            AppConstants.SARASOTA_PROXIMATE_ID
        }


        else {
            ""
        }


    var event_link =
        if (BuildConfig.FLAVOR.equals(AppConstants.LANSING_AREA_SHOP_LOCAL)) {
            AppConstants.LANSING_AREA_SHOP_LOCAL_EVENT_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.SAN_ANGELO_SHOP_LOCAL)) {
            AppConstants.SAN_ANGELO_SHOP_EVENT_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.TALLAHASSEE_SMART_CITY)) {
            AppConstants.TALLAHASSEE_SMART_CITY_EVENT_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY)) {
            AppConstants.STALKEYES_SMART_CITY_EVENT_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.EVERETT_WASHINGTON)) {
            AppConstants.EVERETT_WASHINGTON_EVENT_LINK
        } else if (BuildConfig.FLAVOR.equals(AppConstants.NASHVILE)) {
            AppConstants.NASHVILE_EVENT_LINK
        }

        else if (BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
            AppConstants.SARASOTA_EVENT_LINK
        } else {
            ""
        }


    fun getCurrentTimeInString(): String {
        //date output format
        val dateFormat = SimpleDateFormat(AppConstants.DATE_FORMATE_APP);
        val cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    fun getCurrentTimeInDateFormate(): Date {
        //date output format
        val cal = Calendar.getInstance();
        val dateInString = SimpleDateFormat(AppConstants.DATE_FORMATE_APP)
            .format(cal.getTime())
        val dateFormat = SimpleDateFormat(AppConstants.DATE_FORMATE_APP);

        return dateFormat.parse(dateInString)
    }

    fun getTimeInDateFormate(timeDate: String): Date {
        //date output format

        val dateFormat = SimpleDateFormat(AppConstants.DATE_FORMATE_APP);

        return dateFormat.parse(timeDate);
    }


    fun isDateCross24Hours(startDate: Date, endDate: Date): Boolean {
        //milliseconds
        var different = endDate.getTime() - startDate.getTime()


        return different > 86400000


    }


    fun openCustomtab(context: Context, valve: String) {


        try {
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()
            builder.setToolbarColor(ContextCompat.getColor(context, R.color.colorWhite))
            customTabsIntent.intent.setPackage("com.android.chrome")
            customTabsIntent.launchUrl(context, Uri.parse(valve.trim()))
        } catch (e: Exception) {
            Toast.makeText(context, "Invalid link", Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }


    }

    fun getZeroTimeDate(fecha: Date): Date {
        var res = fecha
        val calendar = Calendar.getInstance()

        calendar.time = fecha
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        res = calendar.time

        return res
    }

    fun shareApp(context: Context) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND

            putExtra(
                Intent.EXTRA_TEXT, getShareAppText(context)
            )
            type = "text/plain"
        }
        val shareIntent = Intent.createChooser(sendIntent, null)
        context.startActivity(shareIntent)
    }

    fun isLocationAppPermissionAvalible(context: Context): Boolean {


        return (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        ))
    }

    public fun showGpsLocationRequireAlert(context: Context) {
        GpsUtils(context).turnGPSOn {
        }
    }


    fun isMyServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
        for (service in manager!!.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.getClassName()) {
                return true
            }
        }
        return false
    }

    fun getShareAppText(context: Context): String {
        return "Have you seen our community's mobile app? It's great!!!\n" +
                "${CommonMethods.playStoreLink}\n" +
                "${context.getString(R.string.app_name_full_name)}\n" +
                "Download it today and support our community and our local small businesses! #ShopLocal #Community\n"

    }

    fun getCameraPermissionAlert(context: Context): String {
        return "${context.getString(R.string.app_name)} ${context.getString(R.string.camera_permission)}"
    }

    fun getStoragePermissionAlert(context: Context): String {
        return "${context.getString(R.string.app_name)} ${context.getString(R.string.store_permission)}"
    }


    fun getLocationPermissionAlert(context: Context): String {
        return "${context.getString(R.string.app_name)} ${context.getString(R.string.location_permission_alert)}"

    }

    fun getCalenderPermissionAlert(context: Context): String {
        return "${context.getString(R.string.app_name)} ${context.getString(R.string.calender_permisson_laert)}"

    }

    fun getDeepChildOffset(
        mainParent: ViewGroup,
        parent: ViewParent,
        child: View,
        accumulatedOffset: Point
    ) {
        var parentGroup = parent as ViewGroup
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }


    fun getCityNameFromLocation(
        latitude: Double, longitude: Double,
        context: Context, handler: Handler
    ): String {
        var cityName: String = ""
        val geocoder = Geocoder(context, Locale.getDefault())
        try {
            val addressList = geocoder.getFromLocation(
                latitude, longitude, 1
            )
            if (addressList != null && addressList.size > 0) {
                val address = addressList[0]
                cityName = address.locality
            }
            return cityName

        } catch (e: IOException) {
            return ""
        }


    }

    fun getCategoryIcons(context: Context): MutableList<CategoryBitMap> {
        val sharedPreferencesObject = getSharePreferenceObject(context)
        val gson = Gson()
        val json = sharedPreferencesObject.getString(AppConstants.CATEGORY_BITMAP_DATA, "")
        val mutableListType = object : TypeToken<MutableList<CategoryBitMap>>() {}.type
        val categoryBitMapList: MutableList<CategoryBitMap> = gson.fromJson(json, mutableListType)
        return categoryBitMapList
    }

    fun getFileNameFromURl(key: String): String {
        val items = key.split("/")
        val lastKey = items.get(items.size - 1)

        return lastKey
    }

    fun getCapsSentences(tagName: String): String? {
        val splits =
            tagName.toLowerCase().split(",".toRegex()).toTypedArray()
        val sb = java.lang.StringBuilder()
        for (i in splits.indices) {
            val eachWord = splits[i].trim()
            if (i > 0 && eachWord.length > 0) {
                sb.append("    ")
            }
            val cap = (eachWord.substring(0, 1).toUpperCase()
                    + eachWord.substring(1))
            sb.append(cap)
        }
        return sb.toString()
    }


}


