package com.lansingareashoplocal.utility.helper

import android.content.Context
import android.graphics.Typeface
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import com.facebook.stetho.inspector.elements.android.HighlightableDescriptor
import com.lansingareashoplocal.utility.helper.others.CustomTypefaceSpan


fun TextView.getTrimmedText(): String {
    return this.text.toString().trim()
}

fun TextView.isEmptyText(): Boolean {
    return TextUtils.isEmpty(this.getTrimmedText())
}

fun EditText.showKeyboard() {
    val inputManager = this.context
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun TextView.setDifferentColor(normalText: String, highlightText: String, context: Context, textView: TextView, typeface: Typeface,color:Int) {
    val mSpannableStringBuilder = SpannableStringBuilder(normalText)
    val name = SpannableString(highlightText)
    name.setSpan(ForegroundColorSpan(color),
            0, name.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    if (typeface != null) {
        name.setSpan(CustomTypefaceSpan("", typeface), 0, name.length, Spanned.SPAN_EXCLUSIVE_INCLUSIVE)
    }
    mSpannableStringBuilder.append(name)
    textView.movementMethod = LinkMovementMethod.getInstance()
    textView.text = mSpannableStringBuilder
}
