package com.lansingareashoplocal.utility.helper.customclass;

import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.lansingareashoplocal.R;
import com.nex3z.notificationbadge.NotificationBadge;

import org.jetbrains.annotations.NotNull;

public class TabCustomView extends LinearLayout {

    private RelativeLayout tabInnerLayout;
    private ImageView tabImage;
    public NotificationBadge notificationBadge;

    public TabCustomView(Context context) {
        super(context);
    }

    public TabCustomView(Context context, int drawableInActive, int drawableActive,
                         String title, boolean haveBadge) {
        super(context);
        this.tabInnerLayout = (RelativeLayout) LayoutInflater.from(context).
                inflate(R.layout.component_tab_view, null);

        if (drawableActive != -1) {
            StateListDrawable listDrawable = new StateListDrawable();
            listDrawable.addState(SELECTED_STATE_SET, this.getResources().getDrawable(drawableActive));
            listDrawable.addState(ENABLED_STATE_SET, this.getResources().getDrawable(drawableInActive));
            tabImage = tabInnerLayout.findViewById(R.id.imgTabImageCTV);
            tabImage.setImageDrawable(listDrawable);
            StateListDrawable listBgDrawable = new StateListDrawable();
            tabInnerLayout.setBackground(listBgDrawable);
            TextView tabTitleTxt = tabInnerLayout.findViewById(R.id.txtTabTitleCTV);
            tabTitleTxt.setText(title);
            tabTitleTxt.setGravity(Gravity.CENTER);
            setGravity(Gravity.CENTER);
            notificationBadge = tabInnerLayout.findViewById(R.id.notificationBadge);
            notificationBadge.setVisibility(View.GONE);
        }
        addView(tabInnerLayout);
    }


    /*public void updateNotificationCount(@NotNull String notificationCount, boolean state) {
        if(state){
            notificationBadge.setVisibility(View.VISIBLE);
        }else{
            notificationBadge.setVisibility(View.GONE);
        }
        if(TextUtils.isEmpty(notificationCount)){
            notificationBadge.setVisibility(View.GONE);
            return;
        }
        if(notificationCount.equalsIgnoreCase("0")){
            notificationBadge.setVisibility(View.GONE);
        }

        notificationBadge.setText(notificationCount);
        invalidate();
    }*/
}