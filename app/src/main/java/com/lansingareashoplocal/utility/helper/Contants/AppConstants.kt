import com.lansingareashoplocal.ui.model.Temperature

interface AppConstants {
    companion object {
        val USER_DATA = "userdata"
        val MyPREFERENCES = "user_pref"
        val USER_ID = "user_id"

        val MAIL = "mail"
        val APP_DATE_FORMATE = "MM/dd/yyyy"
        var WEATHERDATA: Temperature? = null
        var WEATHER_API_LAST_CALL_TIME = 0L
        val TEN_MINUTES = 10 * 60 * 1000
        val CURRENT_TEMPERATURE = "current_temperature"
        val CURRENT_WEATHER_ICON_ID="current_weather_icon_id"
        val LAST_API_CALL_TIME = "last_api_call_time"
        val WEATHER_DATA = "weather_data"
        val PAGE_CODE = "page_code"
        val PASSWORD = "password"
        val FACEBOOK_ID = "facebook_id"
        val PROFILE_UPDATE_REQUEST = 700
        val ONLY_DATE_FORMATE_API = "yyyy-MM-dd"
        val ONLY_DATE_FORMATE_APP = "MM/dd/yyyy"
        val DATE_FORMATE_API = "yyyy-MM-dd hh:mm:ss"
        val DATE_FORMATE_APP = "MMM dd,yyyy hh:mm a"
        val FEATURED_BUSINESS = "Featured Business"
        val NON_FEATURED_BUSINESS = " Non Featured Business"
        val ITEM_CLICK = "Item_click"
        val SHARE_KEY = "share_key"
        val TIME_FORMATE_API = "HH:mm:ss"
        val TIME_FORMATE_APP = "hh:mm a"
        val YOUTUBE = "Youtube"
        val INSTAGRAM = "Instagram"
        val LINKEDIN = "LinkedIn"
        val TWITTER = "Twitter"
        val FACEBOOK = "Facebook"
        val WWW_YOUTUBE_COM = "www.youtube.com"

        val COMMUNITY_ID_FOR_LANSING_AREA_SHOP_LOCAL = "1"
        val COMMUNITY_ID_SANANGELO_SHOP_LOCAL = "2"
        val COMMUNITY_ID_TALLAHASSEE_SMART_CITY = "4"
        val COMMUNITY_ID_STALKEYES_SMART_CITY = "3"
        val COMMUNITY_ID_EVERETT_WASHINGTON = "5"
        val COMMUNITY_ID_NASHVILE = "6"

        val COMMUNITY_ID_SARASOTA = "7"


        var WETHER_ICON_URL = "http://openweathermap.org/img/wn/"

        var LANSING_AREA_SHOP_LOCAL_BUSINESS_LINK =
            "https://www.lansingareashoplocal.com/bronze-mobile-submission"
        var SAN_ANGELO_SHOP_BUSINESS_LINK =
            "https://www.sanangeloshoplocal.com/in-app-bronze-submission"
        var TALLAHASSEE_SMART_CITY_BUSINESS_LINK =
            "https://www.tallahasseesmartcity.com/in-app-bronze-submission"
        var STALKEYES_SMART_CITY_BUSINESS_LINK =
            "https://www.stalkeyessmartcity.com/contact-us"

        var  EVERETT_WASHINGTON_BUSINESS_LINK =
            "https://www.everettcommunityapp.com/in-app-bronze-free-listing"

        var  NASHVILE_BUSINESS_LINK =
            "https://www.nashvillecommunityapp.com/in-app-free-bronze-submission"

        var  SARASOTA_BUSINESS_LINK =
            "https://www.staylocalsrq.com/in-app-free-bronze-submission"




        var LANSING_AREA_SHOP_LOCAL_EVENT_LINK =
            "https://www.lansingareashoplocal.com/event-submission"
        var SAN_ANGELO_SHOP_EVENT_LINK =
            "https://www.sanangeloshoplocal.com/request-community-event"
        var TALLAHASSEE_SMART_CITY_EVENT_LINK =
            "https://www.tallahasseesmartcity.com/in-app-event-submission"
        var STALKEYES_SMART_CITY_EVENT_LINK =
            "https://www.stalkeyessmartcity.com/request-community-event"

        var  EVERETT_WASHINGTON_EVENT_LINK =
            "https://www.everettcommunityapp.com/in-app-event-submission"

        var  NASHVILE_EVENT_LINK =
            "https://www.nashvillecommunityapp.com/in-app-event-submit"
        var  SARASOTA_EVENT_LINK =
            "https://www.staylocalsrq.com/in-app-event-submit"






        var LANSING_AREA_SHOP_LOCAL = "lansingAreaShopLocal"
        var SAN_ANGELO_SHOP_LOCAL = "sanAngeloShopLocal"
        var TALLAHASSEE_SMART_CITY = "tallahasseeSmartCity"
        var STALKEYES_SMART_CITY = "stalkeyesSmartCity"
        var EVERETT_WASHINGTON=    "everettWashington"
        var NASHVILE="nashville"
        var SARASOTA="sarasota"

        var LAT_LANSING_AREA_SHOP_LOCAL = "42.73254"
        var LOG_LANSING_AREA_SHOP_LOCAL = "-84.555527"



        var LAT_SAN_ANGELO_SHOP_LOCAL = "31.4638"
        var LOG_SAN_ANGELO_SHOP_LOCAL = "-100.4370"


        var LAT_TALLAHASSEE_SMART_CITY = "30.4383"
        var LOG_TALLAHASSEE_SMART_CITY = "-84.2807"


        var LAT_STALKEYES_SMART_CITY = "38.9696"
        var LOG_STALKEYES_SMART_CITY = "-77.3861"


        var LAT_EVERETT_WASHINGTON = "47.982567"
        var LOG_EVERETT_WASHINGTON = "-122.193375"

        var LAT_NASHVILE = "36.1627"
        var LOG_NASHVILE = "-86.7816"

        var LAT_SARASOTA = "27.3364"
        var LOG_SARASOTA = "-82.5307"





        var LANSING_AREA_SHOP_LOCAL_WEATHER_API_ID = "9c6fc0da36d88b70cad41da573c882ef"
        var SAN_ANGELO_SHOP_WEATHER_API_ID = "9c6fc0da36d88b70cad41da573c882ef"
        var TALLAHASSEE_SMART_CITY_WEATHER_API_ID = "9c6fc0da36d88b70cad41da573c882ef"
        var STALKEYES_SMART_CITY_WEATHER_API_ID = "9c6fc0da36d88b70cad41da573c882ef"
        var EVERETT_WASHINGTON_WEATHER_API_ID = "9c6fc0da36d88b70cad41da573c882ef"
        var NASHVILE_WEATHER_API_ID = "9c6fc0da36d88b70cad41da573c882ef"
        var SARASOTA_WEATHER_API_ID = "9c6fc0da36d88b70cad41da573c882ef"

        var WEATHER_UNITS = "imperial"


        var LANSING_AREA_SHOP_LOCAL_STORE_LINK = "https://lansingareashoplocal.page.link/u9DC"
        var SAN_ANGELO_SHOP_LOCAL_PLAY_STORE_LINK = "https://sanangeloshoplocal.page.link/Eit5"
        var TALLAHASSEE_SMART_CITY_PLAY_STORE_LINK = "https://tallahasseesmartcity.page.link/76UZ"
        var STALKEYES_SMART_CITY_PLAY_STORE_LINK = "https://stalkeyessmartcity.page.link/6SuK"
        var EVERETT_WASHINGTON_PLAY_STORE_LINK = "https://everettcommunityapp.page.link/XktS"
        var NASHVILE_PLAY_STORE_LINK = "https://nashvillecommunityapp.page.link/jdF1"
        var SARASOTA_PLAY_STORE_LINK = "https://staylocalsrq.page.link/XktS"



        var DEVICE_TOKEN = "device_token"


        var LANSING_AREA_CITY_NAME = "Lansing Area"

        var SAN_ANGELO_CITY_NAME = "San Angelo"

        var TALLAHASSEE_CITY_NAME = "Tallahassee"

        var STALKEYES_CITY_NAME = "StalkEyes"

        var NASHVILE_CITY_NAME="Nashville Area"


        var EVERETT_WASHINGTON_CITY_NAME="Everett"

        var SARASOTA_CITY_NAME="Sarasota"


        var EVENT = "EVENT"
        var ALERT = "ALERT"
        var DIRECT = "DIRECT"
        var B_DIR = "B_DIR"
        var DATE_IN_MILLI_SEC = "date_in_milli_sec"
        var PUSH = "PUSH"
        var BUSINESS = "BUSINESS"

        var COMMUNITY_ID = "community_id"
        var EVENT_ID = "event_id"
        var NEWS_ID="news_id"
        var IS_NEED_TO_CALL_API = "is_need_to_call_api"
        var NOTIFICATION_ID = "notification_id"
        var BUSINESS_ID = "business_place_id"
        var DIRECT_MESSAGE = "direct_message"
        var IMAGE_URL="image_url"

        var LOCATION_REQUEST = 300
        var GPS_REQUEST = 1000
        var BLUETOOTH_REQUEST = 100


        var BUSINESS_PLACE = "business_place"

        var TYPE = "type"
        var DB_NAME = "lansing"
        var NEW = "new"
        var UPDATE = "update"

        var LATITUDE = "latitude"
        var LONGITUDE = "longitude"




        var LANSING_AREA_SHOP_LOCAL_PROXIMATE_ID =
            "86bad320-c335-4959-87d5-f6290f5fa98c"
        var SAN_ANGELO_SHOP_PROXIMATE_ID =
            "4f8b9659-988f-49f8-b456-f67f6abec718"
        var TALLAHASSEE_SMART_CITY_PROXIMATE_ID =
            "d929a533-17a7-4d38-89e0-a7911af76a03"
        var STALKEYES_SMART_CITY_PROXIMATE_ID =
            "afdf474b-08d4-404c-93fb-958599053a32"

        var  EVERETT_WASHINGTON_PROXIMATE_ID =

            "08bd2a22-db14-4b89-aa50-d092ade06014"

        var NASHVILE_PROXIMATE_ID =

            "0e2d0ee3-e23d-4aef-99ca-8b09fcf19fac"

        var SARASOTA_PROXIMATE_ID =

            "d74b24e2-8542-4a2a-8da2-96b3c52410f0"



        var WEATHER_API_ID="9c6fc0da36d88b70cad41da573c882ef"
        val GOOGLE_DETAILS_REQUEST = 200
        val CALENDER_REQUEST = 300
        val WEATHER_REQUEST=1000
        val LOGIN_REQUEST=400
        var MY_REVIEWS_REQUEST=500
        var LOG_IN_REQUEST_FOR_ADD_REVIEW=600
        var LOG_IN_REQUEST_FOR_ADD_REPORT=900
        var ADD_REVIEW_REQUEST=700
        var UPDATE_REVIEW_REQUEST=800
        var MAJOR = "major"
        var MINOR = "minor"
        var NO_OF_DAYS_FOR_SHAREAPP = 6
        var ONE_DAY_IN_MILLI_SECONDS = 86400000
        var ONE_HOUR_IN_MILLI_SEC=3600000

        var POSITION = "position"
        var TOOL_BAR_IS_REQUIRE = "tool_bar_is_require"
        var BUSINESS_DETAILS_MODEL = "businessDetailsModel"
        var IS_FROM_SEARCH="is_from_search"
        var IS_FROM_BECON_NOTIFICATION ="is_from_becon_notification"

        var OWNER="OWNER"
        var PROMOTION="PROMOTION"
        var IMPERIAL="imperial"
        var DISTANCE_ASC="distance_asc"
        var USER_CITY_NAME="user_city_name"
        var YELP="Yelp"
        var PINTREST="Pinterest"
        var TIMETYPE="timeType"
        var BUSINESS_NAME= "business_name"
        var IS_EDITING="is_editing"
        var RATING_ID="rating_id"
        var REVIEW="review"
        var RATING="rating"
        var BUSINESS_LOGO="business_logo"
        var BUSINESS_MODEL="business_model"


        var YOUTUBE_KEY="AIzaSyATosH8UKpszNHmnAPPQew-le_uqeb0vyU"
        var YOUTUBE_ID="youtube_id"
        var CUSTOM_URL="custom_url"
        var VIMEO_ID="vimeo_id"
        var VIMEO="vimeo"
        var CATEGORY_BITMAP_DATA="category_bitmap_data"
        var IMMEDIATE_UPDATE_REQUEST_CODE= 1200
        var FLEXIBLE_UPDATE_REQUEST_CODE= 1300


        var ANDROID_MINIMUM_REQUIRED_VERSION_CODE="android_minimum_required_version_code"
        val REQUEST_GALLERY = 1400

        val REQUEST_CAMERA = 1500

        val IMAGE="Image"
        val UN_SPLASH_CLIENT_ID="DYqv9vBODkazZGe1pcRAZnwemJ0-gquKNGhlrf1sr3g"
        val UN_SPLASH_REQUEST_CODE=1600
        val RESULT_URL="result_url"
        val UNSPLASH_SECRET_KEY="4rZHGhgbZbNJ7eUmOwLhIHmSS3QghlmG_KoB4Kjs2q8"

        val UNSPLASH_ACCESS_KEY="DYqv9vBODkazZGe1pcRAZnwemJ0-gquKNGhlrf1sr3g"

        val ACTIVE_PROMOTIONS_ONLY="active_promotions_only"
        val OPEN_NOW="open_now"
        val FEATURE_BUSINESS_SUB_CATEGORY_FILTER_REQUEST_CODE=1700
        val MAP_VIEW_SUB_CATEGORY_FILTER_REQUEST_CODE=1800
        val SUB_CATEGORY_ID="sub_category_id"

        var MULTI_PART_FROM_DATA="multipart/form-data"
        val Last_Month = "last_mnth"
        val EVENT_MODE="event_mode"
        val CALENDER_MODE="calender_mode"
        val EVENT_LIST_TYPE="calender_mode"

        val IS_FROM_RATING= "is_from_rating"





    }
}