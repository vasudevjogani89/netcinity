package com.lansingareashoplocal.utility.helper.others

import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.widget.AppCompatEditText
import com.lansingareashoplocal.ui.interfaces.EditTextChangeCallBack

class CustomTextWatcher(private val mEditText: AppCompatEditText, private val editTextChangeCallBack: EditTextChangeCallBack) :
    TextWatcher {


    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(s: Editable) {
        editTextChangeCallBack.afterTextChanged(mEditText)

    }
}