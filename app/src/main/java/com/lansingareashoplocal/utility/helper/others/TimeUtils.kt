package com.awoko.utility.helper

import java.text.ParseException
import java.text.SimpleDateFormat

object TimeUtils {


  /*  fun convertDateForSendToServer(inputDateStr: String?): String ?{
        val inputFormat = SimpleDateFormat(ApiConstants.DATE_FORMATE)
        val outputFormat = SimpleDateFormat(AppConstants.APP_DATE_FORMATE)
        val date = inputFormat.parse(inputDateStr)
        val outputDateStr = outputFormat.format(date)
        return outputDateStr
    }*/

    fun isValidFormate(dateStr: String, dateFormate: String): Boolean {
        val sdf = SimpleDateFormat(dateFormate)
        sdf.isLenient = false
        try {
            sdf.parse(dateStr)
        } catch (e: ParseException) {
            return false
        }

        return true
    }

}