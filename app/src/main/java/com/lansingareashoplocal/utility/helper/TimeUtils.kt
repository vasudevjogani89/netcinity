package com.lansingareashoplocal.utility.helper

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object TimeUtils {


    fun convertDateForSendToServer(inputDateStr: String?): String {
        val inputFormat = SimpleDateFormat(AppConstants.ONLY_DATE_FORMATE_APP, Locale.ENGLISH)
        val outputFormat = SimpleDateFormat(AppConstants.ONLY_DATE_FORMATE_API, Locale.ENGLISH)

        if (inputDateStr?.isNotEmpty()!!) {
            val date = inputFormat.parse(inputDateStr)
            val outputDateStr = outputFormat.format(date)
            return outputDateStr
        } else return ""

    }

    fun convertDateForShowingApp(inputDateStr: String?): String {
        val inputFormat = SimpleDateFormat(AppConstants.ONLY_DATE_FORMATE_API, Locale.ENGLISH)
        val outputFormat = SimpleDateFormat(AppConstants.ONLY_DATE_FORMATE_APP, Locale.ENGLISH)
        return if (inputDateStr?.isNotEmpty()!!) {
            try {
                val date = inputFormat.parse(inputDateStr)
                val outputDateStr = outputFormat.format(date)
                outputDateStr
            } catch (e:Exception){
                ""
            }
        } else ""

    }


    fun convertDateWithTimeForShowingApp(inputDateStr: String?): String {
        val inputFormat = SimpleDateFormat(AppConstants.DATE_FORMATE_API, Locale.ENGLISH)
        val outputFormat = SimpleDateFormat(AppConstants.DATE_FORMATE_APP, Locale.ENGLISH)
        return if (inputDateStr?.isNotEmpty()!!) {
            try {
                val date = inputFormat.parse(inputDateStr)
                val outputDateStr = outputFormat.format(date)
                outputDateStr
            } catch (e: Exception) {
                ""
            }


        } else ""

    }


    fun getDateOnly(timeFromServer: String): String {
        try {
            val mSimpleDateFormat = SimpleDateFormat("MM/DD/YYYY hh:mm:ss", Locale.ENGLISH)
            val serverDate = mSimpleDateFormat.parse(timeFromServer)
            val sdf = SimpleDateFormat("YYYY-MM-dd", Locale.ENGLISH)
            return sdf.format(serverDate)
        } catch (e: Exception) {
            e.printStackTrace()
            return timeFromServer
        }

    }

    fun isValidFormate(dateStr: String, dateFormate: String): Boolean {
        val sdf = SimpleDateFormat(dateFormate)
        sdf.isLenient = false
        try {
            sdf.parse(dateStr)
        } catch (e: ParseException) {
            return false
        }

        return true
    }

    fun getFormatedDateTime(
        dateStr: String,
        strReadFormat: String,
        strWriteFormat: String
    ): String {

        var formattedDate = dateStr

        val readFormat = SimpleDateFormat(strReadFormat, Locale.getDefault())
        val writeFormat = SimpleDateFormat(strWriteFormat, Locale.getDefault())

        var date: Date? = null

        try {
            date = readFormat.parse(dateStr)
        } catch (e: ParseException) {
            return ""
        }

        if (date != null) {
            formattedDate = writeFormat.format(date)
        }

        return formattedDate
    }
}