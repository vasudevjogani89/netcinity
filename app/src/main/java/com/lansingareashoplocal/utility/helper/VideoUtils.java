package com.lansingareashoplocal.utility.helper;

import androidx.annotation.NonNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VideoUtils {

    // regular expressions
    private static final Pattern YOUTUBE_PATTERN = Pattern.compile(".*(?:youtu.be\\/|v\\/|u\\/\\w\\/|embed\\/|watch\\?v=)([^#\\&\\?]*).*", Pattern.CASE_INSENSITIVE);
    private static final Pattern VIMEO_PATTERN = Pattern.compile("[http|https]+:\\/\\/(?:www\\.|)vimeo\\.com\\/([a-zA-Z0-9_\\-]+)(&.+)?", Pattern.CASE_INSENSITIVE);
    
    // youtube thumbnail settings
    private static final String YOUTUBE_THUMBNAIL_URL = "http://i.ytimg.com/vi/";
    private static final String YOUTUBE_THUMBNAIL_SIZE = "/hqdefault.jpg";

    public static boolean isYouTubeUrl(String url) {
        return YOUTUBE_PATTERN.matcher(url).matches();
    }

    public static boolean isVimeoUrl(String url) {
        return VIMEO_PATTERN.matcher(url).matches();
    }

    public static String getYouTubeThumbnailUrl(@NonNull String url) {
        Matcher youtube = YOUTUBE_PATTERN.matcher(url);
        if (!youtube.find())
            return null;

        String group = youtube.group(1);

        return YOUTUBE_THUMBNAIL_URL + group + YOUTUBE_THUMBNAIL_SIZE;
    }

    public static long getVimeoVideoId(@NonNull String url) {
        Matcher vimeo = VIMEO_PATTERN.matcher(url);
        if (!vimeo.find())
            return -1;

        return Long.valueOf(vimeo.group(1));
    }
}