package com.lansingareashoplocal.utility.helper.others;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

public class AndroidUtils {

    private static Context appContext;
    private static float density;
    private static float scaledDensity;

    public static void init(Context context) {
        appContext = context.getApplicationContext();
        density = appContext.getResources().getDisplayMetrics().density;
        scaledDensity = appContext.getResources().getDisplayMetrics().scaledDensity;
    }

    public static int toDp(int px) {
        return (int) (px / density);
    }

    public static int toPx(int dp) {
        return (int) (dp * density);
    }

    public static float toPx(float dp) {
        return dp * density;
    }

    public static int toPxFromSp(int sp) {
        return (int) (sp * scaledDensity);
    }

    public static int toSp(int px) {
        return (int) (px / scaledDensity);
    }

    public static void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) appContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) appContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static boolean isKeyboardShowed(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) appContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            return imm.isActive(view);
        }
        return false;
    }

    public static boolean isNetworkConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static boolean isPermissionGranted(Context context, String permission) {
        return ActivityCompat.checkSelfPermission(context, permission)
                == PackageManager.PERMISSION_GRANTED;
    }



    public static DisplayMetrics getScreenSize(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay()
                .getMetrics(metrics);
        return metrics;
    }

    public static int getActionBarHeight(Context context) {
        TypedArray a = context.obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
        int actionBarHeight = a.getDimensionPixelSize(0, -1);
        a.recycle();
        return actionBarHeight;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources()
                .getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources()
                    .getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getMinHeightForScrollContainer(Context context, int topPadding) {
        DisplayMetrics screenSize = AndroidUtils.getScreenSize(context);
        int actionBarSize = AndroidUtils.getActionBarHeight(context);
        int statusBarSize = AndroidUtils.getStatusBarHeight(context);
        int minHeight = screenSize.heightPixels + topPadding - actionBarSize - statusBarSize;
        if (Build.VERSION.SDK_INT >= 21) {
            minHeight -= statusBarSize;
        }
        return minHeight;
    }



    public static void copyToClipBoard(Context context, String text) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", text);
        clipboard.setPrimaryClip(clip);
    }

    public static String toString(@NonNull TextView editText) {
        return editText.getText().toString().trim();
    }
}
