package com.lansingareashoplocal.utility.helper.customclass;


import android.content.Context;
import android.util.AttributeSet;

import androidx.fragment.app.FragmentTabHost;

import com.lansingareashoplocal.ui.interfaces.ReClickListener;
import com.lansingareashoplocal.ui.interfaces.TabClickableListener;

public class ReClickableTabHost extends FragmentTabHost {

    private ReClickListener reClickListener;
    private TabClickableListener clickListener;

    public void setReClickListener(ReClickListener reClickListener) {
        this.reClickListener = reClickListener;
    }

    public void tabClickableListener(TabClickableListener clickListener) {
        this.clickListener = clickListener;
    }

    public ReClickableTabHost(Context context) {
        super(context);
    }

    public ReClickableTabHost(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setCurrentTab(int index) {
        if (index == getCurrentTab() && reClickListener != null) {
            reClickListener.reClickedTabId(index);
        } else {
            super.setCurrentTab(index);
            try {
                clickListener.clickedTabId(index);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}