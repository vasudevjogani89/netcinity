package com.lansingareashoplocal.utility.helper.Contants.others

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FaceBookData {
    @Expose
    @SerializedName("short_name")
    var short_name: String = ""
    @Expose
    @SerializedName("id")
    var id: String = ""
    @Expose
    @SerializedName("picture")
    var picture: Picture? = null
    @Expose
    @SerializedName("name_format")
    var name_format: String = ""
    @Expose
    @SerializedName("firstName")
    var name: String = ""
    @Expose
    @SerializedName("last_name")
    var last_name: String = ""
    @Expose
    @SerializedName("birthday")
    var birthday: String = ""

    @SerializedName("gender")
    var gender: String = ""


    @Expose
    @SerializedName("email")
    var email: String = ""


    @SerializedName("first_name")
    var first_name: String = ""

    class Picture {
        @Expose
        @SerializedName("data")
        var data: Data? = null
    }

    class Data {
        @Expose
        @SerializedName("width")
        var width: Int = 0
        @Expose
        @SerializedName("url")
        var url: String = ""
        @Expose
        @SerializedName("is_silhouette")
        var is_silhouette: Boolean = false
        @Expose
        @SerializedName("height")
        var height: Int = 0
    }


}