package com.lansingareashoplocal.utility.helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;


public class AlertUtils {

    private static AlertDialog mAlertDialog;

    /**
     * @param mContext
     * @param mMessage
     */
    public static void showSimpleMessage(Context mContext, String mMessage) {
        showAlert(mContext, mMessage, "OK", "", false, null);
    }

    public static void showSimpleMessage(Context mContext, String mMessage, IAlertCallback mIAlertCallback) {
        showAlert(mContext, mMessage, "OK", "", false, mIAlertCallback);
    }

    public static void showAlert(Context mContext, String mMessage, String mButtonTitle,
                                 String mTitle, boolean showTitle,
                                 final IAlertCallback mIAlertCallback) {

        AlertDialog.Builder mAlertBuilder = new AlertDialog.Builder(mContext);

        if (showTitle) {
            mAlertBuilder.setTitle(mTitle);
        }

        mAlertBuilder.setMessage(mMessage);

        mAlertBuilder.setPositiveButton(mButtonTitle, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAlertDialog = null;
                if (mIAlertCallback != null)
                    mIAlertCallback.onOKClick();
            }
        });

        mAlertBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mAlertDialog = null;
                if (mIAlertCallback != null)
                    mIAlertCallback.onCancelClick();
            }
        });

        mAlertBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mAlertDialog = null;
            }
        });

        try {
            if (mAlertDialog != null && mAlertDialog.isShowing()) {
                mAlertDialog.cancel();
                mAlertDialog = null;
            }
        } catch (Exception e) {
        }

        mAlertDialog = mAlertBuilder.create();
        mAlertDialog.show();
    }


    public interface IAlertCallback {
        void onOKClick();

        void onCancelClick();
    }
        public static void showMessageWithCancelOk(Context mContext, String mMessage,
                                                   String OkTitle, String cancelTitle,
                                                   String mTitle, boolean showTitle,
                                                   final IAlertCallback mIAlertCallback) {

            AlertDialog.Builder mAlertBuilder = new AlertDialog.Builder(mContext);

            if (showTitle&&mTitle!=null) {
                mAlertBuilder.setTitle(mTitle);
            }

            mAlertBuilder.setMessage(mMessage);
            mAlertBuilder.setCancelable(true);
            mAlertBuilder.setPositiveButton(OkTitle, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mIAlertCallback != null)
                        mIAlertCallback.onOKClick();
                }
            });

            mAlertBuilder.setNegativeButton(cancelTitle, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mIAlertCallback != null)
                        mIAlertCallback.onCancelClick();
                }
            });




            mAlertBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    if (mIAlertCallback != null)
                        mIAlertCallback.onCancelClick();
                }
            });
            mAlertBuilder.create().show();
        }





}
