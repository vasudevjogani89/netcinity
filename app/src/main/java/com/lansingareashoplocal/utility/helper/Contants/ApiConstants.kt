interface ApiConstants {
    companion object {

        val ABOUT_US = "aboutus"
        val TERMS_CONDITION = "termsconditions"
        val PRIVACY_POLICY = "privacypolicy"
        val FAQ = "faq"

        val FIRST_NAME = "first_name"
        val LAST_NAME = "last_name"
        val EMAIL = "email"
        val MOBILE = "mobile"
        var PASSWORD = "password"
        var DOB = "dob"
        var GENDER = "gender"
        val DEVICE_TOKEN = "device_token"
        val DEVICE_TYPE = "device_type"
        val DEVICE_OS = "device_os"
        val DEVICE_NAME = "device_name"
        val APP_VERSION = "app_version"
        val DEVICE_UDID = "device_udid"
        val VERIFY_CODE = "verify_code"
        val SOCIAL_TYPE = "social_type"
        val OLD_PASSWORD = "old_password"
        val NEW_PASSWORD = "new_password"
        val USER_ID = "user_id"
        val FACEBOOK = "facebook"
        val AVATAR = "avatar"
        val FACEBOOK_ID = "facebook_id"
        val GOOGLE_ID = "google_id"
        val GOOGLE = "google"
        val CATEGORY_ID = "category_id"
        val PAGE_INDEX = "page_index"
        val KEYWORD = "keyword"
        val PROMOTION_ID = "promotion_id"
        val BUSINESS_PLACE_ID = "business_place_id"
        val COMMUNITY_ID = "community_id"
        val MAJOR = "major"
        val MINOR = "minor"
        val PROXIMITY = "proximity"
        val LAT = "lat"
        val LON = "lon"

        val APPID = "appid"
        var UNITS = "units"
        var SORT_BY = "sort_by"
        var DISTANCE_ASC = "distance_asc"
        var DISTANCE_DESC = "distance_desc"
        var DATE_ASC = "date_asc"
        var DATE_DESC = "date_desc"
        var FROM_DATE = "from_date"
        var TO_DATE = "to_date"
        var REVIEW = "review"
        var RATING = "rating"
        var REVIEW_ID = "review_id"
        var COMMENT = "comment"
        var REPORT_TYPE_ID = "report_type_id"
        var RADIUS = "radius"

        var QUERY = "query"
        var PAGE = "page"
        var PER_PAGE = "per_page"
        var ORIENTATION = "orientation"
        var NAME = "name"
        var MESSAGE = "message"
        var REASON_ID = "reason_id"


        var OPEN_STATUS = "open_status"
        var SUB_CATEGORY_ID = "sub_category_id"
        var IS_PROMOTION = "is_promotion"
        var IS_FEATURED="is_featured"
        var TYPE="type"


    }
}