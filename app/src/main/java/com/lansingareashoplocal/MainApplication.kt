package com.lansingareashoplocal

import androidx.multidex.MultiDexApplication
import com.crashlytics.android.Crashlytics
import com.kontakt.sdk.android.common.KontaktSDK
import com.lansingareashoplocal.core.LifecycleHandler
import io.fabric.sdk.android.Fabric

/**
 * Created by HB on 21/8/19.
 */
class MainApplication : MultiDexApplication() {

    var sApplication: MainApplication? = null
    override fun onCreate() {
        super.onCreate()

        if(!BuildConfig.DEBUG){
            Fabric.with(this, Crashlytics())
        }
        //   Stetho.initializeWithDefaults(this);
        sApplication = this
        registerActivityLifecycleCallbacks(LifecycleHandler())
        KontaktSDK.initialize("GrGwhkTOGHxjtldxiIrYgfJfSKijREqo")

    }


    fun getApplication(): MainApplication? {
        return sApplication
    }


}
