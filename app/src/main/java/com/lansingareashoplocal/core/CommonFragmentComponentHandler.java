package com.lansingareashoplocal.core;

public interface CommonFragmentComponentHandler {

     Boolean onBackPressed();

    int getLayoutId();

    void init();

}
