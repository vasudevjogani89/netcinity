package com.lansingareashoplocal.core

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.lansingareashoplocal.ui.intro.SplashActivity
import com.lansingareashoplocal.utility.helper.logs.Log

class LifecycleHandler : Application.ActivityLifecycleCallbacks {
    private var activityReferences = 0
    private var isActivityChangingConfigurations = false


    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
    }

    override fun onActivityStarted(activity: Activity) {
        activityReferences++
        if (activityReferences == 1 && !isActivityChangingConfigurations) {
            if (currentActivity != null) {
                if (currentActivity !is SplashActivity)
                    try {
                        (currentActivity as BaseActivity).checkUpdate()
                    }
                    catch (e: Exception){

                    }

            }
            Log.d(
                "App enters foreground",
                "App enters foreground"
            )
        }
    }


    override fun onActivityResumed(activity: Activity) {
        currentActivity = activity
        isAppBackgrounded++
    }

    override fun onActivityPaused(activity: Activity) {
        isAppBackgrounded--
    }

    override fun onActivityStopped(activity: Activity) {
        activityReferences--
        isActivityChangingConfigurations = activity.isChangingConfigurations
        if (activityReferences == 0 && !isActivityChangingConfigurations) {
            Log.d(
                " App enters background",
                " App enters background"
            )
        }
    }

    override fun onActivitySaveInstanceState(
        activity: Activity, outState: Bundle?
    ) {
    }

    override fun onActivityDestroyed(activity: Activity) {}

    companion object {
        private var isAppBackgrounded = 0
        var currentActivity: Activity? = null
            private set
        val isApplicationInForeground: Boolean
            get() = isAppBackgrounded > 0

    }
}