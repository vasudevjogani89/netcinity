package com.lansingareashoplocal.core;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;



import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by hb on 27/9/18.
 */

public abstract class BaseFragment<T extends ViewDataBinding> extends Fragment implements CommonFragmentComponentHandler {

    public BaseActivity mActivity;

    public View rootView;

      public T binding;

    public boolean showLandScape = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
            rootView = binding.getRoot();
            init();
            //      showLandScapeMode(false);
        } else {
            container.removeView(rootView);
        }


        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setCurrentFragment(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity.setCurrentFragment(this);
    }

    public void push(BaseFragment fragment, int containerId, Boolean addBackStack, Boolean animation) {
        mActivity.push(fragment, containerId, addBackStack, animation);
    }

    public void popAll() {
        mActivity.popAll();
    }

    public void popUpTo(String className) {
        mActivity.popUpTo(className);
    }

    public void pop() {
        mActivity.pop();
    }

    public void exitApp() {
        mActivity.finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    /* @Override
     public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
         super.onRequestPermissionsResult(requestCode, permissions, grantResults);
         EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
     }
 */
    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public void showLandScapeMode(boolean showLandscape) {
        if (showLandscape) {
            mActivity.setRequestedOrientation(
                    ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            showLandscape = false;
        } else {
            mActivity.setRequestedOrientation(
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }
}