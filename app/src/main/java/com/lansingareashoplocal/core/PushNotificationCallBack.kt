package com.lansingareashoplocal.core

import com.google.firebase.messaging.RemoteMessage


interface PushNotificationCallBack {
    fun showPushAlert(remoteMessage: RemoteMessage?)
}