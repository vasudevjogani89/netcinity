package com.lansingareashoplocal.core

import android.Manifest
import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Location
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import com.google.android.gms.location.*
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.tasks.OnFailureListener
import com.google.android.play.core.tasks.Task
import com.google.firebase.messaging.RemoteMessage
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lansingareashoplocal.BuildConfig
import com.lansingareashoplocal.R
import com.lansingareashoplocal.service.NotificationsMessagingService
import com.lansingareashoplocal.ui.communityevents.CommunityEventDetailsActivity
import com.lansingareashoplocal.ui.direct_message.DialogDirectMessage
import com.lansingareashoplocal.ui.intro.SplashActivity
import com.lansingareashoplocal.ui.newsandalert.NewsAndAlertDetailsActivity
import com.lansingareashoplocal.ui.promotions.PromotionDetailsActivity
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import org.json.JSONObject
import java.util.*


/**
 * Created by HB on 21/8/19.
 */
public open class BaseActivity : AppCompatActivity(), CommonActivityComponentHandler,
    PushNotificationCallBack {
    var appUpdateInfoTask: Task<AppUpdateInfo>? = null
    var appUpdateManager: AppUpdateManager? = null
    private var notificationManager: NotificationManager? = null

    override fun showPushAlert(remoteMessage: RemoteMessage?) {

        if (remoteMessage != null) {
            val code = remoteMessage.data["code"]

            if (code?.equals(AppConstants.EVENT)!!) {
                val message = remoteMessage.data["message"]
                val event = remoteMessage.data["others"]

                if (message != null) {

                    runOnUiThread(Runnable {
                        alert(message) {
                            okButton {
                                if (event != null) {
                                    val map = Gson().fromJson<HashMap<String, String>>(
                                        event,
                                        object : TypeToken<HashMap<String, String>>() {
                                        }.type
                                    )
                                    if (map != null) {
                                        if (event.contains(AppConstants.COMMUNITY_ID) && event.contains(
                                                AppConstants.EVENT_ID
                                            )
                                        ) {
                                            val community_id = map[AppConstants.COMMUNITY_ID]
                                            val event_id = map[AppConstants.EVENT_ID]
                                            if (community_id != null && event_id != null) {
                                                val intent = Intent(
                                                    this@BaseActivity,
                                                    CommunityEventDetailsActivity::class.java
                                                )
                                                intent.putExtra(
                                                    AppConstants.COMMUNITY_ID,
                                                    community_id
                                                )
                                                intent.putExtra(AppConstants.EVENT_ID, event_id)
                                                intent.putExtra(
                                                    AppConstants.IS_NEED_TO_CALL_API,
                                                    true
                                                )
                                                startActivity(intent)

                                            }

                                        }
                                    }
                                }
                            }
                        }.show()
                    })

                }


            } else if (code?.equals(AppConstants.ALERT)!!) {
                val message = remoteMessage.data["message"]
                val news = remoteMessage.data["others"]

                if (message != null) {
                    runOnUiThread(Runnable {
                        alert(message) {
                            okButton {
                                if (news != null) {
                                    val map = Gson().fromJson<HashMap<String, String>>(news, object : TypeToken<HashMap<String, String>>() {}.type)
                                    if (map != null) {
                                        if (news.contains(AppConstants.NOTIFICATION_ID)) {
                                            val notification_id = map[AppConstants.NOTIFICATION_ID]
                                            if (notification_id != null) {
                                                val intentNewAndAlert = Intent(this@BaseActivity, NewsAndAlertDetailsActivity::class.java)
                                                intentNewAndAlert.putExtra(AppConstants.NOTIFICATION_ID,notification_id)
                                                startActivity(intentNewAndAlert)
                                            }
                                        }
                                    }
                                }
                            }
                        }.show()
                    })

                }
            } else if (code?.equals(AppConstants.DIRECT)!!) {
                val directMessage = remoteMessage.data["message"]
                val imageUrl = remoteMessage.data["image"]
                if (imageUrl != null && imageUrl.isNotEmpty() && directMessage != null && directMessage.isNotEmpty()) {
                    val dialogDirectMessage =
                        DialogDirectMessage(this@BaseActivity, imageUrl, directMessage)
                    runOnUiThread {
                        dialogDirectMessage.showDirectMessageAlert()
                    }

                } else {
                    if (directMessage != null && directMessage.isNotEmpty()) {
                        runOnUiThread {
                            alert(directMessage) {
                                okButton { }
                            }.show()
                        }

                    }
                }


            } else if (code?.equals(AppConstants.B_DIR)!!) {


                val message = remoteMessage.data["message"]
                val businessDetails = remoteMessage.data["others"]

                if (message != null) {

                    runOnUiThread(Runnable {
                        alert(message) {
                            okButton {
                                if (businessDetails != null) {
                                    val map = Gson().fromJson<HashMap<String, String>>(
                                        businessDetails,
                                        object : TypeToken<HashMap<String, String>>() {
                                        }.type
                                    )
                                    if (map != null) {
                                        if (businessDetails.contains(AppConstants.BUSINESS_ID)) {
                                            val business_id = map[AppConstants.BUSINESS_ID]
                                            if (business_id != null) {
                                                val intent = Intent(
                                                    this@BaseActivity,
                                                    PromotionDetailsActivity::class.java
                                                )
                                                intent.putExtra(
                                                    AppConstants.BUSINESS_ID,
                                                    business_id
                                                )
                                                startActivity(intent)

                                            }

                                        }
                                    }
                                }
                            }
                        }.show()
                    })

                }

            }
        }


    }

    private val INTERVAL = (1000 * 10).toLong() // 10 sec
    private val FASTEST_INTERVAL = (1000 * 5).toLong()
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    var currentRunningFragment: BaseFragment<*>? = null
    override fun setCurrentFragment(mCurrentFragment: BaseFragment<*>?) {
        this.currentRunningFragment = mCurrentFragment


    }

    override fun onNotificationReceive(pushObject: JSONObject?) {
    }

    override fun onTapOfNotification(pushObject: JSONObject?) {
    }


    override fun onStart() {
        super.onStart()
        val decor = getWindow().getDecorView()
        decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
    }


    fun push(
        fragment: BaseFragment<*>,
        containerId: Int,
        needToAddBackStack: Boolean?,
        needAnimation: Boolean
    ) {
        hideKeyBoardAndProgress()
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        if (needAnimation) {
            ft.setCustomAnimations(
                R.anim.fragment_slide_in_right, R.anim.fragment_slide_out_left,
                R.anim.fragment_slide_in_left, R.anim.fragment_slide_out_right
            )
        }


        if (needToAddBackStack!!) {
            ft.replace(containerId, fragment, fragment.javaClass.name)
                .addToBackStack(null).commit()
        } else {
            ft.replace(containerId, fragment, fragment.javaClass.name).commit()
        }

        fm.executePendingTransactions()

    }


    fun pop() {
        hideKeyBoardAndProgress()
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
        } else {
            finish()
        }
    }

    fun popUpTo(upToName: String) {
        hideKeyBoardAndProgress()
        supportFragmentManager.popBackStackImmediate(
            upToName,
            FragmentManager.POP_BACK_STACK_INCLUSIVE
        )
    }

    fun popAll() {
        hideKeyBoardAndProgress()
        supportFragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }


    override fun onResume() {
        Log.d("onResume", "onResume")
        super.onResume()
    }

    override fun onPause() {
        Log.d("onPause", "onPause")
        super.onPause()
    }

    override fun onStop() {
        Log.d("onStop", "onStop")
        super.onStop()
    }


    override fun onRestart() {
        super.onRestart()
        Log.d("onRestart", "onRestart")
    }

    private fun hideKeyBoardAndProgress() {
        // CommonUtils.hideProgress()
        //  CommonUtils.hideSoftKeyboard(this)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    public fun getLastLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if ((PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ))
        ) {
            if (CommonMethods.isLocationEnabled(this@BaseActivity)) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    val location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        requestNewLocationData()
                        val lat = location.latitude.toString()
                        val log = location.longitude.toString()
                        updateLocation(lat, log)
                        if (BuildConfig.FLAVOR.equals(AppConstants.STALKEYES_SMART_CITY) || BuildConfig.FLAVOR.equals(AppConstants.SARASOTA)) {
                            saveCityName(lat, log)
                        }


                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        }
    }


    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        /*
     * Sets the desired interval for active location updates. This interval is
     * inexact. You may not receive updates at all if no location sources are available, or
     * you may receive them slower than requested. You may also receive updates faster than
     * requested if other applications are requesting location at a faster interval.
     */
        mLocationRequest.interval = INTERVAL

        /*
     * Sets the fastest rate for active location updates. This interval is exact, and your
     * application will never receive updates faster than this value.
     */
        mLocationRequest.fastestInterval = FASTEST_INTERVAL


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }


    public val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            val lat = mLastLocation.latitude.toString()
            val log = mLastLocation.longitude.toString()
            updateLocation(lat, log)
            saveCityName(lat, log)

        }
    }

    fun updateLocation(lat: String, log: String) {
        CommonMethods.setDataIntoSharePreference(
            this@BaseActivity,
            AppConstants.LATITUDE,
            lat
        )
        CommonMethods.setDataIntoSharePreference(
            this@BaseActivity,
            AppConstants.LONGITUDE,
            log
        )
    }

    fun saveCityName(lat: String, log: String) {
        if (lat.isNotEmpty() && log.isNotEmpty()) {
            try {
                val cityName = CommonMethods.getCityNameFromLocation(lat.toDouble(), log.toDouble(), this@BaseActivity, Handler())
                if (!(CommonMethods.getDataFromSharePreference(this@BaseActivity, AppConstants.USER_CITY_NAME).equals(cityName, true))) {
                    if (cityName != null && cityName.isNotEmpty()) {
                        com.lansingareashoplocal.utility.helper.logs.Log.e("cityName", "chnaged")
                        CommonMethods.setDataIntoSharePreference(this@BaseActivity, AppConstants.USER_CITY_NAME, cityName)
                        CommonMethods.setDataIntoSharePreference(this@BaseActivity, AppConstants.LAST_API_CALL_TIME, 0L)

                    }
                }
            } catch (e: Exception) {

            }

        }
    }

    public fun checkUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(this@BaseActivity)
        // Returns an intent object that you use to check for an update.
        appUpdateInfoTask = appUpdateManager?.appUpdateInfo
        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask?.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                showAlertForRestartApp()
            }

            else if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                val firebaseRemoteConfig: FirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
                val appVersionCode =
                    firebaseRemoteConfig.getString(AppConstants.ANDROID_MINIMUM_REQUIRED_VERSION_CODE)
                try {
                    if (appVersionCode != null && appVersionCode.isNotEmpty()) {
                        if (appVersionCode.toInt() < com.google.firebase.remoteconfig.BuildConfig.VERSION_CODE) {
                            immediateRequest(appUpdateInfo)
                        } else {
                            appUpdateManager?.registerListener(listener)
                            flexibleUpdateRequest(appUpdateInfo)

                        }
                    } else {
                        appUpdateManager?.registerListener(listener)
                        flexibleUpdateRequest(appUpdateInfo)
                    }
                } catch (e: Exception) {
                    if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                        showAlertForRestartApp()
                    } else {
                        //Register the listener
                        appUpdateManager?.registerListener(listener)
                        flexibleUpdateRequest(appUpdateInfo)
                    }
                }


            }
        }
        appUpdateInfoTask?.addOnFailureListener(object : OnFailureListener {
            override fun onFailure(e: java.lang.Exception?) {

            }

        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun flexibleUpdateRequest(appUpdateInfo: AppUpdateInfo) {
        appUpdateManager?.startUpdateFlowForResult(
            // Pass the intent that is returned by 'getAppUpdateInfo()'.
            appUpdateInfo,
            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
            AppUpdateType.FLEXIBLE,
            // The current activity making the update request.
            this,
            // Include a request code to later monitor this update request.
            AppConstants.FLEXIBLE_UPDATE_REQUEST_CODE
        )
    }

    fun immediateRequest(appUpdateInfo: AppUpdateInfo) {
        appUpdateManager?.startUpdateFlowForResult(
            // Pass the intent that is returned by 'getAppUpdateInfo()'.
            appUpdateInfo,
            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
            AppUpdateType.IMMEDIATE,
            // The current activity making the update request.
            this,
            // Include a request code to later monitor this update request.
            AppConstants.IMMEDIATE_UPDATE_REQUEST_CODE
        )
    }

    val listener = InstallStateUpdatedListener { installState ->
        when (installState.installStatus()) {
            //Cancelled,Downloaded,Downloading,Failed,Installed,Installing,
            //Pending, Unknown
            InstallStatus.DOWNLOADED -> {
                if (LifecycleHandler.currentActivity!=null && LifecycleHandler.isApplicationInForeground) {
                    showAlertForRestartApp()
                }

                //show a button to restart the app (which installs the //download
            }
        }
    }


    fun showAlertForRestartApp() {
        if( !LifecycleHandler.currentActivity?.isFinishing!!){
            try {
                runOnUiThread(Runnable {
                    LifecycleHandler.currentActivity?.alert("An update has just been downloaded.") {
                        positiveButton("Restart") {
                            appUpdateManager?.completeUpdate()
                        }
                        negativeButton("Cancel"){
                            if(LifecycleHandler.currentActivity is SplashActivity){
                                (LifecycleHandler.currentActivity as SplashActivity).checkBluetoothAndLocationPermissions()
                            }

                        }

                    }?.show()
                })
            }
            catch (e: WindowManager.BadTokenException){

            }
        }





        /*     Snackbar.make(
                 ivLoader,
                 "An update has just been downloaded.",
                 Snackbar.LENGTH_INDEFINITE
             ).apply {
                 setAction("RESTART") { appUpdateManager?.completeUpdate() }
                 setActionTextColor(resources.getColor(R.color.colorPrimary))
                 show()
             }*/
    }
    fun showUpdateNotification() {
        // logger.dumpCustomEvent("LocalNotification:starting", "")
        var title = "An update has just been downloaded."

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels()
        }

        val notificationId = 1
        //        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        val intent = Intent(this, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra(AppConstants.TYPE, "update")

        val contentIntent =
            PendingIntent.getActivity(
                this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_ONE_SHOT
            )

        val notificationBuilder = NotificationCompat.Builder(
            this,
            "200"
        )
            .setSmallIcon(notificationIcon)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setContentTitle(title)
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/raw/push_notification_sound"))
        //               .setSound(defaultSoundUri);
        notificationBuilder.setContentIntent(contentIntent)
        notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager!!.notify(notificationId, notificationBuilder.build())


        //logger.dumpCustomEvent("LocalNotification","completed")

    }
    val notificationIcon: Int
        get() {
            val useWhiteIcon =
                android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP
            return if (useWhiteIcon) {
                R.drawable.ic_stat_notiicon // we need to add transparent imag
            } else {
                R.mipmap.ic_launcher
            }
        }

    private fun setupChannels() {
        val channelName = getString(R.string.notifications_admin_channel_name)
        val channelDescription = getString(R.string.notifications_admin_channel_description)
        var adminChannel: NotificationChannel? = null
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            adminChannel = NotificationChannel(
                NotificationsMessagingService.ADMIN_CHANNEL_ID,
                channelName,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()

            adminChannel.description = channelDescription
            adminChannel.enableLights(true)
            adminChannel.setSound(
                Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/raw/push_notification_sound"),
                attributes
            )
            adminChannel.lightColor = Color.RED
            adminChannel.enableVibration(true)
            if (notificationManager != null) {
                notificationManager!!.createNotificationChannel(adminChannel)
            }
        }


    }
}